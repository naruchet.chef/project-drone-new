export const jobTypeId = {
  0: { text: "", color: "", },
  1: { text: "Cinematic Aerial Photography", },
  2: { text: "Orthophoto", color: "green", },
  3: { text: "DSM/DTM", color: "blue", },
  4: { text: "Point Cloud 3D Model", color: "volcano", },
  5: { text: "Contour", color: "volcano", },
  6: { text: "Index & Zonation Map", color: "volcano", },
  7: { text: "อื่นๆ", color: "volcano", },
}

const bank = {
  1: { name: "ธนาคารกรุงไทย จำกัด (มหาชน)" },
  2: { name: "ธนาคารกรุงศรีอยุธยา จำกัด (มหาชน)" },
  3: { name: "ธนาคารกรุงเทพ จำกัด (มหาชน)" },
  4: { name: "ธนาคารกสิกรไทย จำกัด (มหาชน)" },
  5: { name: "ธนาคารทหารไทยธนชาติ จำกัด (มหาชน)" },
  6: { name: "ธนาคารซีไอเอ็มบี จำกัด (มหาชน)" },
  7: { name: "ธนาคารไทยพาณิชย์ จำกัด (มหาชน)" },
  8: { name: "ธนาคารสแตนดาร์ดชาร์เตอร์ด(ไทย) จำกัด (มหาชน)" }
}

export const status = {
  "find_pilot": { text: "ประกาศหานักบิน", color: "volcano", code: "#3397FF" },
  "waiting_bid": { text: "รอเสนอราคา", color: "default", code: "#BDBDBD" },
  "waiting": { text: "รอดำเนินการ", color: "cyan", code: "#BDBDBD" },
  "waited": { text: "ดำเนินการ", color: "blue", code: "rgba(33, 150, 243, 0.5)" },
  "waiting_pay": { text: "รอชำระเงิน", color: "gold", code: "#FFB400" },
  "success": { text: "เสร็จสิ้น", color: "green", code: "#4CAF50" },
  "cancel": { text: "ยกเลิก", color: "red", code: "#F44336" },
}

export const sex = {
  "male": { text: "ชาย" },
  "female": { text: "หญิง" },
  "other": { text: "อื่นๆ" },
}