import React, { createContext, useContext, useMemo, useState } from "react";
import { Modal } from "antd";
const modalContext = createContext({
  show: () => {},
  hide: () => {},
});



function ModalContextProvider({ children }) {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const initContent = {
    title: "",
    okText: "ตกลง",
    cancelText: "ยกเลิก",
    onOk: () => {},
    body: "",
  };
  const [content, setContent] = useState(initContent);
  const show = (content) => {
    if (content) {
      setContent(content);
      console.log(`content`, content)
    }
    setIsModalVisible(true);
  };
  const hide = () => {
    setIsModalVisible(false);
  };
  const value = useMemo(() => {
    return { show, hide };
  }, [isModalVisible]);

  return (
    <modalContext.Provider value={value}>
      <Modal
        title={content.title}
        visible={isModalVisible}
        onOk={content.onOk}
        onCancel={hide}
        okText={content.okText}
        cancelText={content.cancelText}

      >
        {content.body}
      </Modal>
      {children}
    </modalContext.Provider>
  );
}

function useModalContext() {
  const context = useContext(modalContext);
  if (context === undefined) {
    throw new Error("useCrudContext must be used within a CrudContextProvider");
  }
  const { show, hide } = context;

  return { show, hide };
}

export { ModalContextProvider, useModalContext };
