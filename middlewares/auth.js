import handleResponse from '../services/backend/handleResponse'
const withAuth = (handle) => {
  return async (req, res) => {
    const basicAuth = req.headers["authorization"]
    if (!basicAuth) {
      return handle(req, res)
    } else {
      return handleResponse.unAuth(res)
    }
  }
}
export default withAuth