import "antd/dist/antd.css";
import React, { useState } from "react";
import { useRouter } from "next/router";
import { HeartOutlined, PushpinOutlined, StopOutlined } from '@ant-design/icons';
import { createBid } from "../../services/bid.service";
import helper from "../../utils/helper";
import { block, unBlock } from "../../services/block.service";
import { favarite, unFavarite } from "../../services/favarite.service";

import {
  Button,
  Row,
  Col,
  Card,
  Modal,
  Form,
  notification,
  Typography,
  Space,
  Spin
} from "antd";
import { Field, Formik } from "formik";
import * as Yup from "yup";
import {
  Input as Inputs,
} from "..";
import { get } from "lodash-es";
const { Text, Title } = Typography;

export default function CardJobList(data) {
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const [over, setOver] = useState(false);
  const [isModalVisibleBid, setIsModalVisibleBid] = useState(false);
  const [valueBid, setValueBid] = useState();
  const job = data.profile
  const pilotId = typeof window !== 'undefined' ? localStorage.getItem('profile_id') : null
  const favId = get(job, "favorite.id", 0)
  const blockId = get(job, "block.id", 0)
  const [isFavarite, setIsFavarite] = useState(favId)
  const [isBlock, setIsBlock] = useState(blockId)

  const Schema = Yup.object().shape({});
  const handleSubmitBid = async (values) => {
    setLoading(true);
    let jobBody = {
      pilot_profile_id: pilotId,
      job_id: job.id,
      budget: Number(values.bid),
      bid_by: "pilot"
    };
    const { result, success } = await createBid(jobBody);
    if (success) {
      notification.success({
        message: `success`,
        description: "เสนอราคา เรียบร้อยแล้ว",
      });
      setLoading(false);
      handleCancelBid()
    }
  };

  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };

  const showModalBid = (value) => {
    let body = {
      bid: 0
    };
    setValueBid(body)
    setIsModalVisibleBid(true);
  };

  const handleCancelBid = () => {
    setIsModalVisibleBid(false);
  };


  const renderLeave = () => {
    return (
      <>
        <center>
          <p style={{
            fontStyle: 'normal',
            fontFamily: 'Prompt',
            fontWeight: '600',
            fontSize: '16px',
            lineHeight: '19px',
            marginTop: '30px',
            color: 'rgba(0, 0, 0, 0.54)'
          }}>{job.job_detail} </p>
          <p style={{
            fontStyle: 'normal',
            fontFamily: 'Prompt',
            fontWeight: '600',
            fontSize: '11px',
            lineHeight: '13px',
            color: 'rgba(0, 0, 0, 0.38)',
          }}>{job.job_locations.length > 0 ? job.job_locations[0].name : ""}</p>
          {job.budget_from} - {job.budget_to} บาท
        </center>
      </>
    )
  }

  const renderOver = () => {
    return (
      <>
        <center>
          <p style={{
            fontStyle: 'normal',
            fontFamily: 'Prompt',
            fontWeight: '600',
            fontSize: '11px',
            lineHeight: '13px',
            color: 'rgba(0, 0, 0, 0.38)',
          }}>ชื่อโครงการ : {job.job_name}</p>
          <p style={{
            fontStyle: 'normal',
            fontFamily: 'Prompt',
            fontWeight: '600',
            fontSize: '11px',
            lineHeight: '13px',
            color: 'rgba(0, 0, 0, 0.38)',
          }}>สถานที่ :{job.job_locations.length > 0 ? job.job_locations[0].name : ""}</p>
          <p style={{
            fontStyle: 'normal',
            fontFamily: 'Prompt',
            fontWeight: '600',
            fontSize: '11px',
            lineHeight: '13px',
            color: 'rgba(0, 0, 0, 0.38)',
          }}>งบประมาณ : {job.budget_from} - {job.budget_to} บาท</p>
          <Button
            style={{ background: "#4CAF50", borderRadius: "10px" }}
            type="default"
          >
            <text onClick={showModalBid} style={{ color: "white" }}>เสนอราคา</text>
          </Button>
          <br />
          <a
            onClick={onClick}
            style={{
              fontStyle: 'normal',
              fontFamily: 'Prompt',
              fontWeight: '600',
              // fontSize: '10px',
              lineHeight: '12px',
              fontSize: '10px',
              color: 'rgba(0, 125, 255, 0.5)'
            }}>รายละเอียดงาน</a>
        </center>
      </>
    )
  }


  const onClick = (values) => {
    router.push("/job/view/" + job.id);
  };

  const MouseOver = (values) => {
    setOver(true)
  };

  const MouseLeave = (values) => {
    setOver(false)
  };

  const hadlefavaritClick = async () => {
    if (isFavarite === 0) {
      const body = {
        ref_id: job.id,
        category: "job",
        account_id: pilotId,
      }
      const { result, success } = await favarite(body);
      if (success) {
        const _id = get(result, "data.id", 0)

        setIsFavarite(_id)
      }
    } else {
      const _fvId = isFavarite > 0 ? isFavarite : favId
      const body = {
        id: _fvId,
      }
      const { result, success } = await unFavarite(body);
      if (success) {

        setIsFavarite(0)
      }

    }
  }

  const hadleBlockClick = async () => {
    if (isBlock === 0) {
      const body = {
        ref_id: job.id,
        category: "job",
        account_id: pilotId,
      }

      const { result, success } = await block(body);
      if (success) {
        const _id = get(result, "data.id", 0)
        setIsBlock(_id)
      }
    } else {
      const _blockId = isBlock > 0 ? isBlock : blockId
      const body = {
        id: _blockId,
      }
      const { result, success } = await unBlock(body);
      if (success) {
        setIsBlock(0)
      }

    }
  }



  console.log(job);
  return (
    <>
      <Card
        onMouseLeave={MouseLeave}
        onMouseOver={MouseOver}
        style={{ width: '250px', height: '330px', borderRadius: '14px' }}
      >
        <Row gutter={[16, 24]}>
          <Col className="gutter-row" span={6}>
            <Button
              style={{ borderRadius: '10%' }}
              icon={<StopOutlined />}
              type={isBlock !== 0 && "danger"}
              onClick={hadleBlockClick}
            />
          </Col>
          <Col className="gutter-row" span={8} />
          {/* <Col className="gutter-row" span={5}>
            <Button
              style={{ borderRadius: '10%' }}
              icon={<HeartOutlined />}
              onClick={() => { }}
            />
          </Col> */}
          <Col className="gutter-row" offset={5} span={5}>
            <Button
              style={{ borderRadius: '10%' }}
              icon={<HeartOutlined />}
              type={isFavarite !== 0 && "primary"}
              onClick={hadlefavaritClick}
            />
          </Col>
        </Row>
        <center>
          <img style={{
            width: '88px',
            height: '88px',
            marginTop: '10px',
            borderRadius: '50%',
          }} src={job.job_images.length > 0 ? "../image/" + job.job_images[0].image : "../img/image_error.png"} />
          <p style={{
            fontStyle: 'normal',
            fontFamily: 'Prompt',
            fontWeight: '600',
            fontSize: '16px',
            lineHeight: '19px',
            marginTop: '10px',
            color: 'rgba(0, 0, 0, 0.87)'
          }}>{job.job_name} </p>
        </center>
        {!over ? renderLeave() : renderOver()}
        <Modal footer={null} width={'30%'} visible={isModalVisibleBid} onCancel={handleCancelBid}>
          <Spin spinning={loading}>
            <>
              <Formik
                enableReinitialize={true}
                initialValues={valueBid}
                onSubmit={handleSubmitBid}
                validationSchema={Schema}
              >
                {({ values, handleSubmit, handleChange, setFieldValue }) => (
                  <Form
                    layout="vertical"
                    onValuesChange={onFormLayoutChange}
                    size={"medium"}
                    onFinish={handleSubmit}
                  >

                    <Row gutter={16}>
                      <Col className="gutter-row" span={24} style={{ textAlign: "center" }}>
                        <Title style={{ color: "#7F7F7F" }} level={3}>
                          เสนอราคา
                        </Title>
                      </Col>
                      <Col className="gutter-row" span={24} style={{ marginTop: "10px" }}>
                        <Row gutter={[16, 24]}>
                          <Col className="gutter-row" span={24}>
                            <Text type="">ชื่อโครงการ: </Text><Text type="secondary">{job.job_name}</Text>
                          </Col>
                        </Row>
                        <Row gutter={[16, 24]}>
                          <Col className="gutter-row" span={12}>
                            <Text type="">วันที่ต้องการว่าจ้าง: </Text><Text type="secondary">{helper.dateFormat(job.date_range_start)}</Text>
                          </Col>
                          <Col className="gutter-row" span={12}>
                            <Text type="">ระยะเวลาในการจ้างงาน: </Text><Text type="secondary">{job.period_date}</Text>
                          </Col>
                        </Row>
                        <Row gutter={[16, 24]}>
                          <Col className="gutter-row" span={24}>
                            <Text type="">งบประมาณ: </Text> <Text type="secondary">{job.budget_from} - {job.budget_to}</Text>
                          </Col>
                        </Row>
                        <Row gutter={[16, 24]}>
                          <Col className="gutter-row" span={24}>
                            <Field
                              label={{ text: "เสนอราคา" }}
                              id="bid"
                              onChange={handleChange}
                              name="bid"
                              rows={4}
                              type={"number"}
                              component={Inputs}
                              placeholder="0"
                              validate={(value) => {
                                if (value == "") {
                                  return "กรุณาเสนอราคา"
                                }
                              }}
                            />
                          </Col>
                        </Row>
                      </Col>
                      <Col
                        className="gutter-row" span={24}
                        style={{ textAlign: "center" }}
                      >
                        <Space>
                          <Button
                            style={{ marginRight: "10px", borderRadius: "10px" }}
                            type="primary" htmlType="submit" primary
                          >
                            เสนอราคา
                          </Button>
                          <Button
                            style={{ borderRadius: '10px', boxShadow: '0px 2px 20px rgba(0, 0, 0, 0.3)' }}
                            type="secondary"
                            onClick={handleCancelBid}
                          >
                            ปิด
                          </Button>
                        </Space>
                      </Col>
                    </Row>
                  </Form>
                )}
              </Formik>
            </>
          </Spin>
        </Modal>
      </Card>
    </>
  );
}
