import React, { useState, useEffect } from "react";
import { Layout, Menu, Row, Col, Button } from "antd";
// import Link from "next/link";
import { SearchOutlined, BellOutlined, UserOutlined, SolutionOutlined, SettingOutlined, UsergroupAddOutlined, UsergroupDeleteOutlined, WalletOutlined, UploadOutlined } from "@ant-design/icons";

import { useRouter } from "next/router";
import { find } from "lodash";

import { routes } from "../../routes";
// import { Login } from "react-facebook";

const { Header } = Layout;

export default function LayoutHeader({ isLogin = true, show_menu = true }) {
  const router = useRouter();
  const [selected, setSelected] = useState("1");
  const [auth, setAuth] = useState("")
  const accountType = typeof window !== 'undefined' ? localStorage.getItem('account_type') : null
  const search = accountType == "pilot" ? "ค้นหางาน" : "ค้นหานักบิน"
  const searchKey = accountType == "pilot" ? "job_list" : "pilot_list"
  const profileKey = accountType == "pilot" ? "pilot_manage" : "customer_manage"
  const profileId = typeof window !== 'undefined' ? localStorage.getItem('profile_id') : null

  const menuLeft = [
    { label: "ทำงานอย่างไร", key: "how_work" },
    { label: "โพสต์งาน", key: "post_job" },
    { label: "ค้นหานักบิน", key: "find_pilot" },
    { label: "ผลงานนักบิน", key: "pilot_work" },
    { label: "เกี่ยวกับเรา", key: "about_us" },
  ];

  useEffect(() => {
    setAuth(localStorage.getItem("token"))
    console.log(localStorage.getItem("token"))
  }, []);

  const subMenuPilot = [
    {
      icon: <SettingOutlined />,
      label: 'ตั้งค่าบัญชี',
      key: 'setting_pilot',
    },
    {
      icon: <WalletOutlined />,
      label: "สถานะงาน",
      key: 'transactions',
    },
    {
      icon: <UploadOutlined />,
      label: 'อัพโหลดผลงาน',
      key: 'upload',
    },
    {
      // icon: <UserOutlined />,
      label: 'ออกจากระบบ',
      // onClick: () => {
      //   router.push('/login');
      // },
      key: 'logout',
    },
  ]

  const subMenuCustomer = [
    {
      icon: <SolutionOutlined />,
      label: 'งานของฉัน',
      key: 'my_job',
    },
    {
      icon: <SettingOutlined />,
      label: 'ตั้งค่าบัญชี',
      key: 'setting',
    },
    {
      icon: <UsergroupAddOutlined />,
      label: 'นักบินที่ชื่นชอบ',
      key: 'favarite',
    },
    {
      icon: <UsergroupDeleteOutlined />,
      label: 'นักบินที่ถูกบล็อค',
      key: 'block',
    },
    {
      // icon: <UserOutlined />,
      label: 'ออกจากระบบ',
      // onClick: () => {
      //   router.push('/login');
      // },
      key: 'logout',
    },
  ]

  const menuRight = [
    // { label: "ทำงานอย่างไร", key: "how_work" },
    accountType != "pilot" ? { label: "งานของฉัน", key: "my_work" } : null,
    accountType != "pilot" ? { label: "โพสต์งาน", key: "my_post_create" } : null,
    { icon: <SearchOutlined />, label: search, key: searchKey },
    { icon: <BellOutlined />, key: "notification" },
    {
      icon: <UserOutlined />, key: profileKey, children: accountType == "pilot" ? subMenuPilot : subMenuCustomer
    },
  ];
  return (
    <Header
      style={{
        background: "#FFFFFF",
        boxShadow: "0px 2px 20px rgba(0, 0, 0, 0.3)",
      }}
    >
      <Row>
        <Col>
          <Row>
            <Col>
               <a  onClick={() => {
                  router.push('/');
                }}>
              <img
                className="logo-header"
                src="../../img/bb74612d42425e677257d0b2f4e465996bd22ee-1-1@2x.png"
              />
              </a>
            </Col>
            <Col>
              <h5 className="header-bold-gray">
                <a  onClick={() => {
                  router.push('/');
                }}>
                  SUT Platform Drone
                </a>
              </h5>
            </Col>
          </Row>
        </Col>
        {isLogin ? (
          <Col xs={18} sm={18} md={12} lg={18} xl={18}>
            <Row justify="left">
              {/* <Col span={14}>
                <Menu
                  // theme="dark"
                  onClick={({ item, key, keyPath, domEvent }) => setSelected(key)}
                  onSelect={({ item, key, keyPath, selectedKeys, domEvent }) => {
                    let path = find(routes, (v) => v.key == key);
                    router.push(path.link);
                  }}
                  className="menu-header"
                  mode="horizontal"
                  selectedKeys={selected}
                  items={menuLeft}
                />
              </Col> */}
              {auth ?
                <Col xs={12} sm={12} md={24} lg={24} xl={24} style={{ textAlign: "right" }}>
                  <Menu
                    // theme="dark"
                    // expandIcon={}
                    className="menu-header menu-header-right"
                    onClick={({ item, key, keyPath, domEvent }) => setSelected(key)}
                    onSelect={({ item, key, keyPath, selectedKeys, domEvent }) => {
                      let path = find(routes, (v) => v.key == key);
                      if (path.key == "pilot_manage") {
                        path.link = "/pilot/manage/" + profileId
                      }
                      if (path.key == "customer_manage") {
                        path.link = "/customer/manage/" + profileId
                      }
                      if (["setting", "setting_pilot", "transactions", "upload", "my_job", "favarite", "block"].indexOf(path.key) != -1) {
                        path.link = path.link.replace("{id}", profileId)
                      }
                      router.push(path.link);
                    }}
                    style={{ display: "revert !important" }}
                    mode="horizontal"
                    selectedKeys={selected}
                    items={menuRight}
                  />
                </Col>
                :
                <Col className="authorized-menu" xs={24} sm={24} md={24} lg={24} xl={24} style={{ textAlign: "right" }}>
                  <Row justify="left">
                    <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                    <Button
                      type="text"
                      className="btnMenu margin-5"
                      style={{
                        fontFamily: "Prompt",
                        fontStyle: "normal",
                        fontWeight: 700,
                        fontSize: "16px",
                        lineHeight: "19px",
                        color: "#7F7F7F",
                        flex: "none",
                        order: 0,
                        flexGrow: 0,
                        padding: "0px"
                      }}
                      onClick={() => {
                        router.push('/about_us');
                      }}
                    >
                      เกี่ยวกับเรา
                    </Button>
                    <Button
                      type="text"
                      className="btnMenu margin-5"
                      style={{
                        fontFamily: "Prompt",
                        fontStyle: "normal",
                        fontWeight: 700,
                        fontSize: "16px",
                        lineHeight: "19px",
                        color: "#7F7F7F",
                        flex: "none",
                        order: 0,
                        flexGrow: 0,
                        padding: "0px"
                      }}
                      onClick={() => {
                        router.push('/job/list');
                      }}
                    >
                      ค้นหางาน
                    </Button>
                    <Button
                      type="text"
                      className="btnMenu margin-5"
                      style={{
                        fontFamily: "Prompt",
                        fontStyle: "normal",
                        fontWeight: 700,
                        fontSize: "16px",
                        lineHeight: "19px",
                        color: "#7F7F7F",
                        flex: "none",
                        order: 0,
                        flexGrow: 0,
                        padding: "0px"
                      }}
                      onClick={() => {
                        router.push('/pilot/list');
                      }}
                    >
                      ค้นหานักบิน
                    </Button>
                    <span style={{ color: "#7f7f7F!important" }}>|</span>
                      <Button
                        type="text"
                        className="margin-5"
                        style={{
                          fontFamily: "Prompt",
                          fontStyle: "normal",
                          fontWeight: 900,
                          fontSize: "16px",
                          lineHeight: "19px",
                          color: "#7F7F7F",
                          flex: "none",
                          order: 0,
                          flexGrow: 0,
                          padding: "0px",
                          textDecoration: "underline overline"
                        }}
                        onClick={() => {
                          router.push('/login');
                        }}
                      >
                        เข้าสู่ระบบ
                      </Button>
                      <Button
                        shape="round"
                        className="margin-5"
                        style={{
                          fontFamily: "Prompt",
                          fontStyle: "normal",
                          fontWeight: 700,
                          fontSize: "16px",
                          lineHeight: "19px",
                          color: "rgba(0, 0, 0, 0.54)",
                          flex: "none",
                          order: 0,
                          flexGrow: 0,
                        }}
                        onClick={() => {
                          router.push('/register');
                        }}
                      >
                        ลงทะเบียน
                      </Button>
                    </Col>
                  </Row>
                </Col>
              }
            </Row>
          </Col >
        ) : (
          <>
            {show_menu && <>
              <Col xs={0} sm={0} md={0} lg={10} xl={10}></Col>
              <Col xs={0} sm={0} md={12} lg={8} xl={8} style={{ textAlign: "right" }}>
                <Button
                  type="text"
                  className="margin-5"
                  style={{
                    // width: "66px",
                    // height: "19px",

                    fontFamily: "Prompt",
                    fontStyle: "normal",
                    fontWeight: 700,
                    fontSize: "16px",
                    lineHeight: "19px",
                    color: "#7F7F7F",
                    flex: "none",
                    order: 0,
                    flexGrow: 0,
                  }}
                  onClick={() => {
                    router.push('/login');
                  }}
                >
                  เข้าสู่ระบบ
                </Button>
                <Button
                  shape="round"
                  className="margin-5"
                  style={{
                    // width: "66px",
                    // height: "19px",
                    fontFamily: "Prompt",
                    fontStyle: "normal",
                    fontWeight: 700,
                    fontSize: "16px",
                    lineHeight: "19px",
                    color: "rgba(0, 0, 0, 0.54)",
                    flex: "none",
                    order: 0,
                    flexGrow: 0,
                  }}
                  onClick={() => {
                    router.push('/register');
                  }}
                >
                  ลงทะเบียน
                </Button>
                <Button
                  shape="circle"
                  className="margin-5"
                  icon={<UserOutlined />}
                />
              </Col>
            </>
            }
          </>
        )
        }
      </Row >
    </Header >
  );
}
