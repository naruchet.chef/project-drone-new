import "antd/dist/antd.css";
import React from "react";
import { Layout, Space } from "antd";

const { Footer } = Layout;

export default function LayoutFooter() {
  return (
    <Footer
      className="main-page-footer roboto-normal-white-24px"
      style={{
        marginBottom: "0px",
        textAlign: "right",
        // position: "fixed",
        height: "100px",
      }}
    >
      {/* <div className="space-align-container"> */}
      {/* <div className="space-align-block"> */}
      {/* <Space align="center" style={{ fontSize: "18px" }}> */}
      <span style={{ fontSize: "18px", fontFamily: "Prompt" }}>
        © 2022 SUT Platform Drone, All rights reserved.
      </span>
      {/* </Space> */}
      {/* </div> */}
      {/* </div> */}
    </Footer>
  );
}
