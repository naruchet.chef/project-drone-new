import React, { useState, useEffect, useCallback } from "react";
import GoogleMapReact from "google-map-react";
import { getPlace } from "../services/map.service";
import { Button, Space, Radio, Select } from "antd";
import MapPin from "./MapPin";
import { PlusOutlined, SearchOutlined } from "@ant-design/icons";
import { get, debounce, map, isEmpty } from "lodash"
const { Option } = Select;;

export default function GoogleMap({
  setJobLocation,
  jobLocations,
  isDisable = false,
}) {
  const defaultProps = {
    center: {
      lat: 14.8817767,
      lng: 102.0185022,
    },
    zoom: 14,
  };
  const [location, setLocation] = useState([
    {
      name: "",
      lat: 14.8817767,
      lng: 102.0185022,
    },
  ]);

  const [mapGroupSelect, setMapGroupSelect] = useState(1);
  const [mapGroup, setMapGroup] = useState([
    {
      name: "ตำแหน่งที่ 1",
      value: "1",
    },
  ]);
  const [mapOptions, setMapOptions] = useState([[]]);
  const [mapSelect, setMapSelect] = useState([]);

  useEffect(() => {
    if (jobLocations && jobLocations.length > 0) {
      initMapLocation();
    }
  }, [jobLocations]);

  const initMapLocation = () => {
    const formatLocation = map(jobLocations, (val) => {
      return {
        name: val.name,
        lat: parseFloat(val.lat),
        lng: parseFloat(val.lng),
      };
    });

    const formatMapGroup = map(jobLocations, (val, index) => {
      return {
        name: `ตำแหน่งที่ ${index + 1}`,
        value: `${index + 1}`,
      };
    });

    const formatMapSelect = map(jobLocations, (val, index) => {
      return val.name || "";
    });

    setLocation(formatLocation);
    setMapGroup(formatMapGroup);
    setMapSelect(formatMapSelect);
  };

  const mapClick = (event) => {
    if (isDisable) {
      return;
    }
    const newLocation = {
      lat: event.lat,
      lng: event.lng,
      name: "",
    };

    let locaList = [...location];
    locaList[mapGroupSelect - 1] = newLocation;
    setLocation(locaList);
    selectedLocation(locaList);
  };

  const onSearch = async (searchText, _mapOptions, index) => {
    if (searchText) {
      const { result, success } = await getPlace(searchText);
      if (success) {
        const { results: placeList } = result.data;
        let mapPlot = map(placeList, (value) => {
          const location =
            get(value, "geometry.location.lat") +
            "$_$" +
            get(value, "geometry.location.lng") +
            "$_$" +
            get(value, "name");
          return {
            value: location,
            name: get(value, "name"),
          };
        });
        let fullOption = [..._mapOptions];
        fullOption[index - 1] = mapPlot;
        setMapOptions(fullOption);
      }
    }
  };
  const debounceLoadData = useCallback(debounce(onSearch, 200), []);

  const onMapSearchSelect = (textSelect) => {
    if (isDisable) {
      return;
    }
    let selected = [...mapSelect];
    selected[mapGroupSelect - 1] = textSelect;
    setMapSelect(selected);
    const [lat, lng, name] = `${textSelect}`.split("$_$");
    const newLocation = {
      lat: parseFloat(lat),
      lng: parseFloat(lng),
      name: name,
    };
    let locaList = [...location];
    locaList[mapGroupSelect - 1] = newLocation;
    setLocation(locaList);
    selectedLocation(locaList);
  };

  const addMapGroup = () => {
    const count = mapGroup.length + 1;
    const newMapGroup = [
      ...mapGroup,
      {
        name: `ตำแหน่งที่ ${count}`,
        value: `${count}`,
      },
    ];
    setMapGroup(newMapGroup);
    setLocation([...location, defaultProps.center]);
  };

  const getLocation = () => {
    const index = mapGroupSelect - 1;
    return location[index] ? location[index] : location[0];
  };

  const onMapGroupSelect = (test) => {
    const select = test.target.value;
    setMapGroupSelect(select);
    let mOption = [...mapOptions];
    if (isEmpty(mapOptions[select - 1])) {
      mOption[select - 1] = [];
    }
    setMapOptions(mOption);
  };

  const selectedLocation = (_location) => {
    if (isDisable) {
      return;
    }
    setJobLocation(_location);
  };

  return (
    <>
      <Space>
        <Radio.Group onChange={onMapGroupSelect} defaultValue="1">
          {mapGroup.map((spot, index) => {
            return (
              <Radio.Button checked key={"spot" + index} value={spot.value}>
                {spot.name}
              </Radio.Button>
            );
          })}
        </Radio.Group>
        {mapGroup.length < 3 && !isDisable && (
          <Button onClick={addMapGroup} icon={<PlusOutlined />} />
        )}
      </Space>

      <div>
        <Select
          disabled={isDisable}
          showSearch
          value={mapSelect[mapGroupSelect - 1]}
          defaultActiveFirstOption={false}
          showArrow={false}
          filterOption={false}
          onSearch={(event) => {
            debounceLoadData(event, mapOptions, mapGroupSelect);
          }}
          onChange={onMapSearchSelect}
          notFoundContent={null}
          allowClear
          placeholder="สถานที่"
          style={{ width: "100%" }}
          suffixIcon={<SearchOutlined />}
        >
          {mapOptions[mapGroupSelect - 1] &&
            mapOptions[mapGroupSelect - 1].map((val, index) => {
              return (
                <Option
                  disabled={val.disabled}
                  key={`select_${index}`}
                  value={val.value}
                >
                  {val.name}
                </Option>
              );
            })}
        </Select>
      </div>

      <div style={{ height: "300px", width: "100%" }}>
        <GoogleMapReact
          bootstrapURLKeys={{
            key: "AIzaSyBFOBNefRvOgaaLrTxbD1i2J0P0_jGd0fs",
          }}
          defaultCenter={defaultProps.center}
          defaultZoom={defaultProps.zoom}
          onClick={mapClick}
          center={getLocation()}
        >
          <MapPin lat={getLocation().lat} lng={getLocation().lng} />
        </GoogleMapReact>
      </div>
    </>
  );
}
