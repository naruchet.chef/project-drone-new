import { Button as Buttons } from 'antd'
import React, { ReactElement } from 'react'

function Button({
  children,
  type = 'primary',
  htmlType,
  icon,
  size = 'middle',
  loading,
  disabled,
  className,
  isDanger = false,
  shape,
  ...Props
}) {
  return (
    <Buttons
      disabled={disabled}
      loading={loading}
      {...Props}
      type={type}
      htmlType={htmlType}
      icon={icon}
      size={size}
      className={className}
      danger={isDanger}
      shape={shape}
    >
      {children}
    </Buttons>
  )
}

export default Button
