import React from "react";
import { DatePicker, Form, Typography } from "antd";
import { ErrorMessage } from "formik";
import moment from "moment";
const { Text } = Typography;

export default function CustomDatePicker({ label, field, ...props }) {
  if (field.value) {
    field.value = moment(field.value);
  }

  const handleChange = (e) => {
    let value = "";
    if (e) {
      value = e.format();
    }
    props.form.setFieldValue(field.name, value);
  };

  return (
    <div className="ant-form ant-form-vertical">
      <Form.Item required={label?.required}
        label={
          props.required == "true" ? (
            <>
              {label?.text}
              <text style={{ color: "red" }}>*</text>
            </>
          ) : (
            label?.text
          )
        }>
        <DatePicker
          {...field}
          {...props}
          name={field.name}
          onChange={handleChange}
          style={{
            backgroundColor: "#f8fafc",
            border: "none",
            width: "100%",
          }}
        />

        <Text type="danger">
          <div style={{ marginTop: "7px" }}>
            <ErrorMessage
              name={field.name}
              component="div"
              className="validate-error"
            />
          </div>
        </Text>
      </Form.Item>
    </div>
  );
}
