import React from "react";
import { Form, Select as Selects, Typography } from "antd";
import { ErrorMessage } from "formik";
const { Text } = Typography;
const { Option } = Selects;

export default function CustomSelect({ label, field, selectOption, ...props }) {
  const handleChange = (e) => {
    let value = e;
    props.form.setFieldValue(field.name, value);
  };
  const onChangeHandle = () => {
    if (props.onChange) {
      return props.onChange;
    } else {
      return handleChange;
    }
  };

  return (
    <div className="ant-form ant-form-vertical">
      <Form.Item required={label?.required} label={label?.text}>
        <Selects {...field} {...props} onChange={onChangeHandle()}>
          {selectOption.map((val, index) => {
            return (
              <Option
                disabled={val.disabled}
                key={`select_${index}`}
                value={val.value}
              >
                {val.name}
              </Option>
            );
          })}
        </Selects>
        <Text type="danger">
          <div style={{ marginTop: "7px" }}>
            <ErrorMessage
              name={field.name}
              component="div"
              className="validate-error"
            />
          </div>
        </Text>
      </Form.Item>
    </div>
  );
}
