import React from "react";
import { Form, Input, Typography } from "antd";
import { ErrorMessage } from "formik";
const { TextArea } = Input;


const { Text } = Typography;
export default function CustomTextArea({ label, field, ...props }) {
  const handleChange = (e) => {
    let value = e?.target?.value;
    props.form.setFieldValue(field.name, value);
  };

  return (
    <div className="ant-form ant-form-vertical">
      <Form.Item
        required={label?.required}
        label={
          props.required == "true" ? (
            <>
              {label?.text}
              <text style={{ color: "red" }}>*</text>
            </>
          ) : (
            label?.text
          )
        }
      >
        <TextArea
          id={props.id}
          type={props.type}
          name={field.name}
          className={props.className}
          value={field.value}
          placeholder={props.placeholder}
          onChange={handleChange}
          autoComplete={props.autoComplete}
          disabled={props.disabled}
          style={{
            backgroundColor: "#f8fafc",
            border: "none",
          }}
          rows={4}
        />
        <Text type="danger">
          <div style={{ marginTop: "7px" }}>
            <ErrorMessage
              name={field.name}
              component="div"
              className="validate-error"
            />
          </div>
        </Text>
      </Form.Item>
    </div>
  );
}
