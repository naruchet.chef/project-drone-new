import React from "react";
import { Form, Input as Inputs, Typography } from "antd";
import { ErrorMessage } from "formik";

const { Text } = Typography;
export default function CustomInput({ label, field, ...props }) {
  const handleChange = (e) => {
    let value = e?.target?.value;
    props.form.setFieldValue(field.name, value);
  };

  return (
    <div className="ant-form ant-form-vertical">
      <Form.Item
        label={
          props.required == "true" ? (
            <>
              {label?.text}
              <text style={{ color: "red" }}>*</text>
            </>
          ) : (
            label?.text
          )
        }
      >
        <Inputs
          id={props.id}
          type={props.type}
          name={field.name}
          className={props.className}
          value={field.value}
          placeholder={props.placeholder}
          onChange={handleChange}
          autoComplete={props.autoComplete}
          disabled={props.disabled}
          style={{
            backgroundColor: "#f8fafc",
            border: "none",
          }}
        />
        <Text type="danger">
          <div style={{ marginTop: "7px" }}>
            <ErrorMessage
              name={field.name}
              component="div"
              className="validate-error"
            />
          </div>
        </Text>
      </Form.Item>
    </div>
  );
}
