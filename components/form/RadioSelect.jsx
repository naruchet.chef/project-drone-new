import React from "react";
import { Form, Radio as Radios, Typography, Space } from "antd";
import { ErrorMessage } from "formik";
const { Text } = Typography;


export default function CustomRadioSelect({ label, field, selectOption, ...props }) {
  const handleChange = (e) => {
    let value = e;
    props.form.setFieldValue(field.name, value);
  };
  const onChangeHandle = () => {
    if (props.onChange) {
      return props.onChange;
    } else {
      return handleChange;
    }
  };

  return (
    <div className="ant-form ant-form-vertical">
      <Form.Item required={label?.required} label={label?.text}>
        <Radios.Group {...field} onChange={onChangeHandle()}>
          <Space direction="vertical">
            {selectOption.map((val, index) => {
              return (
                <Radios
                  disabled={val.disabled}
                  key={`radio_${index}`}
                  value={val.value}
                >
                  {val.name}
                </Radios>
              );
            })}
          </Space>
        </Radios.Group>
        <Text type="danger">
          <div  style={{marginTop:"7px"}}>
            <ErrorMessage
              name={field.name}
              component="div"
              className="validate-error"
            />
          </div>
        </Text>
      </Form.Item>
    </div>
  );
}
