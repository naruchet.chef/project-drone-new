import "antd/dist/antd.css";
import React from "react";
import { Tabs } from 'antd';
import CreateProfile from './tabs-profile/create-profile'
import PilotInfo from './tabs-profile/pilot-info'
import License from './tabs-profile/license'
import WorkingDay from './tabs-profile/working-day'
import DroneEquipment from './tabs-profile/drone-equipment'
import ServicePrice from './tabs-profile/service-price'
const { TabPane } = Tabs;

import {
  Layout,
  Breadcrumb,
} from "antd";
const { Content } = Layout;

export default function ManageProfile(props) {
  return (
    <div
      className="site-layout-background"
      style={{ padding: 24, minHeight: 380 }}
    >
      <Tabs tabBarStyle={{ color: '#689FF2' }} type="card">
        <TabPane tab="ตั้งค่าบัญชี" key="1">
          <CreateProfile pilotProfile={props.pilotProfile} />
        </TabPane>
        <TabPane tab="ข้อมูลนักบิน" key="2">
          <PilotInfo pilotProfile={props.pilotProfile} />
        </TabPane>
        <TabPane tab="ใบอนุญาต" key="3">
          <License pilotProfile={props.pilotProfile} />
        </TabPane>
        <TabPane tab="บริการ & ราคา" key="4">
          <ServicePrice pilotProfile={props.pilotProfile} />
        </TabPane>
        <TabPane tab="วันทำการ" key="5">
          <WorkingDay pilotProfile={props.pilotProfile} />
        </TabPane>
        <TabPane tab="โดรน & อุปกรณ์" key="6">
          <DroneEquipment pilotProfile={props.pilotProfile} />
        </TabPane>
      </Tabs>
    </div>

  );
}
