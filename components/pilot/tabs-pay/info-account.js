import "antd/dist/antd.css";
import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { Field, Formik } from "formik";
import * as Yup from "yup";
import { get } from "lodash";
import { updatePilot } from "../../../services/pilot.service";

import { Input as Inputs, Select as Selects } from "../../../components";
import {
  Button,
  Space,
  Form,
  Upload,
  Row,
  Col,
  notification,
  Spin,
  Typography,
} from "antd";
const { Title } = Typography;

export default function Profile(props) {
  const initialValues = {
    tax_payer: get(props.pilotProfile, "tax_payer") != null ? get(props.pilotProfile, "tax_payer") : "",
    bank_account_name: get(props.pilotProfile, "bank_account_name") != null ? get(props.pilotProfile, "bank_account_name") : "",
    bank_account_no: get(props.pilotProfile, "bank_account_no") != null ? get(props.pilotProfile, "bank_account_no") : "",
    bank_name_id: get(props.pilotProfile, "bank_name_id") != null ? get(props.pilotProfile, "bank_name_id") : "",
  }

  const router = useRouter();
  const { id } = router.query;
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => { });

  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };

  const bank = [
    { value: 1, name: "ธนาคารกรุงไทย จำกัด (มหาชน)" },
    { value: 2, name: "ธนาคารกรุงศรีอยุธยา จำกัด (มหาชน)" },
    { value: 3, name: "ธนาคารกรุงเทพ จำกัด (มหาชน)" },
    { value: 4, name: "ธนาคารกสิกรไทย จำกัด (มหาชน)" },
    { value: 5, name: "ธนาคารทหารไทยธนชาติ จำกัด (มหาชน)" },
    { value: 6, name: "ธนาคารซีไอเอ็มบี จำกัด (มหาชน)" },
    { value: 7, name: "ธนาคารไทยพาณิชย์ จำกัด (มหาชน)" },
    { value: 8, name: "ธนาคารสแตนดาร์ดชาร์เตอร์ด(ไทย) จำกัด (มหาชน)" },
  ];

  const handleSubmitForm = async (values) => {
    let jobBody = {
      id: id,
      tax_payer: get(values, "tax_payer"),
      bank_account_name: get(values, "bank_account_name"),
      bank_account_no: get(values, "bank_account_no"),
      bank_name_id: get(values, "bank_name_id"),
    };
    setIsLoading(true);
    const { result, success } = await updatePilot(jobBody);
    if (success) {
      notification.success({
        message: `success`,
        description: "อัพเดท เรียบร้อยแล้ว",
      });
      setIsLoading(false);
    }
  };

  const Schema = Yup.object().shape({
    bank_account_name: Yup.string().required("กรุณากรอก ชื่อบัญชี"),
    bank_account_no: Yup.string().required("กรุณากรอก เลขบัญชี"),
    bank_name_id: Yup.string().required("กรุณาเลือกบัชชีธนาคาร"),

  });

  return (
    <div
      className="site-layout-background"
      style={{ padding: 24, minHeight: 380 }}
    >
      <Spin spinning={isLoading}>
        <Formik
          enableReinitialize={true}
          initialValues={initialValues}
          onSubmit={handleSubmitForm}
          validationSchema={Schema}
        >
          {({ values, handleSubmit, handleChange, setFieldValue }) => (
            <Form
              layout="vertical"
              onValuesChange={onFormLayoutChange}
              size={"medium"}
              onFinish={handleSubmit}
            >
              <Row gutter={16}>
                <Col className="gutter-row" span={16} offset={4}>
                  <Title style={{ color: "#7F7F7F" }} level={3}>
                    ข้อมูลบัญชี
                  </Title>
                </Col>
                <Col className="gutter-row" span={16} offset={4}>
                  <Field
                    label={{ text: "บัญชีธนาคาร" }}
                    id="bank_name_id"
                    name="bank_name_id"
                    component={Selects}
                    placeholder="กรุณาเลือกธนาคาร"
                    selectOption={bank}
                    required="true"
                  />
                </Col>
                <Col className="gutter-row" span={16} offset={4}>
                  <Row gutter={16}>
                    <Col className="gutter-row" span={12}>
                      <Field
                        label={{ text: "ชื่อบัญชี" }}
                        required="true"
                        id="bank_account_name"
                        onChange={handleChange}
                        name="bank_account_name"
                        rows={4}
                        type={"text"}
                        component={Inputs}
                        placeholder="กรอกข้อมูล"
                      />
                    </Col>
                    <Col className="gutter-row" span={12}>
                      <Field
                        label={{ text: "เลขบัญชี" }}
                        id="bank_account_no"
                        required="true"
                        onChange={handleChange}
                        name="bank_account_no"
                        rows={4}
                        type={"text"}
                        component={Inputs}
                        placeholder="กรอกข้อมูล"
                      />
                    </Col>
                  </Row>
                </Col>
                <Col className="gutter-row" span={16} offset={4}>
                  <Field
                    label={{ text: "เลขที่ผู้เสียภาษี (ถ้ามี)" }}
                    id="tax_payer"
                    onChange={handleChange}
                    name="tax_payer"
                    rows={4}
                    type={"text"}
                    component={Inputs}
                    placeholder="กรอกข้อมูล"
                  />
                </Col>
                <Col
                  style={{ textAlign: "left" }}
                  className="gutter-row"
                  span={16}
                  offset={4}
                >
                  <Space>
                    <Button
                      style={{ marginRight: "10px", borderRadius: "10px" }}
                      type="primary"
                      htmlType="submit"
                      primary
                    >
                      บันทึก
                    </Button>
                  </Space>
                </Col>
              </Row>
            </Form>
          )}
        </Formik>
      </Spin>
    </div>
  );
}
