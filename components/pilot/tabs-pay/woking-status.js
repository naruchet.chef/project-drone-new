import "antd/dist/antd.css";
import React, { useState, useEffect } from "react";
import { Layout, Breadcrumb, Table, Tag, Space, Button } from "antd";
import { jobListWithBids } from "../../../services/job.service";
import { jobTypeId, status } from "../../../constants/common";
import useFetchTable from "../../../hooks/useFetchTable";
import { EditOutlined } from "@ant-design/icons";
import { useRouter } from "next/router";
import Link from "next/link";
import { get, last } from "lodash";

const { Content } = Layout;

export default function Jobs(props) {
  const router = useRouter();
  const { id, tab } = router.query;
  useEffect(() => {
  }, []);

  const filterRequest = {
    pilot_profile_id: id
  };
  const {
    isLoading,
    dataTable,
    handelDataTableChange,
    handleFetchData,
    pagination,
  } = useFetchTable(jobListWithBids, filterRequest);

  const columns = [
    {
      title: "status",
      dataIndex: "status",
      render: (data) => {
        const { text, code } = status[data];
        return (
          <div style={{ textAlign: "center" }}>
            <Button style={{ background: code, borderRadius: '7px' }}><p style={{ color: 'white' }}>{text}</p></Button>
          </div>
        );
      },
    },
    {
      title: "ชื่อโครงการ",
      dataIndex: "job_name",
      render: (text, row) => {
        return (
          <Link href={`/job/detail/${row.id}`}>
            <a>{text}</a>
          </Link>
        );
      },
    },
    {
      title: "งบประมาณ",
      dataIndex: "budget_from",
      render: (text, record) => {
        return getPrice(record)
      }
    },
    {
      title: "รายละเอียดโครงการ",
      dataIndex: "job_detail",
    },
    {
      title: "ประเภทงาน",
      dataIndex: "job_type_id",
      render: (jobType) => {
        if (jobType > 0) {
          const { text, color } = jobTypeId[jobType];
          return (
            <>
              <Tag color={color} key={jobType}>
                {text}
              </Tag>
            </>
          );
        }
      },
    }, {
      title: "จัดการ",
      key: "action",
      render: (text, record) => {
        const { id } = record;
        return (
          <>
            <Space>
              <Link href={`/job/detail/${id}`}>
                <Button
                  icon={<EditOutlined />}
                  style={{ borderRadius: "10px" }}
                >
                </Button>
              </Link>
            </Space>
          </>
        );
      },
    },

  ];

  const getPrice = (jobData) => {
    if (jobData.bids.length > 0) {
      const bid = last(jobData.bids)
      return bid.budget
    } else {
      return jobData.budget_from
    }
  }

  return (
    <div
      className="site-layout-background"
      style={{ padding: 24, minHeight: 380 }}
    >
      <h2>งานของฉัน</h2>
      <Table
        rowKey="id"
        loading={isLoading}
        columns={columns}
        pagination={pagination}
        dataSource={dataTable}
        onChange={handelDataTableChange}
        scroll={{ x: '100%', y: '100%' }}
      />
    </div>
  );
}
