import "antd/dist/antd.css";
import React, { useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { HeartOutlined, PushpinOutlined, StopOutlined } from '@ant-design/icons';
import { RatingStar } from '..'
import useFetchTable from "../../hooks/useFetchTable";
import { jobList, updateJob } from "../../services/job.service";
import { favarite, unFavarite } from "../../services/favarite.service";
import { block, unBlock } from "../../services/block.service";
import { createBid } from "../../services/bid.service";
import {
  Button,
  Row,
  Col,
  Card,
  Modal,
  Spin,
  Table,
  Tag,
  notification,
  InputNumber
} from "antd";
import { jobTypeId, status } from "../../constants/common";
import { get } from "lodash-es";


export default function CardPilotList(data) {
  const router = useRouter();
  const [over, setOver] = useState(false);
  const profile = data.profile
  const [isModalVisibleBid, setIsModalVisibleBid] = useState(false);
  const [loading, setLoading] = useState(false);
  const [jobId, setJobId] = useState(0)
  const [budget, setBudget] = useState()

  const profileId = typeof window !== 'undefined' ? localStorage.getItem('profile_id') : null
  const filterRequest = { profile_id: profileId, status: "find_pilot" }
  const favId = get(profile, "favorite.id", 0)
  const blockId = get(profile, "block.id", 0)
  const [isFavarite, setIsFavarite] = useState(favId)
  const [isBlock, setIsBlock] = useState(blockId)


  const {
    isLoading,
    dataTable,
    handelDataTableChange,
    handleFetchData,
    pagination,
  } = useFetchTable(jobList, filterRequest);

  const handleCancelBid = () => {
    setIsModalVisibleBid(false);
  };

  const renderLeave = () => {
    return (
      <>
        <center>
          < p style={{
            fontStyle: 'normal',
            fontFamily: 'Prompt',
            fontWeight: '600',
            fontSize: '11px',
            lineHeight: '13px',
            color: 'rgba(0, 0, 0, 0.38)'
          }
          }> <RatingStar rating={profile.rating} />({profile.job_review})  {profile.job_all} งาน</p >
          <p style={{
            fontStyle: 'normal',
            fontFamily: 'Prompt',
            fontWeight: '600',
            fontSize: '11px',
            lineHeight: '13px',
            color: 'rgba(0, 0, 0, 0.38)'
          }}> {profile.pilot_licenses.length > 0 ? "license #" + profile.pilot_licenses[0].license_no : ""}</p>
        </center>
      </>
    )
  }

  const renderOver = () => {
    return (
      <>
        <center>
          <Button
            style={{ background: isBlock !== 0 ? "#BDBDBD" : "#4CAF50", borderRadius: "10px", marginBottom: '5px' }}
            type="default"
            disabled={isBlock !== 0 ? true : false}
            onClick={showModalBid}
          >
            <text style={{ color: "white" }}>เสนองาน</text>
          </Button>
          <br />

          <a
            onClick={onClick}
            style={{
              fontStyle: 'normal',
              fontFamily: 'Prompt',
              fontWeight: '600',
              // fontSize: '10px',
              lineHeight: '12px',
              color: 'rgba(0, 125, 255, 0.5)'
            }}>ดูโปรไฟล์</a>

        </center>
      </>
    )
  }

  const showModalBid = (value) => {
    setIsModalVisibleBid(true);
  };


  const onClick = (values) => {
    router.push("/pilot/detail/" + profile.id);
  };

  const MouseOver = (values) => {
    setOver(true)
  };

  const MouseLeave = (values) => {
    setOver(false)
  };

  const onChange = (value) => {
    console.log('changed', value);
    setBudget(value)
  };

  const columns = [
    {
      title: "status",
      dataIndex: "status",
      render: (data) => {
        const { text, color } = status[data];
        return (
          <Tag color={color} key={text}>
            {text}
          </Tag>
        );
      },
    },
    {
      title: "ชื่อโครงการ",
      dataIndex: "job_name",
      render: (text, row) => {
        return (
          <Link href={`/my-post/view/${row.id}`}>
            <a>{text}</a>
          </Link>
        );
      },
    },
    {
      title: "งบประมาณ",
      render: (data) => {
        return (
          <InputNumber onChange={onChange} placeholder="กรอกราคา" />
        );
      },
    },
    {
      title: "รายละเอียดโครงการ",
      dataIndex: "job_detail",
    },
    {
      title: "ประเภทงาน",
      dataIndex: "job_type_id",
      render: (jobType) => {
        if (jobType > 0) {
          const { text, color } = jobTypeId[jobType];
          return (
            <>
              <Tag color={color} key={jobType}>
                {text}
              </Tag>
            </>
          );
        }
      },
    }
  ];

  const handleSubmitBid = async (values) => {
    setLoading(true);
    let jobBody = {
      pilot_profile_id: profile.id,
      job_id: jobId[0],
      budget: Number(budget),
      bid_by: "customer"
    };
    const { result, success } = await createBid(jobBody);
    setLoading(false);
    if (success) {
      notification.success({
        message: `success`,
        description: "เสนองาน เรียบร้อยแล้ว",
      });
      handleCancelBid()
      router.reload()
    }
  };

  const updatePost = async () => {
    const jobBody = {
      id: jobId[0],
      status: "waiting"
    }
    const { result, success } = await updateJob(jobBody);
  };

  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      setJobId(selectedRowKeys)
      setBudget(selectedRows[0].budget_from)
    },
  };

  const hadlefavaritClick = async () => {
    if (isFavarite === 0) {
      const body = {
        ref_id: profile.id,
        category: "pilot",
        account_id: profileId,
      }
      const { result, success } = await favarite(body);
      if (success) {
        const _id = get(result, "data.id", 0)

        setIsFavarite(_id)
      }
    } else {
      const _fvId = isFavarite > 0 ? isFavarite : favId
      const body = {
        id: _fvId,
      }
      const { result, success } = await unFavarite(body);
      if (success) {

        setIsFavarite(0)
      }

    }
  }

  const hadleBlockClick = async () => {
    if (isBlock === 0) {
      const body = {
        ref_id: profile.id,
        category: "pilot",
        account_id: profileId,
      }

      const { result, success } = await block(body);
      if (success) {
        const _id = get(result, "data.id", 0)
        setIsBlock(_id)
      }
    } else {
      const _blockId = isBlock > 0 ? isBlock : blockId
      const body = {
        id: _blockId,
      }
      const { result, success } = await unBlock(body);
      if (success) {
        setIsBlock(0)
      }

    }
  }


  return (
    <>
      <Card
        onMouseLeave={MouseLeave}
        onMouseOver={MouseOver}
        style={{ width: '250px', height: '320px', borderRadius: '14px' }}
      >
        <Row gutter={[16, 24]}>
          <Col className="gutter-row" span={6}>
            <Button
              style={{ borderRadius: '10%' }}
              icon={<StopOutlined />}
              type={isBlock !== 0 && "danger"}
              onClick={hadleBlockClick}
            />
          </Col>
          <Col className="gutter-row" span={8} />
          {/* <Col className="gutter-row" span={5}>
            <Button
              style={{ borderRadius: '10%' }}
              icon={<HeartOutlined />}
              onClick={() => { }}
            />
          </Col> */}
          <Col className="gutter-row" offset={5} span={5}>
            <Button
              style={{ borderRadius: '10%' }}
              icon={<HeartOutlined />}
              type={isFavarite !== 0 && "primary"}
              onClick={hadlefavaritClick}
            />
          </Col>
        </Row>
        <center>
          <img style={{
            width: '88px',
            height: '88px',
            marginTop: '10px',
            borderRadius: '50%',
          }} src={profile.photo != null ? ("../image/" + profile.photo) : "../img/image_error.png"} />
          <p style={{
            fontStyle: 'normal',
            fontFamily: 'Prompt',
            fontWeight: '600',
            fontSize: '16px',
            lineHeight: '19px',
            marginTop: '10px',
            color: 'rgba(0, 0, 0, 0.87)'
          }}>{profile.first_name} {profile.last_name}</p>
        </center>
        {!over ? renderLeave() : renderOver()}
        <center>
          <p style={{
            fontStyle: 'normal',
            fontFamily: 'Prompt',
            fontWeight: '600',
            fontSize: '11px',
            lineHeight: '13px',
            color: 'rgba(0, 0, 0, 0.38)',
          }}>{profile.address}</p>
        </center>
      </Card>

      <Modal footer={null} width={'50%'} visible={isModalVisibleBid} onCancel={handleCancelBid}>
        <Spin spinning={loading}>
          <>
            <Table
              width={'50%'}
              rowSelection={{
                type: "radio",
                ...rowSelection
              }}
              rowKey="id"
              loading={isLoading}
              columns={columns}
              pagination={pagination}
              dataSource={dataTable}
              onChange={handelDataTableChange}
              scroll={{ x: '100%', y: '100%' }}
            />
            <Button
              disabled={jobId == 0 ? true : false}
              style={{ background: "#4CAF50", borderRadius: "10px", marginBottom: '5px' }}
              type="default"
            >
              <text onClick={handleSubmitBid} style={{ color: "white" }}>เสนองาน</text>
            </Button>
          </>
        </Spin>
      </Modal>
    </>
  );
}
