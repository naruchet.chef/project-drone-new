import "antd/dist/antd.css";
import React from "react";
import LayoutHeader from "../layout/header";
import LayoutFooter from "../layout/footer";
import { Tabs } from 'antd';
import VedioPerformance from './tabs-performance/vedio-performance'
import PhotoPerformance from './tabs-performance/photo-performance'
const { TabPane } = Tabs;

import {
  Layout,
  Breadcrumb,
} from "antd";
const { Content } = Layout;

export default function ManagePay(props) {


  return (
    <div
      className="site-layout-background"
      style={{ padding: 24, minHeight: 380 }}
    >
      <Tabs tabBarStyle={{ color: '#689FF2' }} type="card">
        <TabPane tab="ภาพถ่ายผลงาน" key="1">
          <PhotoPerformance pilotProfile={props.pilotProfile} />
        </TabPane>
        <TabPane tab="วีดีโอผลงาน" key="2">
          <VedioPerformance pilotProfile={props.pilotProfile} />
        </TabPane>
      </Tabs>
    </div>
  );
}
