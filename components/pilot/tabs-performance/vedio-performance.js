import "antd/dist/antd.css";
import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { Field, Formik } from "formik";
import * as Yup from "yup";
import { get, filter } from "lodash";
import { createPilotPortfolio, deletePilotPortfolio, updatePilotPortfolio } from "../../../services/pilotPortfolio.service";
import { UploadOutlined, DeleteOutlined, EditOutlined, InfoCircleOutlined } from "@ant-design/icons";

import {
  Input as Inputs,
  Select as Selects,
} from "../../../components";
import {
  Button,
  Space,
  Form,
  Upload,
  Row,
  Col,
  notification,
  Spin,
  Typography,
  Table,
  Modal,
} from "antd";
const { Title, Text } = Typography;

export default function Profile(props) {

  const [isModalVisibleUpdate, setIsModalVisibleUpdate] = useState(false);
  const [isModalVisibleDelete, setIsModalVisibleDelete] = useState(false);
  const [deleteId, setDeleteId] = useState(0);
  const [updateId, setUpdateId] = useState(0);
  const [updateValue, setUpdateValue] = useState();

  const showModalDelete = (id) => {
    setDeleteId(id)
    setIsModalVisibleDelete(true);
  };

  const handleCancelDelete = () => {
    setIsModalVisibleDelete(false);
  };

  const showModalUpdate = (value) => {
    setUpdateId(value.id)
    setUpdateValue(value)
    setIsModalVisibleUpdate(true);
  };

  const handleCancelUpdate = () => {
    setIsModalVisibleUpdate(false);
  };


  const deletePortfolio = async () => {
    handleCancelDelete()
    setIsLoading(true);
    let jobBody = {
      id: deleteId
    };
    const { result, success } = await deletePilotPortfolio(jobBody);
    if (success) {
      notification.success({
        message: `success`,
        description: "ลบผลงาน เรียบร้อยแล้ว",
      });
    }
    router.reload()
  };

  const initialValues = {
    portfolio_name: "",
    portfolio_detail: "",
    portfolio_photo: "",
  };
  const router = useRouter();
  const [isLoading, setIsLoading] = useState(false);

  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };

  const getImage = (url) => {
    if (url != null) {
      const image = `https://img.youtube.com/vi/${url.split('v=')[1]}/default.jpg`
      return image
    }
  }

  const handleSubmitForm = async (values) => {
    const body = new FormData();
    let jobBody = {
      portfolio_type: "vedio",
      portfolio_name: get(values, "portfolio_name"),
      portfolio_detail: get(values, "portfolio_detail"),
      portfolio_photo: get(values, "portfolio_photo"),
      pilot_profile_id: props.pilotProfile.id,
    };
    for (const [key, value] of Object.entries(jobBody)) {
      body.append(key, value);
    }

    setIsLoading(true);
    const { result, success } = await createPilotPortfolio(body);
    if (success) {
      notification.success({
        message: `success`,
        description: "อัพเดท เรียบร้อยแล้ว",
      });
      router.reload()
    }
  };

  const handleSubmitFormUpdate = async (values) => {
    handleCancelUpdate()
    const body = new FormData();
    let jobBody = {
      id: updateId,
      portfolio_type: "vedio",
      portfolio_name: get(values, "portfolio_name"),
      portfolio_detail: get(values, "portfolio_detail"),
      portfolio_photo: get(values, "portfolio_photo"),
      pilot_profile_id: props.pilotProfile.id,
    };
    for (const [key, value] of Object.entries(jobBody)) {
      body.append(key, value);
    }
    setIsLoading(true);
    const { result, success } = await updatePilotPortfolio(body);
    if (success) {
      notification.success({
        message: `success`,
        description: "อัพเดท เรียบร้อยแล้ว",
      });
      router.reload()
    }
  };

  const Schema = Yup.object().shape({
    portfolio_name: Yup.string().required("กรุณากรอก ชื่อโครงการ"),
    portfolio_detail: Yup.string().required("กรุณากรอก คำอธิบาย"),
    portfolio_photo: Yup.string().required("กรุณากรอก link ผลงาน"),
  });

  const columns = [
    {
      title: 'ชื่อโครงการ',
      dataIndex: 'portfolio_name',
      key: 'portfolio_name',
    },
    {
      title: 'คำอธิบาย',
      dataIndex: 'portfolio_detail',
      key: 'portfolio_detail',
    },
    {
      title: 'วีดีโอ',
      dataIndex: 'portfolio_photo',
      key: 'portfolio_photo',
      render: (row) => {
        return (
          <>
            <a rel="noreferrer" href={row} target="_blank">
              <img src={getImage(row)} />
            </a>
          </>
        )
      }
    },
    {
      align: 'center',
      render: (row) => {
        return (
          <>
            <Space>
              <Button
                icon={<EditOutlined />}
                style={{ borderRadius: "10px" }}
                onClick={() => showModalUpdate(row)}
              >
              </Button>
              <Button
                icon={<DeleteOutlined />}
                style={{ borderRadius: "10px" }}
                onClick={() => showModalDelete(row.id)}
              >
              </Button>
            </Space>
          </>
        )
      },
    },
  ];

  return (
    <div
      className="site-layout-background"
      style={{ padding: 24, minHeight: 380 }}
    >
      <Spin spinning={isLoading}>
        <Formik
          enableReinitialize={true}
          initialValues={initialValues}
          onSubmit={handleSubmitForm}
          validationSchema={Schema}
        >
          {({ values, handleSubmit, handleChange, setFieldValue }) => (
            <Form
              layout="vertical"
              onValuesChange={onFormLayoutChange}
              size={"medium"}
              onFinish={handleSubmit}
            >
              <Row gutter={16}>
                <Col className="gutter-row" span={16} offset={4}>
                  <Title style={{ color: "#7F7F7F" }} level={3}>
                    วีดีโอผลงาน
                  </Title>
                </Col>
                <Col className="gutter-row" span={16} offset={4}>
                  <Field
                    label={{ text: "ชื่อโครงการ" }}
                    required="true"
                    id="portfolio_name"
                    onChange={handleChange}
                    name="portfolio_name"
                    rows={4}
                    type={"text"}
                    component={Inputs}
                    placeholder="กรอกข้อมูล"
                  />
                </Col>
                <Col className="gutter-row" span={16} offset={4}>
                  <Field
                    label={{ text: "คำอธิบาย" }}
                    required="true"
                    id="portfolio_detail"
                    onChange={handleChange}
                    name="portfolio_detail"
                    rows={4}
                    type={"text"}
                    component={Inputs}
                    placeholder="กรอกข้อมูล"
                  />
                </Col>
                <Col className="gutter-row" span={16} offset={4}>
                  <Field
                    label={{ text: "link ผลงาน" }}
                    required="true"
                    id="portfolio_photo"
                    onChange={handleChange}
                    name="portfolio_photo"
                    rows={4}
                    type={"text"}
                    component={Inputs}
                    placeholder="กรอกข้อมูล"
                  />
                </Col>
                <Col
                  style={{ textAlign: "left" }}
                  className="gutter-row"
                  span={16}
                  offset={4}
                >
                  <Space>
                    <Button
                      style={{ marginRight: "10px", borderRadius: "10px" }}
                      type="primary" htmlType="submit" primary
                    >
                      บันทึก
                    </Button>
                  </Space>
                </Col>
              </Row>
            </Form>
          )}
        </Formik>

        <Row gutter={16} style={{
          marginTop: "30px"
        }}>
          <Col className="gutter-row" span={16} offset={4}>
            <Table
              columns={columns}
              dataSource={filter(props.pilotProfile.pilot_portfolios, function (data) { return data.portfolio_type == "vedio" })}
              pagination={false}
              scroll={{ x: '100%', y: '100%' }}
            />
          </Col>
        </Row>
      </Spin>

      <Modal footer={null} visible={isModalVisibleUpdate} onCancel={handleCancelUpdate}>
        <>
          <Formik
            enableReinitialize={true}
            initialValues={updateValue}
            onSubmit={handleSubmitFormUpdate}
            validationSchema={Schema}
          >
            {({ values, handleSubmit, handleChange, setFieldValue }) => (
              <Form
                layout="vertical"
                onValuesChange={onFormLayoutChange}
                size={"medium"}
                onFinish={handleSubmit}
              >

                <Row gutter={16}>
                  <Col className="gutter-row" span={16} offset={4}>
                    <Title style={{ color: "#7F7F7F" }} level={3}>
                      แก้ไข วีดีโอผลงาน
                    </Title>
                  </Col>
                  <Col className="gutter-row" span={16} offset={4}>
                    <Field
                      label={{ text: "ชื่อโครงการ" }}
                      required="true"
                      id="portfolio_name"
                      onChange={handleChange}
                      name="portfolio_name"
                      rows={4}
                      type={"text"}
                      component={Inputs}
                      placeholder="กรอกข้อมูล"
                    />
                  </Col>
                  <Col className="gutter-row" span={16} offset={4}>
                    <Field
                      label={{ text: "คำอธิบาย" }}
                      required="true"
                      id="portfolio_detail"
                      onChange={handleChange}
                      name="portfolio_detail"
                      rows={4}
                      type={"text"}
                      component={Inputs}
                      placeholder="กรอกข้อมูล"
                    />
                  </Col>
                  <Col className="gutter-row" span={16} offset={4}>
                    <Field
                      label={{ text: "link ผลงาน" }}
                      required="true"
                      id="portfolio_photo"
                      onChange={handleChange}
                      name="portfolio_photo"
                      rows={4}
                      type={"text"}
                      component={Inputs}
                      placeholder="กรอกข้อมูล"
                    />
                  </Col>
                  <Col
                    className="gutter-row" span={24}
                    style={{ textAlign: "center" }}
                  >
                    <Space>

                      <Button
                        style={{ borderRadius: "10px" }}
                        type="danger"
                        onClick={handleCancelUpdate}
                      >
                        <text style={{ color: "white" }}>ยกเลิก</text>
                      </Button>
                      <Button
                        style={{ marginRight: "10px", borderRadius: "10px" }}
                        type="primary" htmlType="submit" primary
                      >
                        ยืนยัน
                      </Button>
                    </Space>
                  </Col>
                </Row>
              </Form>
            )}
          </Formik>
        </>
      </Modal>

      <Modal
        footer={null}
        visible={isModalVisibleDelete}
        onCancel={handleCancelDelete}>
        <Text><InfoCircleOutlined /> คุณต้องการลบ ผลงานหรือไม</Text>
        <center style={{ marginTop: "20px" }}>
          <Space >
            <Button
              style={{ borderRadius: "10px" }}
              type="danger"
              onClick={handleCancelDelete}
            >
              <Text style={{ color: "white" }}>ยกเลิก</Text>
            </Button>
            <Button
              style={{ marginRight: "10px", borderRadius: "10px" }}
              type="primary" htmlType="submit" primary
              onClick={deletePortfolio}
            >
              ยืนยัน
            </Button>
          </Space>
        </center>
      </Modal>
    </div>
  );
}
