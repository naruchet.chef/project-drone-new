import "antd/dist/antd.css";
import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { Field, Formik } from "formik";
import * as Yup from "yup";
import { get } from "lodash";
import { updatePilot } from "../../../services/pilot.service";

import {
  Input as Inputs,
  TextArea as TextAreas,
} from "../../../components";
import {
  Button,
  Space,
  Form,
  Row,
  Col,
  notification,
  Spin,
  Typography,
} from "antd";
const { Title } = Typography;

export default function Info(props) {
  const initialValues = {
    company_name: get(props.pilotProfile, "company_name") != null ? get(props.pilotProfile, "company_name") : "",
    tel: get(props.pilotProfile, "tel") != null ? get(props.pilotProfile, "tel") : "",
    address: get(props.pilotProfile, "address") != null ? get(props.pilotProfile, "address") : "",
    insurance: get(props.pilotProfile, "insurance") != null ? get(props.pilotProfile, "insurance") : "",
    introduce_yourself: get(props.pilotProfile, "introduce_yourself") != null ? get(props.pilotProfile, "introduce_yourself") : "",
    experience: get(props.pilotProfile, "experience") != null ? get(props.pilotProfile, "experience") : "",
  }
  const router = useRouter();
  const { id } = router.query;
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
  });

  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };

  const handleSubmitForm = async (values) => {
    let jobBody = {
      id: id,
      company_name: get(values, "company_name"),
      tel: get(values, "tel"),
      address: get(values, "address"),
      insurance: get(values, "insurance"),
      introduce_yourself: get(values, "introduce_yourself"),
      experience: get(values, "experience"),
    };

    setIsLoading(true);
    const { result, success } = await updatePilot(jobBody);
    if (success) {
      notification.success({
        message: `success`,
        description: "อัพเดท เรียบร้อยแล้ว",
      });
      setIsLoading(false);
    }
  };

  const Schema = Yup.object().shape({
    company_name: Yup.string().required("กรุณากรอก ชื่อ"),
    tel: Yup.string().required("กรุณากรอก นามสกุล"),
    address: Yup.string().required("กรุณากรอก อีเมลล์"),
    introduce_yourself: Yup.string().required("กรุณากรอก แนะนำตัว"),
    experience: Yup.string().required("กรุณากรอก ประสบการณ์"),
  });

  return (
    <div
      className="site-layout-background"
      style={{ padding: 24, minHeight: 380 }}
    >
      <Spin spinning={isLoading}>
        <Formik
          enableReinitialize={true}
          initialValues={initialValues}
          onSubmit={handleSubmitForm}
          validationSchema={Schema}
        >
          {({ values, handleSubmit, handleChange, setFieldValue }) => (
            <Form
              layout="vertical"
              onValuesChange={onFormLayoutChange}
              size={"medium"}
              onFinish={handleSubmit}
            >

              <Row gutter={16}>
                <Col className="gutter-row" span={16} offset={4}>
                  <Title style={{ color: "#7F7F7F" }} level={3}>
                    ข้อมูลนักบิน
                  </Title></Col>
                <Col className="gutter-row" span={16} offset={4}>
                  <Row gutter={16}>
                    <Col className="gutter-row" span={8}>
                      <Field
                        label={{ text: "ชื่อบริษัท" }}
                        required="true"
                        id="company_name"
                        onChange={handleChange}
                        name="company_name"
                        rows={4}
                        type={"text"}
                        component={Inputs}
                        placeholder="กรอกข้อมูล"
                      />
                    </Col>
                    <Col className="gutter-row" span={8}>
                      <Field
                        label={{ text: "เบอร์โทรศัพท์" }}
                        id="tel"
                        required="true"
                        onChange={handleChange}
                        name="tel"
                        rows={4}
                        type={"text"}
                        component={Inputs}
                        placeholder="กรอกข้อมูล"
                      />
                    </Col>
                    <Col className="gutter-row" span={8}>
                      <Field
                        label={{ text: "ทุนประกัน(ถ้ามี)" }}
                        id="insurance"
                        onChange={handleChange}
                        name="insurance"
                        rows={4}
                        type={"text"}
                        component={Inputs}
                        placeholder="กรอกข้อมูล"
                      />
                    </Col>
                  </Row>
                </Col>
                <Col className="gutter-row" span={16} offset={4}>
                  <Field
                    label={{ text: "ที่อยู่" }}
                    required="true"
                    id="address"
                    onChange={handleChange}
                    name="address"
                    rows={4}
                    component={TextAreas}
                    placeholder="กรอกข้อมูล"
                  />
                </Col>
                <Col className="gutter-row" span={16} offset={4}>
                  <Field
                    label={{ text: "แนะนำตัว" }}
                    id="introduce_yourself"
                    required="true"
                    onChange={handleChange}
                    name="introduce_yourself"
                    rows={4}
                    component={TextAreas}
                    placeholder="กรอกข้อมูล"
                  />
                </Col>
                <Col className="gutter-row" span={16} offset={4}>
                  <Field
                    label={{ text: "ประสบการณ์" }}
                    id="experience"
                    onChange={handleChange}
                    name="experience"
                    required="true"
                    rows={4}
                    component={TextAreas}
                    placeholder="กรอกข้อมูล"
                  />
                </Col>
                <Col
                  style={{ textAlign: "left" }}
                  className="gutter-row"
                  span={10}
                  offset={4}
                >
                  <Space>
                    <Button
                      style={{ marginRight: "10px", borderRadius: "10px" }}
                      type="primary" htmlType="submit" primary
                    >
                      บันทึก
                    </Button>
                  </Space>
                </Col>
              </Row>
            </Form>
          )}
        </Formik>
      </Spin>
    </div>
  );
}
