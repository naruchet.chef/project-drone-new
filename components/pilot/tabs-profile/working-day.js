import "antd/dist/antd.css";
import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { Field, Formik } from "formik";
import * as Yup from "yup";
import { get } from "lodash";
import { updatePilot } from "../../../services/pilot.service";

import {
  Button,
  Space,
  Form,
  Row,
  Col,
  notification,
  Spin,
  Typography,
  Checkbox,
} from "antd";
const { Title } = Typography;

export default function WokingDay(props) {
  const initialValues = {
    working_day: JSON.parse("[" + props.pilotProfile.working_day + "]"),
  };
  const router = useRouter();
  const [isLoading, setIsLoading] = useState(false);
  const [workingDay, setWorkingDay] = useState([]);

  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };

  const handleSubmitForm = async (values) => {
    let jobBody = {
      id: props.pilotProfile.id,
      working_day: workingDay.toString(),
    };
    console.log(jobBody);

    setIsLoading(true);
    const { result, success } = await updatePilot(jobBody);
    if (success) {
      notification.success({
        message: `success`,
        description: "อัพเดท เรียบร้อยแล้ว",
      });
      setIsLoading(false);
    }
  };

  const onChange = (checkedValues) => {
    console.log('checked = ', checkedValues);
    setWorkingDay(checkedValues)
  };

  const Schema = Yup.object().shape({});

  return (
    <div
      className="site-layout-background"
      style={{ padding: 24, minHeight: 380 }}
    >
      <Spin spinning={isLoading}>
        <Formik
          enableReinitialize={true}
          initialValues={initialValues}
          onSubmit={handleSubmitForm}
          validationSchema={Schema}
        >
          {({ values, handleSubmit, handleChange, setFieldValue }) => (
            <Form
              layout="vertical"
              onValuesChange={onFormLayoutChange}
              size={"medium"}
              onFinish={handleSubmit}
            >

              <Row gutter={16}>
                <Col className="gutter-row" span={16} offset={4}>
                  <Title style={{ color: "#7F7F7F" }} level={3}>
                    วันทำการ
                  </Title>
                </Col>
                <Col className="gutter-row" span={16} offset={4}>
                  <Col className="gutter-row" span={12}>
                    <label>
                      วันที่สามารถให้บริการได้
                    </label>
                    <br></br>
                    <Checkbox.Group style={{ width: '100%', marginTop: '20px' }} onChange={onChange} defaultValue={values.working_day}>
                      <Row>
                        <Col flex={1}>
                          <label>Mon</label><br />
                          <Checkbox value={1}> </Checkbox>
                        </Col>
                        <Col flex={1}>
                          <label>Tue</label><br />
                          <Checkbox value={2}> </Checkbox>
                        </Col>
                        <Col flex={1}>
                          <label>Wed</label><br />
                          <Checkbox value={3}> </Checkbox>
                        </Col>
                        <Col flex={1}>
                          <label>Thu</label><br />
                          <Checkbox value={4}> </Checkbox>
                        </Col>
                        <Col flex={1}>
                          <label>Fri </label><br />
                          <Checkbox value={5}> </Checkbox>
                        </Col>
                        <Col flex={1}>
                          <label>Sat</label><br />
                          <Checkbox value={6}> </Checkbox>
                        </Col>
                        <Col flex={1}>
                          <label>Sun</label><br />
                          <Checkbox value={7}> </Checkbox>
                        </Col>
                      </Row>
                    </Checkbox.Group>
                  </Col>
                </Col>
                <Col
                  style={{ textAlign: "left", marginTop: "20px" }}
                  className="gutter-row"
                  span={16}
                  offset={4}
                >
                  <Space>
                    <Button
                      style={{ marginRight: "10px", borderRadius: "10px" }}
                      type="primary" htmlType="submit" primary
                    >
                      บันทึก
                    </Button>
                  </Space>
                </Col>
              </Row>
            </Form>
          )}
        </Formik>
      </Spin>
    </div >
  );
}
