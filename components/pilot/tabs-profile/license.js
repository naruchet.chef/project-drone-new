import "antd/dist/antd.css";
import React, { useState } from "react";
import { useRouter } from "next/router";
import { Field, Formik } from "formik";
import { get } from "lodash";
import { updatePilot } from "../../../services/pilot.service";
import { createPilotLicense, deletePilotLicense, updatePilotLicense } from "../../../services/pilotLicense.service";
import * as Yup from "yup";
import moment from 'moment'
import { UploadOutlined, DeleteOutlined, EditOutlined, InfoCircleOutlined } from "@ant-design/icons";

import {
  Input as Inputs,
  DatePicker,
} from "../../../components";
import {
  Button,
  Upload,
  Space,
  Modal,
  Form,
  Row,
  Col,
  notification,
  Spin,
  Typography,
  Radio,
  Table,
  Image
} from "antd";
const { Title, Text } = Typography;
const { warning } = Modal;
export default function Info(props) {
  const [isModalVisibleUpdate, setIsModalVisibleUpdate] = useState(false);
  const [isModalVisibleDelete, setIsModalVisibleDelete] = useState(false);
  const [deleteId, setDeleteId] = useState(0);
  const [updateId, setUpdateId] = useState(0);
  const [updateValue, setUpdateValue] = useState();
  const [uploadList, setUploadList] = useState([]);

  const uploadFile = (value) => {
    console.log(`value`, value);
    const { file, fileList = [] } = value;
    if (fileList.length > 0) {
      console.log(`file`, file);
      console.log(`fileList`, fileList);
      setUploadList(fileList);
    }
  };

  const showModalDelete = (id) => {
    setDeleteId(id)
    setIsModalVisibleDelete(true);
  };

  const handleCancelDelete = () => {
    setIsModalVisibleDelete(false);
  };


  const showModalUpdate = (value) => {
    setUpdateId(value.id)
    setUpdateValue(value)
    setIsModalVisibleUpdate(true);
  };

  const handleCancelUpdate = () => {
    setIsModalVisibleUpdate(false);
  };


  const initialValues = {
    license: props.pilotProfile.license,
    license_name: "",
    license_no: "",
    license_date: ""
  }
  const router = useRouter();
  const [isLoading, setIsLoading] = useState(false);

  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };

  const handleSubmitForm = async (values) => {
    if (uploadList.length == 0) {
      warning({
        title: `กรุณา อัพโหลดรูปภาพใบอนุญาต`,
        afterClose() { },
      })
      return null
    }
    let jobBody = {
      id: props.pilotProfile.id,
      license: get(values, "license"),
    };

    setIsLoading(true);
    const { result, success } = await updatePilot(jobBody);
    if (success) {
      notification.success({
        message: `success`,
        description: "อัพเดท เรียบร้อยแล้ว",
      });
      if (get(values, "license") == true) {
        handleCreateLicense(values)
      }
      router.reload()
    }
  };

  const handleCreateLicense = async (values) => {
    const body = new FormData();
    let jobBody = {
      license_date: get(values, "license_date"),
      license_name: get(values, "license_name"),
      license_no: get(values, "license_no"),
      pilot_profile_id: props.pilotProfile.id,
    };

    if (uploadList.length > 0) {
      uploadList.forEach((file) => {
        body.append("file", file.originFileObj);
      });
    }
    for (const [key, value] of Object.entries(jobBody)) {
      body.append(key, value);
    }
    const { result, success } = await createPilotLicense(body);
  };

  const deleteLicense = async () => {
    handleCancelDelete()
    setIsLoading(true);
    let jobBody = {
      id: deleteId
    };
    const { result, success } = await deletePilotLicense(jobBody);
    if (success) {
      notification.success({
        message: `success`,
        description: "ลบใบอนุญาต เรียบร้อยแล้ว",
      });
    }
    router.reload()
  };


  const updateLicense = async (values) => {
    handleCancelUpdate()
    setIsLoading(true);
    const body = new FormData();
    let jobBody = {
      id: updateId,
      license_date: get(values, "license_date"),
      license_name: get(values, "license_name"),
      license_no: get(values, "license_no"),
    };
    if (uploadList.length > 0) {
      uploadList.forEach((file) => {
        body.append("file", file.originFileObj);
      });
    }
    for (const [key, value] of Object.entries(jobBody)) {
      body.append(key, value);
    }
    const { result, success } = await updatePilotLicense(body);
    if (success) {
      notification.success({
        message: `success`,
        description: "อัพเดท เรียบร้อยแล้ว",
      });
    }
    router.reload()
  };

  const Schema = Yup.object().shape({});

  const columns = [
    {
      title: 'ชื่อ-สกุล ผู้ถือใบอนุญาต',
      dataIndex: 'license_name',
      key: 'license_name',
      responsive: ['md'],
    },
    {
      title: 'ใบอนุญาตเลขที่',
      dataIndex: 'license_no',
      key: 'license_no',
      responsive: ['md'],
    },
    {
      title: 'วันที่ออกใบอนุญาต',
      dataIndex: 'license_date',
      key: 'license_date',
      render: (row) => {
        const start = moment(row).format('YYYY-MM-DD')
        return (
          <>
            <div>{start}</div>
          </>
        )
      },
      responsive: ['md'],
    },
    {
      title: 'รูปภาพ ใบอนุญาต',
      dataIndex: 'license_photo',
      key: 'license_photo',
      width: 200,
      render: (row) => {
        console.log(row);
        return (
          <Image width='40%' src={"/image/" + row} />
        )
      }
    },
    {
      align: 'center',
      responsive: ['md'],
      render: (row) => {
        return (
          <>
            <Space>
              <Button
                icon={<EditOutlined />}
                style={{ borderRadius: "10px" }}
                onClick={() => showModalUpdate(row)}
              >
              </Button>
              <Button
                icon={<DeleteOutlined />}
                style={{ borderRadius: "10px" }}
                onClick={() => showModalDelete(row.id)}
              >
              </Button>
            </Space>
          </>
        )
      },
    },
  ];


  return (
    <div
      className="site-layout-background"
      style={{ padding: 24, minHeight: 380 }}
    >
      <Spin spinning={isLoading}>
        <Formik
          enableReinitialize={true}
          initialValues={initialValues}
          onSubmit={handleSubmitForm}
          validationSchema={Schema}
        >
          {({ values, handleSubmit, handleChange, setFieldValue }) => (
            <Form
              layout="vertical"
              onValuesChange={onFormLayoutChange}
              size={"medium"}
              onFinish={handleSubmit}
            >

              <Row gutter={16}>
                <Col className="gutter-row" span={16} offset={4}>
                  <Title style={{ color: "#7F7F7F" }} level={3}>
                    ใบอนุญาต
                  </Title>
                </Col>
                <Col className="gutter-row" span={16} offset={4}>
                  <Col className="gutter-row" span={12}>
                    <label>
                      สถานะใบอนุญาต<text style={{ color: "red" }}>*</text>
                    </label>
                    <br></br>
                    <Radio.Group onChange={(event) => {
                      setFieldValue("license", event.target.value);
                    }} value={values.license}>
                      <Space direction="vertical">
                        <Radio value={false}>ไม่มีใบอนุญาต</Radio>
                        <Radio value={true}>มีใบอนุญาตบังคับอากาศยานไร้คนขับ</Radio>
                      </Space>
                    </Radio.Group>
                  </Col>
                </Col>
                {
                  values.license ? (
                    <Col className="gutter-row" span={16} offset={4} style={{ marginTop: "10px" }}>
                      <Row gutter={16}>
                        <Col className="gutter-row" span={12}>
                          <Field
                            label={{ text: "ชื่อ-สกุล ผู้ถือใบอนุญาต" }}
                            required="true"
                            id="license_name"
                            onChange={handleChange}
                            name="license_name"
                            rows={4}
                            type={"text"}
                            component={Inputs}
                            placeholder="กรอกชื่อ-สกุล"
                            validate={(value) => {
                              if (values.license == true) {
                                if (value == "") {
                                  return "กรุณากรอก ชื่อ-สกุล ผู้ถือใบอนุญาต"
                                }
                              }
                            }}
                          />
                        </Col>
                        <Col className="gutter-row" span={12}>
                          <Field
                            label={{ text: "ใบอนุญาตเลขที่" }}
                            id="license_no"
                            required="true"
                            onChange={handleChange}
                            name="license_no"
                            rows={4}
                            type={"text"}
                            component={Inputs}
                            placeholder="กรอกใบอนุญาตเลขที่"
                            validate={(value) => {
                              if (values.license == true) {
                                if (value == "") {
                                  return "กรุณากรอก ใบอนุญาตเลขที่"
                                }
                              }
                            }}
                          />
                        </Col>
                      </Row>
                      <Row gutter={16}>
                        <Col className="gutter-row" span={12}>
                          <Form.Item label={"อัพโหลดใบอนุญาติ"} >
                            <Upload maxCount={1} onRemove={(file) => setUploadList([])} onChange={(file) => uploadFile(file)}>
                              <Button
                                icon={<UploadOutlined />}
                              >
                                Click to Upload
                              </Button>
                            </Upload>
                          </Form.Item>
                        </Col>
                        <Col className="gutter-row" span={12}>
                          <Form.Item label={<>วันที่ออกใบอนุญาต<text style={{ color: "red" }}>*</text></>}>
                            <Field
                              id="license_date"
                              name="license_date"
                              component={DatePicker}
                              placeholder="กรุณาเลือก"
                              validate={(value) => {
                                if (values.license == true) {
                                  if (value == "") {
                                    return "กรุณาเลือก วันที่ออกใบอนุญาต"
                                  }
                                }
                              }}
                            />
                          </Form.Item>
                        </Col>
                      </Row>
                    </Col>
                  ) : (
                    ""
                  )
                }
                <Col
                  style={{ textAlign: "left", marginTop: "20px" }}
                  className="gutter-row"
                  span={16}
                  offset={4}
                >
                  <Space>
                    <Button
                      style={{ marginRight: "10px", borderRadius: "10px" }}
                      type="primary" htmlType="submit" primary
                    >
                      บันทึก
                    </Button>
                  </Space>
                </Col>
              </Row>
            </Form>
          )}
        </Formik>

        <Row gutter={16} style={{
          marginTop: "30px"
        }}>
          <Col className="gutter-row" span={16} offset={4}>
            <Table
              columns={columns}
              dataSource={props.pilotProfile.pilot_licenses}
              pagination={false}
              scroll={{ x: '100%', y: '100%' }}
            />
          </Col>
        </Row>
      </Spin >
      <Modal footer={null} width={'30%'} visible={isModalVisibleUpdate} onCancel={handleCancelUpdate}>
        <>
          <Formik
            enableReinitialize={true}
            initialValues={updateValue}
            onSubmit={updateLicense}
            validationSchema={Schema}
          >
            {({ values, handleSubmit, handleChange, setFieldValue }) => (
              <Form
                layout="vertical"
                onValuesChange={onFormLayoutChange}
                size={"medium"}
                onFinish={handleSubmit}
              >

                <Row gutter={16}>
                  <Col className="gutter-row" span={24} style={{ textAlign: "center" }}>
                    <Title style={{ color: "#7F7F7F" }} level={3}>
                      แก้ไข ใบอนุญาต
                    </Title>
                  </Col>
                  <Col className="gutter-row" span={24} style={{ marginTop: "10px" }}>
                    <Row gutter={16}>
                      <Col className="gutter-row" span={12}>
                        <Field
                          label={{ text: "ชื่อ-สกุล ผู้ถือใบอนุญาต" }}
                          required="true"
                          id="license_name"
                          onChange={handleChange}
                          name="license_name"
                          rows={4}
                          type={"text"}
                          component={Inputs}
                          placeholder="กรอกชื่อ-สกุล"
                          validate={(value) => {
                            if (value == "") {
                              return "กรุณากรอก ชื่อ-สกุล ผู้ถือใบอนุญาต"
                            }
                          }}
                        />
                      </Col>
                      <Col className="gutter-row" span={12}>
                        <Field
                          label={{ text: "ใบอนุญาตเลขที่" }}
                          id="license_no"
                          required="true"
                          onChange={handleChange}
                          name="license_no"
                          rows={4}
                          type={"text"}
                          component={Inputs}
                          placeholder="กรอกใบอนุญาตเลขที่"
                          validate={(value) => {
                            if (value == "") {
                              return "กรุณากรอก ใบอนุญาตเลขที่"
                            }
                          }}
                        />
                      </Col>
                    </Row>
                    <Row gutter={16}>
                      <Col className="gutter-row" span={12}>
                        <Form.Item label={"อัพโหลดใบอนุญาติ"} >
                          <Upload maxCount={1} onRemove={(file) => setUploadList([])} onChange={(file) => uploadFile(file)}>
                            <Button
                              icon={<UploadOutlined />}
                            >
                              Click to Upload
                            </Button>
                          </Upload>
                        </Form.Item>
                      </Col>
                      <Col className="gutter-row" span={12}>
                        <Form.Item label={<>วันที่ออกใบอนุญาต<text style={{ color: "red" }}>*</text></>}>
                          <Field
                            id="license_date"
                            name="license_date"
                            component={DatePicker}
                            placeholder="กรุณาเลือก"
                            validate={(value) => {
                              if (value == "") {
                                return "กรุณาเลือก วันที่ออกใบอนุญาต"
                              }
                            }}
                          />
                        </Form.Item>
                      </Col>
                    </Row>
                  </Col>
                  <Col
                    className="gutter-row" span={24}
                    style={{ textAlign: "center" }}
                  >
                    <Space>

                      <Button
                        style={{ borderRadius: "10px" }}
                        type="danger"
                        onClick={handleCancelUpdate}
                      >
                        <text style={{ color: "white" }}>ยกเลิก</text>
                      </Button>
                      <Button
                        style={{ marginRight: "10px", borderRadius: "10px" }}
                        type="primary" htmlType="submit" primary
                      >
                        ยืนยัน
                      </Button>
                    </Space>
                  </Col>
                </Row>
              </Form>
            )}
          </Formik>
        </>
      </Modal>

      <Modal
        footer={null}
        width={'30%'}
        visible={isModalVisibleDelete}
        onCancel={handleCancelDelete}>
        <Text><InfoCircleOutlined /> คุณต้องการลบ ใบอนุญาตหรือไม</Text>
        <center style={{ marginTop: "20px" }}>
          <Space >
            <Button
              style={{ borderRadius: "10px" }}
              type="danger"
              onClick={handleCancelDelete}
            >
              <Text style={{ color: "white" }}>ยกเลิก</Text>
            </Button>
            <Button
              style={{ marginRight: "10px", borderRadius: "10px" }}
              type="primary" htmlType="submit" primary
              onClick={deleteLicense}
            >
              ยืนยัน
            </Button>
          </Space>
        </center>
      </Modal>
    </div >
  );
}
