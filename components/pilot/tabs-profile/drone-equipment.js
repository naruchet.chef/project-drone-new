import "antd/dist/antd.css";
import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { Field, Formik } from "formik";
import * as Yup from "yup";
import { get } from "lodash";
import { updatePilot } from "../../../services/pilot.service";

import {
  Select as Selects,
  Input as Inputs,
} from "../../../components";
import {
  Button,
  Space,
  Form,
  Row,
  Col,
  notification,
  Spin,
  Typography,
  Checkbox,
} from "antd";
const { Title } = Typography;

export default function Info(props) {
  const initialValues = {
    drone_type: props.pilotProfile.drone_type != null ? JSON.parse("[" + props.pilotProfile.drone_type + "]") : 0,
    drone_model: props.pilotProfile.drone_model != null ? props.pilotProfile.drone_model : ""
  };

  const [isLoading, setIsLoading] = useState(false);

  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };

  const handleSubmitForm = async (values) => {
    let jobBody = {
      id: props.pilotProfile.id,
      drone_type: get(values, "drone_type").toString(),
      drone_model: get(values, "drone_model"),
    };

    if (get(values, "drone_model") === "other") {
      jobBody["drone_model"] = get(values, "other_drone_model");
    }

    setIsLoading(true);
    const { result, success } = await updatePilot(jobBody);
    if (success) {
      notification.success({
        message: `success`,
        description: "อัพเดท เรียบร้อยแล้ว",
      });
      setIsLoading(false);
    }
  };

  const droneModel = [
    { name: "ไม่ระบุ", value: "0" },
    { name: "DJI AIR 2s", value: "dji_air_2s" },
    { name: "DJI MINI 2", value: "dji_mini_2" },
    { name: "DJI FPV COMBO", value: "dji_fpv_combo" },
    { name: "อื่น", value: "other" },
  ];


  const Schema = Yup.object().shape({});

  const droneType = [
    { value: 0, name: "ไม่ระบุ" },
    { value: 1, name: "Multirotors" },
    { value: 2, name: "Fixed Wing" },
    { value: 3, name: "Hybrid VTOL" },
  ];

  return (
    <div
      className="site-layout-background"
      style={{ padding: 24, minHeight: 380 }}
    >
      <Spin spinning={isLoading}>
        <Formik
          enableReinitialize={true}
          initialValues={initialValues}
          onSubmit={handleSubmitForm}
          validationSchema={Schema}
        >
          {({ values, handleSubmit, handleChange, setFieldValue }) => (
            <Form
              layout="vertical"
              onValuesChange={onFormLayoutChange}
              size={"medium"}
              onFinish={handleSubmit}
            >
              <Row gutter={16}>
                <Col className="gutter-row" span={16} offset={4}>
                  <Title style={{ color: "#7F7F7F" }} level={3}>
                    โดรน & อุปกรณ์
                  </Title>
                </Col>
                <Col className="gutter-row" span={16} offset={4}>
                  <Field
                    label={{ text: "ประเภทโดรน" }}
                    id="drone_type"
                    name="drone_type"
                    component={Selects}
                    placeholder="กรุณาเลือก"
                    selectOption={droneType}
                    required="true"
                    mode="multiple"
                  />
                </Col>
                <Col className="gutter-row" span={16} offset={4}>
                  <Row gutter={16}>
                    <Col className="gutter-row" span={18}>
                      <Field
                        label={{ text: "รุ่นโดรน" }}
                        id="drone_model"
                        name="drone_model"
                        component={Selects}
                        placeholder="กรุณาเลือก"
                        required="true"
                        selectOption={droneModel}
                      />
                    </Col>
                    <Col className="gutter-row" span={6}>
                      <Field
                        label={{ required: true, text: "ระบุ" }}
                        id="other_drone_model"
                        name="other_drone_model"
                        component={Inputs}
                        placeholder="กรุณาระบุรุ่นโดรน"
                        disabled={values.drone_model === "other" ? false : true}
                      />
                    </Col>
                  </Row>
                </Col>
                <Col
                  style={{ textAlign: "left" }}
                  className="gutter-row"
                  span={16}
                  offset={4}
                >
                  <Space>
                    <Button
                      style={{ marginRight: "10px", borderRadius: "10px" }}
                      type="primary" htmlType="submit" primary
                    >
                      บันทึก
                    </Button>
                  </Space>
                </Col>
              </Row>
            </Form>
          )}
        </Formik>
      </Spin>
    </div >
  );
}
