import "antd/dist/antd.css";
import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { Field, Formik } from "formik";
import * as Yup from "yup";
import { get } from "lodash";
import { updatePilot } from "../../../services/pilot.service";
import {
  Input as Inputs,
  DatePicker
} from "../../../components";
import {
  Button,
  Space,
  Form,
  Upload,
  Row,
  Col,
  notification,
  Spin,
  Typography,
  Modal,
  Radio,
  Image
} from "antd";
const { Title } = Typography;
import { UploadOutlined } from "@ant-design/icons";

export default function Profile(props) {
  const [visible, setVisible] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [modalText, setModalText] = useState('Content of the modal');
  const [uploadList, setUploadList] = useState([]);

  const uploadFile = (value) => {
    console.log(`value`, value);
    const { file, fileList = [] } = value;
    if (fileList.length > 0) {
      console.log(`file`, file);
      console.log(`fileList`, fileList);
      setUploadList(fileList);
    }
  };

  const initialValues = {
    first_name: get(props.pilotProfile, "first_name") != null ? get(props.pilotProfile, "first_name") : "",
    last_name: get(props.pilotProfile, "last_name") != null ? get(props.pilotProfile, "last_name") : "",
    email: get(props.pilotProfile, "email") != null ? get(props.pilotProfile, "email") : "",
    account_name: get(props.pilotProfile, "account_name") != null ? get(props.pilotProfile, "account_name") : "",
    sex: get(props.pilotProfile, "sex") != null ? get(props.pilotProfile, "sex") : "",
    birth_day: get(props.pilotProfile, "birth_day") != null ? get(props.pilotProfile, "birth_day") : "",
    photo: get(props.pilotProfile, "photo") != null ? get(props.pilotProfile, "photo") : "",
  }
  const initialValuesPassword = {
    password: "",
    new_password: "",
    confirm_password: ""
  }
  const router = useRouter();
  const { id } = router.query;
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
  });

  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };

  const handleSubmitForm = async (values) => {
    const body = new FormData();
    let jobBody = {
      id: id,
      first_name: get(values, "first_name"),
      last_name: get(values, "last_name"),
      email: get(values, "email"),
      account_name: get(values, "account_name"),
      sex: get(values, "sex"),
      birth_day: get(values, "birth_day"),
    };

    if (uploadList.length > 0) {
      uploadList.forEach((file) => {
        body.append("file", file.originFileObj);
      });
    }
    for (const [key, value] of Object.entries(jobBody)) {
      body.append(key, value);
    }

    console.log(body);

    setIsLoading(true);
    const { result, success } = await updatePilot(body);
    if (success) {
      notification.success({
        message: `success`,
        description: "อัพเดท เรียบร้อยแล้ว",
      });
      setIsLoading(false);
      router.reload()
    }
  };

  const handleResetPassword = async (values) => {

  };

  const Schema = Yup.object().shape({
    first_name: Yup.string().required("กรุณากรอก ชื่อ"),
    last_name: Yup.string().required("กรุณากรอก นามสกุล"),
    email: Yup.string().required("กรุณากรอก อีเมลล์"),
    account_name: Yup.string().required("กรุณากรอก ชื่อบัญชี"),
    sex: Yup.string().required("กรุณากรอก เพศ"),
  });

  const SchemaPassword = Yup.object().shape({
    password: Yup.string().required("กรุณากรอก กรอกรหัสผ่านเดิม"),
    new_password: Yup.string().required("กรุณากรอก กรอกรหัสผ่านใหม่"),
    confirm_password: Yup.string().required("กรุณากรอก กรอกยืนยันรหัสผ่านใหม่"),
  });

  const showModal = () => {
    setVisible(true);
  };

  const handleCancel = () => {
    console.log('Clicked cancel button');
    setVisible(false);
  };

  return (
    <>
      <div
        className="site-layout-background"
        style={{ padding: 24, minHeight: 380 }}
      >
        <Spin spinning={isLoading}>
          <Formik
            enableReinitialize={true}
            initialValues={initialValues}
            onSubmit={handleSubmitForm}
            validationSchema={Schema}
          >
            {({ values, handleSubmit, handleChange, setFieldValue }) => (
              <Form
                layout="vertical"
                onValuesChange={onFormLayoutChange}
                size={"medium"}
                onFinish={handleSubmit}
              >

                <Row gutter={16}>
                  <Col className="gutter-row" span={16} offset={4}>
                    <Title style={{ color: "#7F7F7F" }} level={3}>
                      ตั้งค่าบัญชี
                    </Title>
                  </Col>
                  <Col className="gutter-row" span={16} offset={4}>
                    <Row gutter={16}>
                      <Col className="gutter-row" span={12}>
                        <Field
                          label={{ text: "ชื่อ" }}
                          required="true"
                          id="first_name"
                          onChange={handleChange}
                          name="first_name"
                          rows={4}
                          type={"text"}
                          component={Inputs}
                          placeholder="กรอกชื่อ"
                        />
                      </Col>
                      <Col className="gutter-row" span={12}>
                        <Field
                          label={{ text: "นามสกุล" }}
                          id="last_name"
                          required="true"
                          onChange={handleChange}
                          name="last_name"
                          rows={4}
                          type={"text"}
                          component={Inputs}
                          placeholder="กรอกนามสกุล"
                        />
                      </Col>
                    </Row>
                  </Col>
                  <Col className="gutter-row" span={16} offset={4}>
                    <Row gutter={16}>
                      <Col className="gutter-row" span={12}>
                        <Field
                          label={{ text: "อีเมลล์" }}
                          required="true"
                          id="email"
                          onChange={handleChange}
                          name="email"
                          rows={4}
                          type={"text"}
                          component={Inputs}
                          placeholder="กรอกอีเมล"
                        />
                      </Col>
                      <Col className="gutter-row" span={12}>
                        <Field
                          label={{ text: "ชื่อบัญชี" }}
                          id="account_name"
                          required="true"
                          onChange={handleChange}
                          name="account_name"
                          rows={4}
                          type={"text"}
                          component={Inputs}
                          placeholder="กรอกชื่อบัญชี"
                        />
                      </Col>
                    </Row>
                  </Col>
                  <Col className="gutter-row" span={16} offset={4}>
                    <Row gutter={16}>
                      <Col className="gutter-row" span={12}>
                        <label>
                          เพศ<text style={{ color: "red" }}>*</text>
                        </label>
                        <br></br>
                        <Radio.Group onChange={(event) => {
                          setFieldValue("sex", event.target.value);
                        }} value={values.sex}>
                          <Space direction="horizontal">
                            <Row>
                              <Col flex={1}>
                                <Radio value={"male"}>ชาย</Radio>
                              </Col>
                              <Col flex={1}>
                                <Radio value={"female"}>หญิง</Radio>
                              </Col>
                              <Col flex={1}>
                                <Radio value={"other"}>อื่นๆ</Radio>
                              </Col>
                            </Row>
                          </Space>
                        </Radio.Group>
                      </Col>
                      <Col className="gutter-row" span={12}>
                        <Field
                          label={{ text: "วันเกิด" }}
                          id="birth_day"
                          required="true"
                          name="birth_day"
                          component={DatePicker}
                          placeholder="กรุณาเลือก"
                          validate={(value) => {
                            if (value == "") {
                              return "กรุณาเลือก วันเกิด"
                            }
                          }}
                        />
                      </Col>
                    </Row>
                  </Col>
                  <Col className="gutter-row" span={16} offset={4}>
                    {get(props.pilotProfile, "photo") != null ? (
                      <>
                        <Image width={100} src={"/image/" + get(props.pilotProfile, "photo")} />
                      </>
                    ) : ("")
                    }
                  </Col>
                  < Col className="gutter-row" style={{ marginTop: "20px", }} span={16} offset={4}>
                    <Form.Item label="เพิ่มรูปโปรไฟล์ของคุณ">
                      <Upload maxCount={1} onRemove={(file) => setUploadList([])} onChange={(file) => uploadFile(file)}>
                        <Button
                          icon={<UploadOutlined />}
                        >
                          Click to Upload
                        </Button>
                      </Upload>
                    </Form.Item>
                  </Col>
                  <Col
                    className="gutter-row" span={16}
                    offset={4}
                    style={{ textAlign: "left" }}
                  >
                    <Space>
                      <Button
                        style={{ marginTop: "20px", marginRight: "10px", borderRadius: "10px" }}
                        type="primary" htmlType="submit" primary
                      >
                        บันทึก
                      </Button>
                      {/* <Button
                        style={{ background: "#7BC67E", borderRadius: "10px" }}
                        type="default"
                        onClick={showModal}
                      >
                        <text style={{ color: "white" }}>เปลี่ยนรหัสผ่าน</text>
                      </Button> */}
                    </Space>
                  </Col>
                </Row>
              </Form>
            )}
          </Formik>
        </Spin >
      </div >
      <Modal
        visible={visible}
        // onOk={false}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
        footer={null}
      >
        <>
          <div
            className="site-layout-background"
            style={{ padding: 24, minHeight: 380 }}
          >
            <Spin spinning={isLoading}>
              <Formik
                enableReinitialize={true}
                initialValues={initialValuesPassword}
                onSubmit={handleResetPassword}
                validationSchema={SchemaPassword}
              >
                {({ values, handleSubmit, handleChange, setFieldValue }) => (
                  <Form
                    layout="vertical"
                    onValuesChange={onFormLayoutChange}
                    size={"medium"}
                    onFinish={handleSubmit}
                  >

                    <Row gutter={16}>
                      <Col className="gutter-row" span={16} offset={4} style={{ textAlign: "center" }}>
                        <Title style={{ color: "#7F7F7F" }} level={3}>
                          เปลี่ยนรหัสผ่าน
                        </Title>
                      </Col>
                      <Col className="gutter-row" span={16} offset={4}>
                        <Field
                          label={{ text: "รหัสผ่านเดิม" }}
                          required="true"
                          id="password"
                          onChange={handleChange}
                          name="password"
                          rows={4}
                          type={"text"}
                          component={Inputs}
                          placeholder="กรอกรหัสผ่านเดิม"
                        />
                      </Col>
                      <Col className="gutter-row" span={16} offset={4}>
                        <Field
                          label={{ text: "รหัสผ่านใหม่" }}
                          required="true"
                          id="new_password"
                          onChange={handleChange}
                          name="new_password"
                          rows={4}
                          type={"text"}
                          component={Inputs}
                          placeholder="กรอกรหัสผ่านใหม่"
                        />
                      </Col>
                      <Col className="gutter-row" span={16} offset={4}>
                        <Field
                          label={{ text: "ยืนยันรหัสผ่านใหม่" }}
                          required="true"
                          id="confirm_password"
                          onChange={handleChange}
                          name="confirm_password"
                          rows={4}
                          type={"text"}
                          component={Inputs}
                          placeholder="กรอกยืนยันรหัสผ่านใหม่"
                        />
                      </Col>
                      <Col
                        className="gutter-row" span={16}
                        offset={4}
                        style={{ textAlign: "center" }}
                      >
                        <Space>

                          <Button
                            style={{ borderRadius: "10px" }}
                            type="danger"
                            onClick={handleCancel}
                          >
                            <text style={{ color: "white" }}>ยกเลิก</text>
                          </Button>
                          <Button
                            style={{ marginRight: "10px", borderRadius: "10px" }}
                            type="primary" htmlType="submit" primary
                          >
                            ยืนยัน
                          </Button>
                        </Space>
                      </Col>
                    </Row>
                  </Form>
                )}
              </Formik>
            </Spin>
          </div>
        </>
      </Modal>
    </>
  );
}
