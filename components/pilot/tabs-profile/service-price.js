import "antd/dist/antd.css";
import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { Field, Formik } from "formik";
import * as Yup from "yup";
import { get } from "lodash";
import { deletePilotJobType, createPilotJobType } from "../../../services/pilotJobType.service";
import { jobTypeId } from "../../../constants/common"
import {
  Input as Inputs,
} from "../../../components";
import {
  Button,
  Space,
  Form,
  Row,
  Col,
  notification,
  Spin,
  Typography,
  Checkbox,
} from "antd";
const { Title } = Typography;

export default function WokingDay(props) {
  const initialValues = {
    pilot_job_type: JSON.parse("[" + props.pilotProfile.pilot_job_types.map((v, k) => { return v.job_type_id }) + "]"),
    salary_1: "",
    salary_2: "",
    salary_3: "",
    salary_4: "",
    salary_5: "",
    salary_6: "",
    salary_7: ""
  };

  props.pilotProfile.pilot_job_types.map((v, k) => {
    if (v.job_type_id == 1) {
      initialValues.salary_1 = v.salary
    }
    if (v.job_type_id == 2) {
      initialValues.salary_2 = v.salary
    }
    if (v.job_type_id == 3) {
      initialValues.salary_3 = v.salary
    }
    if (v.job_type_id == 4) {
      initialValues.salary_4 = v.salary
    }
    if (v.job_type_id == 5) {
      initialValues.salary_5 = v.salary
    }
    if (v.job_type_id == 6) {
      initialValues.salary_6 = v.salary
    }
    if (v.job_type_id == 7) {
      initialValues.salary_7 = v.salary
    }
  })


  const router = useRouter();
  const [isLoading, setIsLoading] = useState(false);
  const [pilotJobType, setPilotJobType] = useState([]);

  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };

  const onChange = (checkedValues) => {
    setPilotJobType(checkedValues)
  };

  const handleSubmitForm = async (values) => {
    setIsLoading(true);

    if (props.pilotProfile.pilot_job_types.length > 0) {
      props.pilotProfile.pilot_job_types.map((v, k) => {
        deleteJobType(v.id)
      })
    }
    pilotJobType.map((v, k) => {
      let keySalary = "salary_" + v
      let jobBody = {
        pilot_profile_id: props.pilotProfile.id,
        job_type_id: v,
        salary: get(values, keySalary)
      };
      createJobType(jobBody)
    })

    notification.success({
      message: `success`,
      description: "อัพเดท เรียบร้อยแล้ว",
    });
    router.reload()
  };

  const deleteJobType = async (id) => {
    let jobBody = {
      id: id
    };
    const { result, success } = await deletePilotJobType(jobBody);
  };
  const createJobType = async (value) => {
    const { result, success } = await createPilotJobType(value);
  };


  const Schema = Yup.object().shape({});

  return (
    <div
      className="site-layout-background"
      style={{ padding: 24, minHeight: 380 }}
    >
      <Spin spinning={isLoading}>
        <Formik
          enableReinitialize={true}
          initialValues={initialValues}
          onSubmit={handleSubmitForm}
          validationSchema={Schema}
        >
          {({ values, handleSubmit, handleChange, setFieldValue }) => (
            <Form
              layout="vertical"
              onValuesChange={onFormLayoutChange}
              size={"medium"}
              onFinish={handleSubmit}
            >

              <Row gutter={16}>
                <Col className="gutter-row" span={16} offset={4}>
                  <Title style={{ color: "#7F7F7F" }} level={3}>
                    บริการ & ราคา
                  </Title>
                </Col>
                <Col className="gutter-row" span={16} offset={4}>
                  <Col className="gutter-row" span={24}>
                    <Row>
                      <Col span={12}>
                        <label>
                          บริการ
                        </label>
                      </Col>
                      <Col span={12}>
                        <label>
                          อัตราค่าบริการ (ต่อชั่วโมง)
                        </label>
                      </Col>
                    </Row>
                    <Checkbox.Group style={{ width: '100%', marginTop: '20px' }} onChange={onChange} defaultValue={values.pilot_job_type}>
                      <Row>
                        <Col span={12}>
                          <Checkbox value={1}> {jobTypeId[1].text}</Checkbox>
                        </Col>
                        <Col span={12}>
                          <Field
                            disable={true}
                            id="salary_1"
                            onChange={handleChange}
                            name="salary_1"
                            rows={4}
                            type={"number"}
                            component={Inputs}
                            placeholder="0 บาท"
                          />
                        </Col>
                      </Row>
                      <Row>
                        <Col span={12}>
                          <Checkbox value={2}> {jobTypeId[2].text}</Checkbox>
                        </Col>
                        <Col span={12}>
                          <Field
                            id="salary_2"
                            onChange={handleChange}
                            name="salary_2"
                            rows={4}
                            type={"number"}
                            component={Inputs}
                            placeholder="0 บาท"
                          />
                        </Col>
                      </Row>
                      <Row>
                        <Col span={12}>
                          <Checkbox value={3}> {jobTypeId[3].text}</Checkbox>
                        </Col>
                        <Col span={12}>
                          <Field
                            id="salary_3"
                            onChange={handleChange}
                            name="salary_3"
                            rows={4}
                            type={"number"}
                            component={Inputs}
                            placeholder="0 บาท"
                          />
                        </Col>
                      </Row>
                      <Row>
                        <Col span={12}>
                          <Checkbox value={4}> {jobTypeId[4].text}</Checkbox>
                        </Col>
                        <Col span={12}>
                          <Field
                            id="salary_4"
                            onChange={handleChange}
                            name="salary_4"
                            rows={4}
                            type={"number"}
                            component={Inputs}
                            placeholder="0 บาท"
                          />
                        </Col>
                      </Row>
                      <Row>
                        <Col span={12}>
                          <Checkbox value={5}> {jobTypeId[5].text}</Checkbox>
                        </Col>
                        <Col span={12}>
                          <Field
                            id="salary_5"
                            onChange={handleChange}
                            name="salary_5"
                            rows={4}
                            type={"number"}
                            component={Inputs}
                            placeholder="0 บาท"
                          />
                        </Col>
                      </Row>
                      <Row>
                        <Col span={12}>
                          <Checkbox value={6} > {jobTypeId[6].text}</Checkbox>
                        </Col>
                        <Col span={12}>
                          <Field
                            id="salary_6"
                            onChange={handleChange}
                            name="salary_6"
                            rows={4}
                            type={"number"}
                            component={Inputs}
                            placeholder="0 บาท"
                          />
                        </Col>
                      </Row>
                      <Row>
                        <Col span={12}>
                          <Checkbox value={7} > {jobTypeId[7].text}</Checkbox>
                        </Col>
                        <Col span={12}>
                          <Field
                            id="salary_7"
                            onChange={handleChange}
                            name="salary_7"
                            rows={4}
                            type={"number"}
                            component={Inputs}
                            placeholder="0 บาท"
                          />
                        </Col>
                      </Row>
                    </Checkbox.Group>
                  </Col>
                </Col>
                <Col
                  style={{ textAlign: "left", marginTop: "20px" }}
                  className="gutter-row"
                  span={16}
                  offset={4}
                >
                  <Space>
                    <Button
                      style={{ marginRight: "10px", borderRadius: "10px" }}
                      type="primary" htmlType="submit" primary
                    >
                      บันทึก
                    </Button>
                  </Space>
                </Col>
              </Row>
            </Form>
          )}
        </Formik>
      </Spin>
    </div >
  );
}
