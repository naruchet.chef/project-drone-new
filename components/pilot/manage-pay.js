import "antd/dist/antd.css";
import React from "react";
import LayoutHeader from "../../pages/layout/header";
import LayoutFooter from "../../pages/layout/footer";
import { Tabs } from "antd";
import InfoAccount from "./tabs-pay/info-account";
import ListWork from "./tabs-pay/woking-status";
import PilotInfo from "./tabs-profile/pilot-info";
import License from "./tabs-profile/license";
import WorkingDay from "./tabs-profile/working-day";
import DroneEquipment from "./tabs-profile/drone-equipment";
const { TabPane } = Tabs;

import { Layout, Breadcrumb } from "antd";
const { Content } = Layout;

export default function ManagePay(props) {
  return (
    <>
      <div
        className="site-layout-background"
        style={{ padding: 24, minHeight: 380 }}
      >
        <Tabs tabBarStyle={{ color: "#689FF2" }} type="card">
          <TabPane tab="สถานะงาน" key="1">
            <ListWork pilotProfile={props.pilotProfile} />
          </TabPane>
          <TabPane tab="ข้อมูลบัญชี" key="2">
            <InfoAccount pilotProfile={props.pilotProfile} />
          </TabPane>
        </Tabs>
      </div>
    </>
  );
}
