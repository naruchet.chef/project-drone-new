import React from 'react'
import { Input } from 'antd'


export default function FieldsetInput({ label, field, ...props }) {

    return (
        <>
            <fieldset style={{
                    borderRadius: "25px",
                    padding: "0 18px",
                    borderColor: "rgba(0, 0, 0, 0.23)",
                    borderWidth: "1px"
            }}>
                <legend>{label}</legend>
            </fieldset>
            <Input {...props} />
        </>
    )
}
