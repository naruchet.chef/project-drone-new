import React from 'react'
import { Image } from 'antd'

function RatingStar({
  rating = 0
}) {
  if (rating <= 1) {
    return (
      <>
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector1.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector.png" src="error" />
      </>
    )
  } else if (rating <= 1.5) {
    return (
      <>
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector1.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector3.png" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector.png" src="error" />
      </>
    )
  } else if (rating <= 2) {
    return (
      <>
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector1.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector1.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector.png" src="error" />
      </>
    )
  } else if (rating <= 2.5) {
    return (
      <>
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector1.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector1.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector3.png" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector.png" src="error" />
      </>
    )
  } else if (rating <= 3) {
    return (
      <>
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector1.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector1.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector1.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector.png" src="error" />
      </>
    )
  } else if (rating <= 3.5) {
    return (
      <>
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector1.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector1.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector1.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector3.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector.png" src="error" />
      </>
    )
  } else if (rating <= 4) {
    return (
      <>
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector1.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector1.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector1.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector1.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/../img/vector.png" src="error" />
      </>
    )
  } else if (rating <= 4.5) {
    return (
      <>
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector1.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector1.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector1.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector1.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/../img/vector3.png" src="error" />
      </>
    )
  } else if (rating <= 5) {
    return (
      <>
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector1.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector1.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector1.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector1.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector1.png" src="error" />
      </>
    )
  } else {
    return (
      <>
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector.png" src="error" />
        <Image style={{
          width: '14.17px',
          height: '13.46px',
          marginRight: '5px'
        }} fallback="/img/vector.png" src="error" />
      </>
    )
  }

}

export default RatingStar