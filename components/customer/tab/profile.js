import "antd/dist/antd.css";
import React, { useState } from "react";
import { useRouter } from "next/router";
import { Field, Formik } from "formik";
import * as Yup from "yup";
import { get } from "lodash";
import { updateCustomer } from "../../../services/customer.service";
import {
  Input as Inputs,
} from "../../../components";
import {
  Button,
  Space,
  Form,
  Upload,
  Row,
  Col,
  notification,
  Spin,
  Typography,
  Image,
  Modal
} from "antd";
const { Title } = Typography;
const { warning } = Modal;
import { UploadOutlined } from "@ant-design/icons";

export default function Profile(props) {

  const initialValues = props.customerProfile
  const router = useRouter();
  const { id } = router.query;
  const [isLoading, setIsLoading] = useState(false);
  const [uploadList, setUploadList] = useState([]);

  const uploadFile = (value) => {
    console.log(`value`, value);
    const { file, fileList = [] } = value;
    if (fileList.length == 1) {
      console.log(`file`, file);
      console.log(`fileList`, fileList);
      setUploadList(fileList);
    }
  };

  const removeFile = (value) => {
    setUploadList([]);
  };

  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };

  const handleSubmitForm = async (values) => {
    const body = new FormData();
    let jobBody = {
      id: id,
      first_name: get(values, "first_name"),
      last_name: get(values, "last_name"),
      email: get(values, "email"),
      account_name: get(values, "account_name"),
    };
    if (uploadList.length > 0) {
      uploadList.forEach((file) => {
        body.append("file", file.originFileObj);
      });
    }
    for (const [key, value] of Object.entries(jobBody)) {
      body.append(key, value);
    }
    console.log(body);

    setIsLoading(true);
    const { result, success } = await updateCustomer(body);
    if (success) {
      notification.success({
        message: `success`,
        description: "อัพเดท เรียบร้อยแล้ว",
      });
      setIsLoading(false);
    }
    router.reload()
  };

  const Schema = Yup.object().shape({
    first_name: Yup.string().required("กรุณากรอก ชื่อ"),
    last_name: Yup.string().required("กรุณากรอก นามสกุล"),
    email: Yup.string().required("กรุณากรอก อีเมลล์"),
    account_name: Yup.string().required("กรุณากรอก ชื่อบัญชี"),
  });

  return (
    <>
      <div
        className="site-layout-background"
        style={{ padding: 24, minHeight: 380 }}
      >
        <Spin spinning={isLoading}>
          <Formik
            enableReinitialize={true}
            initialValues={initialValues}
            onSubmit={handleSubmitForm}
            validationSchema={Schema}
          >
            {({ values, handleSubmit, handleChange, setFieldValue }) => (
              <Form
                layout="vertical"
                onValuesChange={onFormLayoutChange}
                size={"medium"}
                onFinish={handleSubmit}
              >

                <Row gutter={16}>
                  <Col className="gutter-row" span={16} offset={4}>
                    <Title style={{ color: "#7F7F7F" }} level={3}>
                      ตั้งค่าบัญชี
                    </Title>
                  </Col>
                  <Col className="gutter-row" span={16} offset={4}>
                    <Row gutter={16}>
                      <Col className="gutter-row" span={12}>
                        <Field
                          label={{ text: "ชื่อ" }}
                          required="true"
                          id="first_name"
                          onChange={handleChange}
                          name="first_name"
                          rows={4}
                          type={"text"}
                          component={Inputs}
                          placeholder="กรอกชื่อ"
                        />
                      </Col>
                      <Col className="gutter-row" span={12}>
                        <Field
                          label={{ text: "นามสกุล" }}
                          id="last_name"
                          required="true"
                          onChange={handleChange}
                          name="last_name"
                          rows={4}
                          type={"text"}
                          component={Inputs}
                          placeholder="กรอกนามสกุล"
                        />
                      </Col>
                    </Row>
                  </Col>
                  <Col className="gutter-row" span={16} offset={4}>
                    <Row gutter={16}>
                      <Col className="gutter-row" span={12}>
                        <Field
                          label={{ text: "อีเมลล์" }}
                          required="true"
                          id="email"
                          onChange={handleChange}
                          name="email"
                          rows={4}
                          type={"text"}
                          component={Inputs}
                          placeholder="กรอกอีเมล"
                        />
                      </Col>
                      <Col className="gutter-row" span={12}>
                        <Field
                          label={{ text: "ชื่อบัญชี" }}
                          id="account_name"
                          required="true"
                          onChange={handleChange}
                          name="account_name"
                          rows={4}
                          type={"text"}
                          component={Inputs}
                          placeholder="กรอกชื่อบัญชี"
                        />
                      </Col>
                    </Row>
                  </Col>
                  <Col className="gutter-row" span={16} offset={4}>
                    {get(props.customerProfile, "photo") != null ? (
                      <>
                        <Image width={100} src={"/image/" + get(props.customerProfile, "photo")} />
                      </>
                    ) : ("")
                    }
                  </Col>
                  <Col className="gutter-row" style={{ marginTop: "20px", }} span={16} offset={4}>
                    <Form.Item label="เพิ่มรูปโปรไฟล์ของคุณ">
                      <Upload maxCount={1} onRemove={(file) => setUploadList([])} onChange={(file) => uploadFile(file)}>
                        <Button
                          icon={<UploadOutlined />}
                        >
                          Click to Upload
                        </Button>
                      </Upload>
                    </Form.Item>
                  </Col>
                  <Col
                    className="gutter-row" span={16}
                    offset={4}
                    style={{ textAlign: "left" }}
                  >
                    <Space>
                      <Button
                        style={{ marginRight: "10px", borderRadius: "10px" }}
                        type="primary" htmlType="submit" primary
                      >
                        บันทึก
                      </Button>
                    </Space>
                  </Col>
                </Row>
              </Form>
            )}
          </Formik>
        </Spin>
      </div>
    </>
  );
}
