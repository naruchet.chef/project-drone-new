import React from 'react'
import { Layout, Breadcrumb, Table, Image, Space, Modal, Button } from "antd";
import { paymentHistoryList } from "../../../services/paymentHistory.service";
import useFetchTable from "../../../hooks/useFetchTable";
import helper from "../../../utils/helper"
export default function Payment(props) {
  const customer = props.customerProfile
  const filterRequest = { profile_id: customer.id };
  const requestApi = paymentHistoryList;
  const {
    isLoading,
    dataTable,
    handelDataTableChange,
    handleFetchData,
    pagination,
  } = useFetchTable(requestApi, filterRequest);

  const bank = {
    1: "ธนาคารกรุงไทย จำกัด (มหาชน)",
    2: "ธนาคารกรุงศรีอยุธยา จำกัด (มหาชน)",
    3: "ธนาคารกรุงเทพ จำกัด (มหาชน)",
    4: "ธนาคารกสิกรไทย จำกัด (มหาชน)",
    5: "ธนาคารทหารไทยธนชาติ จำกัด (มหาชน)",
    6: "ธนาคารซีไอเอ็มบี จำกัด (มหาชน)",
    7: "ธนาคารไทยพาณิชย์ จำกัด (มหาชน)",
    8: "ธนาคารสแตนดาร์ดชาร์เตอร์ด(ไทย) จำกัด (มหาชน)",
  };

  const columns = [
    {
      title: "ชื่อบัญชี",
      render: (text, record) => {
        return (
          record.account_bank_name
        );
      },
    },
    {
      title: "ชื่อธนาคาร",
      render: (text, record) => {
        return (
          bank[record.bank_name]
        );
      },
    },
    {
      title: "เลขที่บัญชี",
      render: (text, record) => {
        return (
          record.bank_no
        );
      },
    },
    {
      title: "ยอดเงิน",
      dataIndex: "amount",
    },
    {
      title: 'สลิปโอนเงิน',
      dataIndex: 'slip',
      key: 'slip',
      width: 200,
      render: (row) => {
        console.log(row);
        return (
          <Image width='40%' src={"/image/" + row} />
        )
      }
    },
    {
      title: "วันที่โอน",
      render: (text, record) => {
        return (
          helper.dateFormat(record.created_at)
        );
      },
    }
  ];

  return (
    <div
      className="site-layout-background"
      style={{ padding: 24, minHeight: 600 }}
    >
      <h2>ประวัติการชำระเงิน</h2>
      <Table
        rowKey="id"
        loading={isLoading}
        columns={columns}
        pagination={pagination}
        dataSource={dataTable}
        onChange={handelDataTableChange}
        scroll={{ x: '100%', y: '100%' }}
      />
    </div>
  )
}
