import React from 'react'
import { Layout, Breadcrumb, Table, Image, Space, Modal, Button } from "antd";
import { blockList } from "../../../services/block.service";
import useFetchTable from "../../../hooks/useFetchTable";
import helper from "../../../utils/helper"
import { get } from 'lodash-es';
export default function Payment(props) {
  const customerId = props.customerId

  const filterRequest = { account_id: customerId, category: "pilot" };
  const requestApi = blockList;
  const {
    isLoading,
    dataTable,
    handelDataTableChange,
    handleFetchData,
    pagination,
  } = useFetchTable(requestApi, filterRequest);


  const columns = [
    {
      title: "ชื่อนักบิน",
      render: (text, record) => {
        const profile = get(record, "pilot_profile", {})
        const fullName = `${get(profile, "first_name", "")} ${get(profile, "last_name", "")}`
        return fullName
      },
    },
  ];

  return (
    <div
      className="site-layout-background"
      style={{ padding: 24, minHeight: 600 }}
    >
      <h2>นักบินที่ถูกบล็อก</h2>
      <Table
        rowKey="id"
        loading={isLoading}
        columns={columns}
        pagination={pagination}
        dataSource={dataTable}
        onChange={handelDataTableChange}
      />
    </div>
  )
}
