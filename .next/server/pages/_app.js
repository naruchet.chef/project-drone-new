/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/_app";
exports.ids = ["pages/_app"];
exports.modules = {

/***/ "./contexts/Modal.jsx":
/*!****************************!*\
  !*** ./contexts/Modal.jsx ***!
  \****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"ModalContextProvider\": () => (/* binding */ ModalContextProvider),\n/* harmony export */   \"useModalContext\": () => (/* binding */ useModalContext)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! antd */ \"antd\");\n/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(antd__WEBPACK_IMPORTED_MODULE_2__);\n\n\n\nconst modalContext = /*#__PURE__*/ (0,react__WEBPACK_IMPORTED_MODULE_1__.createContext)({\n    show: ()=>{},\n    hide: ()=>{}\n});\nfunction ModalContextProvider({ children  }) {\n    const { 0: isModalVisible , 1: setIsModalVisible  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);\n    const initContent = {\n        title: \"\",\n        okText: \"ตกลง\",\n        cancelText: \"ยกเลิก\",\n        onOk: ()=>{},\n        body: \"\"\n    };\n    const { 0: content , 1: setContent  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(initContent);\n    const show = (content)=>{\n        if (content) {\n            setContent(content);\n            console.log(`content`, content);\n        }\n        setIsModalVisible(true);\n    };\n    const hide = ()=>{\n        setIsModalVisible(false);\n    };\n    const value = (0,react__WEBPACK_IMPORTED_MODULE_1__.useMemo)(()=>{\n        return {\n            show,\n            hide\n        };\n    }, [\n        isModalVisible\n    ]);\n    return /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(modalContext.Provider, {\n        value: value,\n        children: [\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(antd__WEBPACK_IMPORTED_MODULE_2__.Modal, {\n                title: content.title,\n                visible: isModalVisible,\n                onOk: content.onOk,\n                onCancel: hide,\n                okText: content.okText,\n                cancelText: content.cancelText,\n                children: content.body\n            }, void 0, false, {\n                fileName: \"/Users/chetta.cro.admin/Documents/cj-lab/lab04/gitlab/project-drone-new/contexts/Modal.jsx\",\n                lineNumber: 36,\n                columnNumber: 7\n            }, this),\n            children\n        ]\n    }, void 0, true, {\n        fileName: \"/Users/chetta.cro.admin/Documents/cj-lab/lab04/gitlab/project-drone-new/contexts/Modal.jsx\",\n        lineNumber: 35,\n        columnNumber: 5\n    }, this);\n}\nfunction useModalContext() {\n    const context = (0,react__WEBPACK_IMPORTED_MODULE_1__.useContext)(modalContext);\n    if (context === undefined) {\n        throw new Error(\"useCrudContext must be used within a CrudContextProvider\");\n    }\n    const { show , hide  } = context;\n    return {\n        show,\n        hide\n    };\n}\n\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9jb250ZXh0cy9Nb2RhbC5qc3guanMiLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQTtBQUE0RTtBQUMvQztBQUM3QixNQUFNTSxZQUFZLGlCQUFHTCxvREFBYSxDQUFDO0lBQ2pDTSxJQUFJLEVBQUUsSUFBTSxDQUFDLENBQUM7SUFDZEMsSUFBSSxFQUFFLElBQU0sQ0FBQyxDQUFDO0NBQ2YsQ0FBQztBQUlGLFNBQVNDLG9CQUFvQixDQUFDLEVBQUVDLFFBQVEsR0FBRSxFQUFFO0lBQzFDLE1BQU0sS0FBQ0MsY0FBYyxNQUFFQyxpQkFBaUIsTUFBSVIsK0NBQVEsQ0FBQyxLQUFLLENBQUM7SUFDM0QsTUFBTVMsV0FBVyxHQUFHO1FBQ2xCQyxLQUFLLEVBQUUsRUFBRTtRQUNUQyxNQUFNLEVBQUUsTUFBTTtRQUNkQyxVQUFVLEVBQUUsUUFBUTtRQUNwQkMsSUFBSSxFQUFFLElBQU0sQ0FBQyxDQUFDO1FBQ2RDLElBQUksRUFBRSxFQUFFO0tBQ1Q7SUFDRCxNQUFNLEtBQUNDLE9BQU8sTUFBRUMsVUFBVSxNQUFJaEIsK0NBQVEsQ0FBQ1MsV0FBVyxDQUFDO0lBQ25ELE1BQU1OLElBQUksR0FBRyxDQUFDWSxPQUFPLEdBQUs7UUFDeEIsSUFBSUEsT0FBTyxFQUFFO1lBQ1hDLFVBQVUsQ0FBQ0QsT0FBTyxDQUFDLENBQUM7WUFDcEJFLE9BQU8sQ0FBQ0MsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLEVBQUVILE9BQU8sQ0FBQztRQUNqQyxDQUFDO1FBQ0RQLGlCQUFpQixDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzFCLENBQUM7SUFDRCxNQUFNSixJQUFJLEdBQUcsSUFBTTtRQUNqQkksaUJBQWlCLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDM0IsQ0FBQztJQUNELE1BQU1XLEtBQUssR0FBR3BCLDhDQUFPLENBQUMsSUFBTTtRQUMxQixPQUFPO1lBQUVJLElBQUk7WUFBRUMsSUFBSTtTQUFFLENBQUM7SUFDeEIsQ0FBQyxFQUFFO1FBQUNHLGNBQWM7S0FBQyxDQUFDO0lBRXBCLHFCQUNFLDhEQUFDTCxZQUFZLENBQUNrQixRQUFRO1FBQUNELEtBQUssRUFBRUEsS0FBSzs7MEJBQ2pDLDhEQUFDbEIsdUNBQUs7Z0JBQ0pTLEtBQUssRUFBRUssT0FBTyxDQUFDTCxLQUFLO2dCQUNwQlcsT0FBTyxFQUFFZCxjQUFjO2dCQUN2Qk0sSUFBSSxFQUFFRSxPQUFPLENBQUNGLElBQUk7Z0JBQ2xCUyxRQUFRLEVBQUVsQixJQUFJO2dCQUNkTyxNQUFNLEVBQUVJLE9BQU8sQ0FBQ0osTUFBTTtnQkFDdEJDLFVBQVUsRUFBRUcsT0FBTyxDQUFDSCxVQUFVOzBCQUc3QkcsT0FBTyxDQUFDRCxJQUFJOzs7OztvQkFDUDtZQUNQUixRQUFROzs7Ozs7WUFDYSxDQUN4QjtBQUNKLENBQUM7QUFFRCxTQUFTaUIsZUFBZSxHQUFHO0lBQ3pCLE1BQU1DLE9BQU8sR0FBRzFCLGlEQUFVLENBQUNJLFlBQVksQ0FBQztJQUN4QyxJQUFJc0IsT0FBTyxLQUFLQyxTQUFTLEVBQUU7UUFDekIsTUFBTSxJQUFJQyxLQUFLLENBQUMsMERBQTBELENBQUMsQ0FBQztJQUM5RSxDQUFDO0lBQ0QsTUFBTSxFQUFFdkIsSUFBSSxHQUFFQyxJQUFJLEdBQUUsR0FBR29CLE9BQU87SUFFOUIsT0FBTztRQUFFckIsSUFBSTtRQUFFQyxJQUFJO0tBQUUsQ0FBQztBQUN4QixDQUFDO0FBRWdEIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vZHJvbmUtcHJvamVjdC8uL2NvbnRleHRzL01vZGFsLmpzeD85OWI0Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCwgeyBjcmVhdGVDb250ZXh0LCB1c2VDb250ZXh0LCB1c2VNZW1vLCB1c2VTdGF0ZSB9IGZyb20gXCJyZWFjdFwiO1xuaW1wb3J0IHsgTW9kYWwgfSBmcm9tIFwiYW50ZFwiO1xuY29uc3QgbW9kYWxDb250ZXh0ID0gY3JlYXRlQ29udGV4dCh7XG4gIHNob3c6ICgpID0+IHt9LFxuICBoaWRlOiAoKSA9PiB7fSxcbn0pO1xuXG5cblxuZnVuY3Rpb24gTW9kYWxDb250ZXh0UHJvdmlkZXIoeyBjaGlsZHJlbiB9KSB7XG4gIGNvbnN0IFtpc01vZGFsVmlzaWJsZSwgc2V0SXNNb2RhbFZpc2libGVdID0gdXNlU3RhdGUoZmFsc2UpO1xuICBjb25zdCBpbml0Q29udGVudCA9IHtcbiAgICB0aXRsZTogXCJcIixcbiAgICBva1RleHQ6IFwi4LiV4LiB4Lil4LiHXCIsXG4gICAgY2FuY2VsVGV4dDogXCLguKLguIHguYDguKXguLTguIFcIixcbiAgICBvbk9rOiAoKSA9PiB7fSxcbiAgICBib2R5OiBcIlwiLFxuICB9O1xuICBjb25zdCBbY29udGVudCwgc2V0Q29udGVudF0gPSB1c2VTdGF0ZShpbml0Q29udGVudCk7XG4gIGNvbnN0IHNob3cgPSAoY29udGVudCkgPT4ge1xuICAgIGlmIChjb250ZW50KSB7XG4gICAgICBzZXRDb250ZW50KGNvbnRlbnQpO1xuICAgICAgY29uc29sZS5sb2coYGNvbnRlbnRgLCBjb250ZW50KVxuICAgIH1cbiAgICBzZXRJc01vZGFsVmlzaWJsZSh0cnVlKTtcbiAgfTtcbiAgY29uc3QgaGlkZSA9ICgpID0+IHtcbiAgICBzZXRJc01vZGFsVmlzaWJsZShmYWxzZSk7XG4gIH07XG4gIGNvbnN0IHZhbHVlID0gdXNlTWVtbygoKSA9PiB7XG4gICAgcmV0dXJuIHsgc2hvdywgaGlkZSB9O1xuICB9LCBbaXNNb2RhbFZpc2libGVdKTtcblxuICByZXR1cm4gKFxuICAgIDxtb2RhbENvbnRleHQuUHJvdmlkZXIgdmFsdWU9e3ZhbHVlfT5cbiAgICAgIDxNb2RhbFxuICAgICAgICB0aXRsZT17Y29udGVudC50aXRsZX1cbiAgICAgICAgdmlzaWJsZT17aXNNb2RhbFZpc2libGV9XG4gICAgICAgIG9uT2s9e2NvbnRlbnQub25Pa31cbiAgICAgICAgb25DYW5jZWw9e2hpZGV9XG4gICAgICAgIG9rVGV4dD17Y29udGVudC5va1RleHR9XG4gICAgICAgIGNhbmNlbFRleHQ9e2NvbnRlbnQuY2FuY2VsVGV4dH1cblxuICAgICAgPlxuICAgICAgICB7Y29udGVudC5ib2R5fVxuICAgICAgPC9Nb2RhbD5cbiAgICAgIHtjaGlsZHJlbn1cbiAgICA8L21vZGFsQ29udGV4dC5Qcm92aWRlcj5cbiAgKTtcbn1cblxuZnVuY3Rpb24gdXNlTW9kYWxDb250ZXh0KCkge1xuICBjb25zdCBjb250ZXh0ID0gdXNlQ29udGV4dChtb2RhbENvbnRleHQpO1xuICBpZiAoY29udGV4dCA9PT0gdW5kZWZpbmVkKSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKFwidXNlQ3J1ZENvbnRleHQgbXVzdCBiZSB1c2VkIHdpdGhpbiBhIENydWRDb250ZXh0UHJvdmlkZXJcIik7XG4gIH1cbiAgY29uc3QgeyBzaG93LCBoaWRlIH0gPSBjb250ZXh0O1xuXG4gIHJldHVybiB7IHNob3csIGhpZGUgfTtcbn1cblxuZXhwb3J0IHsgTW9kYWxDb250ZXh0UHJvdmlkZXIsIHVzZU1vZGFsQ29udGV4dCB9O1xuIl0sIm5hbWVzIjpbIlJlYWN0IiwiY3JlYXRlQ29udGV4dCIsInVzZUNvbnRleHQiLCJ1c2VNZW1vIiwidXNlU3RhdGUiLCJNb2RhbCIsIm1vZGFsQ29udGV4dCIsInNob3ciLCJoaWRlIiwiTW9kYWxDb250ZXh0UHJvdmlkZXIiLCJjaGlsZHJlbiIsImlzTW9kYWxWaXNpYmxlIiwic2V0SXNNb2RhbFZpc2libGUiLCJpbml0Q29udGVudCIsInRpdGxlIiwib2tUZXh0IiwiY2FuY2VsVGV4dCIsIm9uT2siLCJib2R5IiwiY29udGVudCIsInNldENvbnRlbnQiLCJjb25zb2xlIiwibG9nIiwidmFsdWUiLCJQcm92aWRlciIsInZpc2libGUiLCJvbkNhbmNlbCIsInVzZU1vZGFsQ29udGV4dCIsImNvbnRleHQiLCJ1bmRlZmluZWQiLCJFcnJvciJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./contexts/Modal.jsx\n");

/***/ }),

/***/ "./pages/_app.js":
/*!***********************!*\
  !*** ./pages/_app.js ***!
  \***********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _styles_globals_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../styles/globals.css */ \"./styles/globals.css\");\n/* harmony import */ var _styles_globals_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_styles_globals_css__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var antd_dist_antd_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! antd/dist/antd.css */ \"./node_modules/antd/dist/antd.css\");\n/* harmony import */ var antd_dist_antd_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(antd_dist_antd_css__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _contexts_Modal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../contexts/Modal */ \"./contexts/Modal.jsx\");\n/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! next/head */ \"next/head\");\n/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_4__);\n\n\n\n\n\nfunction MyApp({ Component , pageProps  }) {\n    return /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {\n        children: [\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((next_head__WEBPACK_IMPORTED_MODULE_4___default()), {\n                children: [\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"link\", {\n                        rel: \"shortcut icon\",\n                        href: \"/favicon_io/favicon.ico\"\n                    }, void 0, false, {\n                        fileName: \"/Users/chetta.cro.admin/Documents/cj-lab/lab04/gitlab/project-drone-new/pages/_app.js\",\n                        lineNumber: 10,\n                        columnNumber: 7\n                    }, this),\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"title\", {\n                        children: \"SUT Platform Drone เครือข่ายนักบินโดรนเชิงพาณิชย์ที่ใหญ่ที่สุดในประเทศไทย\"\n                    }, void 0, false, {\n                        fileName: \"/Users/chetta.cro.admin/Documents/cj-lab/lab04/gitlab/project-drone-new/pages/_app.js\",\n                        lineNumber: 11,\n                        columnNumber: 7\n                    }, this)\n                ]\n            }, void 0, true, {\n                fileName: \"/Users/chetta.cro.admin/Documents/cj-lab/lab04/gitlab/project-drone-new/pages/_app.js\",\n                lineNumber: 9,\n                columnNumber: 6\n            }, this),\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_contexts_Modal__WEBPACK_IMPORTED_MODULE_3__.ModalContextProvider, {\n                children: [\n                    /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(Component, {\n                        ...pageProps\n                    }, void 0, false, {\n                        fileName: \"/Users/chetta.cro.admin/Documents/cj-lab/lab04/gitlab/project-drone-new/pages/_app.js\",\n                        lineNumber: 14,\n                        columnNumber: 7\n                    }, this),\n                    \";\"\n                ]\n            }, void 0, true, {\n                fileName: \"/Users/chetta.cro.admin/Documents/cj-lab/lab04/gitlab/project-drone-new/pages/_app.js\",\n                lineNumber: 13,\n                columnNumber: 5\n            }, this)\n        ]\n    }, void 0, true);\n}\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MyApp);\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9fYXBwLmpzLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUErQjtBQUNIO0FBQzRCO0FBQzNCO0FBRTdCLFNBQVNFLEtBQUssQ0FBQyxFQUFFQyxTQUFTLEdBQUVDLFNBQVMsR0FBRSxFQUFFO0lBQ3ZDLHFCQUNFOzswQkFDQyw4REFBQ0gsa0RBQUk7O2tDQUNKLDhEQUFDSSxNQUFJO3dCQUFDQyxHQUFHLEVBQUMsZUFBZTt3QkFBQ0MsSUFBSSxFQUFDLHlCQUF5Qjs7Ozs7NEJBQUc7a0NBQzNELDhEQUFDQyxPQUFLO2tDQUFDLDJFQUF5RTs7Ozs7NEJBQVE7Ozs7OztvQkFDbkY7MEJBQ1AsOERBQUNSLGlFQUFvQjs7a0NBQ25CLDhEQUFDRyxTQUFTO3dCQUFFLEdBQUdDLFNBQVM7Ozs7OzRCQUFJO29CQUFBLEdBQzlCOzs7Ozs7b0JBQXVCOztvQkFDcEIsQ0FDSjtBQUVILENBQUM7QUFFRCxpRUFBZUYsS0FBSyxFQUFDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vZHJvbmUtcHJvamVjdC8uL3BhZ2VzL19hcHAuanM/ZTBhZCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgXCIuLi9zdHlsZXMvZ2xvYmFscy5jc3NcIjtcbmltcG9ydCBcImFudGQvZGlzdC9hbnRkLmNzc1wiO1xuaW1wb3J0IHsgTW9kYWxDb250ZXh0UHJvdmlkZXIgfSBmcm9tICcuLi9jb250ZXh0cy9Nb2RhbCdcbmltcG9ydCBIZWFkIGZyb20gJ25leHQvaGVhZCc7XG5cbmZ1bmN0aW9uIE15QXBwKHsgQ29tcG9uZW50LCBwYWdlUHJvcHMgfSkge1xuICByZXR1cm4gKFxuICAgIDw+XG4gICAgIDxIZWFkPlxuICAgICAgPGxpbmsgcmVsPVwic2hvcnRjdXQgaWNvblwiIGhyZWY9XCIvZmF2aWNvbl9pby9mYXZpY29uLmljb1wiIC8+XG4gICAgICA8dGl0bGU+U1VUIFBsYXRmb3JtIERyb25lIOC5gOC4hOC4o+C4t+C4reC4guC5iOC4suC4ouC4meC4seC4geC4muC4tOC4meC5guC4lOC4o+C4meC5gOC4iuC4tOC4h+C4nuC4suC4k+C4tOC4iuC4ouC5jOC4l+C4teC5iOC5g+C4q+C4jeC5iOC4l+C4teC5iOC4quC4uOC4lOC5g+C4meC4m+C4o+C4sOC5gOC4l+C4qOC5hOC4l+C4ojwvdGl0bGU+XG4gICAgPC9IZWFkPlxuICAgIDxNb2RhbENvbnRleHRQcm92aWRlcj5cbiAgICAgIDxDb21wb25lbnQgey4uLnBhZ2VQcm9wc30gLz47XG4gICAgPC9Nb2RhbENvbnRleHRQcm92aWRlcj5cbiAgICA8Lz5cbiAgKVxuXG59XG5cbmV4cG9ydCBkZWZhdWx0IE15QXBwO1xuIl0sIm5hbWVzIjpbIk1vZGFsQ29udGV4dFByb3ZpZGVyIiwiSGVhZCIsIk15QXBwIiwiQ29tcG9uZW50IiwicGFnZVByb3BzIiwibGluayIsInJlbCIsImhyZWYiLCJ0aXRsZSJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./pages/_app.js\n");

/***/ }),

/***/ "./node_modules/antd/dist/antd.css":
/*!*****************************************!*\
  !*** ./node_modules/antd/dist/antd.css ***!
  \*****************************************/
/***/ (() => {



/***/ }),

/***/ "./styles/globals.css":
/*!****************************!*\
  !*** ./styles/globals.css ***!
  \****************************/
/***/ (() => {



/***/ }),

/***/ "antd":
/*!***********************!*\
  !*** external "antd" ***!
  \***********************/
/***/ ((module) => {

"use strict";
module.exports = require("antd");

/***/ }),

/***/ "next/head":
/*!****************************!*\
  !*** external "next/head" ***!
  \****************************/
/***/ ((module) => {

"use strict";
module.exports = require("next/head");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/***/ ((module) => {

"use strict";
module.exports = require("react");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-dev-runtime");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/_app.js"));
module.exports = __webpack_exports__;

})();