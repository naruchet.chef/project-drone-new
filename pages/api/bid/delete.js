import Bid from '../../../controllers/bid.controllers'
import handleResponse from '../../../services/backend/handleResponse'
import withAuth from '../../../middlewares/auth'

const handler = async (req, res) => {
  try {
    const { method, headers, body, query } = req
    const allowMethod = ["DELETE"]
    if (allowMethod.includes(method)) {
      const { id } = query
      const response = await Bid.deletes(id)
      if (response) {
        return handleResponse.success(res, "success", response)
      } else {
        return handleResponse.notFound(res, "Bid id not found")
      }
    } else {
      return handleResponse.methodNotAllow(res)
    }
  } catch (error) {
    return handleResponse.error(res, error)
  }
}

export default withAuth(handler)


