import axios from 'axios'
import handleResponse from '../../../services/backend/handleResponse'
const apiKey = "AIzaSyAMxKgOUzEZZfsEUQPIbKxwoJC7W5U9aSA"

var config = {
  method: 'get',
  url: `https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=Museum%20of%20Contemporary%20Art%20Australia&inputtype=textquery&fields=formatted_address%2Cname%2Crating%2Copening_hours%2Cgeometry&key=${apiKey}`,
  headers: {}
};

// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

const handler = async (req, res) => {
  try {
    const { method, headers, body, query } = req
    const { text = "" } = query
    console.log('text :>> ', text);

    const options = {
      headers: {
        "Accept": "*",
        "Content-Type": "application/json",
      }
    }
    let url = `https://maps.googleapis.com/maps/api/place/textsearch/json?query=${text}&key=${apiKey}`
    const result = await axios.get(encodeURI(url), options)
    const { data } = result;
    handleResponse.success(res, "success", data)
  } catch (error) {
    return handleResponse.error(res, error)
  }

}

export default handler
