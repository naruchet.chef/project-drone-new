import Block from '../../../controllers/block.controlleres'
import handleResponse from '../../../services/backend/handleResponse'
import withAuth from '../../../middlewares/auth'
import { get } from 'lodash-es'

const handler = async (req, res) => {
  try {
    const { method, headers, body, query } = req
    if (method === 'DELETE') {
      const id = get(query, "id", "")
      if (id === "") {
        return handleResponse.notFound(res)
      }
      const response = await Block.deletes(id)
      if (response) {
        handleResponse.success(res, "success", response)
      } else {
        handleResponse.error(res)
      }
    } else {
      handleResponse.methodNotAllow(res)
    }
  } catch (error) {
    return handleResponse.error(res, error)
  }
}

export default withAuth(handler)


