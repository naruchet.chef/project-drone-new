import Favarite from '../../../controllers/favarite.controlleres'
import handleResponse from '../../../services/backend/handleResponse'
import withAuth from '../../../middlewares/auth'

const handler = async (req, res) => {
  try {
    const { method, headers, body } = req
    if (method === 'POST') {
      const response = await Favarite.create(body)
      if (response) {
        handleResponse.success(res, "success", response)
      } else {
        handleResponse.error(res)
      }
    } else {
      handleResponse.methodNotAllow(res)
    }
  } catch (error) {
    return handleResponse.error(res, error)
  }
}

export default withAuth(handler)


