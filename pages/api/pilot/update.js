import Pilot from '../../../controllers/pilot.controllers'
import handleResponse from '../../../services/backend/handleResponse'
import withAuth from '../../../middlewares/auth'
import helper from '../../../utils/helper'

const imageUpload = async (image) => {
  const uuidName = helper.generateUUID()
  let imageType = helper.getFileType(image.name)
  const newImaageName = `${uuidName}.${imageType}`
  const path = `./public/image/${newImaageName}`
  await image.mv(path)
  const iamgeObj = {
    image: newImaageName
  }
  return iamgeObj
}
const handler = async (req, res) => {
  try {
    const { method, headers, body } = req
    if (method === 'POST') {
      let image = req.files
      if (image != null) {
        const result = await imageUpload(image.file)
        body.photo = result.image
      }
      const response = await Pilot.update(body)
      if (response) {
        handleResponse.success(res, "success", response)
      } else {
        handleResponse.error(res)
      }
    } else {
      handleResponse.methodNotAllow(res)
    }
  } catch (error) {
    return handleResponse.error(res, error)
  }
}

export default withAuth(handler)
