import Pilot from '../../../controllers/pilot.controllers'
import handleResponse from '../../../services/backend/handleResponse'
import withAuth from '../../../middlewares/auth'

const handler = async (req, res) => {
  try {
    const { method, headers, body, query } = req
    const allowMethod = ["GET"]
    if (allowMethod.includes(method)) {
      const { rows, meta } = await Pilot.findAll(query)
      if (rows) {
        return handleResponse.success(res, "success", rows, meta)
      } else {
        return handleResponse.notFound(res, "Pilot id not found")
      }
    } else {
      return handleResponse.methodNotAllow(res)
    }
  } catch (error) {
    return handleResponse.error(res, error)
  }
}

export default withAuth(handler)


