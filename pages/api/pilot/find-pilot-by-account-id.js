import Pilot from '../../../controllers/pilot.controllers'
import handleResponse from '../../../services/backend/handleResponse'
import withAuth from '../../../middlewares/auth'

const handler = async (req, res) => {
  try {
    const { method, headers, body, query } = req
    const allowMethod = ["GET"]
    if (allowMethod.includes(method)) {
      const { account_id } = query
      const response = await Pilot.findPilotByAccount(account_id)
      if (response) {
        return handleResponse.success(res, "success", response)
      } else {
        return handleResponse.notFound(res, "pilot id not found")
      }
    } else {
      return handleResponse.methodNotAllow(res)
    }
  } catch (error) {
    return handleResponse.error(res, error)
  }
}

export default withAuth(handler)


