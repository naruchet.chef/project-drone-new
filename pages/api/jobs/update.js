import Job from '../../../controllers/job.controllers'
import handleResponse from '../../../services/backend/handleResponse'
import withAuth from '../../../middlewares/auth'
import { get, isArray } from 'lodash-es';
import helper from '../../../utils/helper'

export const config = {
  api: {
    bodyParser: false
  }
};


const handler = async (req, res) => {
  try {
    const { method, headers, body } = req
    if (method === 'POST') {
      let image = get(req, "files.file")
      let job_images = []
      if (image) {
        if (isArray(image)) {
          for (let img of image) {
            const result = await helper.imageUpload(img)
            job_images.push(result)
          }
        } else {
          const result = await helper.imageUpload(image)
          job_images.push(result)
        }
      }

      let jobLocation = []
      let location = get(body, "job_locations")
      if (location !== "") {
        jobLocation = JSON.parse(location)
      }
    
      const newBody = {
        ...body,
        job_images: job_images,
        job_locations: jobLocation,
      }
      const response = await Job.update(newBody)
      if (response) {
        handleResponse.success(res, "success", response)
      } else {
        handleResponse.error(res)
      }
    } else {
      handleResponse.methodNotAllow(res)
    }
  } catch (error) {
    return handleResponse.error(res, error)
  }
}

export default withAuth(handler)
