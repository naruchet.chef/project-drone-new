import Job from '../../../controllers/job.controllers'
import handleResponse from '../../../services/backend/handleResponse'
import withAuth from '../../../middlewares/auth'

const handler = async (req, res) => {
  try {
    const { method, headers, body, query } = req
    const allowMethod = ["GET"]
    if (allowMethod.includes(method)) {
      const { id } = query
      const response = await Job.findOne(id)
      if (response) {
        return handleResponse.success(res, "success", response)
      } else {
        return handleResponse.notFound(res, "job id not found")
      }
    } else {
      return handleResponse.methodNotAllow(res)
    }
  } catch (error) {
    return handleResponse.error(res, error)
  }
}

export default withAuth(handler)


