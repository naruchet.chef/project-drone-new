import authentication from '../../../controllers/authentication.controllers'
import handleResponse from '../../../services/backend/handleResponse'
import withAuth from '../../../middlewares/auth'

// import { OAuth2Client } from 'google-auth-library'

// const { GOOGLE_CLIENT_ID } = process.env
// const client = new OAuth2Client(GOOGLE_CLIENT_ID);

const handler = async (req, res) => {
    try {
        const { method, headers, body } = req

        // console.log(" req --> ", req)
        const allowMethod = ["POST"]
        if (allowMethod.includes(method)) {
            // const { id } = query

            const response = await authentication.register(body)
            if (response) {
            return handleResponse.success(res, "success", response)
            } else {
                return handleResponse.error(res, "Authentication faield")
            }
        } else {
            return handleResponse.methodNotAllow(res)
        }
    } catch (error) {
        console.log("catch error --> ", error)
        return handleResponse.error(res, error)
    }
}

export default withAuth(handler)


