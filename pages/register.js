import React, { useState } from "react";
import { Layout, Row, Col, Form, Input, Button, Modal, Select } from 'antd';
import { useRouter } from 'next/router'
import { GoogleOAuthProvider, useGoogleLogin } from '@react-oauth/google';
import { FacebookProvider, LoginButton } from 'react-facebook';
import { get } from 'lodash'

const { Option } = Select;
import LayoutHeader from '../components/layout/header'
import LayoutFooter from '../components/layout/footer'

import { socialLogin, register } from "../services/authentication.service";

const { Content, Footer } = Layout;

const Wrapper = React.forwardRef((props, ref) => {
    return (
        <GoogleOAuthProvider
            clientId="378771911273-9jv0grs96nb9noul4mf9a2i881rqkkko.apps.googleusercontent.com"
            onScriptLoadSuccess={(cc) => { console.log("onScriptLoadSuccess --> ", cc) }}
            onScriptLoadError={(cc) => { console.log("onScriptLoadError --> ", cc) }}
        >
            <FacebookProvider appId="5915063525218549">
                <PageLogin {...props} />
            </FacebookProvider>
        </GoogleOAuthProvider>
    )
})

Wrapper.displayName = 'GoogleOAuthProvider';

const PageLogin = () => {
    const router = useRouter()
    const [visible, setVisible] = useState(false);
    const [accountType, setAccountType] = useState("employer");
    const [requestToken, setRequestToken] = useState({})

    const googleLogin = useGoogleLogin({
        onSuccess: async (tokenResponse) => {
            console.log(tokenResponse)
            let body = {
                token: tokenResponse.access_token,
                social: "google",
                userId: ""
            }
            await handleSocialLogin(body)
        },
    });

    const handleSocialLogin = async (body) => {
        // setVisible(true)
        setRequestToken(body)
        let data = await fetchTokenLogin(body)
        if (data) {
            // data = await fetchRegister(body)
            // if (!data) {
            //     return false
            // }

            setLogin(data)
            // localStorage.setItem('token', data.token)
            // localStorage.setItem('account_type', data.account_type)
            // localStorage.setItem('account_name', data.account_name)
            return null
        }

        setVisible(true)
        return null

    }

    const fetchTokenLogin = async (data) => {
        const { result, success } = await socialLogin(data);
        if (success) {
            console.log(`result`, result);

            return result.data
            // const { data = [] } = result;
            // setJobData(data);
            // setIsLoading(false);
            // localStorage.setItem('token', result.data)
            // localStorage.setItem('account_type', accountType)
            // router.push("/home")
        }
        // setVisible(false)
    }

    const fetchRegister = async () => {
        const { result, success } = await register({...requestToken, account_type: accountType});
        if (success) {
            console.log(`result`, result);
            // const { data = [] } = result;
            // setJobData(data);
            // setIsLoading(false);
            setLogin(result.data)

        }
        setVisible(false)
    }

    const setLogin = (data) => {
        localStorage.setItem('token', data.token)
        localStorage.setItem('account_type', data.account_type)
        localStorage.setItem('account_name', data.account_name)
        router.push("/")
    }

    const handleResponse = async (data) => {
        console.log(data);``
        let body = {
            token: get(data, "tokenDetail.accessToken"),
            social: "facebook",
            userId: get(data, "tokenDetail.userID"),
            email: get(data, "tokenDetail.email"),
            userName: ""
        }
        await handleSocialLogin(body)
    }

    const handleError = (error) => {
        console.log(error);
        // this.setState({ error });
    }

    const handleChange = (value) => {
        console.log("handleChange --> ", value)
        setAccountType(value)
    };

    return (
        <>
            <Layout className="layout">
                <LayoutHeader authen={false} />
                <Content
                    className="main-layout"
                    style={{
                        padding: "64px 0px",
                    }}
                >
                    <div className='main-wrapper margin-top-30' >
                        <Row justify="center" className="margin-top-50" >
                            <Col span={8} className="text-center">
                                <Row justify="center" className="margin-top-50">
                                    <Col span={24} className="text-center">
                                        <span style={{
                                            fontFamily: 'var(--font-family-roboto)',
                                            fontStyle: 'normal',
                                            fontWeight: 700,
                                            fontSize: '22px',
                                            lineHeight: '26px',
                                            color: "rgba(0, 0, 0, 0.54)",
                                        }}>
                                            สมัครสมาชิก
                                        </span>
                                        <p style={{
                                            marginTop: "10px",

                                            fontFamily: 'Prompt',
                                            fontStyle: "normal",
                                            fontWeight: 400,
                                            fontSize: "18px",
                                            lineHeight: "21px",
                                            /* identical to box height */

                                            textAlign: "center",

                                            /* text / secondary */

                                            color: "rgba(0, 0, 0, 0.54)"
                                        }}>
                                            ลงทะเบียนด้วย Social
                                        </p>
                                    </Col>
                                </Row>
                                <Row justify="center" className="margin-top-20">
                                    <Col xs={24} sm={24} md={24} lg={24} xl={24} className="text-center">
                                        {/* <FacebookProvider appId="5915063525218549"> */}
                                        <LoginButton
                                            scope="email"
                                            onCompleted={handleResponse}
                                            autoLoad={true}
                                            onError={handleError}
                                            className="ant-btn ant-btn-round ant-btn-default btn-login"
                                        >
                                            <img src="/img/facebook.svg" width="32" /> ลงทะเบียนด้วย Facebook
                                        </LoginButton>
                                        {/* </FacebookProvider> */}
                                    </Col>
                                </Row>
                                <Row justify="center" className="margin-top-20">
                                <Col xs={24} sm={24} md={24} lg={24} xl={24} className="text-center">
                                        <Button shape="round" className="btn-login" onClick={() => googleLogin()}>
                                            <span>
                                                <img src="/img/google.svg" width="32" /> ลงทะเบียนด้วย Google
                                            </span>
                                        </Button>
                                    </Col>
                                </Row>
                                {/* <Row justify="center" style={{ marginTop: "30px" }}>
                                    <Col span={12} className="text-center">
                                        <span>
                                            หรือ
                                        </span>
                                    </Col>
                                </Row>
                                <Row justify="center" className="margin-top-20">
                                    <Col span={12} className="text-center">
                                        <Row>
                                            <Col span={24}>
                                                <span className="label-register">
                                                    ลงทะเบียนด้วยอีเมล
                                                </span>
                                            </Col>
                                            <Col span={24}>
                                                <div className="ant-form ant-form-vertical">
                                                    <Form.Item label={"อีเมล"}   >
                                                        <Input placeholder={"อีเมล"} />
                                                    </Form.Item>
                                                </div>
                                            </Col>
                                        </Row>
                                    </Col>

                                </Row>
                                <Row justify="center" className="margin-top-20" gutter={[16, 16]}>
                                    <Col span={24} className="text-center">
                                        <Button shape="round" type="primary" style={{ width: "120px", height: "40px" }} onClick={() => {
                                            router.push("/home")
                                        }}>
                                            ลงทะเบียน
                                        </Button>
                                    </Col>
                                    <Col span={24}>
                                        <span>
                                            มีบัญชีแล้วหรือยัง ? <a href="/">ลงชื่อเข้าใช้</a>
                                        </span>
                                    </Col>

                                </Row> */}
                            </Col>
                        </Row>
                    </div>
                </Content>
                <LayoutFooter />
            </Layout>

            <Modal
                visible={visible}
                // onOk={() => {
                //     setVisible(false)
                // }}
                onCancel={() => {
                    setVisible(false)
                }}
                footer={null}
            // forceRender={false}
            >
                <Row justify="space-around" align="middle" style={{ height: "240px" }}>
                    <Col span={12} className="text-center">
                        <p>เลือกประเภทบัญชี</p>
                        <Select size={"large"} defaultValue="employer" onChange={handleChange} style={{ width: 200 }}>
                            <Option key={"employer"}>ผู้จ้างงาน</Option>
                            <Option key={"pilot"}>นักบินโดรน</Option>
                        </Select>
                        <Button shape="round" type="primary" size="large" onClick={fetchRegister} style={{ marginTop: "48px" }}>
                            ตกลง
                        </Button>
                    </Col>
                </Row>
            </Modal>
        </>
    )
};

export default Wrapper