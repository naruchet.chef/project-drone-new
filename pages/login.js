import React, { useState, useEffect } from "react";
import { Layout, Row, Col, Spin, Input, Button, Modal, Select, Space, Checkbox } from 'antd';
import { useRouter } from 'next/router'
import { GoogleOAuthProvider, useGoogleLogin } from '@react-oauth/google';
import { FacebookProvider, LoginButton } from 'react-facebook';
import { get } from 'lodash'

const { Option } = Select;
import LayoutHeader from '../components/layout/header'
import LayoutFooter from '../components/layout/footer'

import { socialLogin, register } from "../services/authentication.service";
import { findPilotByAccountId } from "../services/pilot.service";
import { findCustomerByAccountId } from "../services/customer.service";


const { Content, Footer } = Layout;

const Wrapper = React.forwardRef((props, ref) => {
  return (
    <GoogleOAuthProvider
      clientId="378771911273-9jv0grs96nb9noul4mf9a2i881rqkkko.apps.googleusercontent.com"
      onScriptLoadSuccess={(cc) => { console.log("onScriptLoadSuccess --> ", cc) }}
      onScriptLoadError={(cc) => { console.log("onScriptLoadError --> ", cc) }}
    >
      <FacebookProvider appId="5915063525218549">
        <PageLogin {...props} />
      </FacebookProvider>
    </GoogleOAuthProvider>
  )
})

Wrapper.displayName = "GoogleOAuthProvider"

const PageLogin = () => {
  const router = useRouter()
  const [visible, setVisible] = useState(false);
  const [accountType, setAccountType] = useState("employer");
  const [selectAccountType, setSelectAccountType] = useState(false);
  const [requestToken, setRequestToken] = useState({})
  const [isLoading, setIsLoading] = useState(false);
  const [disableBottom, setDisableBottom] = useState(true);
  const [acceptTerm, setAcceptTerm] = useState(false);

  useEffect(() => {
    clearLogin()
  }, []);

  const googleLogin = useGoogleLogin({
    onSuccess: async (tokenResponse) => {
      console.log(tokenResponse)
      let body = {
        token: tokenResponse.access_token,
        social: "google",
        userId: ""
      }
      await handleSocialLogin(body)
    },
  });

  const handleSocialLogin = async (body) => {
    // setVisible(true)
    setIsLoading(true)
    setRequestToken(body)
    let data = await fetchTokenLogin(body)
    if (data) {
      return null
    }
    setSelectAccountType(false)
    setAcceptTerm(false)
    setVisible(true)
    return null

  }

  const fetchTokenLogin = async (data) => {
    const { result, success } = await socialLogin(data);
    if (success) {
      console.log(`result`, result);

      setLogin(result.data, "login")
      return result.data
      // const { data = [] } = result;
      // setJobData(data);
      // setIsLoading(false);
      // localStorage.setItem('token', result.data)
      // localStorage.setItem('account_type', accountType)
      // router.push("/home")
    }
    // setVisible(false)
  }

  const submitAccountType = () => {

  }

  const fetchRegister = async () => {
    setVisible(false)
    setSelectAccountType(false)
    setAcceptTerm(false)
    const { result, success } = await register({ ...requestToken, account_type: accountType, accept_term: acceptTerm });
    if (success) {
      console.log(`result`, result);
      // const { data = [] } = result;
      // setJobData(data);
      // setIsLoading(false);
      setLogin(result.data)

    }
  }

  const handleScroll = (e) => {
    const _bottom = e.target.scrollHeight - e.target.scrollTop === e.target.clientHeight;
    if (!_bottom != disableBottom) {
      setDisableBottom(!_bottom)
    }
  };

  const setLogin = async (data, type) => {
    localStorage.setItem('token', data.token)
    localStorage.setItem('account_type', data.account_type)
    localStorage.setItem('account_name', data.account_name)
    localStorage.setItem('account_id', data.account_id)
    const filter = {
      account_id: data.account_id,
    };
    if (data.account_type == "pilot") {
      const { result, success } = await findPilotByAccountId(filter);
      if (success) {
        localStorage.setItem('profile_id', result.data.id)
        if (type === "login") {
          router.push("/")
        } else {
          router.push("/pilot/manage/" + result.data.id)
        }
      }
    } else {
      const { result, success } = await findCustomerByAccountId(filter);
      if (success) {
        localStorage.setItem('profile_id', result.data.id)
        if (type === "login") {
          router.push("/")
        } else {
          router.push("/customer/manage/" + result.data.id)
        }
      }
    }
  }

  const clearLogin = () => {
    localStorage.removeItem("token")
    localStorage.removeItem("account_type")
    localStorage.removeItem("account_name")
    localStorage.removeItem("profile_id")
  }

  const handleFacebookLogin = async (data) => {
    setIsLoading(true)
    console.log(data);
    let body = {
      token: get(data, "tokenDetail.accessToken"),
      social: "facebook",
      account_id: get(data, "tokenDetail.userID"),
      email: get(data, "profile.email"),
      account_name: get(data, "profile.name"),
      first_name: get(data, "profile.first_name"),
      last_name: get(data, "profile.last_name")
    }
    await handleSocialLogin(body)
  }

  const handleError = (error) => {
    console.log(error);
    // this.setState({ error });
  }

  const handleChange = (value) => {
    setAccountType(value)
  };

  const policy_employer = () => {
    return (
      <Row>
        <Col span={24}>
          <div>
            <h3 className="text-center roboto-front-bold-24px"><b> Policy การสมัครสมาชิก แพลตฟอร์ม บพข</b></h3>
            <Space />
            <div style={{
              border: "1px solid rgba(0, 0, 0, 0.54)",
              borderRadius: "15px",
              height: "500px",
              overflow: "scroll",
              padding: "12px"
            }}
              onScroll={handleScroll}
            >
              {/* <Space direction="vertical" size="small" style={{ display: 'flex', margin: "12px", overflow: "scroll" }}> */}
              <b className="roboto-front-bold-12px">
                ข้อตกลง ผู้ว่าจ้าง (คนโพสต์งาน)
              </b>
              <p className="roboto-front-12px">
                เงื่อนไขข้อตกลงการใช้งานนี้ (&quot;ข้อตกลงการใช้งาน&quot;) มีผลใช้บังคับตั้งแต่วันที่ 14 มิถุนายน 2565 เป็นต้นไป จนกว่าจะมีการประกาศแก้ไข เปลี่ยนแปลง เพิ่มเติม หรือมีประกาศเงื่อนไขฉบับใหม่โดยมหาวิทยาลัยเทคโนโลยีสุรนารี
              </p>

              <b className="roboto-front-bold-12px">
                คําจํากัดความ
              </b>
              <br />
              <b className="roboto-front-12px" style={{ textDecoration: "underline" }}>ผู้ว่าจ้างคือผู้</b>
              <p className="roboto-front-12px">
                ผู้ใช้ที่จ้างงานและรับมอบงานผ่านเว็บไซต์ซึ่งตกลงยอมรับตามข้อกําหนดการใช้งาน
              </p>

              <b className="roboto-front-12px" style={{ textDecoration: "underline" }}>ผู้ให้บริการ</b>
              <p className="roboto-front-12px">
                คือผู้ใช้งานที่ประกาศรับทําางานและจะส่งมอบงานผ่านเว็บไซต์ซึ่งตกลงยอมรับ
                ตามข้อกําหนดการใช้งาน คือ รูปภาพ  วีดีโอหรือผลลัพธ์จากการทําางาน ที่มีการจ้างงาน ผ่านแพลตฟอร์ม
              </p>

              <b className="roboto-front-12px" style={{ textDecoration: "underline" }}>ผู้ใช้งาน</b>
              <p className="roboto-front-12px">
                คือผู้ที่เข้าใช้บริการแพลตฟอร์มทั้งหมด

              </p>
              <b className="roboto-front-bold-12px">
                ข้อตกลงการใช้งานแพลตฟอร์ม
              </b>
              <p className="roboto-front-12px">
                1.ผู้ใช้ยืนยันว่าข้อมูลส่วนตัว รูปภาพ และเอกสารที่อัปโหลดเข้าสู่ระบบเป็นของผู้ใช้และ
                เป็นข้อมูลจริง หากทางแพลตฟอร์มพบว่ามีการใช้ข้อมูลที่ไม่ตรงกับความจริงหรือมี
                การใช้ข้อมูลของบุคคลอื่น ทางแพลตฟอร์มมีสิทธิ์ในการพิจารณาระงับการใช้งานของ
                ผู้ใช้ชั่วคราว หรือถาวรได้
              </p>
              <p className="roboto-front-12px">
                2.สําหรับงานที่โพสต์ผู้ว่าจ้างจําเป็นต้องให้ข้อมูลที่เป็นความจริงและครบถ้วนแก่ผู้ให้
                บริการ  เพื่อให้ผู้ให้บริการสามารถประเมินราคาได้อย่างถูกต้อง หากพบว่าผู้ว่าจ้างให้
                ข้อมูลที่ไม่ตรงหรือบิดเบือนความเป็นจริง ทางแพลตฟอร์มอาจมีการส่งข้อความตัก
                เตือนไปทางอีเมล หรือพิจารณาระงับการใช้งานของผู้ใช้ชั่วคราว หรือถาวรได้ และ
                ทางแพลตฟอร์มจะไม่รับผิดชอบความเสียหายใดๆ อันเป็นเหตุมาจากการกระทําาดัง
                กล่าวในทุกกรณี
              </p>
              <p className="roboto-front-12px">
                3.ผู้ว่าจ้างต้องตรวจสอบงานที่ผู้ให้บริการอัปโหลดเพื่อส่งงานภายใน 7 วัน ทางผู้ให้
                บริการมีสิทธิ์ส่งเรื่องรายงานและดําาเนินการตามกฎหมายต่อไป โดยทางแพลตฟอร์มมี
                สิทธิ์ในการเปิดเผยข้อมูลของผู้ว่าจ้างให้กับผู้ให้บริการและหน่วยงานที่เข้ามามีส่วน
                ร่วม เพื่อให้สามารถดําาเนินการทางกฎหมายต่อได้
              </p>
              <p className="roboto-front-12px">
                4.หลังจากผู้ว่าจ้างตรวจสอบและยอมรับว่างานทั ้งหมดที่ลูกค้าว่าจ้างได้รับจากผู้ให้
                บริการถูกต้องตามข้อตกลง ผู้ว่าจ้างต้องชําาระเงินให้กับผู้ให้บริการ ภายใน 48 ชั่วโมง
                โดยกระทําาตามขั ้นตอนที่ทางแพลตฟอร์มกําาหนดไว้ หากมีการโอนเงินนอกเหนือจาก
                ช่องทางที่แพลตฟอร์มกําาหนด ทางแพลตฟอร์มจะไม่รับผิดชอบต่อความเสียหายในทุก
                กรณี และหากผู้ว่าจ้างไม่ชําาระเงินตามกําาหนด ทางแพลตฟอร์มอาจพิจารณาระงับการ
                ใช้งานของผู้ใช้ชั่วคราว หรือถาวรได้ และทางแพลตฟอร์มมีสิทธิ์ให้ความร่วมมือใน
                การเปิดเผยข้อมูลของผู้ว่าจ้างให้กับผู้ให้บริการและหน่วยงานที่เข้ามามีส่วนร่วม เพื่อ
                ให้สามารถดําาเนินการทางกฎหมายต่อได้
              </p>
              <p className="roboto-front-12px">
                5.ผู้ว่าจ้างจะต้องติดต่อกับทางผู้ให้บริการตามช่องทางที่แพลตฟอร์มกําาหนดเท่านั ้น หาก
                มีการติดต่อในช่องทางที่แพลตฟอร์มไม่ได้กําาหนดไว้ทางแพลตฟอร์มมีสิทธิ์ที่จะ
                พิจารณาระงับการใช้งานของผู้ใช้ชั่วคราว หรือถาวร
              </p>
              <p className="roboto-front-12px">
                6.ผู้ใช้จะต้องไม่แจ้งความจําานง หรือชําาระเงินในช่องทางอื่นที่ไม่ได้นําาหลักฐานอัปโหลด
                เข้าสู่แพลตฟอร์ม หากมีการตรวจพบทางแพลตฟอร์มจะทําการระงับการใช้งานถาวร และจะไม่รับผิดชอบต่อความเสียหายที่ผู้ใช้ได้รับจากการกระทําาดังกล่าวเนื่องจากไม่
                ได้มีการนําหลักฐานการชําระเงิน เข้าสู่แพลตฟอร์ม ผู้ใช้จะต้องไม่กระทําาการใดๆ เพื่อ
                เป็นการชักจูง หรือจูงใจให้นําไปสู่การติดต่อ เพื่อจ้างงานนอกเว็บไซต์
              </p>
              <p className="roboto-front-12px">
                7.แพลตฟอร์มมีสิทธิ์เด็ดขาดแต่เพียงผู้เดียวในการใช้ดุลพินิจระงับการใช้งานบัญชี
                ชั่วคราวหรือถาวร ยกเลิกบัญชี ลบการประกาศงานดังกล่าว หรืองานอื่นๆ ยกเลิกงานที่
                มีการจ้างงาน และการอื่นใดที่ทางแพลตฟอร์มเห็นว่าเหมาะสม หากแพลตฟอร์มพบว่า
                ผู้ใช้งานคนเดียวกันมีบัญชีมากกว่าหนึ่งบัญชี หรือผู้ใช้งานใช้ชื่อและรายละเอียดส่วน
                ตัวของบุคคลอื่นมาสมัครใช้งาน หรือพบว่ามีการซื้อขายบัญชี ทั้งนี้ทางแพลตฟอร์มมี
                สิทธิ์ดําเนินการกับบัญชีที่เกี่ยวข้องทั้งหมดได้
              </p>
              <p className="roboto-front-12px">
                8.ผู้ใช้งานยินยอมชดใช้ค่าเสียหาย รวมถึงค่าทนายความและค่าใช้จ่ายในการดําาเนินการ
                ต่างๆ ทางคดี ค่าปรับ ค่าสินไหมทดแทน ความสูญหาย ความเสียหายและหนี้สินใดๆ
                อันเกิดจากการอ้างสิทธิ์ การเรียกร้อง หรือการกระทําาอื่นใดที่แพลตฟอร์มได้รับอัน
                เป็นผลมาจากการใช้งานที่ไม่ถูกต้องหรือผิดกฎหมาย การผิดข้อตกลงการใช้งานของผู้
                ใช้งาน
              </p>
              <p className="roboto-front-12px">
                9.การยกเลิกงานผู้ใช้สามารถทําาได้จนกระทั่ง 7 วัน ก่อนถึงวันปฏิบัติงาน หากถึงเวลาดัง
                กล่าวทางผู้ใช้งานจะไม่สามารถยกเลิกงานได้หรือหากเป็นการจ้างงานแบบเร่งด่วนมี
                เวลาก่อนปฏิบัติงานไม่ถึง 7 วัน ผู้ใช้จะไม่สามารถยกเลิกงานได้ แต่หากมีเหตุจําาเป็น
                ทางผู้ใช้ที่ยกเลิกจะต้องเสียค่าเสียหายให้กับผู้ใช้คู่สัญญาตามความเหมาะสม
              </p>
              <p className="roboto-front-12px">
                10.ทางแพลตฟอร์ม ไม่ได้มีความเกี่ยวข้องกับทางผู้ให้บริการ โดยทางแพลตฟอร์ม เป็นเพียงสื่อกลางในการจับคู่การให้บริการข้อมูลทางอากาศเท่านั้น
                สําหรับเงินที่ได้รับเข้าสู่ แพลตฟอร์ม ทางผู้ใช้ชําาระเงินเพื่อเป็นค่าดําเนินการและ
                บริหารจัดการแพลตฟอร์มเท่านั้น ทางแพลตฟอร์มจะไม่รับผิดชอบในคุณภาพงาน
                การส่งงานล่าช้า ของผู้ให้บริการ เนื่องด้วยทางแพลตฟอร์มได้มีระบบรีวิว และผลงาน
                นักบินให้แก่ผู้ว่าจ้างได้ตัดสินใจก่อนแล้ว
              </p>
              <p className="roboto-front-12px">
                11.กรรมสิทธิ์ผลงานข้อมูลทั ้งหมดที่ถูกว่าจ้างผ่านแพลตฟอร์ม จะเป็นของผู้ว่าจ้าง
                ยกเว้นกรณีที่ ผู้ให้บริการจะทําาข้อตกลงในส่วนของกรรมสิทธิ์ กับทางผู้ว่าจ้าง
                กรรมสิทธิ์ดังกล่าว จึงจะเป็นไปตามข้อตกลงที่ทั้งสองฝ่ายเห็นตรงกัน
              </p>
              <p className="roboto-front-12px">
                12.ทางแพลตฟอร์มสามารถลบงานได้ หากงานดังกล่าวเป็นงานบริการที่ผิด
                กฎหมาย หรือขัดต่อความสงบเรียบร้อยหรือศีลธรรมอันดีของประชาชน หรือละเมิด
                สิทธิของบุคคลอื่นๆ หรือละเมิดลิขสิทธิ์ หรือทรัพย์สินทางปั ญญาของบุคคลอื่น งาน
                บริการที่เข้าข่ายสื่อลามกอนาจาร หรือเป็นงานที่แพลตฟอร์มเห็นว่าไม่เหมาะสมรวมถึง
                งานบริการที่เป็นสแปม หรือที่คาดได้ว่าเป็นงานบริการที่หลอกลวงผู้ว่าจ้าง หรือละเมิด
                ข้อตกลงต่างๆของบุคคลที่สาม
              </p>
              <p className="roboto-front-12px">
                13.แพลตฟอร์มมีสิทธิ์ที่จะนําาเนื้อหาต่างๆที่ผู้ใช้งานอัปโหลดขึ้นมาสู่ระบบเว็บไซต์
                เช่น ข้อความ รูปภาพ วีดีโอ ชื่อบัญชี หรือข้อมูลอื่นใดรวมถึงผลงานที่มีการส่งมอบบน
                เว็บไซต์แพลตฟอร์มไป ใช้สําหรับการโฆษณา การประชาสัมพันธ์ทางการตลาดเพื่อ
                ประโยชน์ของแพลตฟอร์มได้
              </p>
              <p className="roboto-front-12px">
                14.กรณีมีการทุจริตด้านการโอนเงิน หรือการโอนเงินล่าช้า ทางแพลตฟอร์มจะไม่
                รับผิดชอบต่อจําานวนเงินที่ค้างชําาระรวมถึงไม่สามารถเร่งรัดการชําาระหนี้หรือดําาเนิน
                การทางกฎหมายให้ได้ในทุกกรณี แต่ทางผู้ใช้สามารถแจ้งคําร้อง เพื่อขอข้อมูลในการดําเนินคดี ตามกฎหมายได้ด้วยตนเอง ทางแพลตฟอร์มยินดีให้ข้อมูลที่เป็นประโยชน์
                ต่อการติดตามชําระหนี้
              </p>
              <b className="roboto-front-bold-12px">
                นโยบายความเป็นส่วนตัว
              </b>
              <p className="roboto-front-12px">
                ทางแพลตฟอร์มของเราให้ความสําาคัญแก่ความปลอดภัยของข้อมูลของท่าน ท่าน
                สามารถอ่านนโยบายความเป็นส่วนตัวได้ที่ xxxxxxxxxxxxxx ซึ่งนโยบายความเป็นส่วนตัว
                นี้ถือเป็นส่วนหนึ่งของข้อตกลงการใช้งานด้วย
                เพื่อปกป้องความเป็นส่วนตัวของผู้ใช้ ข้อมูลผู้ใช้ที่ใช้สําาหรับการระบุตัวบุคคลของผู้ใช้จะถูก
                เก็บเป็นความลับ และจะถูกเปิดเผยแก่บุคคลภายนอกเฉพาะในกรณีที่ผู้ขอให้เปิดเผยนั ้นมี
                อําานาจตามกฎหมายที่จะสามารถขอให้ทางแพลตฟอร์มเปิดเผยข้อมูลได้ หรือได้รับการ
                ยินยอมจากผู้ใช้งานซึ่งเป็นเจ้าของข้อมูล
                ทางแพลตฟอร์มจะไม่มีการเก็บข้อมูลบัตรเครดิตของผู้ใช้เพื่อป้องกันการโกง หรือการ
                ถูกขโมยข้อมูล สามารถเก็บข้อมูลบัตรเครดิต
                ทางแพลตฟอร์มขอสงวนสิทธิ์ในการเข้าถึงและตรวจสอบข้อมูลส่วนบุคคลของผู้ใช้
                งาน รวมถึงการสื่อสารระหว่างผู้ใช้งานที่สื่อสารผ่านระบบของเว็บไซต์ได้ แต่ทางแพลตฟอร์ม
                จะเข้าถึงและตรวจสอบข้อมูลทั ้งหมดดังกล่าวซึ่งถือเป็นข้อมูลส่วนบุคคลของผู้ใช้งานเฉพาะ
                เพื่อวัตถุประสงค์ในการระงับข้อพิพาทระหว่างผู้ใช้งาน ตรวจสอบกรณีมีข้อสงสัยเรื่องการ
                ทุจริต เรื่องการกระทําาผิดข้อตกลงการใช้งาน หรือเพื่อวัตถุประสงค์อื่นตามที่กําาหนดใน
                นโยบายความเป็นส่วนตัว
              </p>
              {/* </Space> */}
            </div>
          </div>
        </Col>
      </Row>
    )
  }

  const policy_pilot = () => {
    return (
      <Row>
        <Col span={24}>
          <div>
            <h3 className="text-center roboto-front-bold-24px"><b> Policy การสมัครสมาชิก แพลตฟอร์ม บพข</b></h3>
            <Space />
            <div style={{
              border: "1px solid rgba(0, 0, 0, 0.54)",
              borderRadius: "15px",
              height: "500px",
              overflow: "scroll",
              padding: "12px"
            }}
              onScroll={handleScroll}
            >
              {/* <Space direction="vertical" size="small" style={{ display: 'flex', margin: "12px", overflow: "scroll" }}> */}
              <b className="roboto-front-bold-12px">
                ข้อตกลง ผู้ให้บริการ (นักบินโดรน)
              </b>
              <p className="roboto-front-12px">
                เงื่อนไขข้อตกลงการใช้งานนี้ (&quot;ข้อตกลงการใช้งาน&quot;) มีผลใช้บังคับตั ้งแต่วันที่ 14 มิถุนายน
                2565 เป็นต้นไป จนกว่าจะมีการประกาศแก้ไข เปลี่ยนแปลง เพิ่มเติม หรือมีประกาศเงื่อนไข
                ฉบับใหม่โดยมหาวิทยาลัยเทคโนโลยีสุรนารี
              </p>

              <b className="roboto-front-bold-12px">
                คําจํากัดความ
              </b>
              <br />
              <b className="roboto-front-12px" style={{ textDecoration: "underline" }}>ผู้ว่าจ้างคือผู้</b>
              <p className="roboto-front-12px">
                ผู้ใช้ที่จ้างงานและรับมอบงานผ่านเว็บไซต์ซึ่งตกลงยอมรับตามข้อกําหนดการใช้งาน
              </p>

              <b className="roboto-front-12px" style={{ textDecoration: "underline" }}>ผู้ให้บริการ</b>
              <p className="roboto-front-12px">
                คือผู้ใช้งานที่ประกาศรับทําางานและจะส่งมอบงานผ่านเว็บไซต์ซึ่งตกลงยอมรับ
                ตามข้อกําหนดการใช้งาน คือ รูปภาพ  วีดีโอหรือผลลัพธ์จากการทําางาน ที่มีการจ้างงาน ผ่านแพลตฟอร์ม
              </p>

              <b className="roboto-front-12px" style={{ textDecoration: "underline" }}>ผู้ใช้งาน</b>
              <p className="roboto-front-12px">
                คือผู้ที่เข้าใช้บริการแพลตฟอร์มทั้งหมด

              </p>
              <b className="roboto-front-bold-12px">
                ข้อตกลงการใช้งานแพลตฟอร์ม
              </b>
              <p className="roboto-front-12px">
                1.ผู้ใช้ยืนยันว่าข้อมูลส่วนตัว รูปภาพ และเอกสารที่อัปโหลดเข้าสู่ระบบเป็นของผู้ใช้และ
                เป็นข้อมูลจริง หากทางแพลตฟอร์มพบว่ามีการใช้ข้อมูลที่ไม่ตรงกับความจริงหรือมี
                การใช้ข้อมูลของบุคคลอื่น ทางแพลตฟอร์มมีสิทธิ์ในการพิจารณาระงับการใช้งานของ
                ผู้ใช้ชั่วคราว หรือถาวรได้
              </p>
              <p className="roboto-front-12px">
                2.ในส่วนผลงานภาพ และวีดีโอที่อัปโหลดขึ้นเป็นผลงาน ข้อมูลที่ใช้จะต้องไม่นําาผลงาน
                ของผู้อื่นมาใช้เพื่อแอบอ้างเป็นของตนเอง ไม่เป็นภาพที่ไม่เหมาะสม เช่นภาพความ
                รุนแรง อนาจาร หรือชี้นําาให้เกิดข้อพิพาทต่างๆ หากทางแพลตฟอร์มตรวจพบมีสิทธิ์ที่
                จะลบข้อมูลดังกล่าวออกจากระบบ และ ระงับการใช้งานของผู้ใช้ชั่วคราว หรือถาวรได้
              </p>
              <p className="roboto-front-12px">
                3.หลังจากการปฏิบัติงานเสร็จสิ้นผู้ให้บริการต้องอัปโหลดงานขึ้นสู่ Cloud Drive ที่ทาง
                แพลตฟอร์มกําาหนดและใส่ลิงค์ให้กับผู้ว่าจ้าง ผ่านแพลตฟอร์มตามระยะเวลาที่ตกลง
                ไว้ หากเลยระยะเวลาที่กําาหนด และมีผู้ว่าจ้างร้องเรียน ทางแพลตฟอร์มมีสิทธิ์ให้ความ
                ร่วมมือในการเปิดเผยข้อมูลของผู้ให้บริการให้กับผู้ว่าจ้างและหน่วยงานที่เข้ามามีส่วน
                ร่วม เพื่อให้สามารถดําาเนินการทางกฎหมายต่อได้ และสามารถพิจารณา ระงับการใช้
                งานของผู้ใช้ชั่วคราว หรือถาวรได้  และการกระทําาดังกล่าวอาจส่งผลกระทบต่อ
                คะแนนรีวิวที่จะส่งผลต่อเนื่องในอนาคต
              </p>
              <p className="roboto-front-12px">
                4.ผู้ให้บริการจะได้รับเงินจากงานที่ตกลงและอัปโหลดสลิปผ่านระบบเท่านั ้นหากมีการ
                รับเงินนอกเหนือจากช่องทางที่แพลตฟอร์มกําาหนด ทางแพลตฟอร์มจะไม่รับผิดชอบ
                ต่อความเสียหายในทุกกรณี
              </p>
              <p className="roboto-front-12px">
                5.ผู้ให้บริการจะต้องติดต่อกับทางผู้ว่างจ้างตามช่องทางที่แพลตฟอร์มกําาหนดเท่านั ้น
                หากมีการติดต่อในช่องทางที่แพลตฟอร์มไม่ได้กําาหนดไว้ทางแพลตฟอร์มมีสิทธิ์ที่จะ
                พิจารณาระงับการใช้งานของผู้ใช้ชั่วคราว หรือถาวร
              </p>
              <p className="roboto-front-12px">
                6.ผู้ใช้จะต้องไม่แจ้งความจําานง หรือเรียกร้องการชําาระเงินในช่องทางอื่นที่ไม่ได้นําาหลัก
                ฐานอัปโหลดเข้าสู่แพลตฟอร์ม หากมีการตรวจพบทางแพลตฟอร์มจะทําาการระงับการ
                ใช้งานถาวร และจะไม่รับผิดชอบต่อความเสียหายที่ผู้ใช้ได้รับจากการกระทําาดังกล่าว
                เนื่องจากไม่ได้มีการนําาหลักฐานการชําาระเงินเข้าสู่แพลตฟอร์ม ผู้ใช้จะต้องไม่กระทําา
                การใดๆ เพื่อเป็นการชักจูง หรือจูงใจให้นําาไปสู่การติดต่อเพื่อจ้างงานนอกเว็บไซต์
              </p>
              <p className="roboto-front-12px">
                7.แพลตฟอร์มมีสิทธิ์เด็ดขาดแต่เพียงผู้เดียวในการใช้ดุลพินิจระงับการใช้งานบัญชี
                ชั่วคราวหรือถาวร ยกเลิกบัญชี ลบการประกาศงานดังกล่าว หรืองานอื่นๆ ยกเลิกงานที่
                มีการจ้างงาน และการอื่นใดที่แพลตฟอร์มเห็นว่าเหมาะสม หากแพลตฟอร์มพบว่าผู้ใช้
                งานคนเดียวกันมีบัญชีมากกว่าหนึ่งบัญชี หรือผู้ใช้งานใช้ชื่อและรายละเอียดส่วนตัว
                ของบุคคลอื่นมาสมัครใช้งาน หรือพบว่ามีการซื้อขายบัญชี ทั ้งนี้ทางแพลตฟอร์มมีสิทธิ์
                ดําาเนินการกับบัญชีที่เกี่ยวข้องทั้งหมดได้
              </p>
              <p className="roboto-front-12px">
                8.ผู้ใช้งานยินยอมชดใช้ค่าเสียหาย รวมถึงค่าทนายความและค่าใช้จ่ายในการดําาเนินการ
                ต่างๆ ทางคดี ค่าปรับ ค่าสินไหมทดแทน ความสูญหาย ความเสียหายและหนี้สินใดๆ
                อันเกิดจากการอ้างสิทธิ์ การเรียกร้อง หรือการกระทําาอื่นใดที่แพลตฟอร์มได้รับอัน
                เป็นผลมาจากการใช้งานที่ไม่ถูกต้องหรือผิดกฎหมาย การผิดข้อตกลงการใช้งานของผู้
                ใช้งาน
              </p>
              <p className="roboto-front-12px">
                9.การยกเลิกงานผู้ใช้สามารถทําาได้จนกระทั่ง 7 วัน ก่อนถึงวันปฏิบัติงาน หากถึงเวลาดัง
                กล่าวทางผู้ใช้งานจะไม่สามารถยกเลิกงานได้หรือหากเป็นการจ้างงานแบบเร่งด่วนมี
                เวลาก่อนปฏิบัติงานไม่ถึง 7 วัน ผู้ใช้จะไม่สามารถยกเลิกงานได้ แต่หากมีเหตุจําาเป็น
                ทางผู้ใช้ที่ยกเลิกจะต้องเสียค่าเสียหายให้กับผู้ใช้คู่สัญญาตามความเหมาะสม
              </p>
              <p className="roboto-front-12px">
                10.ทางแพลตฟอร์ม ไม่ได้มีความเกี่ยวข้องกับทางผู้ให้บริการ โดยทาง
                แพลตฟอร์มเป็นเพียงสื่อกลางในการจับคู่การให้บริการข้อมูลทางอากาศเท่านั ้น
                สําาหรับเงินที่ได้รับเข้าสู่แพลตฟอร์ม ทางผู้ใช้ชําาระเงินเพื่อเป็นค่าดําาเนินการและ
                บริหารจัดการแพลตฟอร์มเท่านั ้น ทางแพลตฟอร์มจะไม่รับผิดชอบในคุณภาพงาน
                การส่งงานล่าช้า ของผู้ให้บริการ เนื่องด้วยทางแพลตฟอร์มได้มีระบบรีวิว และผลงาน
                นั กบินให้แก่ผู้ว่าจ้างได้ตัดสินใจก่อนแล้ว
              </p>
              <p className="roboto-front-12px">
                11.กรรมสิทธิ์ผลงานข้อมูลทั ้งหมดที่ถูกว่าจ้างผ่านแพลตฟอร์ม จะเป็นของผู้ว่าจ้าง
                ยกเว้นกรณีที่ผู้ให้บริการจะทําาข้อตกลงในส่วนของกรรมสิทธิ์ กับทางผู้ว่าจ้าง
                กรรมสิทธิ์ดังกล่าวจึงจะเป็นไปตามข้อตกลงที่ทั ้งสองฝ่ายเห็นตรงกัน
              </p>
              <p className="roboto-front-12px">
                12.ทางแพลตฟอร์มสามารถลบงานได้ หากงานดังกล่าวเป็นงานบริการที่ผิด
                กฎหมาย หรือขัดต่อความสงบเรียบร้อยหรือศีลธรรมอันดีของประชาชน หรือละเมิด
                สิทธิของบุคคลอื่นๆ หรือละเมิดลิขสิทธิ์ หรือทรัพย์สินทางปั ญญาของบุคคลอื่น งาน
                บริการที่เข้าข่ายสื่อลามกอนาจาร หรือเป็นงานที่แพลตฟอร์มเห็นว่าไม่เหมาะสมรวมถึง
                งานบริการที่เป็นสแปม หรือที่คาดได้ว่าเป็นงานบริการที่หลอกลวงผู้ว่าจ้าง หรือละเมิด
                ข้อตกลงต่างๆของบุคคลที่สาม
              </p>
              <p className="roboto-front-12px">
                13.แพลตฟอร์มมีสิทธิ์ที่จะนําาเนื้อหาต่างๆที่ผู้ใช้งานอัปโหลดขึ้นมาสู่ระบบเว็บไซต์
                เช่น ข้อความ รูปภาพ วีดีโอ ชื่อบัญชี หรือข้อมูลอื่นใดรวมถึงผลงานที่มีการส่งมอบบน
                เว็บไซต์ของแพลตฟอร์ม ไปใช้สําาหรับการโฆษณา การประชาสัมพันธ์ทางการตลาด
                เพื่อประโยชน์ของแพลตฟอร์มได้
              </p>
              <p className="roboto-front-12px">
                14.กรณีมีการทุจริตด้านการโอนเงิน หรือการโอนเงินล่าช้า ทางแพลตฟอร์มจะไม่
                รับผิดชอบต่อจําานวนเงินที่ค้างชําาระรวมถึงไม่สามารถเร่งรัดการชําาระหนี้หรือดําาเนิน
                การทางกฎหมายให้ได้ในทุกกรณี แต่ทางผู้ใช้สามารถแจ้งคําาร้องเพื่อขอข้อมูลในการ
                ดําาเนินคดีตามกฎหมายได้ด้วยตนเอง ทางแพลตฟอร์มยินดีให้ข้อมูลที่เป็นประโยชน์
                ต่อการติดตามชําาระหนี้
              </p>
              <b className="roboto-front-bold-12px">
                นโยบายความเป็นส่วนตัว
              </b>
              <p className="roboto-front-12px">
                ทางแพลตฟอร์มของเราให้ความสําาคัญแก่ความปลอดภัยของข้อมูลของท่าน ท่าน
                สามารถอ่านนโยบายความเป็นส่วนตัวได้ที่ xxxxxxxxxxxxxx ซึ่งนโยบายความเป็นส่วนตัว
                นี้ถือเป็นส่วนหนึ่งของข้อตกลงการใช้งานด้วย
                เพื่อปกป้องความเป็นส่วนตัวของผู้ใช้ ข้อมูลผู้ใช้ที่ใช้สําาหรับการระบุตัวบุคคลของผู้ใช้จะถูก
                เก็บเป็นความลับ และจะถูกเปิดเผยแก่บุคคลภายนอกเฉพาะในกรณีที่ผู้ขอให้เปิดเผยนั ้นมี
                อําานาจตามกฎหมายที่จะสามารถขอให้ทางแพลตฟอร์มเปิดเผยข้อมูลได้ หรือได้รับการ
                ยินยอมจากผู้ใช้งานซึ่งเป็นเจ้าของข้อมูล
                ทางแพลตฟอร์มจะไม่มีการเก็บข้อมูลบัตรเครดิตของผู้ใช้เพื่อป้องกันการโกง หรือการ
                ถูกขโมยข้อมูล สามารถเก็บข้อมูลบัตรเครดิต
                ทางแพลตฟอร์มขอสงวนสิทธิ์ในการเข้าถึงและตรวจสอบข้อมูลส่วนบุคคลของผู้ใช้
                งาน รวมถึงการสื่อสารระหว่างผู้ใช้งานที่สื่อสารผ่านระบบของเว็บไซต์ได้ แต่ทางแพลตฟอร์ม
                จะเข้าถึงและตรวจสอบข้อมูลทั ้งหมดดังกล่าวซึ่งถือเป็นข้อมูลส่วนบุคคลของผู้ใช้งานเฉพาะ
                เพื่อวัตถุประสงค์ในการระงับข้อพิพาทระหว่างผู้ใช้งาน ตรวจสอบกรณีมีข้อสงสัยเรื่องการ
                ทุจริต เรื่องการกระทําาผิดข้อตกลงการใช้งาน หรือเพื่อวัตถุประสงค์อื่นตามที่กําาหนดใน
                นโยบายความเป็นส่วนตัว
              </p>
              {/* </Space> */}
            </div>
          </div>
        </Col>
      </Row>
    )
  }

  return (
    <>
      <Spin spinning={isLoading}>
        <Layout className="layout">
          {/* <LayoutHeader isLogin={false} show_menu={false}/> */}
          <LayoutHeader authen={false} />
          <Content
            className="main-layout"
            style={{
              padding: "64px 0px",
            }}
          >
            <div className='main-wrapper margin-top-30' >
              <Row justify="center" className="margin-top-50" >
                <Col span={8} className="text-center">
                  <Row justify="center" className="margin-top-50">
                  <Col span={24} className="text-center">
                      <span style={{
                        fontFamily: 'var(--font-family-roboto)',
                        fontStyle: 'normal',
                        fontWeight: 700,
                        fontSize: '22px',
                        lineHeight: '26px',
                        color: "rgba(0, 0, 0, 0.54)",
                      }}>
                        เข้าสู่ระบบ
                      </span>
                      <p style={{
                        marginTop: "10px",

                        fontFamily: 'Prompt',
                        fontStyle: "normal",
                        fontWeight: 400,
                        fontSize: "18px",
                        lineHeight: "21px",
                        /* identical to box height */

                        textAlign: "center",

                        /* text / secondary */

                        color: "rgba(0, 0, 0, 0.54)"
                      }}>
                        เข้าสู่ระบบด้วย Social
                      </p>
                    </Col>
                  </Row>
                  <Row justify="center" className="margin-top-20">
                    <Col xs={24} sm={24} md={24} lg={24} xl={24} className="text-center">
                      {/* <FacebookProvider appId="5915063525218549"> */}
                      <LoginButton
                        scope="email"
                        onCompleted={handleFacebookLogin}
                        autoLoad={true}
                        onError={handleError}
                        className="ant-btn ant-btn-round ant-btn-default btn-login"
                      >
                        <img src="/img/facebook.svg" width="32" />  เข้าสู่ระบบด้วย Facebook
                      </LoginButton>
                      {/* </FacebookProvider> */}
                    </Col>
                  </Row>
                  <Row justify="center" className="margin-top-20">
                    <Col xs={24} sm={24} md={24} lg={24} xl={24} className="text-center">
                      <Button shape="round" className="btn-login" onClick={() => googleLogin()}>
                        <span>
                          <img src="/img/google.svg" width="32" /> เข้าสู่ระบบด้วย Google
                        </span>
                      </Button>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </div>
          </Content>
          <LayoutFooter />
        </Layout>
      </Spin>
      <Modal
        visible={visible}
        // onOk={() => {
        //     setVisible(false)
        // }}
        onCancel={() => {
          setVisible(false)
          setSelectAccountType(false)
          setAcceptTerm(false)
          setIsLoading(false)
        }}
        footer={null}
      // forceRender={false}
      >
        {!selectAccountType ?
          <Row justify="space-around" align="middle" style={{ height: "240px" }}>
            <Col span={12} className="text-center">
              <p>เลือกประเภทบัญชี</p>
              <Select size={"large"} defaultValue="employer" onChange={handleChange} style={{ width: 200 }}>
                <Option key={"employer"}>ผู้จ้างงาน</Option>
                <Option key={"pilot"}>นักบินโดรน</Option>
              </Select>
              <Button shape="round" type="primary" size="large" onClick={() => { setSelectAccountType(true) }} style={{ marginTop: "48px" }}>
                ตกลง
              </Button>
            </Col>
          </Row>
          :
          <>
            {accountType == "employer" ? policy_employer() : policy_pilot()}
            <Space />
            <Row style={{ marginTop: "20px" }}>
              <Col span={24} className="text-center">
                <Checkbox onChange={() => { setAcceptTerm(!acceptTerm) }} disabled={disableBottom}>ยอมรับเงื่อนไข</Checkbox>
              </Col>
            </Row>
            <Row>
              <Col span={24} className="text-center">
                <Space size={"small"}>
                  <Button shape="round" danger size="large" onClick={() => {
                    setVisible(false)
                    setIsLoading(false)
                  }} style={{ marginTop: "48px" }}>
                    ยกเลิก
                  </Button>
                  <Button shape="round" primary size="large" onClick={fetchRegister} style={{ marginTop: "48px" }} disabled={!acceptTerm || disableBottom}>
                    ตกลง
                  </Button>
                </Space>
              </Col>
            </Row>
          </>}
      </Modal>
    </>
  )
};

export default Wrapper