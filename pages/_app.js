import "../styles/globals.css";
import "antd/dist/antd.css";
import { ModalContextProvider } from '../contexts/Modal'
import Head from 'next/head';

function MyApp({ Component, pageProps }) {
  return (
    <>
     <Head>
      <link rel="shortcut icon" href="/favicon_io/favicon.ico" />
      <title>SUT Platform Drone เครือข่ายนักบินโดรนเชิงพาณิชย์ที่ใหญ่ที่สุดในประเทศไทย</title>
    </Head>
    <ModalContextProvider>
      <Component {...pageProps} />;
    </ModalContextProvider>
    </>
  )

}

export default MyApp;
