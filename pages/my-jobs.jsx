import "antd/dist/antd.css";
import React, { useState, useEffect } from "react";
import LayoutHeader from "../components/layout/header";
import LayoutFooter from "../components/layout/footer";
import { Layout, Breadcrumb, Table, Tag, Space, Modal, Button } from "antd";
import { jobList, deleteJob } from "../services/job.service";
import { jobTypeId, status } from "../constants/common";
import {
  ExclamationCircleOutlined,
  DeleteOutlined,
  EditOutlined,
} from "@ant-design/icons";
import useFetchTable from "../hooks/useFetchTable";
const { confirm } = Modal;
import Link from "next/link";
import { get } from "lodash-es";

const { Content } = Layout;

export default function Jobs() {
  const profileId =
    typeof window !== "undefined" ? localStorage.getItem("profile_id") : null;
  useEffect(() => { }, []);
  const filterRequest = { profile_id: profileId };
  const requestApi = jobList;
  const {
    isLoading,
    dataTable,
    handelDataTableChange,
    handleFetchData,
    pagination,
  } = useFetchTable(requestApi, filterRequest);
  const handleDeleteJob = async (id) => {
    confirm({
      title: "แจ้งเตือน",
      icon: <ExclamationCircleOutlined />,
      content: "คุณต้องการลบงานของคุณใช่หรือไม่",
      okText: "ตกลง",
      cancelText: "ยกเลิก",
      async onOk() {
        const filter = {
          id: id,
        };
        const { result, success } = await deleteJob(filter);
        if (success) {
          await getJobData(id);
        }
      },
    });
  };

  const columns = [
    {
      title: "status",
      dataIndex: "status",
      render: (data) => {
        const { text, code } = status[data];
        return (
          <div style={{ textAlign: "center" }}>
            <Button style={{ background: code, borderRadius: "7px" }}>
              <p style={{ color: "white" }}>{text}</p>
            </Button>
          </div>
        );
      },
    },
    {
      title: "ชื่อโครงการ",
      dataIndex: "job_name",
      render: (text, row) => {
        return (
          <Link href={`/my-post/view/${row.id}`}>
            <a>{text}</a>
          </Link>
        );
      },
    },
    {
      title: "งบประมาณ",
      dataIndex: "budget_from",
    },
    {
      title: "รายละเอียดโครงการ",
      dataIndex: "job_detail",
    },
    {
      title: "ประเภทงาน",
      dataIndex: "job_type_id",
      render: (jobType) => {
        if (jobType > 0) {
          const { text, color } = jobTypeId[jobType];
          return (
            <>
              <Tag color={color} key={jobType}>
                {text}
              </Tag>
            </>
          );
        }
      },
    },
    {
      title: "จัดการ",
      key: "action",
      render: (text, record) => {
        const { id, status } = record;
        return (
          <>
            <Space>
              <Link href={`/my-post/view/${id}`}>
                <Button
                  icon={<EditOutlined />}
                  style={{ borderRadius: "10px" }}
                ></Button>
              </Link>
              <Button
                disabled={status == "find_pilot" ? false : true}
                icon={<DeleteOutlined />}
                style={{ borderRadius: "10px" }}
                onClick={() => handleDeleteJob(id)}
              ></Button>
            </Space>
          </>
        );
      },
    },
  ];
  return (
    <Layout className="layout">
      <LayoutHeader />
      <Content
        className="site-layout"
        style={{ padding: "0 50px", marginTop: 64 }}
      >
        <Breadcrumb style={{ margin: "16px 0" }}>
          <Breadcrumb.Item>หน้าหลัก</Breadcrumb.Item>
          <Breadcrumb.Item>งานของฉัน</Breadcrumb.Item>
          {/* <Breadcrumb.Item>App</Breadcrumb.Item> */}
        </Breadcrumb>
        <div
          className="site-layout-background"
          style={{ padding: 24, minHeight: 600 }}
        >
          <h2>งานของฉัน</h2>
          <Table
            rowKey="id"
            loading={isLoading}
            columns={columns}
            pagination={pagination}
            dataSource={dataTable}
            onChange={handelDataTableChange}
            scroll={{ x: '100%', y: '100%' }}
          />
        </div>
      </Content>
      <LayoutFooter />
    </Layout>
  );
}
