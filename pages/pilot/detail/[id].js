import "antd/dist/antd.css";
import React, { useState, useEffect } from "react";
import LayoutHeader from '../../../components/layout/header'
import LayoutFooter from '../../../components/layout/footer'
import { RatingStar } from '../../../components'
import { useRouter } from "next/router";
import { findOnePilot } from "../../../services/pilot.service";
import { pilotPortfolioList } from "../../../services/pilotPortfolio.service";
import { jobTypeId, sex } from "../../../constants/common"
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import {
  Layout,
  Row,
  Col,
  Typography,
  Space,
  Button,
  Table,
  Spin,
  Pagination,
  Image,
  Carousel
} from "antd";
const { Content } = Layout;
const { Title } = Typography;
export default function Detail() {
  const initialValues = {
    account_id: "",
    first_name: "",
    last_name: "",
    account_name: "",
    photo: "",
    password: "",
    company_name: "",
    tel: "",
    insurance: "",
    address: "",
    introduce_yourself: "",
    experience: "",
    license: false,
    working_day: "",
    drone_type: "",
    drone_model: "",
    bank_account: "",
    bank_account_name: "",
    bank_account_no: "",
    tax_payer: "",
    status: "",
    rating: 0,
    pilot_job_types: [],
    pilot_licenses: [],
    pilot_portfolios: []
  };

  const settings = {
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    initialSlide: 0,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          initialSlide: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };

  const router = useRouter();
  const { id } = router.query;
  const [isLoading, setIsLoading] = useState(false);
  const [pilotData, setPilotData] = useState(initialValues);
  const [showVedio, setShowVedio] = useState([]);
  const [pageVedio, setPageVedio] = useState(1);
  const [perPageVedio, setPerPageVedio] = useState(3);
  const [totalVedio, setTotalVedio] = useState(0);
  const [showPhoto, setShowPhoto] = useState([]);
  const [pagePhoto, setPagePhoto] = useState(1);
  const [perPagePhoto, setPerPagePhoto] = useState(3);
  const [totalPhoto, setTotalPhoto] = useState(0);

  useEffect(() => {
    if (id) {
      getPilotProfile(id);
      getPilotVedio(id)
      getPilotPhoto(id)
    }
  }, [id]);

  const getPilotVedio = async (postId) => {
    setIsLoading(true);
    const filter = {
      pilot_profile_id: postId,
      portfolio_type: "vedio",
      per_page: perPageVedio,
      page: pageVedio,
    };
    const { result, success } = await pilotPortfolioList(filter);
    console.log(result);
    if (success) {
      const { data = [], meta } = result;
      console.log(data)
      setTotalVedio(meta.total)
      setShowVedio(data);
      setIsLoading(false);
    }
  };


  const getPilotPhoto = async (postId) => {
    setIsLoading(true);
    const filter = {
      pilot_profile_id: postId,
      portfolio_type: "photo",
      per_page: perPagePhoto,
      page: pagePhoto,
    };
    const { result, success } = await pilotPortfolioList(filter);
    console.log(result);
    if (success) {
      const { data = [], meta } = result;
      console.log(data)
      setTotalPhoto(meta.total)
      setShowPhoto(data);
      setIsLoading(false);
    }
  };

  const handlePageVedioChange = async (page) => {
    setIsLoading(true);
    setPageVedio(page)
    const filter = {
      per_page: perPageVedio,
      page: page,
      pilot_profile_id: id,
      portfolio_type: "vedio",
    };
    const { result, success } = await pilotPortfolioList(filter);
    console.log(result);
    if (success) {
      const { data = [] } = result;
      setShowVedio(data);
    }
    setIsLoading(false);
  }


  const handlePagePhotoChange = async (page) => {
    setIsLoading(true);
    setPagePhoto(page)
    const filter = {
      per_page: perPagePhoto,
      page: page,
      pilot_profile_id: id,
      portfolio_type: "photo",
    };
    const { result, success } = await pilotPortfolioList(filter);
    console.log(result);
    if (success) {
      const { data = [] } = result;
      setShowPhoto(data);
    }
    setIsLoading(false);
  }

  const getPilotProfile = async (postId) => {
    setIsLoading(true);
    const filter = {
      id: postId,
    };
    const { result, success } = await findOnePilot(filter);
    console.log(result);
    if (success) {
      const { data = [] } = result;
      console.log(data)
      setPilotData(data);
      setIsLoading(false);
    }
  };

  const onClick = (values) => {
    router.push("/pilot/list");
  };

  const columns = [
    {
      title: 'ชื่อโครงการ',
      dataIndex: 'portfolio_name',
      key: 'portfolio_name',
    },
    {
      title: 'รายละเอียด',
      dataIndex: 'portfolio_detail',
      key: 'portfolio_detail',
    },
  ];

  const getImage = (url) => {
    console.log(url)
    if (url != null) {
      const image = `https://img.youtube.com/vi/${url.split('v=')[1]}/hqdefault.jpg`
      console.log(image);
      return image
    }
  }

  return (
    <Spin spinning={isLoading}>
      <Layout className="layout">
        <LayoutHeader />
        <Content
          className="site-layout"
          style={{ padding: "0 50px", marginTop: 64 }}
        >
          <div
            className="site-layout-background"
            style={{ padding: 24, minHeight: 380 }}
          >
            <Row gutter={[48, 16]} style={{ marginTop: "50px" }}>
              <Col className="gutter-row" span={16} offset={4}>
                <Row gutter={[48, 16]}>
                  <Col className="gutter-row" span={12}>
                    <img style={{ width: "100%", borderRadius: "10px" }} className="sut-platform-drone-abount-us-image" src={pilotData.photo != null ? ("/image/" + pilotData.photo) : "/img/image_error.png"} />
                    {
                      pilotData.pilot_licenses.length > 3 ? (
                        <>
                          <div>
                            <Slider style={{ marginTop: "10px" }} {...settings}>
                              {pilotData.pilot_licenses.map((v, k) => {
                                return (
                                  <div key={`license_${k}`}>
                                    <Button
                                      style={{
                                        width: "100%",
                                        borderRadius: "10px",
                                        background: k % 3 == 0 ? '#7BC67E' : k % 2 == 0 ? '#3397FF' : '#FF737F',
                                        border: '2px solid #FFFFFF',
                                        color: "white"
                                      }}
                                      type="default" primary
                                    >
                                      {v.license_no}
                                    </Button>
                                  </div>
                                )
                              })}
                            </Slider>
                          </div>
                        </>
                      ) : pilotData.pilot_licenses.length == 3 ? (
                        <Row gutter={[16, 16]} style={{ marginTop: "10px" }}>
                          <Col span={8}>
                            <Button
                              style={{
                                width: "100%",
                                borderRadius: "10px",
                                background: '#3397FF',
                                border: '2px solid #FFFFFF',
                                boxShadow: '0px 2px 20px rgba(0, 0, 0, 0.3)',
                                color: "white"
                              }}
                              type="default" primary
                            >
                              {pilotData.pilot_licenses[0].license_no}
                            </Button>
                          </Col>
                          <Col span={8}>
                            <Button
                              style={{
                                width: "100%",
                                borderRadius: "10px",
                                background: '#FF737F',
                                border: '2px solid #FFFFFF',
                                boxShadow: '0px 2px 20px rgba(0, 0, 0, 0.3)',
                                color: "white"
                              }}
                              type="default" primary
                            >
                              {pilotData.pilot_licenses[1].license_no}
                            </Button>
                          </Col>
                          <Col span={8}>
                            <Button
                              style={{
                                width: "100%",
                                borderRadius: "10px",
                                background: '#7BC67E',
                                border: '2px solid #FFFFFF',
                                boxShadow: '0px 2px 20px rgba(0, 0, 0, 0.3)',
                                color: "white"
                              }}
                              type="default" primary
                            >
                              {pilotData.pilot_licenses[2].license_no}
                            </Button>
                          </Col>
                        </Row>
                      ) : pilotData.pilot_licenses.length == 2 ? (
                        <Row gutter={[16, 16]} style={{ marginTop: "10px" }}>
                          <Col span={8}>
                            <Button
                              style={{
                                width: "100%",
                                borderRadius: "10px",
                                background: '#3397FF',
                                border: '2px solid #FFFFFF',
                                boxShadow: '0px 2px 20px rgba(0, 0, 0, 0.3)',
                                color: "white"
                              }}
                              type="default" primary
                            >
                              {pilotData.pilot_licenses[0].license_no}
                            </Button>
                          </Col>
                          <Col span={8}>
                            <Button
                              style={{
                                width: "100%",
                                borderRadius: "10px",
                                background: '#FF737F',
                                border: '2px solid #FFFFFF',
                                boxShadow: '0px 2px 20px rgba(0, 0, 0, 0.3)',
                                color: "white"
                              }}
                              type="default" primary
                            >
                              {pilotData.pilot_licenses[1].license_no}
                            </Button>
                          </Col>
                        </Row>
                      ) : pilotData.pilot_licenses.length == 1 ? (
                        <Row gutter={[16, 16]} style={{ marginTop: "10px" }}>
                          <Col span={8}>
                            <Button
                              style={{
                                width: "100%",
                                borderRadius: "10px",
                                background: '#3397FF',
                                border: '2px solid #FFFFFF',
                                boxShadow: '0px 2px 20px rgba(0, 0, 0, 0.3)',
                                color: "white"
                              }}
                              type="default" primary
                            >
                              {pilotData.pilot_licenses[0].license_no}
                            </Button>
                          </Col>
                        </Row>
                      ) : ("")
                    }

                  </Col>
                  <Col className="gutter-row" span={12}>
                    <Title style={{ color: "#7F7F7F" }} level={3}>
                      โปรไฟล์นักบิน
                    </Title>
                    {pilotData.first_name} {pilotData.last_name}<br />
                    เพศ {pilotData.sex != undefined ? sex[pilotData.sex].text : ""}<br />
                    {pilotData.address}<br />
                    ประเภทงาน {pilotData.pilot_job_types.map((v, k) => { if (k == (pilotData.pilot_job_types.length - 1)) { return jobTypeId[v.job_type_id].text } else { return jobTypeId[v.job_type_id].text + ", " } })}<br />
                    {pilotData.experience}<br />
                    คะแนนรีวิว <RatingStar rating={pilotData.rating} />
                  </Col>
                </Row>
              </Col >
              <Col className="gutter-row" span={16} offset={4}>
                <Title style={{ color: "#7F7F7F" }} level={5}>
                  รายละเอียดอื่นๆ
                </Title>
                {pilotData.introduce_yourself}
              </Col>
              <Col className="gutter-row" span={16} offset={4}>
                <Title style={{ color: "#7F7F7F" }} level={5}>
                  ตารางผลงาน
                </Title>
                <Table
                  columns={columns}
                  dataSource={pilotData.pilot_portfolios}
                  pagination={false}
                  scroll={{ x: '100%', y: '100%' }}
                />
              </Col>
              <Col className="gutter-row" span={16} offset={4}>
                <Title style={{ color: "#7F7F7F" }} level={5}>
                  วีดีโอผลงาน
                </Title>
                <Row style={{ marginTop: "10px" }}>
                  {
                    showVedio.map((v, k) => (
                      <Col key={`show_video_${k}`} xs={{ span: 5, offset: 1 }} lg={{ span: 6, offset: 2 }}>
                        <a href={v.portfolio_photo} rel="noreferrer" target="_blank">
                          <Image
                            style={{ width: "100%", height: "100%", borderRadius: "10px" }}
                            src={getImage(v.portfolio_photo)}
                            preview={{
                              visible: false
                            }}
                          />
                        </a>
                      </Col>
                    ))
                  }
                </Row>
                <div style={{ textAlign: "right", marginTop: "10px" }}>
                  {showVedio.length > 0 ? <Pagination
                    defaultCurrent={pageVedio}
                    page={pageVedio}
                    size="small"
                    pageSize={3}
                    total={totalVedio}
                    responsive={true}
                    showLessItems={true}
                    onChange={handlePageVedioChange}
                  /> : ""}
                </div>
              </Col>
              <Col className="gutter-row" span={16} offset={4}>
                <Title style={{ color: "#7F7F7F" }} level={5}>
                  รูปถ่ายผลงาน
                </Title>
                <Row style={{ marginTop: "10px" }}>
                  {
                    showPhoto.map((v, k) => (
                      <Col key={`show_photo_${k}`} xs={{ span: 5, offset: 1 }} lg={{ span: 6, offset: 2 }}>
                        <Image
                          style={{ width: "100%", height: "100%", borderRadius: "10px" }}
                          src={"/image/" + v.portfolio_photo}
                        />
                      </Col>
                    ))
                  }
                </Row>
                <div style={{ textAlign: "right", marginTop: "10px" }}>
                  {showPhoto.length > 0 ? <Pagination
                    defaultCurrent={pagePhoto}
                    page={pagePhoto}
                    size="small"
                    pageSize={3}
                    total={totalPhoto}
                    responsive={true}
                    showLessItems={true}
                    onChange={handlePagePhotoChange}
                  /> : ""}
                </div>
              </Col>
              <Col
                className="gutter-row" span={16}
                style={{ textAlign: "center", marginTop: '20px' }}
                offset={4}
              >
                <Space>
                  <Button
                    style={{ marginRight: "10px", borderRadius: "10px" }}
                    type="primary" htmlType="submit" primary
                    onClick={onClick}
                  >
                    กลับไปหน้าค้นหานักบิน
                  </Button>
                </Space>
              </Col>
            </Row >

          </div >
        </Content >
        <LayoutFooter />
      </Layout >
    </Spin >
  );
}
