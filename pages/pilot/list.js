
import "antd/dist/antd.css";
import React, { useState, useEffect } from "react";
import LayoutHeader from '../../components/layout/header'
import LayoutFooter from '../../components/layout/footer'
import { HeartOutlined, PushpinOutlined, StopOutlined } from '@ant-design/icons';
import Link from "next/link";
import CardProfile from '../../components/pilot/card-pilot-list'
import { pilotList } from "../../services/pilot.service";
import { PermissibleRender } from '@brainhubeu/react-permissible'
import {
  Layout,
  Breadcrumb,
  Input,
  Row,
  Card,
  Col,
  Select,
  Typography,
  Pagination,
  Spin,
} from "antd";
import { array } from "yup";

const { Content } = Layout;
const { Search } = Input;
const { Meta } = Card;
const { Option } = Select;
const { Title } = Typography;

export default function Pilot() {
  const [loading, setLoading] = useState(false);
  const [dataPilot, setDataPilot] = useState([]);
  const [totalData, setTotalData] = useState(0);
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(15);
  const [jobType, setJobType] = useState();



  useEffect(() => {
    const profileId = typeof window !== 'undefined' ? localStorage.getItem('profile_id') : null
    if (profileId) {
      const filter = {
        per_page: perPage,
        page: page,
        sort_by: "created_at",
        sort_type: "desc",
        account_id: profileId,
      };
      getPilotDataData(filter);
    }

  }, []);

  const getPilotDataData = async (filter) => {
    setLoading(true);
    const { result, success } = await pilotList(filter);
    if (success) {
      console.log(`result`, result);
      const { data = [], meta } = result;
      setTotalData(meta.total)
      setLoading(false);

      var perChunk = 5 // items per chunk    
      var inputArray = data

      var showResult = inputArray.reduce((resultArray, item, index) => {
        const chunkIndex = Math.floor(index / perChunk)

        if (!resultArray[chunkIndex]) {
          resultArray[chunkIndex] = [] // start a new chunk
        }

        resultArray[chunkIndex].push(item)

        return resultArray
      }, [])
      setDataPilot(showResult);
    }
  };

  const handlePageChange = async (page) => {
    setPage(page)
    const filter = {
      per_page: perPage,
      page: page,
      sort_by: "created_at",
      sort_type: "desc",
    };
    getPilotDataData(filter)
  }

  const onChangeJobType = (jobType) => {
    setJobType(jobType)
    const filter = {
      per_page: perPage,
      page: page,
      sort_by: "created_at",
      sort_type: "desc",
      job_type: jobType
    };
    getPilotDataData(filter)
  };

  const onChangeAddress = (address) => {
    const filter = {
      per_page: perPage,
      page: page,
      sort_by: "created_at",
      sort_type: "desc",
      job_type: jobType,
      address: address.target.value
    };
    getPilotDataData(filter)
  };

  const showData = () => {
    return (
      <>
        {dataPilot.map((v, k) => (
          <Row key={`job_${k}`} flex={5} style={{ marginTop: 20 }}>
            {
              v.map((i, j) => (
                < Col key={`job_item_${k}`} flex={1} > <CardProfile profile={i} /></Col>
              ))

            }
            {
              (5 - v.length != 0) ? (
                showEmptyData(5 - v.length).map((i, j) => (
                  < Col key={`job_item_${j}`} flex={1} style={{ width: '250px' }} ></Col>
                ))
              ) : ""

            }
          </Row>
        ))
        }
      </>
    )
  }

  const showEmptyData = (number) => {
    var y = []
    for (var i = 0; i < number; i++) {
      y.push("1")
    }
    return y
  }
  const requiredPermissions = "pilot"

  return (
    <Spin spinning={loading}>
      <Layout>
        <LayoutHeader />
        <Content
          className="site-layout"
          style={{ padding: "0 50px", marginTop: 64 }}
        >
          {/* <PermissibleRender
          userPermissions={}
          requiredPermissions={requiredPermissions}
          > */}
          <Breadcrumb style={{ margin: "16px 0", fontSize: "16px" }}>
            <Breadcrumb.Item>หน้าหลัก</Breadcrumb.Item>
            <Breadcrumb.Item>ค้นหานักบิน</Breadcrumb.Item>
          </Breadcrumb>

          <img src="../img/unsplash_pMFNnnODrKA.png" style={{ width: '100%' }} />
              <div style={{ marginTop: '10px' }}>
                <p className="roboto-bold-gray-24px">ค้นหานักบิน</p>
              </div>
          <div
            className="site-layout-background"
            style={{ padding: 24, minHeight: 380 }}
          >
            <Row>
              <Col span={12}>
                <Title level={5} style={{ color: "#7F7F7F" }}>สถานที่</Title>
                <Input onChange={onChangeAddress} style={{ width: "80%" }} placeholder="กรอกข้อมูลสถานที่" />
              </Col>
              <Col span={12}>
                <Title level={5} style={{ color: "#7F7F7F" }}>ประเภทงาน</Title>
                <Select
                  onChange={onChangeJobType}
                  style={{ width: "80%" }}
                  showSearch
                  placeholder="เลือกประเภทงาน"
                  optionFilterProp="children"
                >
                  <Option value={undefined}>ทั้งหมด</Option>
                  <Option value={1}>Cinematic Aerial Photography</Option>
                  <Option value={2}>Orthophoto</Option>
                  <Option value={3}>DSM/DTM</Option>
                  <Option value={4}>Point Cloud 3D Model</Option>
                  <Option value={5}>Contour</Option>
                  <Option value={6}>Index & Zonation Map</Option>
                  <Option value={7}>อื่น</Option>
                </Select>
              </Col>
              {/* <Col span={8}>
              <Space style={{ marginTop: "30px" }}>
                <Checkbox onChange={{}}>แสดงรัสมี</Checkbox>
                <Badge status="success" />5 กม.
                <Badge status="warning" />15 กม.
                <Badge status="error" />30 กม.
              </Space>
            </Col> */}
            </Row>
            {showData()}
            {dataPilot.length > 0 ? <Pagination
              style={{ textAlign: "end", marginTop: "20px" }}
              defaultCurrent={1}
              total={totalData}
              pageSize={15}
              onChange={handlePageChange}
              responsive={true}
              showLessItems={true}
            /> : ""}
          </div>
          {/* </PermissibleRender> */}
        </Content>
        <LayoutFooter />
      </Layout >
    </Spin>
  );
}
