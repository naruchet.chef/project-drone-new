import "antd/dist/antd.css";
import React, { useState, useEffect } from "react";
import LayoutHeader from "../../../components/layout/header";
import { useRouter } from "next/router";
import LayoutFooter from "../../../components/layout/footer";
import { findOnePilot } from "../../../services/pilot.service";
import ManageProfile from '../../../components/pilot/manage-profile'
import ManagePay from '../../../components/pilot/manage-pay'
import ManagePerformance from '../../../components/pilot/manage-performance'

import {
  Layout,
  Tabs,
  Spin,
} from "antd";
const { TabPane } = Tabs;
const { Content } = Layout;

export default function Manage() {
  const initialValues = {
    account_id: "",
    first_name: "",
    last_name: "",
    account_name: "",
    photo: "",
    password: "",
    company_name: "",
    tel: "",
    insurance: "",
    address: "",
    introduce_yourself: "",
    experience: "",
    license: false,
    working_day: "",
    drone_type: "",
    drone_model: "",
    bank_account: "",
    bank_account_name: "",
    bank_account_no: "",
    tax_payer: ""
  };
  const router = useRouter();
  const { id, tab } = router.query;
  const [isLoading, setIsLoading] = useState(false);
  const [pilotData, setPilotData] = useState(initialValues);
  const [tabIndex, setTabIndex] = useState("1");

  useEffect(() => {
    if (id) {
      getPilotProfile(id);
    }
  }, [id]);

  useEffect(() => {
    if (tab === "setting") {
      setTabIndex("1")
    } else if (tab === "upload") {
      setTabIndex("3")
    } else if (tab === "transactions") {
      setTabIndex("2")
    }
  }, [tab])

  const handelTabChange = (value) => {
    setTabIndex(value)
  }

  const getPilotProfile = async (postId) => {
    setIsLoading(true);
    const filter = {
      id: postId,
    };
    const { result, success } = await findOnePilot(filter);
    console.log(result);
    if (success) {
      const { data = [] } = result;
      setPilotData(data);
      setIsLoading(false);
    }
  };

  return (
    <Spin spinning={isLoading}>
      <Layout className="layout">
        <LayoutHeader />
        <Content
          className="site-layout"
          style={{ padding: "0 50px", marginTop: 64 }}
        >
          <div
            className="site-layout-background"
            style={{ padding: 24, minHeight: 380 }}
          >
            <Tabs tabBarStyle={{ color: '#689FF2' }} onChange={handelTabChange} activeKey={tabIndex} type="card">
              <TabPane tab="ตั้งค่าบัญชี" key="1">
                <ManageProfile pilotProfile={pilotData} />
              </TabPane>
              <TabPane tab="ข้อมูลนักบิน" key="2">
                <ManagePay pilotProfile={pilotData} />
              </TabPane>
              <TabPane tab="ผลงาน" key="3">
                <ManagePerformance pilotProfile={pilotData} />
              </TabPane>
            </Tabs>
          </div>
        </Content>
        <LayoutFooter />
      </Layout>
    </Spin>
  );
}
