import "antd/dist/antd.css";
import React, { useState } from "react";
import { Layout, Menu } from "antd";
import Link from "next/link";
import {
  ProfileOutlined,
  InfoCircleOutlined,
  SearchOutlined,
  ImportOutlined,
} from "@ant-design/icons";
const { Header } = Layout;

export default function LayoutHeader() {
  const [selected, setSelected] = useState("1");
  return (
    <>
      <Header style={{ position: "fixed", zIndex: 1000, width: "100%" }}>
        <div className="logo" />
        <div />
        {/* {JSON.stringify(selected)} */}
        <Menu theme="light" mode="horizontal" setSelected={selected}>
          <Menu.Item key="0" onClick={(e) => setSelected(e.key)}>
            <ProfileOutlined />
            &nbsp; <Link href="/">หน้าแรก</Link>
          </Menu.Item>
          <Menu.Item key="1" onClick={(e) => setSelected(e.key)}>
            <ProfileOutlined />
            &nbsp; <Link href="/my-jobs">งานของฉัน</Link>
          </Menu.Item>
          <Menu.Item key="2" onClick={(e) => setSelected(e.key)}>
            <InfoCircleOutlined />
            &nbsp;
            <Link href="/my-system">การใช้งานระบบ</Link>
          </Menu.Item>
          <Menu.Item key="3" onClick={(e) => setSelected(e.key)}>
            <ImportOutlined />
            &nbsp; <Link href="/my-post/create">โพสงาน</Link>
          </Menu.Item>
          <Menu.Item key="4" onClick={(e) => setSelected(e.key)}>
            <SearchOutlined />
            &nbsp; <Link href="/my-pilot">ค้นหานักบิน</Link>
          </Menu.Item>
        </Menu>
      </Header>
    </>
  );
}
