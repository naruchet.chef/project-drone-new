import "antd/dist/antd.css";
import React from "react";
import { Layout } from "antd";

const { Footer } = Layout;

export default function LayoutFooter() {
  return (
    <Layout>
      <Footer style={{ textAlign: "center" }}>
        Drone platform ©2018 Created Somsan Tech
      </Footer>
    </Layout>
  );
}
