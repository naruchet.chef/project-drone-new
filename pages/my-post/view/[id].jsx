import "antd/dist/antd.css";
import React, { useState, useEffect } from "react";
import LayoutHeader from "../../../components/layout/header";
import { useRouter } from "next/router";
import LayoutFooter from "../../../components/layout/footer";
import { get } from "lodash";
import { ExclamationCircleOutlined, StarTwoTone, StarOutlined } from "@ant-design/icons";
import { findOne, deleteJob, updateJobStatus } from "../../../services/job.service";
import { createReview } from "../../../services/pilotReview.service";
import { blockList } from "../../../services/block.service";
import { create } from "../../../services/paymentHistory.service";
import helper from "../../../utils/helper";
import { bidList } from "../../../services/bid.service"
import { Field, Formik } from "formik";
import {
  Select as Selects,
  Input as Inputs,
  TextArea as TextAreas,
  GoogleMap,
} from "../../../components";
import {
  Layout,
  Avatar,
  Breadcrumb,
  Button,
  Space,
  Row,
  Form,
  Col,
  notification,
  Image,
  Spin,
  Typography,
  Modal,
  Comment,
  Upload,
  Input,
  Rate,
  Divider
} from "antd";
const { TextArea } = Input;
import { UploadOutlined, DeleteOutlined, EditOutlined, InfoCircleOutlined } from "@ant-design/icons";
import { status } from "../../../constants/common";
const { Text, Title } = Typography;
const { info } = Modal;
const { warning } = Modal;
const { Header, Content, Footer } = Layout;
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import * as Yup from "yup";

export default function Post() {

  const initialValuesPilot = {
    first_name: "",
    last_name: "",
    bank_account_name: "",
    tel: "",
    bank_account_no: "",
    bank_name_id: 0,
    pilot_profile: {
      rating: 0
    }

  };

  const initialValues = {
    job_name: "",
    job_detail: "",
    budget_from: "",
    budget_to: "",
    job_type_id: "",
    drone_type_id: "",
    location: "",
    is_auction: "",
    start_date_type: "",
    date_range_start: "",
    date_range_end: "",
    accepted_term: false,
    latitute: "",
    longtitute: "",
    drone_model: "",
    camera_resolution: "",
    other_drone_model: "",
    period_date: "",
    bids: [],
    job_images: [],
    pilot_profile_id: 0,
    job_link: "",
    price: 0,
    pilot_profile: {
      initialValuesPilot
    }
  };

  const settings = {
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
    initialSlide: 0,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          initialSlide: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };

  const router = useRouter();
  const { id } = router.query;
  const [componentSize, setComponentSize] = useState("medium"); // large, medium, default
  const [value, setValue] = React.useState(1);
  const [value2, setValue2] = React.useState(1);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isCreate, setIsCreate] = useState(true);
  const [isLoading, setIsLoading] = useState(false);
  const [jobData, setJobData] = useState(initialValues);
  const [bidsData, setBidsData] = useState([]);
  const [pageBids, setPageBids] = useState(1);
  const [perPageBids, setPerPageBids] = useState(4);
  const [totalBids, setTotalBids] = useState(0);
  const [locationList, setLocationList] = useState();
  const [pilot, setPilot] = useState(initialValuesPilot);
  const [isModalPilot, setIsModalPilot] = useState(false);
  const [pilotModal, setPilotModal] = useState(initialValuesPilot);
  const [isModalPayment, setIsModalPaymnet] = useState(false);
  const [isModalReview, setIsModalReview] = useState(false);
  const [uploadList, setUploadList] = useState([]);
  const [clickStar, setClickStar] = useState(0);
  const [description, setDescription] = useState("");
  const [pilotBlock, setPilotBlock] = useState([]);
  const profileId =
    typeof window !== "undefined" ? localStorage.getItem("profile_id") : null;

  const handleWaited = async () => {
    setIsLoading(true);
    let jobBody = {
      id: id,
      status: "waited"
    }
    const { result, success } = await updateJobStatus(jobBody);
    if (success) {
      notification.success({
        message: `success`,
        description: "ดำเนินการ เรียบร้อยแล้ว",
      });
      getPost(id);
      setIsLoading(false);
    }

  };

  const uploadFile = (value) => {
    console.log(`value`, value);
    const { file, fileList = [] } = value;
    if (fileList.length > 0) {
      console.log(`file`, file);
      console.log(`fileList`, fileList);
      setUploadList(fileList);
    }
  };


  const getBids = async (postId) => {
    const filter = {
      job_id: postId,
      bid_by: "pilot"
    };
    const { result, success } = await bidList(filter);
    if (success) {
      const { data = [] } = result;
      setBidsData(data);
      setIsLoading(false);
    }
  };

  const openTransferMoney = async () => {
    setIsModalPaymnet(true)
  };

  const handleTransferMoney = async () => {
    if (uploadList.length == 0) {
      warning({
        title: `กรุณา อัพโหลดสลิปโอนเงิน`,
        afterClose() { },
      })
      return null
    }
    setIsModalPaymnet(false)
    setIsLoading(true);
    handlePaymentHistory()
    let jobBody = {
      id: id,
      status: "success"
    }
    const { result, success } = await updateJobStatus(jobBody);
    if (success) {
      notification.success({
        message: `success`,
        description: "แจ้งโอนเงิน เรียบร้อยแล้ว",
      });
      getPost(id);
      setIsLoading(false);
    }
    setIsModalReview(true)
  };

  const handlePaymentHistory = async () => {
    const body = new FormData();
    let jobBody = {
      job_id: id,
      pilot_profile_id: jobData.pilot_profile.id,
      amount: jobData.price,
      profile_id: jobData.profile_id,
      bank_name: jobData.pilot_profile.bank_name_id,
      account_bank_name: jobData.pilot_profile.bank_account_name,
      bank_no: jobData.pilot_profile.bank_account_no
    }
    if (uploadList.length > 0) {
      uploadList.forEach((file) => {
        body.append("file", file.originFileObj);
      });
    }
    for (const [key, value] of Object.entries(jobBody)) {
      body.append(key, value);
    }
    const { result, success } = await create(body);
  };

  const handleRating = async () => {
    setIsModalReview(false)
    setIsLoading(true);
    let jobBody = {
      job_id: id,
      rating: clickStar,
      pilot_profile_id: jobData.pilot_profile.id,
      description: description
    }
    const { result, success } = await createReview(jobBody);
    if (success) {
      notification.success({
        message: `success`,
        description: "ให้คะแนน เรียบร้อยแล้ว",
      });
      setIsLoading(false);
    }
  };

  const handleSeletPilot = async () => {
    setIsLoading(true);
    setIsModalPilot(false)
    let jobBody = {
      id: id,
      status: "waiting",
      pilot_profile_id: pilotModal.pilot_profile_id,
      price: pilotModal.budget,
    }
    const { result, success } = await updateJobStatus(jobBody);
    if (success) {
      notification.success({
        message: `success`,
        description: "เลือกนักบิน เรียบร้อยแล้ว",
      });
      getPost(id);
      setIsLoading(false);
    }
  };

  useEffect(() => {
    if (id) {
      if (id === "create") {
        setIsCreate(true);
        setJobData(initialValues);
      } else {
        setIsLoading(true);
        setIsCreate(false);
        getPost(id);
      }
      getBids(id)
    }
  }, [id]);

  const getPost = async (postId) => {
    const filter = {
      id: postId,
    };
    const { result, success } = await findOne(filter);
    if (success) {
      const { data = [] } = result;
      const editData = {
        ...data,
      };
      setJobData(data);
      setIsLoading(false);
    } else {
      gotoMyJobs();
    }
  };

  const jobType = {
    1: "Cinematic Aerial Photography",
    2: "Orthophoto",
    3: "DSM/DTM",
    3: "Point Cloud 3D Model",
    4: "Contour",
    5: "Index & Zonation Map",
    6: "อื่นๆ",
  };

  const bank = {
    1: "ธนาคารกรุงไทย จำกัด (มหาชน)",
    2: "ธนาคารกรุงศรีอยุธยา จำกัด (มหาชน)",
    3: "ธนาคารกรุงเทพ จำกัด (มหาชน)",
    4: "ธนาคารกสิกรไทย จำกัด (มหาชน)",
    5: "ธนาคารทหารไทยธนชาติ จำกัด (มหาชน)",
    6: "ธนาคารซีไอเอ็มบี จำกัด (มหาชน)",
    7: "ธนาคารไทยพาณิชย์ จำกัด (มหาชน)",
    8: "ธนาคารสแตนดาร์ดชาร์เตอร์ด(ไทย) จำกัด (มหาชน)",
  };

  const droneType = {
    1: "Quadcopter",
    2: "GPS Drone",
    3: "RTF Drone( Ready to Fly Drone )",
    4: "Trick Drone",
    5: "Helicopter Drone",
    6: "Delivery Drone",
    7: "โดรนสำหรับถ่ายภาพ",
    8: "โดรนสำหรับแข่ง",
    9: "โดรนขับเคลื่อนด้วยน้ำมัน",
    1: "โดรนแบบบินได้นาน",
  };

  const droneModel = {
    0: "ไม่ระบุ",
    dji_air_2s: "DJI AIR 2s",
    dji_mini_2: "DJI MINI 2",
    dji_fpv_combo: "DJI FPV COMBO",
    other: "อื่น",
  };

  const cameraResolution = {
    0: "ไม่ระบุ",
    3: "3 cm/px",
    4: "4 cm/px",
    5: "5 cm/px",
    other: "อื่น",
  };

  const periodDate = {
    1: "ไมแน่ใจ",
    2: "ครึ่งวัน",
    3: "1 วัน",
    4: "2 วัน",
    5: "3 วัน",
  };

  const auctionType = {
    all: "นักบินทุกคน",
    select: "นักบินที่เลือกเท่านั้น",
  };

  const startDateType = {
    static: "วันที่กำหนด",
    dynamic: "สามารถยืดหยุ่นได้",
  };

  const renderDroneModel = (inputDrone) => {
    return droneModel[inputDrone] || inputDrone;
  };

  const handleEditClick = () => {
    router.push(`/my-post/${id}`);
  };

  const gotoMyJobs = () => {
    router.replace("/my-jobs");
  };

  const handleDeleteClick = async () => {
    confirm({
      title: "แจ้งเตือน",
      icon: <ExclamationCircleOutlined />,
      content: "คุณต้องการลบงานของคุณใช่หรือไม่",
      okText: "ตกลง",
      cancelText: "ยกเลิก",
      async onOk() {
        const filter = {
          id: id,
        };
        const { result, success } = await deleteJob(filter);
        if (success) {
          gotoMyJobs();
        }
      },
    });
  };

  const pilotBid = async (pilot) => {
    setIsLoading(true);
    const filter = {
      ref_id: pilot.pilot_profile.id,
      category: "pilot",
      account_id: profileId
    };
    const { result, success } = await blockList(filter);
    if (success) {
      const { data } = result
      setPilotBlock(data)
    }
    setIsLoading(false);

    setPilotModal(pilot)
    setIsModalPilot(true)
  };

  const handleCancelPilot = () => {
    setIsModalPilot(false);
  };

  const handleCancelPayment = () => {
    setIsModalPaymnet(false);
  };
  const handleCancelReview = () => {
    setIsModalReview(false);
  };

  const handleDescription = (values) => {
    setDescription(values.target.value)
  }

  const handleStar = (values) => {
    setClickStar(values)
  }


  return (
    <Layout>
      <LayoutHeader />
      <Content
        className="site-layout"
        style={{ padding: "0 50px", marginTop: 64 }}
      >
        <Breadcrumb style={{ margin: "16px 0" }}>
          <Breadcrumb.Item>หน้าหลัก</Breadcrumb.Item>
          <Breadcrumb.Item>โพสงาน</Breadcrumb.Item>
        </Breadcrumb>
        <div
          className="site-layout-background"
          style={{ padding: 24, minHeight: 380 }}
        >
          <Title style={{ textAlign: "center" }} level={3}>
            ข้อมูลงานของฉัน
          </Title>
          <Spin spinning={isLoading}>
            <Row gutter={[16, 24]}>
              <Col className="gutter-row" span={16} offset={4}>
                <Row gutter={[16, 24]}>
                  <Col span={12}>
                    <Text type="">ชื่อโครงการ</Text>
                    <div style={{ paddingLeft: "5px" }}>
                      <Text type="secondary">{jobData.job_name}</Text>
                    </div>
                  </Col>
                  <Col span={12} style={{ textAlign: "end" }}>
                    <div style={{ textAlign: "center" }}>
                      <Text type="">สถานะ</Text>
                      <div style={{ paddingLeft: "5px" }}>
                        <Text type="secondary"></Text>
                        <Button style={{ background: jobData.status != undefined ? status[jobData.status].code : "", borderRadius: '7px' }}><p style={{ color: 'white' }}>{jobData.status != undefined ? status[jobData.status].text : ""}</p></Button>
                      </div>
                    </div>
                  </Col>
                </Row>
              </Col>
              <Col className="gutter-row" span={16} offset={4}>
                <Text type="">รายละเอียดโครงการ</Text>
                <div style={{ paddingLeft: "5px" }}>
                  <Text type="secondary">{jobData.job_detail}</Text>
                </div>
              </Col>
              <Col className="gutter-row" span={16} offset={4}>
                <Text type="">งบประมาณ</Text>
                <div style={{ paddingLeft: "5px" }}>
                  <Text type="secondary">
                    {jobData.budget_from} - {jobData.budget_to}{" "}
                  </Text>
                </div>
              </Col>
              <Col className="gutter-row" span={16} offset={4}>
                <Text type="">ประเภทงานของคุณ</Text>
                <div style={{ paddingLeft: "5px" }}>
                  <Text type="secondary">
                    {get(jobType, jobData.job_type_id)}
                  </Text>
                </div>
              </Col>
            </Row>
            <br />
            <Row gutter={[16, 24]}>
              <Col className="gutter-row" span={6} offset={4}>
                <Text type="">ประเภทโดรน</Text>
                <div style={{ paddingLeft: "5px" }}>
                  <Text type="secondary">
                    {get(droneType, jobData.drone_type_id)}
                  </Text>
                </div>
              </Col>
              <Col className="gutter-row" span={6}>
                <Text type="">รุ่นโดรน</Text>
                <div style={{ paddingLeft: "5px" }}>
                  <Text type="secondary">
                    {renderDroneModel(jobData.drone_model)}
                  </Text>
                </div>
              </Col>
              <Col className="gutter-row" span={6}>
                <Text type="">ความละเอียดกล้องติดโดรน</Text>
                <div style={{ paddingLeft: "5px" }}>
                  <Text type="secondary">
                    {get(cameraResolution, jobData.camera_resolution)}
                  </Text>
                </div>
              </Col>
            </Row>
            <Row style={{ marginTop: "50px" }} gutter={[16, 24]}>
              <Col className="gutter-row" span={16} offset={4}>
                <GoogleMap
                  isDisable={true}
                  jobLocations={get(jobData, "job_locations")}
                />
              </Col>
            </Row>
            <Row style={{ marginTop: "40px" }} gutter={[16, 24]}>
              <Col className="gutter-row" span={6} offset={4}>
                <Text type="">ใครสามารถประมูลได้บ้าง</Text>
                <div style={{ paddingLeft: "5px" }}>
                  <Text type="secondary">
                    {get(auctionType, jobData.is_auction)}
                  </Text>
                </div>
              </Col>
              <Col className="gutter-row" span={6}>
                <Text type="">คุณต้องการเริ่มจ้างเมื่อไหร่</Text>
                <div style={{ paddingLeft: "5px" }}>
                  <Text type="secondary">
                    {get(startDateType, jobData.start_date_type)}
                  </Text>
                </div>
              </Col>
            </Row>
            <br />
            <Row gutter={[16, 24]}>
              <Col className="gutter-row" span={16} offset={4}>
                <Text type="">วันที่ต้องการว่าจ้าง</Text>
                <div style={{ paddingLeft: "5px" }}>
                  <Text type="secondary">
                    {helper.dateFormat(jobData.date_range_start)}
                  </Text>
                </div>
              </Col>
              <Col className="gutter-row" span={16} offset={4}>
                <Text type="">ระยะเวลาในการจ้างงาน</Text>
                <div style={{ paddingLeft: "5px" }}>
                  <Text type="secondary">
                    {get(periodDate, jobData.period_date)}
                  </Text>
                </div>
              </Col>
              <Col className="gutter-row" span={16} offset={4}>
                <Text type="">แนบไฟล์</Text>
                <div style={{ paddingLeft: "5px" }}>
                  {jobData.job_images.map((val) => {
                    console.log(`val`, val);
                    return (
                      <>
                        <Image width={200} src={"/image/" + val.image} />
                      </>
                    );
                  })}
                </div>
              </Col>
            </Row>
            <br />
            {jobData.status != "find_pilot" ? (
              <>
                <Row style={{ marginTop: "40px" }} gutter={[16, 24]}>
                  <Col className="gutter-row" span={6} offset={4}>
                    <Text type="">ชื่อนักบิน</Text>
                    <div style={{ paddingLeft: "5px" }}>
                      <Text type="secondary">
                        {jobData.pilot_profile.first_name} {jobData.pilot_profile.last_name}
                      </Text>
                    </div>
                  </Col>
                  <Col className="gutter-row" span={6}>
                    <Text type="">อีเมล</Text>
                    <div style={{ paddingLeft: "5px" }}>
                      <Text type="secondary">
                        {jobData.pilot_profile.email}
                      </Text>
                    </div>
                  </Col>
                  <Col className="gutter-row" span={6}>
                    <Text type="">เบอร์ติดต่อ</Text>
                    <div style={{ paddingLeft: "5px" }}>
                      <Text type="secondary">
                        {jobData.pilot_profile.tel}
                      </Text>
                    </div>
                  </Col>
                </Row>
                <Row style={{ marginTop: "40px" }} gutter={[16, 24]}>
                  <Col className="gutter-row" span={6} offset={4}>
                    <Text type="">บัญชีธนาคาร</Text>
                    <div style={{ paddingLeft: "5px" }}>
                      <Text type="secondary">
                        {jobData.pilot_profile.bank_name_id > 0 ? bank[jobData.pilot_profile.bank_name_id] : ""}
                      </Text>
                    </div>
                  </Col>
                  <Col className="gutter-row" span={6}>
                    <Text type="">ชื่อบัญชี</Text>
                    <div style={{ paddingLeft: "5px" }}>
                      <Text type="secondary">
                        {jobData.pilot_profile.bank_account_name}
                      </Text>
                    </div>
                  </Col>
                  <Col className="gutter-row" span={6}>
                    <Text type="">เลขที่บัญชี</Text>
                    <div style={{ paddingLeft: "5px" }}>
                      <Text type="secondary">
                        {jobData.pilot_profile.bank_account_no}
                      </Text>
                    </div>
                  </Col>
                </Row>
                <Row style={{ marginTop: "40px" }} gutter={[16, 24]}>
                  <Col className="gutter-row" span={6} offset={4}>
                    <Text type="">ราคาจ้างงาน</Text>
                    <div style={{ paddingLeft: "5px" }}>
                      <Text type="secondary">
                        {jobData.price}
                      </Text>
                    </div>
                  </Col>
                  <Col className="gutter-row" span={6}>
                    <Text type="">ลิงค์อัพโหลดไฟล์งาน</Text>
                    <div style={{ paddingLeft: "5px" }}>
                      <Text type="secondary">
                        <a href={jobData.job_link} rel="noreferrer" target="_blank">
                          {jobData.job_link == "" ? "-" : jobData.job_link}
                        </a>
                      </Text>
                    </div>
                  </Col>
                </Row></>
            ) : ("")}
            <br />
            <div style={{ textAlign: "center", marginTop: "15px" }}>
              <Space>
                {jobData.status == "find_pilot" ? (
                  <>
                    <Button type="danger" onClick={handleDeleteClick} primary>
                      ลบงาน
                    </Button>
                    <Button type="primary" onClick={handleEditClick} primary>
                      แก้ไข
                    </Button>
                    <Button
                      type="secondary"
                      onClick={gotoMyJobs}
                      htmlType="submit"
                      primary
                    >
                      ปิด
                    </Button>
                  </>
                ) : jobData.status == "success" ? (
                  <Button
                    type="secondary"
                    onClick={gotoMyJobs}
                    htmlType="submit"
                    primary
                  >
                    ปิด
                  </Button>
                ) : (
                  <>
                    <Button
                      type="secondary"
                      onClick={gotoMyJobs}
                      htmlType="submit"
                      primary
                    >
                      ปิด
                    </Button>
                    <Button type="primary" disabled={jobData.status == "waiting_pay" ? false : true} onClick={openTransferMoney} primary>
                      แจ้งโอนเงิน
                    </Button>

                  </>
                )}

                <Button type="primary" onClick={handleWaited} primary>
                  ดำเนินการ
                </Button>
              </Space>
            </div>
            {jobData.bids.length > 0 && jobData.status == "find_pilot" ? (
              <>
                <Title style={{ textAlign: "center", marginTop: "30px" }} level={5}>
                  นักบินที่เลือกรับงานของคุณ
                </Title>
                <Row gutter={[16, 24]}>
                  <Col className="gutter-row" span={16} offset={4}>
                    <Slider style={{ marginTop: "10px" }} {...settings}>
                      {

                        bidsData.map((v, k) => {
                          return (
                            <>
                              <Comment
                                onClick={() => pilotBid(v)}
                                avatar={<Avatar size="large" src={"/image/" + v.pilot_profile.photo} />}
                                content={
                                  <div>
                                    <p>{v.pilot_profile.first_name} {v.pilot_profile.last_name}</p>
                                    <Rate disabled defaultValue={v.pilot_profile.rating} />
                                    <p>ราคาที่เสนอ</p>
                                    <p style={{ color: "#007DFF" }}>{v.budget} บาท</p>
                                  </div>
                                }
                              />
                            </>
                          )
                        })

                      }
                    </Slider>
                  </Col>
                </Row>
              </>
            ) : ""}
          </Spin>
        </div>
        <Modal
          footer={null}
          width={'25%'}
          visible={isModalPilot}
          onCancel={handleCancelPilot}
          style={{ borderRadius: '20px' }}
        >
          <>
            <center>
              <Image width={180} height={180} style={{ borderRadius: '50%' }} src="error" fallback={"/image/" + pilotModal.pilot_profile.photo} /><br />
              <Rate disabled defaultValue={pilotModal.pilot_profile.rating} /><br />
              {pilotModal.pilot_profile.first_name} {pilotModal.pilot_profile.last_name}<br />
              <a
                onClick={() => { router.push("/pilot/detail/" + pilotModal.pilot_profile.id); }}
                style={{
                  fontStyle: 'normal',
                  fontFamily: 'Prompt',
                  fontWeight: '400',
                  fontSize: '12px',
                  lineHeight: '12px',
                  color: '#7F7F7F'
                }}>ดูโปรไฟล์</a><br />
              <Divider orientation="left"></Divider>
              <Row>
                <Col span={8}>ราคาที่เสนอ</Col>
                <Col span={8} offset={8}>
                  {pilotModal.budget} บาท
                </Col>
              </Row>
              <Divider orientation="left"></Divider>
              <Button disabled={pilotBlock.length > 0 ? true : false} onClick={handleSeletPilot} style={{ background: pilotBlock.length > 0 ? "#BDBDBD" : "#3397FF", borderRadius: '7px' }}><p style={{ color: 'white' }}>เลือกนักบินคนนี้</p></Button><br />
              <Button onClick={handleCancelPilot} style={{ borderRadius: '7px', marginTop: '10px' }}>ยกเลิก</Button>
            </center>
          </>
        </Modal >

        <Modal
          footer={null}
          width={'50%'}
          visible={isModalPayment}
          onCancel={handleCancelPayment}
        >
          <>
            <Row gutter={16}>
              <Col className="gutter-row" span={24} style={{ textAlign: "left" }}>
                <Title style={{ color: "#7F7F7F" }} level={3}>
                  ยืนยันการโอนเงิน
                </Title>
              </Col>
              <Col className="gutter-row" span={24} style={{ marginTop: "10px" }}>
                <Row gutter={16}>
                  <Col className="gutter-row" span={8}>
                    <Text type="">บัญชีธนาคาร</Text>
                    <div style={{ paddingLeft: "5px" }}>
                      <Text type="secondary">
                        {jobData.pilot_profile?.bank_name_id != null ? bank[jobData.pilot_profile.bank_name_id] : ""}
                      </Text>
                    </div>
                  </Col>
                  <Col className="gutter-row" span={8}>
                    <Text type="">ชื่อบัญชี</Text>
                    <div style={{ paddingLeft: "5px" }}>
                      <Text type="secondary">
                        {jobData.pilot_profile?.bank_account_name}
                      </Text>
                    </div>
                  </Col>
                  <Col className="gutter-row" span={8}>
                    <Text type="">เลขที่บัญชี</Text>
                    <div style={{ paddingLeft: "5px" }}>
                      <Text type="secondary">
                        {jobData.pilot_profile?.bank_account_no}
                      </Text>
                    </div>
                  </Col>
                </Row>
                <Row style={{ marginTop: "20px" }} gutter={16}>
                  <Col className="gutter-row" span={6}>
                    <Text type="">ราคาจ้างงาน</Text>
                    <div style={{ paddingLeft: "5px" }}>
                      <Text type="secondary">
                        {jobData.price}
                      </Text>
                    </div>
                  </Col>
                </Row>
                <Row style={{ marginTop: "20px" }} gutter={16}>
                  <Col className="gutter-row" span={24}>
                    <Form.Item label={"แนบสลิปโอนเงิน"} >
                      <Upload maxCount={1} onRemove={(file) => setUploadList([])} onChange={(file) => uploadFile(file)}>
                        <Button
                          icon={<UploadOutlined />}
                        >
                          Choose Files
                        </Button>
                      </Upload>
                    </Form.Item>
                  </Col>
                </Row>
              </Col>
              <Col
                className="gutter-row" span={24}
                style={{ textAlign: "center" }}
              >
                <Space>
                  <Button
                    style={{ marginRight: "10px", borderRadius: "10px" }}
                    type="primary" htmlType="submit" primary
                    onClick={() => handleTransferMoney()}
                  >
                    แจ้งโอนเงิน
                  </Button>
                </Space>
              </Col>
            </Row>
          </>
        </Modal >

        <Modal
          footer={null}
          width={'50%'}
          visible={isModalReview}
          onCancel={handleCancelReview}
        >
          <>
            <Row gutter={16}>
              <Col className="gutter-row" span={24} style={{ textAlign: "left" }}>
                <Title style={{ color: "#7F7F7F" }} level={3}>
                  ให้คะแนนนักบินของคุณ
                </Title>
              </Col>
              <Col className="gutter-row" span={24} style={{ marginTop: "10px" }}>
                <Row gutter={16}>
                  <Col className="gutter-row" span={24}>
                    <Text type="">ให้ดาวนักบินคนนี้เท่าไร ?</Text><br />
                    <Rate allowHalf defaultValue={0} onChange={handleStar} />
                  </Col>
                </Row>
                <Row style={{ marginTop: "20px" }} gutter={16}>
                  <Col className="gutter-row" span={24}>
                    <Text type="">ความคิดเห็นเพิ่มเติม</Text>
                    <TextArea onChange={handleDescription} rows={4} />
                  </Col>
                </Row>
              </Col>
              <Col
                className="gutter-row" span={24}
                style={{ textAlign: "center" }}
              >
                <Space>
                  <Button
                    style={{ marginRight: "10px", marginTop: "20px", borderRadius: "10px" }}
                    type="primary" htmlType="submit" primary
                    onClick={() => handleRating()}
                  >
                    เสร็จสิ้น
                  </Button>
                </Space>
              </Col>
            </Row>
          </>
        </Modal >
      </Content >
      <LayoutFooter />
    </Layout >

  );
}
