import "antd/dist/antd.css";
import React, { useState, useEffect } from "react";
import LayoutHeader from "../../components/layout/header";
import { useRouter } from "next/router";
import LayoutFooter from "../../components/layout/footer";
import { Field, Formik, ErrorMessage } from "formik";
import * as Yup from "yup";
import { get, map } from "lodash";
import { createJob } from "../../services/job.service";
import { UploadOutlined } from "@ant-design/icons";
import { findOne, updateJob } from "../../services/job.service";

import {
  Select as Selects,
  Input as Inputs,
  TextArea as TextAreas,
  DatePicker,
  GoogleMap,
  RadioSelect,
} from "../../components";
import {
  Layout,
  Breadcrumb,
  Button,
  Space,
  Form,
  Checkbox,
  Modal,
  Upload,
  Row,
  Col,
  notification,
  Spin,
  Typography,
} from "antd";
const { Text, Title } = Typography;
const { warning } = Modal;
const { Header, Content, Footer } = Layout;

export default function Post() {
  const initialValues = {
    job_name: "",
    job_detail: "",
    budget_from: "",
    budget_to: "",
    job_type_id: "",
    drone_type_id: "",
    location: "",
    is_auction: "",
    start_date_type: "",
    date_range_start: "",
    date_range_end: "",
    accepted_term: false,
    drone_model: "",
    camera_resolution: "",
    other_drone_model: "",
    period_date: "",
    job_locations: [],
  };
  const router = useRouter();
  const { id } = router.query;
  const [componentSize, setComponentSize] = useState("medium"); // large, medium, default
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [jobData, setJobData] = useState(initialValues);
  const [locationList, setLocationList] = useState();
  const [isCreate, setIsCreate] = useState(true);
  const [uploadList, setUploadList] = useState([]);
  const profileId =
    typeof window !== "undefined" ? localStorage.getItem("profile_id") : null;
  useEffect(() => {
    if (id) {
      if (id === "create") {
        setIsCreate(true);
      } else {
        setIsCreate(false);
        getPost(id);
      }
    }
  }, [id]);

  const jobType = [
    { name: "Cinematic Aerial Photography", value: 1 },
    { name: "Orthophoto", value: 2 },
    { name: "DSM/DTM", value: 3 },
    { name: "Point Cloud 3D Model", value: 3 },
    { name: "Contour", value: 4 },
    { name: "Index & Zonation Map", value: 5 },
    { name: "อื่นๆ", value: 6 },
  ];

  const droneType = [
    { value: 1, name: "Quadcopter" },
    { value: 2, name: "GPS Drone" },
    { value: 3, name: "RTF Drone( Ready to Fly Drone )" },
    { value: 4, name: "Trick Drone" },
    { value: 5, name: "Helicopter Drone" },
    { value: 6, name: "Delivery Drone" },
    { value: 7, name: "โดรนสำหรับถ่ายภาพ" },
    { value: 8, name: "โดรนสำหรับแข่ง" },
    { value: 9, name: "โดรนขับเคลื่อนด้วยน้ำมัน" },
    { value: 10, name: "โดรนแบบบินได้นาน" },
  ];

  const droneModel = [
    { name: "ไม่ระบุ", value: "0" },
    { name: "DJI AIR 2s", value: "dji_air_2s" },
    { name: "DJI MINI 2", value: "dji_mini_2" },
    { name: "DJI FPV COMBO", value: "dji_fpv_combo" },
    { name: "อื่น", value: "other" },
  ];

  const cameraResolution = [
    { name: "ไม่ระบุ", value: "0" },
    { name: "3 cm/px", value: "3" },
    { name: "4 cm/px", value: "4" },
    { name: "5 cm/px", value: "5" },
    { name: "อื่น", value: "other" },
  ];

  const periodDate = [
    { value: "1", name: "ไมแน่ใจ" },
    { value: "2", name: "ครึ่งวัน" },
    { value: "3", name: "1 วัน" },
    { value: "4", name: "2 วัน" },
    { value: "5", name: "3 วัน" },
  ];

  const dateStartOption = [
    { value: "static", name: "วันที่ฉันกำหนด" },
    { value: "dynamic", name: "สามารถยืดหยุ่นได้" },
  ];

  const isAuctionOption = [
    { value: "all", name: "นักบินทุกคน" },
    { value: "select", name: "นักบินที่เลือกเท่านั้น" },
  ];

  const getPost = async (postId) => {
    setIsLoading(true);
    const filter = {
      id: postId,
    };
    const { result, success } = await findOne(filter);
    if (success) {
      const { data = [] } = result;
      let droneModelId = get(data, "drone_model");
      let otherDroneModel = "";
      const isDroneModelSelect = droneModel
        .map((d) => d.value)
        .includes(droneModelId);
      if (!isDroneModelSelect) {
        otherDroneModel = droneModelId;
        droneModelId = "other";
      }
      const editData = {
        ...data,
        drone_model: droneModelId,
        other_drone_model: otherDroneModel,
      };

      const formatLocation = map(editData.job_locations, (value) => {
        return {
          id: value.id,
          name: value.name,
          lat: value.lat,
          lng: value.lng,
        };
      });
      setJobData(editData);
      setIsLoading(false);
      setLocationList(formatLocation);
    } else {
      gotoMyJob();
    }
  };

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };

  const handleSubmitForm = async (values) => {
    const body = new FormData();
    const drone_model = get(values, "drone_model", "");
    let jobBody = {
      job_name: get(values, "job_name"),
      job_detail: get(values, "job_detail"),
      budget_from: get(values, "budget_from"),
      budget_to: get(values, "budget_to"),
      job_type_id: get(values, "job_type_id"),
      drone_type_id: get(values, "drone_type_id"),
      location: get(values, "location"),
      is_auction: get(values, "is_auction"),
      start_date_type: get(values, "start_date_type"),
      date_range_start: get(values, "date_range_start"),
      accepted_term: get(values, "accepted_term", false),
      camera_resolution: `${get(values, "camera_resolution")}`,
      drone_model: drone_model,
      period_date: get(values, "period_date"),
    };

    if (locationList && locationList.length > 0) {
      body.append("job_locations", JSON.stringify(locationList));
    }

    if (drone_model === "other") {
      jobBody["drone_model"] = get(values, "other_drone_model", "");
    }
    if (uploadList.length > 0) {
      uploadList.forEach((file) => {
        body.append("file", file.originFileObj);
      });
    }

    for (const [key, value] of Object.entries(jobBody)) {
      body.append(key, value);
    }

    if (isCreate) {
      createPost(body);
    } else {
      body.append("id", id);
      updatePost(body);
    }
  };

  const createPost = async (jobBody) => {
    if (uploadList.length == 0) {
      warning({
        title: `กรุณา อัพโหลดรูปภาพ`,
        afterClose() { },
      })
      return null
    }
    setIsLoading(true);
    jobBody.append("status", "find_pilot");
    jobBody.append("profile_id", profileId);
    const { result, success } = await createJob(jobBody);
    if (success) {
      notification.success({
        message: `success`,
        description: "โพสงานของฉันเรียบร้อยแล้ว",
      });
      setIsLoading(false);
      gotoMyJob();
    }
  };

  const updatePost = async (jobBody) => {
    setIsLoading(true);
    const { result, success } = await updateJob(jobBody);
    if (success) {
      notification.success({
        message: `success`,
        description: "แก้ไขข้อมูลเรียบร้อยแล้ว",
      });
      setIsLoading(false);
      router.back()
    }
  };

  const Schema = Yup.object().shape({
    job_name: Yup.string().required("กรุณากรอก ชื่อโครงการ"),
    budget_from: Yup.number()
      .required("กรุณากรอกงบประมาณ")
      .positive("กรุณากรอกประเภทงานของคุณ")
      .integer("กรุณากรอกประเภทงานของคุณ"),
    job_type_id: Yup.number()
      .required("กรุณากรอกประเภทงานของคุณ")
      .positive("กรุณากรอกประเภทงานของคุณ")
      .integer(),
    drone_type_id: Yup.number().required("กรุณากรอกประเภทโดรน").integer(),
    is_auction: Yup.string().required("กรุณากรอก การประมูล"),
    start_date_type: Yup.string().required("กรุณากรอก วันที่ต้องการว่าจ้าง"),
    accepted_term:
      isCreate && Yup.bool().oneOf([true], "กรุณากรอก ยอมรับเงื่อนไข"),
    period_date: Yup.number()
      .required("กรุณากรอกระยะเวลาในการจ้างงาน")
      .integer(),
    drone_model: Yup.string().required("กรุณาเลือกรุ่นโดรน"),
    camera_resolution: Yup.string().required(
      "กรุณาเลือกความละเอียดกล้องติดโดรน"
    ),
    job_locations: Yup.array().test(
      "required",
      "กรุณาเลือกสถานที่ตั้ง",
      function (item) {
        let totalAmount = locationList && locationList.length;
        return totalAmount >= 1;
      }
    ),
  });

  const onCancelClick = () => {
    if (isCreate) {
      router.replace("/my-jobs");
    } else {
      router.replace(`/my-post/view/${id}`);
    }
  };

  const gotoMyJob = () => {
    router.push("/my-jobs");
  };

  const uploadFile = (value) => {
    console.log(`value`, value);
    const { file, fileList = [] } = value;
    if (fileList.length > 0) {
      console.log(`file`, file);
      console.log(`fileList`, fileList);
      setUploadList(fileList);
    }
  };

  return (
    <Layout>
      <LayoutHeader />
      <Content
        className="site-layout"
        style={{ padding: "0 50px", marginTop: 64 }}
      >
        <Breadcrumb style={{ margin: "16px 0" }}>
          <Breadcrumb.Item>หน้าหลัก</Breadcrumb.Item>
          <Breadcrumb.Item>โพสงาน</Breadcrumb.Item>
          {/* <Breadcrumb.Item>App</Breadcrumb.Item> */}
        </Breadcrumb>
        <div
          className="site-layout-background"
          style={{ padding: 24, minHeight: 380 }}
        >
          <Title style={{ textAlign: "center" }} level={3}>
            กรอกข้อมูล
          </Title>
          <Spin spinning={isLoading}>
            <Formik
              enableReinitialize={true}
              initialValues={jobData}
              onSubmit={handleSubmitForm}
              validationSchema={Schema}
            >
              {({ values, handleSubmit, handleChange, setFieldValue }) => (
                <Form
                  layout="vertical"
                  onValuesChange={onFormLayoutChange}
                  size={"medium"}
                  onFinish={handleSubmit}
                >
                  <Row gutter={16}>
                    <Col className="gutter-row" span={16} offset={4}>
                      <Field
                        label={{ required: true, text: "ชื่อโครงการ" }}
                        id="job_name"
                        onChange={handleChange}
                        name="job_name"
                        component={Inputs}
                        placeholder="ชื่อโครงการ"
                      />
                    </Col>
                    <Col className="gutter-row" span={16} offset={4}>
                      <Field
                        label={{ text: "รายละเอียดโครงการ" }}
                        id="job_detail"
                        onChange={handleChange}
                        name="job_detail"
                        rows={4}
                        component={TextAreas}
                        placeholder="รายละเอียดโครงการ"
                      />
                    </Col>
                    <Col className="gutter-row" span={16} offset={4}>
                      <Row gutter={16}>
                        <Col className="gutter-row" span={12}>
                          <Field
                            label={{ required: true, text: "งบประมาณเริ่มต้น" }}
                            id="budget_from"
                            onChange={handleChange}
                            name="budget_from"
                            rows={4}
                            type={"number"}
                            component={Inputs}
                            placeholder="0.00"
                          />
                        </Col>
                        <Col className="gutter-row" span={12}>
                          <Field
                            label={{ required: true, text: "ถึง" }}
                            id="budget_to"
                            onChange={handleChange}
                            name="budget_to"
                            rows={4}
                            type={"number"}
                            component={Inputs}
                            placeholder="0.00"
                          />
                        </Col>
                      </Row>
                    </Col>
                    <Col className="gutter-row" span={16} offset={4}>
                      <Field
                        label={{ required: true, text: "ประเภทงานของคุณ" }}
                        id="job_type_id"
                        name="job_type_id"
                        component={Selects}
                        placeholder="กรุณาเลือก"
                        defaultValue=""
                        selectOption={jobType}
                      />
                    </Col>
                    <Col className="gutter-row" span={16} offset={4}>
                      <Field
                        label={{ required: true, text: "ประเภทโดรน" }}
                        id="drone_type_id"
                        name="drone_type_id"
                        component={Selects}
                        placeholder="กรุณาเลือก"
                        defaultValue=""
                        selectOption={droneType}
                      />
                    </Col>
                    <Col className="gutter-row" span={16} offset={4}>
                      <Row gutter={16}>
                        <Col className="gutter-row" span={14}>
                          <Field
                            label={{ required: true, text: "รุ่นโดรน" }}
                            id="drone_model"
                            name="drone_model"
                            component={Selects}
                            placeholder="กรุณาเลือก"
                            defaultValue=""
                            selectOption={droneModel}
                          />
                        </Col>
                        {values.drone_model === "other" && (
                          <Col className="gutter-row" span={10}>
                            <Field
                              label={{ required: true, text: "ระบุ" }}
                              id="other_drone_model"
                              name="other_drone_model"
                              component={Inputs}
                              placeholder="กรุณาระบุรุ่นโดรน"
                            />
                          </Col>
                        )}
                      </Row>
                    </Col>
                    <Col className="gutter-row" span={16} offset={4}>
                      <Field
                        label={{
                          required: true,
                          text: "ความละเอียดกล้องติดโดรน",
                        }}
                        id="camera_resolution"
                        name="camera_resolution"
                        component={Selects}
                        placeholder="เลือกความละเอียดกล้องที่คุณต้องการ"
                        defaultValue=""
                        selectOption={cameraResolution}
                      />
                    </Col>
                    <Col className="gutter-row" span={16} offset={4}>
                      <Form.Item label="ที่ตั้ง" required={true}>
                        <GoogleMap
                          setJobLocation={setLocationList}
                          jobLocations={jobData.job_locations}
                        />
                        <Field
                          style={{ display: "none" }}
                          id="job_locations"
                          name="job_locations"
                        />
                        <br />
                        <Text type="danger">
                          {" "}
                          <ErrorMessage name="job_locations" />
                        </Text>
                      </Form.Item>
                    </Col>
                    <Col className="gutter-row" span={16} offset={4}>
                      <Row gutter={16}>
                        <Col className="gutter-row" span={12}>
                          <Form.Item
                            label="ใครสามารถประมูลได้บ้าง"
                            required={true}
                          >
                            <Field
                              id="is_auction"
                              onChange={(event) => {
                                setFieldValue("is_auction", event.target.value);
                              }}
                              name="is_auction"
                              component={RadioSelect}
                              selectOption={isAuctionOption}
                            />
                            <br />
                          </Form.Item>
                        </Col>
                        <Col className="gutter-row" span={12}>
                          <Form.Item
                            label="คุณต้องการเริ่มจ้างเมื่อไหร่"
                            required={true}
                          >
                            <Field
                              id="start_date_type"
                              onChange={(event) => {
                                setFieldValue(
                                  "start_date_type",
                                  event.target.value
                                );
                              }}
                              name="start_date_type"
                              component={RadioSelect}
                              selectOption={dateStartOption}
                            />
                            <br />
                          </Form.Item>
                        </Col>
                      </Row>
                    </Col>

                    <Col className="gutter-row" span={16} offset={4}>
                      <Form.Item label="วันที่ต้องการว่าจ้าง" required={true}>
                        <Field
                          id="date_range_start"
                          name="date_range_start"
                          component={DatePicker}
                          placeholder="กรุณาเลือก"
                        />
                      </Form.Item>
                    </Col>
                    <Col className="gutter-row" span={16} offset={4}>
                      <Field
                        label={{ required: true, text: "ระยะเวลาในการจ้างงาน" }}
                        id="period_date"
                        name="period_date"
                        component={Selects}
                        placeholder="กรุณาเลือก"
                        selectOption={periodDate}
                      />
                    </Col>
                    {/* <Col className="gutter-row" span={16} offset={4}>
                      <Form.Item label="อัพโหลดไฟล์" required={true}>
                        <Upload onChange={(file) => uploadFile(file)}>
                          <Button icon={<UploadOutlined />}>
                            Click to Upload
                          </Button>
                        </Upload>
                      </Form.Item>
                    </Col> */}
                    {isCreate && (
                      <Col className="gutter-row" span={16} offset={4}>
                        <Space>
                          <Field
                            id="accepted_term"
                            onChange={(event) => {
                              setFieldValue(
                                "accepted_term",
                                event.target.checked
                              );
                            }}
                            name="accepted_term"
                            component={Checkbox}
                          />
                          ยอมรับเงื่อนไข
                          <a
                            onClick={showModal}
                            style={{
                              color: "#1890ff!important",
                              fontSize: "12px",
                            }}
                          >
                            อ่านเงื่อนไขเพิ่มเติม
                          </a>
                        </Space>

                        <br />
                        <Text type="danger">
                          {" "}
                          <ErrorMessage name="accepted_term" />
                        </Text>
                      </Col>
                    )}

                    <Col
                      style={{ textAlign: "end" }}
                      className="gutter-row"
                      span={10}
                      offset={10}
                    >
                      <Space>
                        <Button onClick={onCancelClick} type="primary" danger>
                          ยกเลิก
                        </Button>

                        <Button type="primary" htmlType="submit" primary>
                          บันทึก
                        </Button>
                      </Space>
                    </Col>
                  </Row>
                </Form>
              )}
            </Formik>
          </Spin>
        </div>
      </Content>
      <LayoutFooter />
      <Modal
        title="เงื่อนไขการใช้งานระบบ"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        okText="ตกลง"
        cancelText="ยกเลิก"
      >
        <p>Some contents...</p>
        <p>Some contents...</p>
        <p>Some contents...</p>
      </Modal>
    </Layout>
  );
}
