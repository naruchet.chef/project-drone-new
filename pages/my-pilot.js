import "antd/dist/antd.css";
import React, { useState, useEffect } from "react";
import LayoutHeader from "./layout/header";
import LayoutFooter from "./layout/footer";
import InfiniteScroll from "react-infinite-scroll-component";
import {
  Layout,
  Breadcrumb,
  List,
  Avatar,
  Skeleton,
  Divider,
  Input,
  Button,
} from "antd";

const { Content } = Layout;
const { Search } = Input;

export default function Pilot() {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);

  useEffect(() => {
    loadMoreData();
  }, []);

  const loadMoreData = () => {
    if (loading) {
      return;
    }
    setLoading(true);
    fetch(
      "https://randomuser.me/api/?results=10&inc=name,gender,email,nat,picture&noinfo"
    )
      .then((res) => res.json())
      .then((body) => {
        setData([...data, ...body.results]);
        setLoading(false);
      })
      .catch(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    loadMoreData();
  }, []);
  return (
    <Layout>
      <LayoutHeader />
      <Content
        className="site-layout"
        style={{ padding: "0 50px", marginTop: 64 }}
      >
        <Breadcrumb style={{ margin: "16px 0" }}>
          <Breadcrumb.Item>หน้าหลัก</Breadcrumb.Item>
          <Breadcrumb.Item>ค้นหานักบิน</Breadcrumb.Item>
          {/* <Breadcrumb.Item>App</Breadcrumb.Item> */}
        </Breadcrumb>
        <div
          className="site-layout-background"
          style={{ padding: 24, minHeight: 380 }}
        >
          <h2>ค้นหานักบิน</h2>
          <Search
            placeholder="ค้นหา..."
            enterButton="ค้นหา"
            size="large"
            style={{ width: "500px", marginBottom: "10px" }}
            onSearch={(value) => console.log(value)}
          />
          <div
            id="scrollableDiv"
            style={{
              height: 400,
              overflow: "auto",
              padding: "0 16px",
              border: "1px solid rgba(140, 140, 140, 0.35)",
            }}
          >
            <InfiniteScroll
              dataLength={data.length}
              next={loadMoreData}
              hasMore={data.length < 50}
              loader={<Skeleton avatar paragraph={{ rows: 1 }} active />}
              endMessage={<Divider plain>It is all, nothing more 🤐</Divider>}
              scrollableTarget="scrollableDiv"
            >
              <List
                dataSource={data}
                renderItem={(item) => (
                  <List.Item key={item.id}>
                    <List.Item.Meta
                      avatar={<Avatar src={item.picture.large} />}
                      title={<a href="https://ant.design">{item.name.last}</a>}
                      description={item.email}
                    />
                    <Button type="primary" primary>
                      ดูรายละเอียด
                    </Button>
                  </List.Item>
                )}
              />
            </InfiniteScroll>
          </div>
        </div>
      </Content>
      <LayoutFooter />
    </Layout>
  );
}
