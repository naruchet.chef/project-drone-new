
import "antd/dist/antd.css";
import React, { useState, useEffect } from "react";
import LayoutHeader from "../layout/header";
import LayoutFooter from "../layout/footer";
import InfiniteScroll from "react-infinite-scroll-component";
import { HeartOutlined, PushpinOutlined, StopOutlined } from '@ant-design/icons';
import Link from "next/link";
import {
  Layout,
  Breadcrumb,
  List,
  Avatar,
  Skeleton,
  Divider,
  Progress,
  Input,
  Button,
  Row,
  Card,
  Col,
  Select,
  Space,
  Checkbox,
  Typography,
  Form, InputNumber,
  Badge,
} from "antd";

const { Content } = Layout;
const { Search } = Input;
const { Meta } = Card;
const { Option } = Select;
const { Title } = Typography;

export default function Pilot() {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);

  useEffect(() => {
    loadMoreData();
  }, []);

  const loadMoreData = () => {
    if (loading) {
      return;
    }
    setLoading(true);
    fetch(
      "https://randomuser.me/api/?results=10&inc=name,gender,email,nat,picture&noinfo"
    )
      .then((res) => res.json())
      .then((body) => {
        setData([...data, ...body.results]);
        setLoading(false);
      })
      .catch(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    loadMoreData();
  }, []);

  const form = {
    name: "",
    age: 0
  }



  return (
    <Layout>
      <LayoutHeader />
      <Content
        className="site-layout"
        style={{ padding: "0 50px", marginTop: 64 }}
      >
        <Breadcrumb style={{ margin: "16px 0" }}>
          <Breadcrumb.Item>หน้าหลัก</Breadcrumb.Item>
          <Breadcrumb.Item>ค้นหานักบิน</Breadcrumb.Item>
        </Breadcrumb>

        <img src="img/unsplash_pMFNnnODrKA.png" style={{ width: '100%' }} />
        <p style={{ marginBottom: '0px' }}>ค้นหานักบิน</p>
        <div
          className="site-layout-background"
          style={{ padding: 24, minHeight: 380 }}
        >
          <Row>
            <Col span={8}>
              <Title level={5}>สถานที่</Title>
              <Input style={{ width: "50%" }} placeholder="กรอกข้อมูลสถานที่" />
            </Col>
            <Col span={8}>
              <Title level={5}>ประเภทงาน</Title>
              <Select
                style={{ width: "50%" }}
                showSearch
                placeholder="เลือกประเภทงาน"
                optionFilterProp="children"
              >
                <Option value="1">ประเภทงาน 1</Option>
                <Option value="2">ประเภทงาน 2</Option>
                <Option value="3">ประเภทงาน 3</Option>
              </Select></Col>
            <Col span={8}>
              <Row style={{ marginTop: "30px" }}>
                <Col span={6}><Checkbox onChange={{}}>แสดงรัสมี</Checkbox></Col>
                <Col span={6}><Badge status="success" />5 กม.</Col>
                <Col span={6}><Badge status="warning" />15 กม.</Col>
                <Col span={6}><Badge status="error" />30 กม.</Col>
              </Row>
            </Col>
          </Row>
          <Row gutter={[32, 8]} style={{ marginTop: "20px" }}>
            <Col span={4} style={{ marginRight: "100px" }}>
              <Link href="/drone-pilot/detail">
                <Card
                  onClick={{}}
                  style={{ width: '294px', height: '350px', borderRadius: '14px' }}
                >
                  <Row gutter={[16, 24]}>
                    <Col className="gutter-row" span={6}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<StopOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                    <Col className="gutter-row" span={8} />
                    <Col className="gutter-row" span={5}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<HeartOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                    <Col className="gutter-row" span={5}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<PushpinOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                  </Row>
                  <center>
                    <img style={{
                      width: '88px',
                      height: '88px',
                      marginTop: '10%',
                      borderRadius: '50%',
                    }} src="img/photo-1488263590619-bc1fff43b6c1-1@1x.png" />
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '16px',
                      lineHeight: '19px',
                      marginTop: '10%',
                      color: 'rgba(0, 0, 0, 0.87)'
                    }}>สมชาย ใจดี</p>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}><img style={{
                      width: '14.17px',
                      height: '13.46px',
                      marginRight: '5px'
                    }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector.png" /> (9)  20 งาน</p>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}> <img style={{
                      width: '14.17px',
                      height: '13.46px',
                      marginRight: '5px'
                    }} src="img/vector2.png" />500,000 บาท license #1234567</p>
                    <label style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}>ทักษะการบิน</label>
                  </center>
                  <Progress percent={70} showInfo={false} />
                  <center>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)',
                      marginBottom: '100%'
                    }}>500 บาท/ชั่วโมง | กรุงเทพ,ประเทศไทย</p>
                  </center>
                </Card>
              </Link>
            </Col>
            <Col span={4} style={{ marginRight: "100px" }}>
              <Link href="/drone-pilot/detail">
                <Card
                  onClick={{}}
                  style={{ width: '294px', height: '350px', borderRadius: '14px' }}
                >
                  <Row gutter={[16, 24]}>
                    <Col className="gutter-row" span={6}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<StopOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                    <Col className="gutter-row" span={8} />
                    <Col className="gutter-row" span={5}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<HeartOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                    <Col className="gutter-row" span={5}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<PushpinOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                  </Row>
                  <center>
                    <img style={{
                      width: '88px',
                      height: '88px',
                      marginTop: '10%',
                      borderRadius: '50%',
                    }} src="img/photo-1488263590619-bc1fff43b6c1-1@1x.png" />
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '16px',
                      lineHeight: '19px',
                      marginTop: '10%',
                      color: 'rgba(0, 0, 0, 0.87)'
                    }}>สมชาย ใจดี</p>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}><img style={{
                      width: '14.17px',
                      height: '13.46px',
                      marginRight: '5px'
                    }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector.png" /> (9)  20 งาน</p>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}> <img style={{
                      width: '14.17px',
                      height: '13.46px',
                      marginRight: '5px'
                    }} src="img/vector2.png" />500,000 บาท license #1234567</p>
                    <label style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}>ทักษะการบิน</label>
                  </center>
                  <Progress percent={70} showInfo={false} />
                  <center>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)',
                      marginBottom: '100%'
                    }}>500 บาท/ชั่วโมง | กรุงเทพ,ประเทศไทย</p>
                  </center>
                </Card>
              </Link>
            </Col>
            <Col span={4} style={{ marginRight: "100px" }}>
              <Link href="/drone-pilot/detail">
                <Card
                  onClick={{}}
                  style={{ width: '294px', height: '350px', borderRadius: '14px' }}
                >
                  <Row gutter={[16, 24]}>
                    <Col className="gutter-row" span={6}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<StopOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                    <Col className="gutter-row" span={8} />
                    <Col className="gutter-row" span={5}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<HeartOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                    <Col className="gutter-row" span={5}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<PushpinOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                  </Row>
                  <center>
                    <img style={{
                      width: '88px',
                      height: '88px',
                      marginTop: '10%',
                      borderRadius: '50%',
                    }} src="img/photo-1488263590619-bc1fff43b6c1-1@1x.png" />
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '16px',
                      lineHeight: '19px',
                      marginTop: '10%',
                      color: 'rgba(0, 0, 0, 0.87)'
                    }}>สมชาย ใจดี</p>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}><img style={{
                      width: '14.17px',
                      height: '13.46px',
                      marginRight: '5px'
                    }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector.png" /> (9)  20 งาน</p>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}> <img style={{
                      width: '14.17px',
                      height: '13.46px',
                      marginRight: '5px'
                    }} src="img/vector2.png" />500,000 บาท license #1234567</p>
                    <label style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}>ทักษะการบิน</label>
                  </center>
                  <Progress percent={70} showInfo={false} />
                  <center>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)',
                      marginBottom: '100%'
                    }}>500 บาท/ชั่วโมง | กรุงเทพ,ประเทศไทย</p>
                  </center>
                </Card>
              </Link>
            </Col>
            <Col span={4} >
              <Link href="/drone-pilot/detail">
                <Card
                  onClick={{}}
                  style={{ width: '294px', height: '350px', borderRadius: '14px' }}
                >
                  <Row gutter={[16, 24]}>
                    <Col className="gutter-row" span={6}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<StopOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                    <Col className="gutter-row" span={8} />
                    <Col className="gutter-row" span={5}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<HeartOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                    <Col className="gutter-row" span={5}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<PushpinOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                  </Row>
                  <center>
                    <img style={{
                      width: '88px',
                      height: '88px',
                      marginTop: '10%',
                      borderRadius: '50%',
                    }} src="img/photo-1488263590619-bc1fff43b6c1-1@1x.png" />
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '16px',
                      lineHeight: '19px',
                      marginTop: '10%',
                      color: 'rgba(0, 0, 0, 0.87)'
                    }}>สมชาย ใจดี</p>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}><img style={{
                      width: '14.17px',
                      height: '13.46px',
                      marginRight: '5px'
                    }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector.png" /> (9)  20 งาน</p>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}> <img style={{
                      width: '14.17px',
                      height: '13.46px',
                      marginRight: '5px'
                    }} src="img/vector2.png" />500,000 บาท license #1234567</p>
                    <label style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}>ทักษะการบิน</label>
                  </center>
                  <Progress percent={70} showInfo={false} />
                  <center>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)',
                      marginBottom: '100%'
                    }}>500 บาท/ชั่วโมง | กรุงเทพ,ประเทศไทย</p>
                  </center>
                </Card>
              </Link>
            </Col>
          </Row>
          <Row gutter={[32, 8]} style={{ marginTop: "20px" }}>
            <Col span={4} style={{ marginRight: "100px" }}>
              <Link href="/drone-pilot/detail">
                <Card
                  onClick={{}}
                  style={{ width: '294px', height: '350px', borderRadius: '14px' }}
                >
                  <Row gutter={[16, 24]}>
                    <Col className="gutter-row" span={6}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<StopOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                    <Col className="gutter-row" span={8} />
                    <Col className="gutter-row" span={5}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<HeartOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                    <Col className="gutter-row" span={5}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<PushpinOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                  </Row>
                  <center>
                    <img style={{
                      width: '88px',
                      height: '88px',
                      marginTop: '10%',
                      borderRadius: '50%',
                    }} src="img/photo-1488263590619-bc1fff43b6c1-1@1x.png" />
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '16px',
                      lineHeight: '19px',
                      marginTop: '10%',
                      color: 'rgba(0, 0, 0, 0.87)'
                    }}>สมชาย ใจดี</p>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}><img style={{
                      width: '14.17px',
                      height: '13.46px',
                      marginRight: '5px'
                    }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector.png" /> (9)  20 งาน</p>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}> <img style={{
                      width: '14.17px',
                      height: '13.46px',
                      marginRight: '5px'
                    }} src="img/vector2.png" />500,000 บาท license #1234567</p>
                    <label style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}>ทักษะการบิน</label>
                  </center>
                  <Progress percent={70} showInfo={false} />
                  <center>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)',
                      marginBottom: '100%'
                    }}>500 บาท/ชั่วโมง | กรุงเทพ,ประเทศไทย</p>
                  </center>
                </Card>
              </Link>
            </Col>
            <Col span={4} style={{ marginRight: "100px" }}>
              <Link href="/drone-pilot/detail">
                <Card
                  onClick={{}}
                  style={{ width: '294px', height: '350px', borderRadius: '14px' }}
                >
                  <Row gutter={[16, 24]}>
                    <Col className="gutter-row" span={6}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<StopOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                    <Col className="gutter-row" span={8} />
                    <Col className="gutter-row" span={5}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<HeartOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                    <Col className="gutter-row" span={5}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<PushpinOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                  </Row>
                  <center>
                    <img style={{
                      width: '88px',
                      height: '88px',
                      marginTop: '10%',
                      borderRadius: '50%',
                    }} src="img/photo-1488263590619-bc1fff43b6c1-1@1x.png" />
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '16px',
                      lineHeight: '19px',
                      marginTop: '10%',
                      color: 'rgba(0, 0, 0, 0.87)'
                    }}>สมชาย ใจดี</p>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}><img style={{
                      width: '14.17px',
                      height: '13.46px',
                      marginRight: '5px'
                    }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector.png" /> (9)  20 งาน</p>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}> <img style={{
                      width: '14.17px',
                      height: '13.46px',
                      marginRight: '5px'
                    }} src="img/vector2.png" />500,000 บาท license #1234567</p>
                    <label style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}>ทักษะการบิน</label>
                  </center>
                  <Progress percent={70} showInfo={false} />
                  <center>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)',
                      marginBottom: '100%'
                    }}>500 บาท/ชั่วโมง | กรุงเทพ,ประเทศไทย</p>
                  </center>
                </Card>
              </Link>
            </Col>
            <Col span={4} style={{ marginRight: "100px" }}>
              <Link href="/drone-pilot/detail">
                <Card
                  onClick={{}}
                  style={{ width: '294px', height: '350px', borderRadius: '14px' }}
                >
                  <Row gutter={[16, 24]}>
                    <Col className="gutter-row" span={6}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<StopOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                    <Col className="gutter-row" span={8} />
                    <Col className="gutter-row" span={5}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<HeartOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                    <Col className="gutter-row" span={5}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<PushpinOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                  </Row>
                  <center>
                    <img style={{
                      width: '88px',
                      height: '88px',
                      marginTop: '10%',
                      borderRadius: '50%',
                    }} src="img/photo-1488263590619-bc1fff43b6c1-1@1x.png" />
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '16px',
                      lineHeight: '19px',
                      marginTop: '10%',
                      color: 'rgba(0, 0, 0, 0.87)'
                    }}>สมชาย ใจดี</p>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}><img style={{
                      width: '14.17px',
                      height: '13.46px',
                      marginRight: '5px'
                    }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector.png" /> (9)  20 งาน</p>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}> <img style={{
                      width: '14.17px',
                      height: '13.46px',
                      marginRight: '5px'
                    }} src="img/vector2.png" />500,000 บาท license #1234567</p>
                    <label style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}>ทักษะการบิน</label>
                  </center>
                  <Progress percent={70} showInfo={false} />
                  <center>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)',
                      marginBottom: '100%'
                    }}>500 บาท/ชั่วโมง | กรุงเทพ,ประเทศไทย</p>
                  </center>
                </Card>
              </Link>
            </Col>
            <Col span={4} >
              <Link href="/drone-pilot/detail">
                <Card
                  onClick={{}}
                  style={{ width: '294px', height: '350px', borderRadius: '14px' }}
                >
                  <Row gutter={[16, 24]}>
                    <Col className="gutter-row" span={6}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<StopOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                    <Col className="gutter-row" span={8} />
                    <Col className="gutter-row" span={5}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<HeartOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                    <Col className="gutter-row" span={5}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<PushpinOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                  </Row>
                  <center>
                    <img style={{
                      width: '88px',
                      height: '88px',
                      marginTop: '10%',
                      borderRadius: '50%',
                    }} src="img/photo-1488263590619-bc1fff43b6c1-1@1x.png" />
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '16px',
                      lineHeight: '19px',
                      marginTop: '10%',
                      color: 'rgba(0, 0, 0, 0.87)'
                    }}>สมชาย ใจดี</p>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}><img style={{
                      width: '14.17px',
                      height: '13.46px',
                      marginRight: '5px'
                    }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector.png" /> (9)  20 งาน</p>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}> <img style={{
                      width: '14.17px',
                      height: '13.46px',
                      marginRight: '5px'
                    }} src="img/vector2.png" />500,000 บาท license #1234567</p>
                    <label style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}>ทักษะการบิน</label>
                  </center>
                  <Progress percent={70} showInfo={false} />
                  <center>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)',
                      marginBottom: '100%'
                    }}>500 บาท/ชั่วโมง | กรุงเทพ,ประเทศไทย</p>
                  </center>
                </Card>
              </Link>
            </Col>
          </Row>
          <Row gutter={[32, 8]} style={{ marginTop: "20px" }}>
            <Col span={4} style={{ marginRight: "100px" }}>
              <Link href="/drone-pilot/detail">
                <Card
                  onClick={{}}
                  style={{ width: '294px', height: '350px', borderRadius: '14px' }}
                >
                  <Row gutter={[16, 24]}>
                    <Col className="gutter-row" span={6}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<StopOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                    <Col className="gutter-row" span={8} />
                    <Col className="gutter-row" span={5}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<HeartOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                    <Col className="gutter-row" span={5}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<PushpinOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                  </Row>
                  <center>
                    <img style={{
                      width: '88px',
                      height: '88px',
                      marginTop: '10%',
                      borderRadius: '50%',
                    }} src="img/photo-1488263590619-bc1fff43b6c1-1@1x.png" />
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '16px',
                      lineHeight: '19px',
                      marginTop: '10%',
                      color: 'rgba(0, 0, 0, 0.87)'
                    }}>สมชาย ใจดี</p>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}><img style={{
                      width: '14.17px',
                      height: '13.46px',
                      marginRight: '5px'
                    }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector.png" /> (9)  20 งาน</p>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}> <img style={{
                      width: '14.17px',
                      height: '13.46px',
                      marginRight: '5px'
                    }} src="img/vector2.png" />500,000 บาท license #1234567</p>
                    <label style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}>ทักษะการบิน</label>
                  </center>
                  <Progress percent={70} showInfo={false} />
                  <center>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)',
                      marginBottom: '100%'
                    }}>500 บาท/ชั่วโมง | กรุงเทพ,ประเทศไทย</p>
                  </center>
                </Card>
              </Link>
            </Col>
            <Col span={4} style={{ marginRight: "100px" }}>
              <Link href="/drone-pilot/detail">
                <Card
                  onClick={{}}
                  style={{ width: '294px', height: '350px', borderRadius: '14px' }}
                >
                  <Row gutter={[16, 24]}>
                    <Col className="gutter-row" span={6}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<StopOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                    <Col className="gutter-row" span={8} />
                    <Col className="gutter-row" span={5}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<HeartOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                    <Col className="gutter-row" span={5}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<PushpinOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                  </Row>
                  <center>
                    <img style={{
                      width: '88px',
                      height: '88px',
                      marginTop: '10%',
                      borderRadius: '50%',
                    }} src="img/photo-1488263590619-bc1fff43b6c1-1@1x.png" />
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '16px',
                      lineHeight: '19px',
                      marginTop: '10%',
                      color: 'rgba(0, 0, 0, 0.87)'
                    }}>สมชาย ใจดี</p>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}><img style={{
                      width: '14.17px',
                      height: '13.46px',
                      marginRight: '5px'
                    }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector.png" /> (9)  20 งาน</p>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}> <img style={{
                      width: '14.17px',
                      height: '13.46px',
                      marginRight: '5px'
                    }} src="img/vector2.png" />500,000 บาท license #1234567</p>
                    <label style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}>ทักษะการบิน</label>
                  </center>
                  <Progress percent={70} showInfo={false} />
                  <center>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)',
                      marginBottom: '100%'
                    }}>500 บาท/ชั่วโมง | กรุงเทพ,ประเทศไทย</p>
                  </center>
                </Card>
              </Link>
            </Col>
            <Col span={4} style={{ marginRight: "100px" }}>
              <Link href="/drone-pilot/detail">
                <Card
                  onClick={{}}
                  style={{ width: '294px', height: '350px', borderRadius: '14px' }}
                >
                  <Row gutter={[16, 24]}>
                    <Col className="gutter-row" span={6}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<StopOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                    <Col className="gutter-row" span={8} />
                    <Col className="gutter-row" span={5}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<HeartOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                    <Col className="gutter-row" span={5}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<PushpinOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                  </Row>
                  <center>
                    <img style={{
                      width: '88px',
                      height: '88px',
                      marginTop: '10%',
                      borderRadius: '50%',
                    }} src="img/photo-1488263590619-bc1fff43b6c1-1@1x.png" />
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '16px',
                      lineHeight: '19px',
                      marginTop: '10%',
                      color: 'rgba(0, 0, 0, 0.87)'
                    }}>สมชาย ใจดี</p>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}><img style={{
                      width: '14.17px',
                      height: '13.46px',
                      marginRight: '5px'
                    }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector.png" /> (9)  20 งาน</p>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}> <img style={{
                      width: '14.17px',
                      height: '13.46px',
                      marginRight: '5px'
                    }} src="img/vector2.png" />500,000 บาท license #1234567</p>
                    <label style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}>ทักษะการบิน</label>
                  </center>
                  <Progress percent={70} showInfo={false} />
                  <center>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)',
                      marginBottom: '100%'
                    }}>500 บาท/ชั่วโมง | กรุงเทพ,ประเทศไทย</p>
                  </center>
                </Card>
              </Link>
            </Col>
            <Col span={4} >
              <Link href="/drone-pilot/detail">
                <Card
                  onClick={{}}
                  style={{ width: '294px', height: '350px', borderRadius: '14px' }}
                >
                  <Row gutter={[16, 24]}>
                    <Col className="gutter-row" span={6}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<StopOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                    <Col className="gutter-row" span={8} />
                    <Col className="gutter-row" span={5}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<HeartOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                    <Col className="gutter-row" span={5}>
                      <Button
                        style={{ borderRadius: '10%' }}
                        icon={<PushpinOutlined />}
                        onClick={() => { }}
                      />
                    </Col>
                  </Row>
                  <center>
                    <img style={{
                      width: '88px',
                      height: '88px',
                      marginTop: '10%',
                      borderRadius: '50%',
                    }} src="img/photo-1488263590619-bc1fff43b6c1-1@1x.png" />
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '16px',
                      lineHeight: '19px',
                      marginTop: '10%',
                      color: 'rgba(0, 0, 0, 0.87)'
                    }}>สมชาย ใจดี</p>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}><img style={{
                      width: '14.17px',
                      height: '13.46px',
                      marginRight: '5px'
                    }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector1.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector.png" />
                      <img style={{
                        width: '14.17px',
                        height: '13.46px',
                        marginRight: '5px'
                      }} src="img/vector.png" /> (9)  20 งาน</p>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}> <img style={{
                      width: '14.17px',
                      height: '13.46px',
                      marginRight: '5px'
                    }} src="img/vector2.png" />500,000 บาท license #1234567</p>
                    <label style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)'
                    }}>ทักษะการบิน</label>
                  </center>
                  <Progress percent={70} showInfo={false} />
                  <center>
                    <p style={{
                      fontStyle: 'normal',
                      fontFamily: 'Prompt',
                      fontWeight: '600',
                      fontSize: '11px',
                      lineHeight: '13px',
                      color: 'rgba(0, 0, 0, 0.38)',
                      marginBottom: '100%'
                    }}>500 บาท/ชั่วโมง | กรุงเทพ,ประเทศไทย</p>
                  </center>
                </Card>
              </Link>
            </Col>
          </Row>

        </div>
      </Content>
      <LayoutFooter />
    </Layout >
  );
}
