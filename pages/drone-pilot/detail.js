import "antd/dist/antd.css";
import React, { useState, useEffect } from "react";
import LayoutHeader from "../layout/header";
import LayoutFooter from "../layout/footer";
import {
  Tabs,
  Layout,
  Breadcrumb,
  List,
  Avatar,
  Skeleton,
  Divider,
  Progress,
  Input,
  Button,
  Row,
  Card,
  Col,
  Select,
  Space,
  Checkbox,
  Typography,
  Form,
  InputNumber,
  Badge,
  Upload,
  message,
} from "antd";
import { LoadingOutlined, PlusOutlined } from "@ant-design/icons";

const { Content } = Layout;
const { TabPane } = Tabs;
// const layout = {
//   labelCol: { span: 8 },
//   wrapperCol: { span: 16 },
// };

export default function Pilot() {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);
  const [imageUrl, setImageUrl] = useState([]);

  useEffect(() => {
    loadMoreData();
  }, []);

  const loadMoreData = () => {
    if (loading) {
      return;
    }
    setLoading(true);
    fetch(
      "https://randomuser.me/api/?results=10&inc=name,gender,email,nat,picture&noinfo"
    )
      .then((res) => res.json())
      .then((body) => {
        setData([...data, ...body.results]);
        setLoading(false);
      })
      .catch(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    loadMoreData();
  }, []);

  const form = {
    name: "",
    age: 0,
  };

  function callback(key) {
    console.log(key);
  }
  const validateMessages = {
    required: "${label} is required!",
    // types: {
    //   email: '${label} is not a valid email!',
    //   number: '${label} is not a valid number!',
    // },
    // number: {
    //   range: '${label} must be between ${min} and ${max}',
    // },
  };

  const onFinish = (values) => {
    console.log(values);
  };

  function beforeUpload(file) {
    const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
    if (!isJpgOrPng) {
      message.error("You can only upload JPG/PNG file!");
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error("Image must smaller than 2MB!");
    }
    return isJpgOrPng && isLt2M;
  }

  const handleChange = (info) => {
    if (info.file.status === "uploading") {
      // setLoading(true)
      return;
    }
    if (info.file.status === "done") {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, (imageUrl) =>
        // setLoading(false)
        setImageUrl(imageUrl)
      );
    }
  };

  return (
    <Layout>
      <LayoutHeader />
      <Content
        className="site-layout"
        style={{ padding: "0 50px", marginTop: 64 }}
      >
        <div
          className="site-layout-background"
          style={{ padding: 24, minHeight: 380 }}
        >
          <Tabs
            style={{
              fontFamily: "Prompt",
              fontStyle: "normal",
              fontWeight: "500",
              fontSize: "22px",
              lineHeight: "26px",
              color: "#689FF2",
            }}
            type="card"
          >
            <TabPane tab="ข้อมูลบัญชี" key="1">
              <p
                style={{
                  fontWeight: "700",
                  fontSize: "28px",
                  lineHeight: "33px",
                  color: "#7F7F7F",
                }}
              >
                ตั้งค่าบัญชี
              </p>
              <Form
                name="nest-messages"
                onFinish={onFinish}
                validateMessages={validateMessages}
              >
                <Row gutter={[32, 8]}>
                  <Col span={12}>
                    <Form.Item
                      name={["user", "name"]}
                      rules={[{ required: true }]}
                    >
                      <label>
                        ชื่อ <text style={{ color: "red" }}>*</text>
                      </label>
                      <Input style={{ background: "#F8FAFC" }} />
                    </Form.Item>
                  </Col>
                  <Col span={12}>
                    <Form.Item
                      name={["user", "lastname"]}
                      rules={[{ required: true }]}
                    >
                      <label>
                        นามสกุล <text style={{ color: "red" }}>*</text>
                      </label>
                      <Input style={{ background: "#F8FAFC" }} />
                    </Form.Item>
                  </Col>
                  <Col span={12}>
                    <Form.Item
                      name={["user", "email"]}
                      rules={[{ required: true }]}
                    >
                      <label>
                        อีเมล <text style={{ color: "red" }}>*</text>
                      </label>
                      <Input style={{ background: "#F8FAFC" }} />
                    </Form.Item>
                  </Col>
                  <Col span={12}>
                    <Form.Item
                      name={["user", "account"]}
                      rules={[{ required: true }]}
                    >
                      <label>
                        ชื่อบัญชี <text style={{ color: "red" }}>*</text>
                      </label>
                      <Input style={{ background: "#F8FAFC" }} />
                    </Form.Item>
                  </Col>
                  <Col span={12}>
                    <label>เพิ่มรูปโปรไฟล์ของคุณ </label>
                    <Upload
                      name="avatar"
                      listType="picture-card"
                      className="avatar-uploader"
                      showUploadList={false}
                      action="https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png"
                      beforeUpload={beforeUpload}
                      onChange={handleChange}
                    >
                      {`https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png` ? (
                        <img
                          src={`https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png`}
                          alt="avatar"
                          style={{ width: "100%" }}
                        />
                      ) : (
                        uploadButton
                      )}
                    </Upload>
                  </Col>
                </Row>
                <Form.Item>
                  <center>
                    <Button
                      style={{ marginRight: "10px", borderRadius: "10px" }}
                      type="primary"
                      htmlType="submit"
                    >
                      บันทึก
                    </Button>
                    <Button
                      style={{ background: "#7BC67E", borderRadius: "10px" }}
                      type="default"
                    >
                      <text style={{ color: "white" }}>เปลี่ยนรหัสผ่าน</text>
                    </Button>
                  </center>
                </Form.Item>
              </Form>
            </TabPane>
            <TabPane tab="ข้อมูลนักบิน" key="2"></TabPane>
            <TabPane tab="ใบอนุญาต" key="3"></TabPane>
            <TabPane tab="บริการ & ราคา" key="4"></TabPane>
            <TabPane tab="วันทำการ" key="5"></TabPane>
            <TabPane tab="โดรน & อุปกรณ์" key="6"></TabPane>
            <TabPane tab="ลูกค้าที่ถูกบล็อค" key="7"></TabPane>
          </Tabs>
        </div>
      </Content>
      <LayoutFooter />
    </Layout>
  );
}
