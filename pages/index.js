import React, { useState } from "react";
import { Layout, Row, Col, Input, Button, Modal, Space } from 'antd';
// import { DownloadOutlined } from "@ant-design/icons";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import LayoutHeader from '../components/layout/header'
import LayoutFooter from '../components/layout/footer'
import { useRouter } from "next/router";

const { Content, Footer } = Layout;

const sliderSettings = {
    dots: false,
    arrows: false,
    lazyLoad: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    initialSlide: 0,
    autoplay: true,
    speed: 7000,
    autoplaySpeed: 3000,
};

const sliderSettings2 = {
    dots: false,
    arrows: false,
    lazyLoad: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    initialSlide: 2,
    autoplay: true,
    speed: 3000,
    autoplaySpeed: 3000,
};

const sliderSettings3 = {
    dots: false,
    arrows: false,
    fade: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    initialSlide: 2,
    autoplay: true,
    speed: 3000,
    autoplaySpeed: 3000,
};
export default function PageHeader() {
    const router = useRouter();
    const [accountType, setAccountType] = useState("user");
    const [visible, setVisible] = useState(false);

    return (
        <Layout>
            <LayoutHeader />
            <Content
                className="site-layout"
                style={{
                    padding: "64px 0px",
                }}
            >
                {/* <div className="home-content-bar" id="know-kh"> */}
                <Row justify="center" align="middle" style={{
                    backgroundImage: "url(img/brand_2.png)",
                    height: "80vh",
                    backgroundSize: "cover"
                }}>
                    <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                        {/* <Row justify="space-around" align="middle">
                            <Col span={24} className="text-center margin-top-100">
                                <Input
                                    className="ant-input-round "
                                    placeholder="ค้นหา"
                                    style={{
                                        width: "40%",
                                        // left: "30%",
                                        // top: "50%",
                                        height: "45px"
                                    }}
                                />
                            </Col>
                        </Row> */}
                        <Row justify="space-around" align="middle">
                            <Col span={24} className="text-center">
                                <div className="sut-platform-drone margin-top-50">SUT Platform Drone</div>
                                <div className="text_label margin-top-20">
                                    เครือข่ายนักบินโดรนเชิงพาณิชย์ที่ใหญ่<br/>ที่สุดในประเทศไทย
                                </div>
                                <div className=" margin-top-20">
                                <Button 
                                    onClick={() => {
                                        router.push('/login');
                                    }}
                                    type="primary" size={`large`}>
                                    เข้าสู่ระบบ
                                </Button> &nbsp;
                                <Button 
                                    onClick={() => {
                                        router.push('/register');
                                    }}
                                    type="default" size={`large`}>
                                    ลงทะเบียน
                                </Button>
                                </div>
                            </Col>
                        </Row>
                        {/* </div> */}
                    </Col>
                </Row>
                {/* </div> */}
                {/* <div className='main-wrapper margin-top-50' > */}
                <Row justify="center" align="top" className='main-wrapper margin-top-50'>
                    <Col span={20}>
                        <Row justify="center" align="top">
                            <Col className='text-center' xs={24} sm={24} md={12} lg={6} xl={6}>
                                <p className="roboto-bold-gray-24px">บริการของเรา</p>
                                <span className="normalTxt" >จับคู่นักบินที่ดีที่สุดสำหรับความต้องการและงบประมาณของคุณ</span>
                            </Col>
                        </Row>

                        <Row justify="center" align="top" gutter={[24, 24]} className="margin-top-20">
                            <Col className='text-center' xs={24} sm={24} md={12} lg={6} xl={6}>
                                <div className="">
                                    <img className="frame-16 image-box-shadow border-radius" src="img/unsplash_a1.png" />
                                    <div className="roboto-bold-gray-24px mt-top-25">บริการของเรา</div>
                                    <div className="normalTxt">
                                        เราช่วยคุณค้นหานักบินโดรนที่มีคุณสมบัติตรงกับงานของคุณและช่วยให้นักบินโดรนค้นพบกับงานที่ต้องการได้รวมถึงให้ความสะดวกในด้านการชำระเงินด้วยแพลตฟอร์มที่มีความปลอดภัย เพื่อชำระค่าจ้างให้กับนักบิน
                                    </div>
                                </div>
                            </Col>
                            <Col className='text-center' xs={24} sm={24} md={12} lg={6} xl={6}>
                                <div className="">
                                    <img className="frame-16 image-box-shadow border-radius" src="img/unsplash_a2.png" />
                                    <div className="roboto-bold-gray-24px mt-top-25">โพสงาน</div>
                                    <div className="normalTxt">
                                        เปรียบเทียบราคาและพอร์ตการลงทุนเพื่อช่วยในการจ้างนักบินที่ดีที่สุดสำหรับความต้องการและงบประมาณของคุณ
                                        นักบินทุกคนได้รับการตรวจสอบใบอนุญาตที่เหมาะสมในการใช้งานโดรนอย่างถูกกฎหมาย
                                    </div>
                                </div>
                            </Col>
                            <Col className='text-center' xs={24} sm={24} md={12} lg={6} xl={6}>
                                <div className="">
                                    <img className="frame-16 image-box-shadow border-radius" src="img/unsplash_a3.png" />
                                    <div className="roboto-bold-gray-24px mt-top-25">ค้นหานักบิน</div>
                                    <div className="normalTxt">
                                        เราจะจับคู่คุณกับนักบินในท้องถิ่นที่มีความสามารถที่จะทำให้งานถ่ายภาพของคุณออกมาดีที่สุด
                                    </div>
                                </div>
                            </Col>
                        </Row>

                        <Row justify="center" className="margin-top-100">
                            <Col span={24}>
                                <Slider {...sliderSettings2}>
                                    <div>
                                        <Row justify="center" gutter={[16, 8]}>
                                            <Col xs={24} sm={24} md={18} lg={18} xl={18}>
                                                <div className="frame-57 margin-10">
                                                    <img className="image-29 image-box-shadow border-radius" src="img/image-29@1x.png" />
                                                </div>
                                            </Col>
                                            <Col xs={24} sm={24} md={6} lg={6} xl={6}>
                                                <Row justify="center" align="middle" gutter={[8, 48]}>
                                                    <Col xs={12} sm={12} md={24} lg={24} xl={24}>
                                                        <div className="frame-57 margin-10">
                                                            <img className="image-32 image-box-shadow border-radius" src="img/image-32@2x.png" />
                                                        </div>
                                                    </Col>
                                                    <Col xs={12} sm={12} md={24} lg={24} xl={24}>
                                                        <div className="frame-57 margin-10">
                                                            <img className="image-32 image-box-shadow border-radius" src="img/image-33@2x.png" />
                                                        </div>
                                                    </Col>
                                                </Row>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                                                <div className="roboto-bold-gray-24px" style={{ marginTop: "10px", marginLeft: "30px" }}>
                                                    Cinematic Aerial Photography
                                                </div>
                                                <div className="normalTxt" style={{ marginTop: "10px", marginLeft: "30px" }}>
                                                    การถ่ายภาพหรือวิดีโอจากอากาศยานหรือโดรนโดยทั่วไปแล้วการถ่ายภาพทางอากาศจะใช้ในการสร้างช็อตติดตามช็อตและซีเควนซ์แอ็กชันในการทำภาพยนตร์เพื่อเพิ่มมุมมองภาพให้ดูมีความน่าสนใจมาก
                                                    ยิ่งขึ้น
                                                </div>
                                            </Col>
                                        </Row>
                                    </div>

                                    <div>
                                        <Row justify="center" gutter={[16, 8]}>
                                            <Col xs={24} sm={24} md={18} lg={18} xl={18}>
                                                <div className="frame-57 margin-10">
                                                    <img className="image-29 image-box-shadow border-radius" src="img/image-28.png" />
                                                </div>
                                            </Col>
                                            <Col xs={24} sm={24} md={6} lg={6} xl={6}>
                                                <Row justify="center" align="middle" gutter={[8, 48]}>
                                                    <Col xs={12} sm={12} md={24} lg={24} xl={24}>
                                                        <div className="frame-57 margin-10">
                                                            <img className="image-32 image-box-shadow border-radius" src="img/image-24.png" />
                                                        </div>
                                                    </Col>
                                                    <Col xs={12} sm={12} md={24} lg={24} xl={24}>
                                                        <div className="frame-57 margin-10">
                                                            <img className="image-32 image-box-shadow border-radius" src="img/image-15.png" />
                                                        </div>
                                                    </Col>
                                                </Row>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                                                <div className="roboto-bold-gray-24px" style={{ marginTop: "10px", marginLeft: "30px" }}>
                                                    Orthophoto
                                                </div>
                                                <div className="normalTxt" style={{ marginTop: "10px", marginLeft: "30px" }}>
                                                    ภาพถ่ายทางอากาศที่มีความละเอียดสูง ผ่านกระบวนการสำรวจรังวัดด้วยภาพถ่ายทางอากาศ (Photogrammetry)
                                                    ด้วยการปรับแก้แบบออร์โธ (Orthorectification) ที่ทำให้ตำแหน่ง ขนาด รูปร่างของวัตถุ
                                                    และภูมิประเทศที่ปรากฏบนภาพที่ปรับแก้แล้ว มีความถูกต้อง สามารถวัดตำแหน่งระยะทางที่แท้จริงได้
                                                </div>
                                            </Col>
                                        </Row>
                                    </div>

                                    <div>
                                        <Row justify="center" gutter={[16, 8]}>
                                            <Col xs={24} sm={24} md={18} lg={18} xl={18}>
                                                <div className="frame-57 margin-10">
                                                    <img className="image-29 image-box-shadow border-radius" src="img/image-32.png" />
                                                </div>
                                            </Col>
                                            <Col xs={24} sm={24} md={6} lg={6} xl={6}>
                                                <Row justify="center" align="middle" gutter={[8, 48]}>
                                                    <Col xs={12} sm={12} md={24} lg={24} xl={24}>
                                                        <div className="frame-57 margin-10">
                                                            <img className="image-32 image-box-shadow border-radius" src="img/image-27.png" />
                                                        </div>
                                                    </Col>
                                                    <Col xs={12} sm={12} md={24} lg={24} xl={24}>
                                                        <div className="frame-57 margin-10">
                                                            <img className="image-32 image-box-shadow border-radius" src="img/image-26.png" />
                                                        </div>
                                                    </Col>
                                                </Row>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                                                <div className="roboto-bold-gray-24px" style={{ marginTop: "10px", marginLeft: "30px" }}>
                                                    {"Digital Surface Model (DSM) & Digital Terrain Model (DTM)"}
                                                </div>
                                                <div className="normalTxt" style={{ marginTop: "10px", marginLeft: "30px" }}>
                                                    การจำลองความสูงของภูมิประเทศและจัดเก็บให้อยู่ในรูปแบบตารางกริดหรือข้อมูลแรสเตอร์โดยรวม
                                                    ความสูงของสิ่งปกคลุมพื้นผิวทางกายภาพของโลกด้วย(DSM)
                                                    และการกำจัดความสูงของสิ่งปกคลุมพื้นผิวทางกายภาพของโลกออก (DTM)
                                                </div>
                                            </Col>
                                        </Row>
                                    </div>

                                    <div>
                                        <Row justify="center" gutter={[16, 8]}>
                                            <Col xs={24} sm={24} md={18} lg={18} xl={18}>
                                                <div className="frame-57 margin-10">
                                                    <img className="image-29 image-box-shadow border-radius" src="img/image-18.png" />
                                                </div>
                                            </Col>
                                            <Col xs={24} sm={24} md={6} lg={6} xl={6}>
                                                <Row justify="center" align="middle" gutter={[8, 48]}>
                                                    <Col xs={12} sm={12} md={24} lg={24} xl={24}>
                                                        <div className="frame-57 margin-10">
                                                            <img className="image-32 image-box-shadow border-radius" src="img/image-30.png" />
                                                        </div>
                                                    </Col>
                                                    <Col xs={12} sm={12} md={24} lg={24} xl={24}>
                                                        <div className="frame-57 margin-10">
                                                            <img className="image-32 image-box-shadow border-radius" src="img/image-31.png" />
                                                        </div>
                                                    </Col>
                                                </Row>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                                                <div className="roboto-bold-gray-24px" style={{ marginTop: "10px", marginLeft: "30px" }}>
                                                    Point Cloud 3D Model
                                                </div>
                                                <div className="normalTxt" style={{ marginTop: "10px", marginLeft: "30px" }}>
                                                    กลุ่มของจุดสามมิติ การเก็บค่าของตำแหน่งของขอบของวัตถุในพิกัด X,Y,Z
                                                    เพื่อใช้ในการประมวลผลข้อมูลและพอยต์คลาวด์สามารถเก็บได้ในรูปแบบของฐานข้อมูล หรือ ไฟล์ในลักษณะ CSV
                                                </div>
                                            </Col>
                                        </Row>
                                    </div>

                                    <div>
                                        <Row justify="center" gutter={[16, 8]}>
                                            <Col xs={24} sm={24} md={18} lg={18} xl={18}>
                                                <div className="frame-57 margin-10">
                                                    <img className="image-29 image-box-shadow border-radius" src="img/image-15_2.png" />
                                                </div>
                                            </Col>
                                            <Col xs={24} sm={24} md={6} lg={6} xl={6}>
                                                <Row justify="center" align="middle" gutter={[8, 48]}>
                                                    <Col xs={12} sm={12} md={24} lg={24} xl={24}>
                                                        <div className="frame-57 margin-10">
                                                            <img className="image-32 image-box-shadow border-radius" src="img/image-14.png" />
                                                        </div>
                                                    </Col>
                                                    <Col xs={12} sm={12} md={24} lg={24} xl={24}>
                                                        <div className="frame-57 margin-10">
                                                            <img className="image-32 image-box-shadow border-radius" src="img/image-16.png" />
                                                        </div>
                                                    </Col>
                                                </Row>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                                                <div className="roboto-bold-gray-24px" style={{ marginTop: "10px", marginLeft: "30px" }}>
                                                    Contour
                                                </div>
                                                <div className="normalTxt" style={{ marginTop: "10px", marginLeft: "30px" }}>
                                                    แผนที่บอกระดับความสูงต่ำของพื้นที่ด้วยเส้นระดับหลาย ๆ เส้น แต่ละเส้นลากเชื่อมโยงจุดต่าง ๆ ในระดับเดียวกัน โดยที่จะระบุความต่างในระดับชั้น (Contour interval) ไว้ที่ส่วนใดส่วนหนึ่งของแผนที่ มีการใส่ค่าระดับและสัญญลักษณ์ต่าง ๆ ที่เกี่ยวข้องกับการปรับระดับ ใช้ประโยชน์ในด้านโยธา สร้างถนน ปรับระดับพื้นที่งานก่อสร้าง เป็นต้น
                                                </div>
                                            </Col>
                                        </Row>
                                    </div>
                                </Slider>
                            </Col>
                        </Row>

                        <Row justify="center" className="margin-top-100">
                            <Col xs={24} sm={24} md={24} lg={12} xl={12}>
                                <Row className="margin-20">
                                    <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                                        <img className="image-box-shadow border-radius" style={{ width: "100%" }} src="img/unsplash-xkkcui44im0@1x.png" />
                                    </Col>
                                </Row>
                                <Row className="margin-20" gutter={[32, 16]}>
                                    <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                        <div className="roboto-bold-gray-18px">สะดวก ประหยัดเวลา</div>
                                        <div className="roboto-normal-gray-14px">
                                            เราจะจับคู่คุณกับนักบินในท้องถิ่นที่มีความสามารถและสนใจที่จะจับภาพวิดีโอของคุณทันที
                                        </div>
                                    </Col>
                                    <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                        <div className="roboto-bold-gray-18px">ค้นหาได้ตามความต้องการ</div>
                                        <div className="roboto-normal-gray-14px">
                                            เราจะจับคู่คุณกับนักบินในท้องถิ่นที่มีความสามารถและสนใจที่จะจับภาพวิดีโอของคุณทันที
                                        </div>
                                    </Col>
                                    <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                        <div className="roboto-bold-gray-18px">ได้ผลงานของระดับมืออาชีพ</div>
                                        <div className="roboto-normal-gray-14px">
                                            เราจะจับคู่คุณกับนักบินในท้องถิ่นที่มีความสามารถ<br />และสนใจที่จะจับภาพวิดีโอของคุณทันที
                                        </div>
                                    </Col>
                                    <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                                        <div className="roboto-bold-gray-18px">ระบบชำระเงินที่ปลอดภัย</div>
                                        <div className="roboto-normal-gray-14px">
                                            เราจะจับคู่คุณกับนักบินในท้องถิ่นที่มีความสามารถ<br />และสนใจที่จะจับภาพวิดีโอของคุณทันที
                                        </div>
                                    </Col>
                                </Row>
                            </Col>
                            <Col xs={24} sm={24} md={24} lg={12} xl={12}>
                                <Slider {...sliderSettings3}>
                                    <img className="frame-20-f2xH2W" src="img/image-fade-1.png" />
                                    <img className="frame-20-f2xH2W" src="img/image-fade-2.png" />
                                    <img className="frame-20-f2xH2W" src="img/image-fade-3.png" />
                                    <img className="frame-20-f2xH2W" src="img/image-fade-4.png" />
                                    <img className="frame-20-f2xH2W" src="img/image-fade-5.png" />
                                    <img className="frame-20-f2xH2W" src="img/image-fade-6.png" />
                                </Slider>
                            </Col>
                        </Row>

                        <Row justify="center" className="margin-top-100">
                            <Col span={24} className="text-center">
                                <div className="rectangle-16">
                                    <div className="component-partner">
                                        <div className="text-label-recommend">ลูกค้าและนักบินของเรา</div>
                                        <div className="frame-slider-partner" style={{ paddingTop: "30px", width: "50%", margin: "auto" }}>
                                            <div className="frame-4-aAdzYi" style={{ paddingBottom: "30px" }}>
                                                <Slider {...sliderSettings}>
                                                    <div className="roboto-bold-white-18px">
                                                        งานดีๆรอบตัว. การสื่อสารเป็นไปอย่างสมบูรณ์แบบ แอนดรูว์ที่ FlightScan 360
                                                        <br />ไปถึงสนามอย่างรวดเร็วเพื่อถ่ายรูป รูปถ่ายตรงกับที่ฉันขอและมีคุณภาพดี ฉันจะจ้างอีกครั้ง
                                                    </div>
                                                    <div className="roboto-bold-white-18px">
                                                        วีดีโองานแต่งของฉันออกมาดูดีจริงๆ ครอบครัวฉันประทับใจมาก งานแต่งน้องสาวฉันจะใช้บริการอีกแน่นอน
                                                    </div>
                                                    <div className="roboto-bold-white-18px">
                                                        ขอบคุณเว็บดีๆ ที่ช่วยให้ผมได้บินในงานที่ถนัด มีลูกค้าติดต่อเข้ามาไม่ขาดสาย<br/>ผมจะทำทุกงานให้ดีที่สุด
                                                    </div>
                                                    <div className="roboto-bold-white-18px">
                                                        งานดีๆรอบตัว. การสื่อสารเป็นไปอย่างสมบูรณ์แบบ แอนดรูว์ที่ FlightScan 360
                                                        <br />ไปถึงสนามอย่างรวดเร็วเพื่อถ่ายรูป รูปถ่ายตรงกับที่ฉันขอและมีคุณภาพดี ฉันจะจ้างอีกครั้ง
                                                    </div>
                                                </Slider>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                        </Row>

                        <Row justify="center" className="margin-top-50">
                            <Col span={24} className="text-center">
                                <div className="rectangle-310" style={{ display: "flex", margin: "auto" }}>
                                    <a onClick={() => { setAccountType("user") }} style={{ width: "50%", background: (accountType == "user" ? "#689FF2" : "#FFFFFF"), borderRadius: "25px 0px 0px 25px", textAlign: "center" }}>
                                        <div
                                        // style={{ width: "50%", background: "#689FF2", borderRadius: "25px 0px 0px 25px", textAlign: "center" }}
                                        >
                                            <p className="text-s18" style={{ marginTop: "revert", fontFamily: "Prompt" }}>ลูกค้า</p>
                                        </div>
                                    </a>
                                    <a onClick={() => { setAccountType("pilot") }} style={{ width: "50%", background: (accountType == "pilot" ? "#689FF2" : "#FFFFFF"), borderRadius: "0px 25px 25px 0px", textAlign: "center" }}>
                                        <div
                                        // style={{ width: "50%", background: "#FFFFFF", borderRadius: "0px 25px 25px 0px", textAlign: "center" }}
                                        >
                                            <p className="text-s18" style={{ marginTop: "revert", fontFamily: "Prompt" }}>นักบิน</p>
                                        </div>
                                    </a>
                                </div>
                            </Col>
                        </Row>

                        {/* <Row justify="center" className="margin-top-100" gutter={[8, 8]}> */}
                        {accountType == "user" ?
                            <Row justify="center" className="margin-top-100" gutter={[32, 16]}>
                                <Col xs={12} sm={12} md={12} lg={6} xl={6}>
                                    <div className="">
                                        <img className="image-box-shadow border-radius image-w-100" src="img/rectangle-35.png" style={{ minHeight: "226px" }} />
                                        <div className="roboto-bold-gray-18px margin-top-20">ภาพมุมสูง สร้างความน่าสนใจให้กับทริป</div>
                                        <div className="roboto-normal-gray-14px">
                                            หลายคนที่ท่องเที่ยวอาจจะเกิดคำถามว่า การถ่ายภาพมุมสูง ๆ จำเป็นไหมสำหรับการท่องเที่ยว ถ้าหากว่าเราต้องการคุณภาพและมุมมองที่น่าสนใจสำหรับทริปเรา แล้วเราอยากนำเสนอ ไม่ว่าจะเป็นการทำให้ Content ใน Facebook Page หรือ Blog เราดูน่าสนใจ การเลือกภาพมุมสูงมานำเสนอทำให้ภาพลักษณ์ของเพจ เราเปลี่ยนไปได้เลย...
                                        </div>
                                        <div style={{ textAlign: "end" }}>
                                            <a className="text-right" onClick={() => setVisible(true)}>อ่านต่อ...</a>
                                        </div>
                                    </div>
                                </Col>
                                <Col xs={12} sm={12} md={12} lg={6} xl={6}>
                                    <div className="">
                                        <img className="image-box-shadow border-radius image-w-100" src="img/rectangle-25.png" style={{ minHeight: "226px" }} />
                                        <div className="roboto-bold-gray-18px margin-top-20">10 สถานที่เที่ยวธรรมชาติที่ฮิตที่สุด</div>
                                        <div className="roboto-normal-gray-14px">
                                            ประเทศไทยเป็นเมืองท่องเที่ยวยอดนิยม ที่นักท่องเที่ยวจากทั่วโลกอยากมาเที่ยวกันมากที่สุด จึงไม่น่าแปลกใจที่การจัดอันดับสถานที่ท่องเที่ยวต่างๆทั่วโลกมักจะมีที่เที่ยวชื่อดังในเมืองไทยติดอันดับด้วยเสมอ อีกทั้งสถานที่ท่องเที่ยวในไทยอีกหลายที่นอกจากจะติดอันดับโลกแล้ว การจัดอันดับในไทยก็ยังเป็นที่สุดอีกด้วย มีที่ไหนบ้าง มาดูกันเลยค่า...
                                        </div>
                                        <div style={{ textAlign: "end" }}>
                                            <a className="text-right" onClick={() => setVisible(true)}>อ่านต่อ...</a>
                                        </div>
                                    </div>
                                </Col>
                                <Col xs={12} sm={12} md={12} lg={6} xl={6}>
                                    <div className="">
                                        <img className="image-box-shadow border-radius image-w-100" src="img/rectangle-6.png" style={{ minHeight: "226px" }} />
                                        <div className="roboto-bold-gray-18px margin-top-20">5 สถานที่จัดงานแต่งงานสุดปัง!</div>
                                        <div className="roboto-normal-gray-14px">
                                            งานแต่งงานสำหรับใครหลายๆ คนนั้นเป็นเรื่องที่ยิ่งใหญ่ และสำหรับบางคนแล้วนั้นอาจจะเป็นวันสำคัญเพียงครั้ง เดียวในชีวิตก็ได้เพราะฉะนั้นต้องมีการวางแผนอย่าง ละเอียดรอบคอบเพื่อที่จะทำให้งานดำเนินไปอย่าง เรียบร้อยและราบรื่นซึ่งสถานที่แต่งงานจึงเป็นสิ่งสำคัญ แรกๆที่คู่รักส่วนใหญ่นึกถึงว่าเราต้องเลือกสไตล์ไหนที่จะ สะดวก เหมาะสม ...
                                        </div>
                                        <div style={{ textAlign: "end" }}>
                                            <a className="text-right" onClick={() => setVisible(true)}>อ่านต่อ...</a>
                                        </div>
                                    </div>
                                </Col>
                                <Col xs={12} sm={12} md={12} lg={6} xl={6}>
                                    <div className="">
                                        <img className="image-box-shadow border-radius image-w-100" src="img/rectangle-5.png" style={{ minHeight: "226px" }} />
                                        <div className="roboto-bold-gray-18px margin-top-20">ข้อดีของการทำแผนที่ด้วยโดรน</div>
                                        <div className="roboto-normal-gray-14px">
                                            การสำรวจพื้นที่เป็นสิ่งที่สำคัญมากในการทำโครงการ ต่างๆโดยเฉพาะโครงการขนาดใหญ่ที่มีรายละเอียดจำนวนมากซึ่งบริการทำแผนที่ทางอากาศก็เป็นอีกหนึ่งบริการที่หลายคนเลือกใช้เนื่องจากสามารถทำให้เห็นภาพรวมของพื้นที่โครงการได้เป็นอย่างดี และในปัจจุบันได้มี บริการทำแผนที่ทางอากาศด้วยโดรนเข้ามา...
                                        </div>
                                        <div style={{ textAlign: "end" }}>
                                            <a className="text-right" onClick={() => setVisible(true)}>อ่านต่อ...</a>
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                            :
                            <Row justify="center" className="margin-top-100" gutter={[32, 16]}>
                                <Col xs={12} sm={12} md={12} lg={6} xl={6}>
                                    <div className="">
                                        <div>
                                            <img className="image-box-shadow border-radius image-w-100" src="img/rectangle-4.png" style={{ minHeight: "226px" }} />
                                        </div>
                                        <div className="roboto-bold-gray-18px margin-top-20">ประเภทของโดรน</div>
                                        <div className="roboto-normal-gray-14px">
                                            เกร็ดความรู้เรื่องโดรนในวันนี้เราจะพาทุกคนไปทำความ รู้จักกับประเภทต่างๆของโดรนว่านอกจากโดรนถ่ายภาพ ที่เราๆคุ้นเคยกันนั้นจริงๆแล้วยังมีโดรนอะไรอย่างอื่นอีก บ้างและแต่ละประเภทมีรูปแบบการใช้งานอย่างไรครับ ...
                                        </div>
                                        <div style={{ textAlign: "end" }}>
                                            <a className="text-right" onClick={() => setVisible(true)}>อ่านต่อ...</a>
                                        </div>
                                    </div>
                                </Col>
                                <Col xs={12} sm={12} md={12} lg={6} xl={6}>
                                    <div className="">
                                        <img className="image-box-shadow border-radius image-w-100" src="img/rectangle-3.png" style={{ minHeight: "226px" }} />
                                        <div className="roboto-bold-gray-18px margin-top-20">รีวิว DJI FPV โดรนรุ่นล่าสุดจาก DJI</div>
                                        <div className="roboto-normal-gray-14px">
                                            ถ้าพูดถึงโดรน FPV โดรน DJI FPV อาจจะไม่ใช่เจ้าแรก เพราะก่อนหน้านี้เราจะเห็นโดรน FPV มาเยอะแยะมากมาย ทั้งการแข่งขัน หรือยูทูปเบอร์สาย FPV Drone ก็มีให้เห็นอยู่เยอะแยะมากมาย เพียงแต่ว่ารูปร่างหน้าตาของโดรน FPV ที่เราคุ้นเคยส่วนใหญ่...
                                        </div>
                                        <div style={{ textAlign: "end" }}>
                                            <a className="text-right" onClick={() => setVisible(true)}>อ่านต่อ...</a>
                                        </div>
                                    </div>
                                </Col>
                                <Col xs={12} sm={12} md={12} lg={6} xl={6}>
                                    <div className="">
                                        <img className="image-box-shadow border-radius image-w-100" src="img/rectangle-2.png" style={{ minHeight: "226px" }} />
                                        <div className="roboto-bold-gray-18px margin-top-20">อัพเดทกลุ่มที่ต้องขึ้นทะเบียนโดรน มีใคร..</div>
                                        <div className="roboto-normal-gray-14px">
                                            ตามกฎหมายในประเทศไทย โดรนถูกแบ่งออกเป็นสองประเภทดังนี้ ประเภทที่ 1 : ใช้สำหรับงานอดิเรกเพื่อการกีฬาหรือ ความบันเทิง โดยจะถูกแบ่งตามนํ้า หนักของโดรนถ้าเกิน 2 กิโลกรัม จะต้องยื่นใน ประเภท 1 ก.และผู้บังคับ ต้องมีอายุ มากกว่า 18 ปี แต่ถ้าเกิน 2 กิโลกรัม...
                                        </div>
                                        <div style={{ textAlign: "end" }}>
                                            <a className="text-right" onClick={() => setVisible(true)}>อ่านต่อ...</a>
                                        </div>
                                    </div>
                                </Col>
                                <Col xs={12} sm={12} md={12} lg={6} xl={6}>
                                    <div className="">
                                        <img className="image-box-shadow border-radius image-w-100" src="img/rectangle-1.png" style={{ minHeight: "226px" }} />
                                        <div className="roboto-bold-gray-18px margin-top-20">กฏหมายใหม่ ผู้ใช้โดรนต้องอบรม-สอบ...</div>
                                        <div className="roboto-normal-gray-14px">
                                            จากกฎหมายที่บังคับใช้อยู่จะเห็นได้ว่าการลงทะเบียน รวมถึงขอใบอนุญาตนั้นเรียกว่าง่ายมากๆเพียงแค่กรอกและยื่นเอกสารครบ รอไม่กี่วันก็ได้รับใบอนุญาตแล้ว แต่นั้นไม่ได้รับประกันว่าคนที่ได้รับใบอนุญาตไปแล้วจะนำไปใช้งานแบบถูกต้องหรือไม่ส่งผลกระทบต่อบุคคลอื่น ล่าสุดทางสำนักงานการบินพลเรือนแห่งประเทศไทย...
                                        </div>
                                        <div style={{ textAlign: "end" }}>
                                            <a className="text-right" onClick={() => setVisible(true)}>อ่านต่อ...</a>
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                        }
                        {/* </Row> */}

                        <Row justify="center" className="margin-top-50">
                            <Col span={24} className="text-center">
                                <p className="roboto-bold-gray-24px">พาร์ทเนอร์ของเรา</p>
                            </Col>
                        </Row>

                        <Row justify="space-around" gutter={[8, 8]}>
                            <Col xs={12} sm={6} md={6} lg={4} xl={4} className="text-center">
                                <div className="frame-69" style={{ margin: "auto", padding: "12px" }}>
                                    <img className="image-w-100" src="img/image-35@2x.png" />
                                </div>
                            </Col>
                            <Col xs={12} sm={6} md={6} lg={4} xl={4} className="text-center">
                                <div className="frame-69" style={{ margin: "auto", padding: "12px" }}>
                                    <img className="image-w-100" src="img/image-34@2x.png" />
                                </div>
                            </Col>
                            <Col xs={12} sm={6} md={6} lg={4} xl={4} className="text-center">
                                <div className="frame-69" style={{ margin: "auto", padding: "12px" }}>
                                    <img className="image-w-100" src="img/bb74612d42425e677257d0b2f4e465996bd22ee-1@2x.png" style={{ marginTop: "6px", marginLeft: "4px" }} />
                                </div>
                            </Col>
                            <Col xs={12} sm={6} md={6} lg={4} xl={4} className="text-center">
                                <div className="frame-69" style={{ margin: "auto", padding: "12px" }}>
                                    <img className="image-w-100" src="img/image-36@2x.png" style={{ marginTop: "40px", marginLeft: "5px" }} />
                                </div>
                            </Col>
                            <Col xs={12} sm={6} md={6} lg={4} xl={4} className="text-center">
                                <div className="frame-69" style={{ margin: "auto", padding: "12px" }}>
                                    <img className="image-w-100" src="img/image-37@2x.png" />
                                </div>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                {/* </div> */}
            </Content>
            <LayoutFooter />
            <Modal
                centered
                visible={visible}
                onOk={null}
                onCancel={() => setVisible(false)}
                footer={null}
                width={1000}
            >
                <Row className="margin-top-50">
                    <Col span={24}>
                        <p className="roboto-bold-gray-24px">รีวิว DJI FPV โดรนรุ่นล่าสุดจาก DJI</p>
                    </Col>
                </Row>
                <Row justify="center" gutter={[8, 8]}>
                    <Col span={24} className="text-center">
                        <div className="" style={{ margin: "auto" }}>
                            <img className="" style={{ width: "100%" }} src="img/dji-fpv-1.png" />
                        </div>
                    </Col>
                </Row>
                <Row className="margin-top-50">
                    <Col span={24}>
                        <div className="roboto-normal-gray-14px">
                            <Space direction="vertical" size="middle" style={{ display: 'flex' }}>
                                <p>
                                    ถ้าพูดถึงโดรน FPV โดรน DJI FPV อาจจะไม่ใช่เจ้าแรก เพราะก่อนหน้านี้เราจะเห็นโดรน FPV มาเยอะแยะมากมาย ทั้งการแข่งขัน หรือยูทูปเบอร์สาย FPV Drone ก็มีให้เห็นอยู่เยอะแยะมากมาย เพียงแต่ว่ารูปร่างหน้าตาของโดรน FPV ที่เราคุ้นเคยส่วนใหญ่มันจะให้อารมณ์ Custom มากกว่า หยิบนั่นมาประกอบนี่อะไรทำนองนี้ แต่ FPV จาก DJI นั้นจะต่างออกไป อารมณ์เหมือนรถสปอร์ตที่แต่งออกมาแล้ว สวยแล้ว และมีความปลอดภัยสูงกว่า เหมาะสำหรับผู้เริ่มต้น หรือมือใหม่หัด FPV มากกว่านั่นเองครับ
                                </p>
                                <p>
                                    แกะกล่องตั้งแต่มาใหม่ ๆ มีชุดเดียวคือชุด Combo บอกราคาก่อนเลย 39,999 บาท ยกเว้นว่าคุณอยากจะได้ Service Plus ด้วยก็เพิ่มไปอีกสองพัน ไปดูได้เลยที่ซูมคาเมร่าทุกสาขา Drone FPV ต่างจาก Drone ทั่วไปที่เราเอาไว้ถ่ายรูปถ่ายวีดีโอกันยังไง?
                                </p>
                                <p>
                                    โดรน FPV หรือ First Person view มุมมองบุคคลที่หนึ่ง เวลาบินต้องใช้ Goggle หรือแว่นในการบิน ภาพที่เห็นก็จะเหมือนว่าเราเป็นเป็นโดรนนั้นเลย ราวกับว่าเราโบยบินอยู่บนท้องฟ้า เราถึงเรียกมันว่า FPV Drone นั่นเอง
                                </p>
                                <p>
                                    ปกติโดรน FPV ทั่วไปที่ไม่ใช่ของ DJI จะเป็นโดรนแนว racing เอาไว้บินฉวัดเฉวียน เล่นท่า มุดนั่น มุดนี่ ตีลังกา จนถึงขั้นไว้แข่ง ก็ว่ากันไป ปกติเนี่ยะ จะดูเป็นแบบเปลือยๆ ซิ่ง ๆ หน่อย จะสามารถ custom ได้ ลำเล็ก ลำใหญ่ เลือกแว่น เลือกรีโมทได้ มีหลากหลายรุ่นหลายราคา
                                </p>
                                <p>
                                    ความเร็วหรือลักษณะอาการตัวลำและการควบคุมจะแตกต่างจากโดรนทั่วไปที่เราเอาไว้บินเก็บภาพสวย ๆ อย่างพวก Mavic mini , mavic Air อยู่ เพราะมันไม่มี Horizon level ให้ หรือว่าไม่มีระดับน้ำนั่นแหละ อย่าง Mavic mini , mavic Air พอปล่อยจอย ตัวลำโดรนจะลอยอยู่เฉย ๆ เพราะเค้ารักษาระดับให้ แต่ถ้าเป็น FPV มันไม่สามารถปล่อยจอยได้ตลอด ถ้าปล่อยโดรนมันจะค่อย ๆ ลง ๆ จนตก
                                </p>
                            </Space>
                        </div>
                    </Col>
                </Row>
                <Row justify="center">
                    <Col>
                        <Button
                            style={{ background: "#FFFFFF", boxShadow: "0px 2px 20px rgb(0 0 0 / 30%)", borderRadius: "10px", width: "100px" }}
                            onClick={() => setVisible(false)}
                        >
                            ปิด
                        </Button>
                    </Col>
                </Row>
            </Modal>
        </Layout>
    )
};