import "antd/dist/antd.css";
import React, { useState, useEffect } from "react";
import LayoutHeader from "../../../components/layout/header";
import { useRouter } from "next/router";
import LayoutFooter from "../../../components/layout/footer";
import { get } from "lodash";
import { ExclamationCircleOutlined } from "@ant-design/icons";
import { findOne, deleteJob, updateJobStatus } from "../../../services/job.service";
import helper from "../../../utils/helper";
import { Field, Formik } from "formik";
import * as Yup from "yup";
import { createBid } from "../../../services/bid.service";
import {
  Select as Selects,
  Input as Inputs,
  TextArea as TextAreas,
  DatePicker,
  GoogleMap,
} from "../../../components";
import {
  Layout,
  Breadcrumb,
  Button,
  Space,
  Row,
  Col,
  Form,
  Spin,
  Typography,
  Modal,
  notification,
  Input,
  Image
} from "antd";
import { status } from "../../../constants/common";
const { Text, Title } = Typography;
const { warning } = Modal;
const { Header, Content, Footer } = Layout;

export default function Post() {
  const initialValues = {
    job_name: "",
    job_detail: "",
    budget_from: "",
    budget_to: "",
    job_type_id: "",
    drone_type_id: "",
    location: "",
    is_auction: "",
    start_date_type: "",
    date_range_start: "",
    date_range_end: "",
    accepted_term: false,
    latitute: "",
    longtitute: "",
    drone_model: "",
    camera_resolution: "",
    other_drone_model: "",
    period_date: "",
    job_images: [],
  };
  const router = useRouter();
  const { id } = router.query;
  const [componentSize, setComponentSize] = useState("medium"); // large, medium, default
  const [value, setValue] = React.useState(1);
  const [value2, setValue2] = React.useState(1);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isCreate, setIsCreate] = useState(true);
  const [isLoading, setIsLoading] = useState(false);
  const [jobData, setJobData] = useState(initialValues);
  const [isModalVisibleBid, setIsModalVisibleBid] = useState(false);
  const [valueBid, setValueBid] = useState();
  const pilotId = typeof window !== 'undefined' ? localStorage.getItem('profile_id') : null
  const [jobLink, setJobLink] = useState("");
  const [locationList, setLocationList] = useState();

  useEffect(() => {
    if (id) {
      if (id === "create") {
        setIsCreate(true);
        setJobData(initialValues);
      } else {
        setIsLoading(true);
        setIsCreate(false);
        getPost(id);
      }
    }
  }, [id]);

  const Schema = Yup.object().shape({});


  const getPost = async (postId) => {
    const filter = {
      id: postId,
    };
    const { result, success } = await findOne(filter);
    if (success) {
      const { data = [] } = result;
      const editData = {
        ...data,
      };
      console.log(data);
      setJobData(data);
      setIsLoading(false);
    } else {
      gotoMyJobs();
    }
  };

  const showModalBid = (value) => {
    let body = {
      bid: 0,
    };
    setValueBid(body)
    setIsModalVisibleBid(true);
  };


  const handleCancelBid = () => {
    setIsModalVisibleBid(false);
  };

  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };

  const jobType = {
    1: "Cinematic Aerial Photography",
    2: "Orthophoto",
    3: "DSM/DTM",
    3: "Point Cloud 3D Model",
    4: "Contour",
    5: "Index & Zonation Map",
    6: "อื่นๆ",
  };

  const droneType = {
    1: "Quadcopter",
    2: "GPS Drone",
    3: "RTF Drone( Ready to Fly Drone )",
    4: "Trick Drone",
    5: "Helicopter Drone",
    6: "Delivery Drone",
    7: "โดรนสำหรับถ่ายภาพ",
    8: "โดรนสำหรับแข่ง",
    9: "โดรนขับเคลื่อนด้วยน้ำมัน",
    1: "โดรนแบบบินได้นาน",
  };

  const droneModel = {
    0: "ไม่ระบุ",
    dji_air_2s: "DJI AIR 2s",
    dji_mini_2: "DJI MINI 2",
    dji_fpv_combo: "DJI FPV COMBO",
    other: "อื่น",
  };

  const cameraResolution = {
    0: "ไม่ระบุ",
    3: "3 cm/px",
    4: "4 cm/px",
    5: "5 cm/px",
    other: "อื่น",
  };

  const periodDate = {
    1: "ไมแน่ใจ",
    2: "ครึ่งวัน",
    3: "1 วัน",
    4: "2 วัน",
    5: "3 วัน",
  };

  const renderDroneModel = (inputDrone) => {
    return droneModel[inputDrone] || inputDrone;
  };

  const goToJobList = () => {
    router.back()
  };

  const [loading, setLoading] = useState(false);

  const handleSubmitBid = async (values) => {
    setLoading(true);
    let jobBody = {
      pilot_profile_id: pilotId,
      job_id: id,
      budget: Number(values.bid),
      bid_by: "pilot"
    };
    const { result, success } = await createBid(jobBody);
    if (success) {
      notification.success({
        message: `success`,
        description: "เสนอราคา เรียบร้อยแล้ว",
      });
      handleCancelBid()
    }
  };

  return (
    <Layout>
      <LayoutHeader />
      <Content
        className="site-layout"
        style={{ padding: "0 50px", marginTop: 64 }}
      >
        <Breadcrumb style={{ margin: "16px 0" }}>
          <Breadcrumb.Item>หน้าหลัก</Breadcrumb.Item>
          <Breadcrumb.Item>โพสงาน</Breadcrumb.Item>
        </Breadcrumb>
        <div
          className="site-layout-background"
          style={{ padding: 24, minHeight: 380 }}
        >
          <Title style={{ textAlign: "center" }} level={3}>
            ข้อมูลงานของฉัน
          </Title>
          <Spin spinning={isLoading}>
            <Row gutter={[16, 24]}>
              <Col className="gutter-row" span={16} offset={4}>
                <Row gutter={[16, 24]}>
                  <Col span={12} >
                    <Text type="">ชื่อโครงการ</Text>
                    <div style={{ paddingLeft: "5px" }}>
                      <Text type="secondary">{jobData.job_name}</Text>
                    </div>
                  </Col>
                  <Col span={12} style={{ textAlign: "end" }}>
                    <div style={{ textAlign: "center" }}>
                      <Text type="">สถานะ</Text>
                      <div style={{ paddingLeft: "5px" }}>
                        <Text type="secondary"></Text>
                        <Button style={{ background: jobData.status != undefined ? status[jobData.status].code : "", borderRadius: '7px' }}><p style={{ color: 'white' }}>{jobData.status != undefined ? status[jobData.status].text : ""}</p></Button>
                      </div>
                    </div>
                  </Col>
                </Row>
              </Col>
              <Col className="gutter-row" span={16} offset={4}>
                <Text type="">รายละเอียดโครงการ</Text>
                <div style={{ paddingLeft: "5px" }}>
                  <Text type="secondary">{jobData.job_detail}</Text>
                </div>
              </Col>
              <Col className="gutter-row" span={16} offset={4}>
                <Text type="">ราคาจ้างงาน</Text>
                <div style={{ paddingLeft: "5px" }}>
                  <Text type="secondary">
                    {jobData.budget_from} - {jobData.budget_to}{" "}
                  </Text>
                </div>
              </Col>
              <Col className="gutter-row" span={16} offset={4}>
                <Text type="">ประเภทงานของคุณ</Text>
                <div style={{ paddingLeft: "5px" }}>
                  <Text type="secondary">
                    {get(jobType, jobData.job_type_id)}
                  </Text>
                </div>
              </Col>
            </Row>
            <br />
            <Row gutter={[16, 24]}>
              <Col className="gutter-row" span={6} offset={4}>
                <Text type="">ประเภทโดรน</Text>
                <div style={{ paddingLeft: "5px" }}>
                  <Text type="secondary">
                    {get(droneType, jobData.drone_type_id)}
                  </Text>
                </div>
              </Col>
              <Col className="gutter-row" span={6}>
                <Text type="">รุ่นโดรน</Text>
                <div style={{ paddingLeft: "5px" }}>
                  <Text type="secondary">
                    {renderDroneModel(jobData.drone_model)}
                  </Text>
                </div>
              </Col>
              <Col className="gutter-row" span={6}>
                <Text type="">ความละเอียดกล้องติดโดรน</Text>
                <div style={{ paddingLeft: "5px" }}>
                  <Text type="secondary">
                    {get(cameraResolution, jobData.camera_resolution)}
                  </Text>
                </div>
              </Col>
            </Row>
            <Row style={{ marginTop: "50px" }} gutter={[16, 24]}>
              <Col className="gutter-row" span={16} offset={4}>
                <GoogleMap isDisable={true} jobLocations={get(jobData, "job_locations")} />
              </Col>
            </Row>
            <br />
            <Row gutter={[16, 24]}>
              <Col className="gutter-row" span={16} offset={4}>
                <Text type="">วันที่ต้องการว่าจ้าง</Text>
                <div style={{ paddingLeft: "5px" }}>
                  <Text type="secondary">
                    {helper.dateFormat(jobData.date_range_start)}
                  </Text>
                </div>
              </Col>
              <Col className="gutter-row" span={16} offset={4}>
                <Text type="">ระยะเวลาในการจ้างงาน</Text>
                <div style={{ paddingLeft: "5px" }}>
                  <Text type="secondary">
                    {get(periodDate, jobData.period_date)}
                  </Text>
                </div>
              </Col>
              <Col className="gutter-row" span={16} offset={4}>
                <Text type="">แนบไฟล์ (ตัวอย่าง ตารางงาน ,รูปสถานที่ถ่ายงาน)</Text>
                <div style={{ paddingLeft: "5px" }}>
                  {jobData.job_images.length > 0 ? (
                    jobData.job_images.map((val) => {
                      return (
                        <>
                          <div style={{ margin: "20px" }}>
                            <Image width={200} src={"/image/" + val.image} />
                          </div>
                        </>
                      );
                    })
                  ) : ("")
                  }
                </div>
              </Col>
            </Row>
            <div style={{ textAlign: "center", marginTop: "15px" }}>
              <Space>
                <Button style={{ borderRadius: '10px', boxShadow: '0px 2px 20px rgba(0, 0, 0, 0.3)' }} onClick={showModalBid} type="primary" primary>
                  เสนอราคา
                </Button>
                <Button
                  style={{ borderRadius: '10px', boxShadow: '0px 2px 20px rgba(0, 0, 0, 0.3)' }}
                  type="secondary"
                  onClick={goToJobList}
                >
                  ย้อนกลับ
                </Button>
              </Space>
            </div>
          </Spin>
          <Modal footer={null} width={'30%'} visible={isModalVisibleBid} onCancel={handleCancelBid}>
            <Spin spinning={loading}>
              <>
                <Formik
                  enableReinitialize={true}
                  initialValues={valueBid}
                  onSubmit={handleSubmitBid}
                  validationSchema={Schema}
                >
                  {({ values, handleSubmit, handleChange, setFieldValue }) => (
                    <Form
                      layout="vertical"
                      onValuesChange={onFormLayoutChange}
                      size={"medium"}
                      onFinish={handleSubmit}
                    >

                      <Row gutter={16}>
                        <Col className="gutter-row" span={24} style={{ textAlign: "center" }}>
                          <Title style={{ color: "#7F7F7F" }} level={3}>
                            เสนอราคา
                          </Title>
                        </Col>
                        <Col className="gutter-row" span={24} style={{ marginTop: "10px" }}>
                          <Row gutter={[16, 24]}>
                            <Col className="gutter-row" span={24}>
                              <Text type="">ชื่อโครงการ: </Text><Text type="secondary">{jobData.job_name}</Text>
                            </Col>
                          </Row>
                          <Row gutter={[16, 24]}>
                            <Col className="gutter-row" span={12}>
                              <Text type="">วันที่ต้องการว่าจ้าง: </Text><Text type="secondary">{helper.dateFormat(jobData.date_range_start)}</Text>
                            </Col>
                            <Col className="gutter-row" span={12}>
                              <Text type="">ระยะเวลาในการจ้างงาน: </Text><Text type="secondary">{jobData.period_date}</Text>
                            </Col>
                          </Row>
                          <Row gutter={[16, 24]}>
                            <Col className="gutter-row" span={24}>
                              <Text type="">งบประมาณ: </Text> <Text type="secondary">{jobData.budget_from} - {jobData.budget_to}</Text>
                            </Col>
                          </Row>
                          <Row gutter={[16, 24]}>
                            <Col className="gutter-row" span={24}>
                              <Field
                                label={{ text: "เสนอราคา" }}
                                id="bid"
                                onChange={handleChange}
                                name="bid"
                                rows={4}
                                type={"number"}
                                component={Inputs}
                                placeholder="0"
                                validate={(value) => {
                                  if (value == "") {
                                    return "กรุณาเสนอราคา"
                                  }
                                }}
                              />
                            </Col>
                          </Row>
                        </Col>
                        <Col
                          className="gutter-row" span={24}
                          style={{ textAlign: "center" }}
                        >
                          <Space>
                            <Button
                              style={{ marginRight: "10px", borderRadius: "10px" }}
                              type="primary" htmlType="submit" primary
                            >
                              เสนอราคา
                            </Button>
                            <Button
                              style={{ borderRadius: '10px', boxShadow: '0px 2px 20px rgba(0, 0, 0, 0.3)' }}
                              type="secondary"
                              onClick={handleCancelBid}
                            >
                              ปิด
                            </Button>
                          </Space>
                        </Col>
                      </Row>
                    </Form>
                  )}
                </Formik>
              </>
            </Spin>
          </Modal>
        </div>
      </Content>
      <LayoutFooter />
    </Layout>

  );
}
