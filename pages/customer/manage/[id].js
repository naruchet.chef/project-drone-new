import "antd/dist/antd.css";
import React, { useState, useEffect } from "react";
import LayoutHeader from "../../../components/layout/header";
import { useRouter } from "next/router";
import LayoutFooter from "../../../components/layout/footer";
import { findOneCustomer } from "../../../services/customer.service";
import ManageProfile from '../../../components/customer/tab/profile'
import ManagePay from '../../../components/customer/tab/payment'
import Favarite from '../../../components/customer/tab/favarite'
import Block from '../../../components/customer/tab/block'
// import ManagePerformance from '../manage-performance'

import {
  Layout,
  Tabs,
  Spin,
  notification
} from "antd";
const { TabPane } = Tabs;
const { Content } = Layout;

export default function Manage() {

  const router = useRouter();
  const { id, tab } = router.query;
  const [isLoading, setIsLoading] = useState(false);
  const [customerData, setCustomerData] = useState();
  const [tabIndex, setTabIndex] = useState("1");

  useEffect(() => {
    if (id) {
      getCustomerProfile(id);
    }
  }, [id]);

  useEffect(() => {
    if (tab === "favarite") {
      setTabIndex("3")
    } else if (tab === "block") {
      setTabIndex("4")
    } else if (tab === "setting") {
      setTabIndex("1")
    } else if (tab === "my_job") {
      router.push("/my-jobs")
    }
  }, [tab])

  const getCustomerProfile = async (postId) => {
    setIsLoading(true);
    const filter = {
      id: postId,
    };
    const { result, success } = await findOneCustomer(filter);
    console.log(result);
    if (success) {
      const { data = [] } = result;
      setCustomerData(data);
      setIsLoading(false);
    } else {
      notification.error({
        description: "ไม่พบ โปรไฟล์",
      });
      setIsLoading(false);
    }
  };

  const handelTabChange = (value) => {
    setTabIndex(value)
  }

  return (
    <Spin spinning={isLoading}>
      <Layout className="layout">
        <LayoutHeader />
        <Content
          className="site-layout"
          style={{ padding: "0 50px", marginTop: 64 }}
        >
          <div
            className="site-layout-background"
            style={{ padding: 24, minHeight: 380 }}
          >
            <Tabs tabBarStyle={{ color: '#689FF2' }} type="card" onChange={handelTabChange} activeKey={tabIndex}>
              <TabPane tab="ตั้งค่าบัญชี" key="1">
                <ManageProfile customerProfile={customerData} />
              </TabPane>
              <TabPane tab="ประวัติชำระเงิน" key="2">
                <ManagePay customerProfile={customerData} />
              </TabPane>
              <TabPane tab="นักบินที่ชื่นชอบ" key="3">
                <Favarite customerId={id} />
              </TabPane>
              <TabPane tab="นักบินที่ถูกบล็อก" key="4">
                <Block customerId={id} />
              </TabPane>
            </Tabs>
          </div>
        </Content>
        <LayoutFooter />
      </Layout>
    </Spin>
  );
}
