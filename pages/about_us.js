import Link from "next/link";
import { Layout, Row, Col, Input, Button } from "antd";

import LayoutHeader from "../components/layout/header";
import LayoutFooter from "../components/layout/footer";

const { Content } = Layout;

export default function HomePage() {
  return (
    <>
      <Layout className="layout">
        <LayoutHeader />
        <Content className="site-layout" style={{
                    padding: "64px 0px",
                }}>
          <Row justify="center" align="middle" >
            <Col span={24}>
              <img
                src="img/bertrand-bouchez-sooakfa6bri-unsplash-4@1x.png"
                style={{ width: "100%" }}
              />
              <div className="centered roboto-bold-white-40px">
                <h1 style={{ color: "#ffffff", fontSize: "30px" }}>
                  {" "}
                  เกี่ยวกับเรา
                </h1>
              </div>
            </Col>
          </Row>
          <Row justify="center" align="middle" className="margin-top-50">
            <Col span={22}>
              <Row>
                <Col span={24} className="text-center">
                  <div className="roboto-bold-gray-24px">เกี่ยวกับเรา</div>
                  <div className="roboto-bold-gray-16px margin-top-20">
                    <h3 style={{ color: "#7f7f7f" }}>
                      เกี่ยวกับแพลตฟอร์มของเราวัตุประสงค์
                    </h3>
                  </div>
                </Col>
              </Row>
              <Row className="margin-top-100" gutter={[24, 24]}>
                <Col xs={24} sm={24} md={14} lg={14} xl={14}>
                  <div
                    className="sut-platform-drone-abount-us"
                    style={{ fontSize: "16px" }}
                  >
                    SUT Platform Drone นี้ ถูกสร้างขึ้นโดย
                    สถาบันโดรนที่จัดตั้งขึ้นในมหาวิทยาลัยเทคโนโลยีสุรนารี <br />
                    จังหวัดนครราชสีมา&nbsp;&nbsp;
                    ซึ่งได้รับความร่วมมือจากเทคโนธานีของมหาวิทยาลัยเทคโนโลยีสุรนารี
                    หน่วยงาน
                    ภาครัฐ สำนักงานการบินพลเรือนแห่งประเทศไทย (CAAT) และ
                    สถาบันเทคโนลยีป้องกันประเทศ (DTI)
                    เพื่อพัฒนาแพลทฟอร์มตลาดกลางสำหรับพัฒนาช่องทางการติดต่อว่าจ้างระหว่างผู้ให้บริการข้อมูลด้วย
                    อากาศยานไร้คนขับ และ ผู้ที่ต้องการใช้บริการข้อมูล
                    <br />
                    <br />
                    แพลทฟอร์มนี้มีเป้าหมายเพื่อพัฒนาตัวกลางขยายตลาดว่าจ้างโดรนให้ผู้คนสามารถเข้าถึงได้ง่าย
                    สร้าง
                    งานให้แก่ผู้ให้บริการข้อมูลด้วยอากาศยานไร้คนขับ
                    และส่งมอบงานที่มีความเป็นมืออาชีพให้กับผู้ที่ต้อง
                    การใช้บริการข้อมูล
                    ทำให้เกิดการใช้งานและการพัฒนาบริการด้วยอากาศยานไร้คนขับ&nbsp;&nbsp;สร้างแรงผลักดัน
                    ให้อุตสาหกรรมโดรนภายในประเทศมีความก้าวหน้า
                    <br />
                    <br />
                    สำหรับการสร้างมาตรฐานให้กับนักบินที่จะเข้ามาเป็นผู้ให้บริการข้อมูลด้วยอากาศยานไร้คนขับในแพลท
                    ฟอร์มนั้น ทางสถาบันโดรนได้เตรียมหลักสูตรอบรมมาตรฐานการบิน
                    ที่นักบินทุกคนจำเป็นต้องเข้าอบรม
                    และผ่านการทดสอบมาตรฐาน
                    เพื่อได้รับสิทธิ์ในการเป็นผู้ให้บริการในแพลทฟอร์ม
                    <br />
                    <br />
                    ตัวแพลทฟอร์มถูกออกแบบและให้คำปรึกษาโดยผู้เชี่ยวชาญด้านอากาศยานไร้นักบิน
                    ของมหาวิทยาลัย
                    เทคโนโลยีสุรนารี ที่เข้าใจการทำงานของอากาศยานไร้คนขับ และ
                    คุณค่าที่อากาศยานไร้คนขับสามารถส่ง
                    มอบข้อมูลด้านต่างๆ ที่สามารถนำมาประยุกต์ใช้พัฒนากระบวนการ
                    และผลลัพธ์ในธุรกิจต่างๆ
                    ภายในประเทศได้เป็นอย่างดี
                  </div>
                </Col>
                <Col xs={24} sm={24} md={10} lg={10} xl={10}>
                  <Row gutter={[12, 12]}>
                    <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                      <div className="frame-57 margin-5">
                        <img
                          className="image-32 image-box-shadow border-radius"
                          src="img/screen-shot-2565-02-12-at-20-13-5@2x.png"
                        />
                      </div>
                    </Col>
                    <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                      <div className="frame-57 margin-5">
                        <img
                          className="image-32 image-box-shadow border-radius"
                          src="img/screen-shot-2565-02-12-at-20-13-6@2x.png"
                        />
                      </div>
                    </Col>
                    <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                      <div className="frame-57 margin-5">
                        <img
                          className="image-32 image-box-shadow border-radius"
                          src="img/screen-shot-2565-02-12-at-20-13-7@2x.png"
                        />
                      </div>
                    </Col>
                    <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                      <div className="frame-57 margin-5">
                        <img
                          className="image-32 image-box-shadow border-radius"
                          src="img/screen-shot-2565-02-12-at-20-13-8@2x.png"
                        />
                      </div>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
          </Row>
          <div className="margin-top-100"></div>
        </Content>
        <LayoutFooter />
      </Layout>
    </>
  );
}
