const express = require('express')
const next = require('next')
require('dotenv').config()
const fileUpload = require('express-fileupload');
const { Sequelize } = require('sequelize');

const port = parseInt(process.env.PORT, 10) || 3000
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()

const db = require("./models");
// db.sequelize.sync();

const testDb = async () => {
  try {
    await db.sequelize.authenticate();
    console.log('Connection has been established successfully.');
  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }
}


app.prepare().then(() => {
  const server = express()
  server.use(fileUpload({
    createParentPath: true
  }));

  server.all('*', (req, res) => {
    return handle(req, res)
  })
  testDb()
  server.listen(port, (err) => {
    if (err) throw err
    console.log(`> Ready on http://localhost:${port}`)
  })
})