import { last } from "lodash-es";
import moment from "moment";
import { v4 as uuidv4 } from 'uuid';
const pagination = (count, page, per_page) => {
  return {
    total: count,
    page,
    per_page,
  }
}


const dateFormat = (inputDate) => {
  if (inputDate) {
    const [day, month, year] = moment(inputDate).format("DD/MM/YYYY").split("/")
    return `${day}-${month}-${(+year) + 543}`
  }
  return ""
}

const generateUUID = () => {
  return uuidv4()
}

const getFileType = (fileName) => {
  let imageType = fileName
  imageType = imageType.split(".")
  imageType = last(imageType)
  return imageType
}

const imageUpload = async (image) => {
  const uuidName = helper.generateUUID()
  let imageType = helper.getFileType(image.name)
  const newImaageName = `${uuidName}.${imageType}`
  const path = `./public/image/${newImaageName}`
  await image.mv(path)
  const iamgeObj = {
    image: newImaageName
  }
  return iamgeObj
}

const helper = {
  pagination: pagination,
  dateFormat: dateFormat,
  generateUUID: generateUUID,
  getFileType: getFileType,
  imageUpload: imageUpload,
}
export default helper