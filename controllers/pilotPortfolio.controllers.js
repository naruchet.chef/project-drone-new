import db from '../models'
const PilotPortfolio = db.pilotPortfolio;
const Op = db.Sequelize.Op;
import helper from '../utils/helper'

const create = async (body) => {
  try {
    const data = await PilotPortfolio.create(body)
    return data
  } catch (error) {
    throw error;
  }
};

const update = async (body) => {
  try {
    const id = body.id
    const filter = { where: { id: id } }
    const findData = await PilotPortfolio.findOne(filter)
    if (findData) {
      const data = await PilotPortfolio.update(body, filter)
      return data
    }
    return null
  } catch (error) {
    console.log('error :>> ', error);
    throw error;
  }
};

const deletes = async (id) => {
  try {
    const data = await PilotPortfolio.destroy({
      where: { id: id }
    })
    return data
  } catch (error) {
    throw error;
  }
};


const findAll = async (query) => {
  console.log(query);
  let { page = 1, per_page = 10, pilot_profile_id = 0, portfolio_type = "" } = query
  page = parseInt(page)
  per_page = parseInt(per_page)
  const offset = (page - 1) * per_page

  try {
    const { count, rows } = await PilotPortfolio.findAndCountAll({
      order: [["created_at", "DESC"]],
      limit: per_page,
      offset: offset,
      where: { pilot_profile_id: pilot_profile_id, portfolio_type: portfolio_type }
    })
    const meta = helper.pagination(count, page, per_page)
    return { rows, meta }
  } catch (error) {
    throw error;
  }
};

const crud = {
  create,
  update,
  deletes,
  findAll
}


export default crud