import db from '../models'
const JobImage = db.jobImage;
const Op = db.Sequelize.Op;
import helper from '../utils/helper'



const create = async (body) => {
  try {
    const data = await JobImage.create(body)
    return data
  } catch (error) {
    throw error;
  }
};

const crud = {
  create,
}


export default crud