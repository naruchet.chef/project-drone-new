import db from '../models'
const JobLocation = db.jobLocation;
const Op = db.Sequelize.Op;
import helper from '../utils/helper'




const create = async (body) => {
  try {
    const data = await JobLocation.create(body)
    return data
  } catch (error) {
    throw error;
  }
};

const crud = {
  create,
}


export default crud