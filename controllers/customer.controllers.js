import { union } from 'lodash';
import db from '../models'
const Customer = db.customer;
const Op = db.Sequelize.Op;
import helper from '../utils/helper'

// Create and Save a new Tutorial
const create = async (body) => {
  try {
    const data = await Customer.create(body)
    return data
  } catch (error) {
    throw error;
  }
};

const findOne = async (id) => {
  try {
    const data = await Customer.findByPk(id)
    return data
  } catch (error) {
    throw error;
  }
};

const update = async (body) => {
  try {
    const id = body.id
    const filter = { where: { id: id } }
    const findData = await Customer.findOne(filter)
    if (findData) {
      const data = await Customer.update(body, filter)
      return data
    }
    return null
  } catch (error) {
    console.log('error :>> ', error);
    throw error;
  }
};

const deletes = async (id) => {
  try {
    const data = await Customer.destroy({
      where: { id: id }
    })
    return data
  } catch (error) {
    throw error;
  }
};


const findCustomerByAccount = async (account_id) => {
  try {
    const data = await Customer.findOne({ where: { account_id: account_id } })
    return data
  } catch (error) {
    throw error;
  }
};

const crud = {
  create,
  findOne,
  update,
  deletes,
  findCustomerByAccount
}

export default crud

