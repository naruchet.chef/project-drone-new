import { get } from 'lodash';
import db, { bid } from '../models'
const Job = db.job;
const Bid = db.bid;
const JobLocation = db.jobLocation;
const JobImage = db.jobImage;
const Pilot = db.pilot;
const FavariteList = db.favariteList;
const BlockList = db.blockList;
const Op = db.Sequelize.Op;
import helper from '../utils/helper'

// Create and Save a new Tutorial
const create = async (body) => {
  try {
    Job.hasMany(JobLocation)
    Job.hasMany(JobImage)
    const data = await Job.create(body, { include: [JobLocation, JobImage] })
    return data
  } catch (error) {
    throw error;
  }
};

const findAll = async (query) => {
  console.log(query);
  Job.hasMany(Bid)
  Job.hasMany(JobLocation)
  Job.hasMany(JobImage)
  let { page = 1, per_page = 10, pilot_profile_id = 0, job_type = 0, profile_id = 0, address = "", status = "", account_id } = query
  page = parseInt(page)
  per_page = parseInt(per_page)
  const offset = (page - 1) * per_page
  const filter = {}

  if (pilot_profile_id != 0) {
    filter.pilot_profile_id = pilot_profile_id
  }
  if (profile_id != 0) {
    filter.profile_id = profile_id
  }
  if (status != "") {
    filter.status = status
  }
  if (job_type != 0) {
    filter.job_type_id = job_type
  }

  let FavBlockModel = []
  if (account_id) {
    Job.belongsTo(FavariteList, {
      foreignKey: 'id',
      targetKey: "ref_id",

    })
    Job.belongsTo(BlockList, {
      foreignKey: 'id',
      targetKey: "ref_id",

    })

    FavBlockModel = [
      {
        model: FavariteList,
        where: {
          account_id: account_id,
          category: "job"
        },
        required: false
      },
      {
        model: BlockList,
        where: {
          account_id: account_id,
          category: "job"
        },
        required: false
      }
    ]
  }

  try {
    if (pilot_profile_id != 0) {
      const { count, rows } = await Job.findAndCountAll({
        order: [["created_at", "DESC"]],
        limit: per_page,
        offset: offset,
        where: filter,
        include: [{
          model: Bid,
          where: filter ? {} : filter
        }, {
          model: JobLocation,
          where: { name: { [Op.like]: `%${address}%` } }
        }, JobImage],
      })
      const meta = helper.pagination(count, page, per_page)
      return { rows, meta }
    } else {
      const { count, rows } = await Job.findAndCountAll({
        order: [["created_at", "DESC"]],
        limit: per_page,
        offset: offset,
        where: filter,
        include: [{
          model: JobLocation,
          where: { name: { [Op.like]: `%${address}%` } }
        }, JobImage, ...FavBlockModel]
      })
      const meta = helper.pagination(count, page, per_page)
      return { rows, meta }
    }
  } catch (error) {
    throw error;
  }
};


const findAllWithBids = async (query) => {
  console.log(query);
  Job.hasMany(Bid)
  let { page = 1, per_page = 10, pilot_profile_id = 0 } = query
  page = parseInt(page)
  per_page = parseInt(per_page)
  const offset = (page - 1) * per_page

  try {
    const { count, rows } = await Job.findAndCountAll({
      order: [["created_at", "DESC"]],
      limit: per_page,
      offset: offset,
      include: [{
        model: Bid,
        where: { pilot_profile_id: pilot_profile_id },
        required: false
      }],
    })
    const meta = helper.pagination(count, page, per_page)
    return { rows, meta }
  } catch (error) {
    throw error;
  }
};

const findOne = async (id) => {
  try {
    Job.hasMany(JobLocation)
    Job.hasMany(JobImage)
    Job.hasMany(Bid)
    const data = await Job.findByPk(id, {
      include: [JobLocation, Bid, JobImage, Pilot]
    })
    return data
  } catch (error) {
    throw error;
  }

};
const update = async (body) => {
  try {
    Job.hasMany(JobLocation)
    Job.hasMany(JobImage)
    const id = body.id
    const filter = { where: { id: id }, include: [JobLocation, JobImage] }
    const findData = await Job.findOne(filter)
    if (findData) {
      const data = await Job.update(body, filter)
      let locationList = get(body, "job_locations", [])
      let imageList = get(body, "job_images", [])
      if (locationList.length > 0) {
        await JobLocation.destroy({
          where: { job_id: id }
        })
        locationList = locationList.map(d => {
          return {
            ...d,
            id: null,
            job_id: id
          }
        })
        await Promise.all(locationList.map(location => JobLocation.create(location)));
      }
      if (imageList.length > 0) {
        await JobImage.destroy({
          where: { job_id: id }
        })
        imageList = imageList.map(d => {
          return {
            ...d,
            id: null,
            job_id: id
          }
        })
        await Promise.all(imageList.map(image => JobImage.create(image)));
      }
      return data
    }
    return null
  } catch (error) {
    console.log('error :>> ', error);
    throw error;
  }

};


const updateStatus = async (body) => {
  try {
    const id = body.id
    const filter = { where: { id: id } }
    const findData = await Job.findOne(filter)
    if (findData) {
      const data = await Job.update(body, filter)
      if (body.status == "success") {
        const { count, rows } = await Job.findAndCountAll({ where: { pilot_profile_id: findData.pilot_profile_id } })
        let pilotUpdate = {
          id: findData.pilot_profile_id,
          job_all: count
        }
        const dataUpdate = await Pilot.update(pilotUpdate, { where: { id: findData.pilot_profile_id } })
      }
      return data
    }
    return null
  } catch (error) {
    console.log('error :>> ', error);
    throw error;
  }
};


const deletes = async (id) => {
  try {
    const data = await Job.destroy({
      where: { id: id }
    })
    return data
  } catch (error) {
    throw error;
  }
};

const crud = {
  create,
  findAll,
  findOne,
  update,
  deletes,
  findAllWithBids,
  updateStatus
}

export default crud

