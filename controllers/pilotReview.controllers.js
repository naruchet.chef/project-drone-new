import db from '../models'
const PilotReview = db.pilotReview;
const Pilot = db.pilot;
const Op = db.Sequelize.Op;
import helper from '../utils/helper'

const create = async (body) => {
  try {
    const data = await PilotReview.create(body)
    const dataPilotReview = await PilotReview.findAll({ where: { pilot_profile_id: body.pilot_profile_id } })

    let rating = 0
    let count = dataPilotReview.length
    dataPilotReview.map((v, k) => {
      rating = rating + v.rating
    })

    let pilotRating = rating / count
    const filter = { where: { id: body.pilot_profile_id } }
    let pilotUpdate = {
      id: body.pilot_profile_id,
      job_review: count,
      rating: pilotRating.toFixed(1)
    }
    const dataUpdate = await Pilot.update(pilotUpdate, filter)

    return data
  } catch (error) {
    throw error;
  }
};

const crud = {
  create,
}

export default crud