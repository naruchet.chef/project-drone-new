import { get } from 'lodash';
import db from '../models'
const Bid = db.bid;
const Pilot = db.pilot;
const Job = db.job;
const Op = db.Sequelize.Op;
import helper from '../utils/helper'

// Create and Save a new Tutorial
const create = async (body) => {
  try {
    const data = await Bid.create(body)
    return data
  } catch (error) {
    throw error;
  }
};

const findAll = async (query) => {
  let { page = 1, per_page = 10, job_id = 0, bid_by = "" } = query
  page = parseInt(page)
  per_page = parseInt(per_page)
  const offset = (page - 1) * per_page
  const filter = {}
  if (job_id != 0) {
    filter.job_id = job_id
  }
  if (bid_by != "") {
    filter.bid_by = bid_by
  }
  try {
    const { count, rows } = await Bid.findAndCountAll({
      order: [["budget", "DESC"]],
      limit: per_page,
      offset: offset,
      where: filter,
      include: [Pilot]
    })
    const meta = helper.pagination(count, page, per_page)
    return { rows, meta }
  } catch (error) {
    throw error;
  }
};

const findOne = async (id) => {
  try {
    const data = await Bid.findByPk(id, {
    })
    return data
  } catch (error) {
    throw error;
  }

};

const update = async (body) => {
  try {
    const id = body.id
    const filter = { where: { id: id } }
    const findData = await Bid.findOne(filter)
    if (findData) {
      const data = await Bid.update(body, filter)
      return data
    }
    return null
  } catch (error) {
    console.log('error :>> ', error);
    throw error;
  }
};

const deletes = async (id) => {
  try {
    const data = await Bid.destroy({
      where: { id: id }
    })
    return data
  } catch (error) {
    throw error;
  }
};

const crud = {
  create,
  findAll,
  findOne,
  update,
  deletes
}

export default crud

