import { get } from 'lodash';
import db from '../models'
const PaymentHistory = db.paymentHistory;
const Pilot = db.pilot;
const Job = db.job;
const Op = db.Sequelize.Op;
import helper from '../utils/helper'

// Create and Save a new Tutorial
const create = async (body) => {
  try {
    const data = await PaymentHistory.create(body)
    return data
  } catch (error) {
    throw error;
  }
};

const findAll = async (query) => {
  let { page = 1, per_page = 10, profile_id = 0 } = query
  page = parseInt(page)
  per_page = parseInt(per_page)
  const offset = (page - 1) * per_page
  const filter = {}
  if (profile_id != 0) {
    filter.profile_id = profile_id
  }

  try {
    const { count, rows } = await PaymentHistory.findAndCountAll({
      order: [["created_at", "DESC"]],
      limit: per_page,
      offset: offset,
      where: filter
    })
    const meta = helper.pagination(count, page, per_page)
    return { rows, meta }
  } catch (error) {
    throw error;
  }
};

const findOne = async (id) => {
  try {
    const data = await PaymentHistory.findByPk(id, {
    })
    return data
  } catch (error) {
    throw error;
  }

};

const update = async (body) => {
  try {
    const id = body.id
    const filter = { where: { id: id } }
    const findData = await PaymentHistory.findOne(filter)
    if (findData) {
      const data = await PaymentHistory.update(body, filter)
      return data
    }
    return null
  } catch (error) {
    console.log('error :>> ', error);
    throw error;
  }
};

const deletes = async (id) => {
  try {
    const data = await PaymentHistory.destroy({
      where: { id: id }
    })
    return data
  } catch (error) {
    throw error;
  }
};

const crud = {
  create,
  findAll,
  findOne,
  update,
  deletes
}

export default crud

