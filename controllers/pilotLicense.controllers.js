import db from '../models'
const PilotLicense = db.pilotLicense;
const Op = db.Sequelize.Op;
import helper from '../utils/helper'

const create = async (body) => {
  try {
    const data = await PilotLicense.create(body)
    return data
  } catch (error) {
    throw error;
  }
};

const update = async (body) => {
  try {
    const id = body.id
    const filter = { where: { id: id } }
    const findData = await PilotLicense.findOne(filter)
    if (findData) {
      const data = await PilotLicense.update(body, filter)
      return data
    }
    return null
  } catch (error) {
    console.log('error :>> ', error);
    throw error;
  }
};

const deletes = async (id) => {
  try {
    const data = await PilotLicense.destroy({
      where: { id: id }
    })
    return data
  } catch (error) {
    throw error;
  }
};

const crud = {
  create,
  update,
  deletes
}


export default crud