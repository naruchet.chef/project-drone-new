import db from '../models'
const PilotJobType = db.pilotJobType;
const Op = db.Sequelize.Op;
import helper from '../utils/helper'

const create = async (body) => {
  try {
    const data = await PilotJobType.create(body)
    return data
  } catch (error) {
    throw error;
  }
};

const update = async (body) => {
  try {
    const id = body.id
    const filter = { where: { id: id } }
    const findData = await PilotJobType.findOne(filter)
    if (findData) {
      const data = await PilotJobType.update(body, filter)
      return data
    }
    return null
  } catch (error) {
    console.log('error :>> ', error);
    throw error;
  }
};

const deletes = async (id) => {
  try {
    const data = await PilotJobType.destroy({
      where: { id: id }
    })
    return data
  } catch (error) {
    throw error;
  }
};

const crud = {
  create,
  update,
  deletes
}


export default crud