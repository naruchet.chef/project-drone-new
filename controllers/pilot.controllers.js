import { union } from 'lodash';
import db from '../models'
const Pilot = db.pilot;
const PilotLicense = db.pilotLicense;
const PilotPortfolio = db.pilotPortfolio;
const PilotJobType = db.pilotJobType;
const FavariteList = db.favariteList;
const BlockList = db.blockList;
const Op = db.Sequelize.Op;
import helper from '../utils/helper'

// Create and Save a new Tutorial
const create = async (body) => {
  try {
    const data = await Pilot.create(body)
    return data
  } catch (error) {
    throw error;
  }
};

const findAll = async (query) => {
  Pilot.hasMany(PilotLicense)
  console.log(query)
  Pilot.hasMany(PilotJobType)
  Pilot.belongsTo(FavariteList, {
    foreignKey: 'id',
    targetKey: "ref_id",

  })
  Pilot.belongsTo(BlockList, {
    foreignKey: 'id',
    targetKey: "ref_id",

  })
  const { page, per_page, job_type, address, account_id } = query
  const offset = (Number(page) - 1) * Number(per_page)

  try {
    if (job_type === undefined) {
      const { count, rows } = await Pilot.findAndCountAll({
        order: [["created_at", "DESC"]],
        limit: Number(per_page),
        offset: offset,
        where: {
          address: { [Op.like]: `%${address != undefined ? address : ""}%` }
        },
        include: [PilotLicense, {
          model: FavariteList,
          where: {
            account_id: account_id,
            category: "pilot"
          },
          required: false
        },
          {
            model: BlockList,
            where: {
              account_id: account_id,
              category: "pilot"
            },
            required: false
          }]
      })
      const meta = helper.pagination(count, page, per_page)
      return { rows, meta }
    } else {
      const { count, rows } = await Pilot.findAndCountAll({
        order: [["created_at", "DESC"]],
        limit: Number(per_page),
        offset: offset,
        where: {
          address: { [Op.like]: `%${address != undefined ? address : ""}%` }
        },
        include: [{
          model: PilotJobType,
          where: { job_type_id: job_type }
        }, PilotLicense],
      })
      const meta = helper.pagination(count, page, per_page)
      return { rows, meta }
    }
  } catch (error) {
    throw error;
  }
};

const findOne = async (id) => {
  try {
    Pilot.hasMany(PilotLicense)
    Pilot.hasMany(PilotPortfolio)
    Pilot.hasMany(PilotJobType)
    const data = await Pilot.findByPk(id, {
      include: [PilotPortfolio, PilotLicense, PilotJobType]
    })
    return data
  } catch (error) {
    throw error;
  }
};

const findPilotByAccount = async (account_id) => {
  try {
    Pilot.hasMany(PilotLicense)
    Pilot.hasMany(PilotPortfolio)
    Pilot.hasMany(PilotJobType)
    const data = await Pilot.findOne({ where: { account_id: account_id }, include: [PilotPortfolio, PilotLicense, PilotJobType] })
    return data
  } catch (error) {
    throw error;
  }
};

const update = async (body) => {
  try {
    console.log("bodybody=>" , body)
    const id = parseInt(body.id)
    const filter = { where: { id: id } }
    const findData = await Pilot.findOne(filter)
    if (findData) {
      const data = await Pilot.update(body, filter)
      return data
    }
    return null
  } catch (error) {
    console.log('error :>> ', error);
    throw error;
  }
};

const deletes = async (id) => {
  try {
    const data = await Job.destroy({
      where: { id: id }
    })
    return data
  } catch (error) {
    throw error;
  }
};

const crud = {
  create,
  findAll,
  findOne,
  update,
  deletes,
  findPilotByAccount
}

export default crud

