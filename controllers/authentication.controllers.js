import db from '../models'
const Account = db.account;
const Op = db.Sequelize.Op;
import helper from '../utils/helper'
import Piolt from './pilot.controllers'
import Consumer from './customer.controllers'

import { OAuth2Client } from 'google-auth-library'
import jwt from 'jsonwebtoken'

// const { GOOGLE_CLIENT_ID } = "378771911273-9jv0grs96nb9noul4mf9a2i881rqkkko.apps.googleusercontent.com"
const { GOOGLE_CLIENT_ID } = process.env
const client = new OAuth2Client(GOOGLE_CLIENT_ID);

const generateToken = async (data) => {
  var token = await jwt.sign({
    sub: data.id,
    email: data.email,
    account_name: data.account_name,
    account_type: data.account_type,
    first_name: data.first_name,
    last_name: data.last_name
  }, 'shhhhh');

  return token
}

// Create and Save a new Tutorial
const socialLogin = async (body) => {
  try {
    console.log("body --> ", body)
    console.log("GOOGLE_CLIENT_ID --> ", GOOGLE_CLIENT_ID)

    let email = ""
    let social_id = ""
    let account_name = ""
    if (body.social == "google") {

      const tokenInfo = await client.getTokenInfo(
        // oAuth2Client.credentials.access_token
        body.token
      );

      console.log("tokenInfo --> ", tokenInfo);
      email = tokenInfo.email
      social_id = tokenInfo.sub

    } else if (body.social == "facebook") {
      email = body.email
      account_name = body.account_name
      social_id = body.userId
      // data = { id: body.userId, account_name: body.account_name }
    }

    let data = {}
    let filter = {
      where: {
        social_id: social_id,
        social_type: body.social
      }
    }
    const findData = await Account.findOne(filter)
    console.log("findData --> ", findData)
    if (findData) {
      // data = { id: findData.id, email: findData.email, account_name: findData.account_name, social_type: findData.social_type }
      const token = await generateToken(findData)
      return { token, account_type: findData.account_type, account_name: findData.account_name, account_id: findData.id }
    }
    return null
  } catch (error) {
    throw error;
  }
};

const register = async (body) => {
  try {
    console.log("body --> ", body)
    console.log("GOOGLE_CLIENT_ID --> ", GOOGLE_CLIENT_ID)

    let social_id = ""
    let first_name = ""
    let last_name = ""
    let account_name = ""
    let account_type = body.account_type
    let email = body?.email || ""
    let social_type = body?.social || ""
    let accept_term = body?.accept_term || false

    if (body.social == "google") {
      const tokenInfo = await client.getTokenInfo(
        // oAuth2Client.credentials.access_token
        body.token
      );

      console.log("tokenInfo --> ", tokenInfo);
      email = tokenInfo.email
      social_id = tokenInfo.sub
      account_name = tokenInfo.email

    } else if (body.social == "facebook") {
      email = body.email
      account_name = body.account_name
      social_id = body.account_id
      first_name = body.first_name
      last_name = body.last_name
      // data = { id: body.userId, account_name: body.account_name }
    }

    let data_create = {
      email,
      account_name,
      social_id,
      social_type,
      account_type: account_type,
      first_name: first_name,
      last_name: last_name,
      address: "",
      accept_term: accept_term
    }

    const filter = { where: { social_id: social_id, social_type: social_type } }
    const findData = await Account.findOne(filter)
    if (!findData) {
      const data = await Account.create(data_create)
      if (data) {
        if (account_type == "pilot") {
          await Piolt.create({ ...data_create, account_id: data.id })
        } else if (account_type == "employer") {
          await Consumer.create({ ...data_create, account_id: data.id })
        }
        const token = await generateToken(data)
        return { token, account_type: data.account_type, account_name: data.account_name, account_id: data.id }
      }
    }
    return null
  } catch (error) {
    throw error;
  }
}

const crud = {
  socialLogin,
  register
}

export default crud

