import db from '../models'
const Favarite = db.favariteList;
const Pilot = db.pilot;
const Job = db.job;
import helper from '../utils/helper'

// Create and Save a new Tutorial
const create = async (body) => {
  try {
    const _body = {
      ...body,
      status: true
    }
    const data = await Favarite.create(_body)
    return data
  } catch (error) {
    throw error;
  }
};

const deletes = async (id) => {
  try {
    const data = await Favarite.destroy({
      where: { id: id }
    })
    return data
  } catch (error) {
    throw error;
  }
};


const findAll = async (query) => {

  let { page = 1, per_page = 10, account_id, category = "" } = query
  page = parseInt(page)
  per_page = parseInt(per_page)
  const offset = (page - 1) * per_page
  let filter = {}

  if (account_id) {
    filter = {
      account_id: account_id,
      category: category,
    }
  }
  let modelToInclude = []
  if (category === "pilot") {
    Favarite.belongsTo(Pilot, {
      foreignKey: 'ref_id',
      targetKey: "id",
    })
    modelToInclude = [Pilot]
  } else if (category === "job") {
    Favarite.belongsTo(Job, {
      foreignKey: 'ref_id',
      targetKey: "id",
    })
    modelToInclude = [Job]
  }

  try {
    const { count, rows } = await Favarite.findAndCountAll({
      order: [["created_at", "DESC"]],
      limit: per_page,
      offset: offset,
      where: filter,
      include: [...modelToInclude]
    })
    const meta = helper.pagination(count, page, per_page)
    return { rows, meta }

  } catch (error) {
    throw error;
  }
};

const crud = {
  create,
  deletes,
  findAll
}

export default crud

