import { useEffect, useState } from 'react'


export default function useFetchTable(
  FetchFunc,
  FilterObj,
  option = { isAutoFetch: true }
) {
  const [isLoading, setIsLoading] = useState(false)
  const [dataTable, setDataTable] = useState([])
  let [pagination, setPagination] = useState({
    total: 0,
    current: 1,
    pageSize: 10,
  })

  let [filter, setFilter] = useState(FilterObj)

  useEffect(() => {
    if (option.isAutoFetch) {
      fetchData()
    }
  }, [])

  const fetchData = async (filterObj = filter, paging = pagination) => {
    const reqBody = {
      page: paging.current,
      per_page: paging.pageSize,
      ...filterObj,
    }
    setIsLoading(true)
    const { result, success } = await FetchFunc(reqBody)
    if (success) {
      const { meta, data } = result
      setPagination({
        pageSize: paging.pageSize,
        current: meta.page,
        total: meta.total,
      })
      setDataTable(data)
      setIsLoading(false)
      setFilter(filterObj)
    }
  }

  const handelDataTableChange = (pagination) => {
    fetchData(filter, pagination)
  }

  const handleFetchData = (filterReq) => {
    fetchData(filterReq, { current: 1, total: 0, pageSize: 10 })
  }

  return { isLoading, dataTable, handelDataTableChange, handleFetchData, pagination }
}
