import fetch from './client/api'
import { errorHandler, successHandler } from './client/apiHandler'

const createReview = async (body) => {
  try {
    const result = await fetch.post(`/api/pilot-review/create`, body)
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}


export {
  createReview,
}