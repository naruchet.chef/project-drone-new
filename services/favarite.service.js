import fetch from "./client/api";
import { errorHandler, successHandler } from "./client/apiHandler";

const favarite = async (body) => {
  try {
    const result = await fetch.post(`/api/favarite/create`, body);
    return successHandler(result);
  } catch (error) {
    return errorHandler(error);
  }
};

const unFavarite = async (params) => {
  try {
    const result = await fetch.delete(`/api/favarite/delete`, {
      params: params,
    });
    return successHandler(result);
  } catch (error) {
    return errorHandler(error);
  }
};

const favariteList = async (params) => {
  try {
    const result = await fetch.get(`/api/favarite/find-all`, { params: params })
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}

export { favarite, unFavarite, favariteList };
