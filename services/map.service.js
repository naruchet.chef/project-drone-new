import fetch from './client/api'
import { errorHandler, successHandler } from './client/apiHandler'


const getPlace = async (textSearch) => {
  try {
    const result = await fetch.get(`/api/map/place?text=${textSearch}`)
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}

export {
  getPlace
}
