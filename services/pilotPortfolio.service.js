import fetch from './client/api'
import { errorHandler, successHandler } from './client/apiHandler'

const createPilotPortfolio = async (body) => {
  try {
    const headers = {
      "Content-Type": "multipart/form-data",
    }
    const result = await fetch.post(`/api/pilot-portfolio/create`, body, { headers })
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}

const updatePilotPortfolio = async (body) => {
  try {
    const headers = {
      "Content-Type": "multipart/form-data",
    }
    const result = await fetch.post(`/api/pilot-portfolio/update`, body, { headers })
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}

const deletePilotPortfolio = async (params) => {
  try {
    const result = await fetch.delete(`/api/pilot-portfolio/delete`, { params: params })
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}

const pilotPortfolioList = async (params) => {
  try {
    const result = await fetch.get(`/api/pilot-portfolio/find-all`, { params: params })
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}

export {
  createPilotPortfolio,
  updatePilotPortfolio,
  deletePilotPortfolio,
  pilotPortfolioList
}