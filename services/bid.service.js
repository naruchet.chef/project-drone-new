import fetch from './client/api'
import { errorHandler, successHandler } from './client/apiHandler'

const bidList = async (params) => {
  try {
    const result = await fetch.get(`/api/bid/find-all`, { params: params })
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}

const createBid = async (body) => {
  try {
    const result = await fetch.post(`/api/bid/create`, body)
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}


const findOneBid = async (params) => {
  try {
    const result = await fetch.get(`/api/bid/find`, { params: params })
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}

const updateBid = async (body) => {
  try {
    const result = await fetch.post(`/api/bid/update`, body)
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}

const deleteBid = async (params) => {
  try {
    const result = await fetch.delete(`/api/bid/delete`, { params: params })
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}


export {
  bidList,
  findOneBid,
  createBid,
  deleteBid,
  updateBid
}