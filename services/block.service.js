import fetch from "./client/api";
import { errorHandler, successHandler } from "./client/apiHandler";

const block = async (body) => {
  try {
    const result = await fetch.post(`/api/block/create`, body);
    return successHandler(result);
  } catch (error) {
    return errorHandler(error);
  }
};

const unBlock = async (params) => {
  try {
    const result = await fetch.delete(`/api/block/delete`, {
      params: params,
    });
    return successHandler(result);
  } catch (error) {
    return errorHandler(error);
  }
};


const blockList = async (params) => {
  try {
    const result = await fetch.get(`/api/block/find-all`, { params: params })
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}

export { block, unBlock, blockList };
