import fetch from './client/api'
import { errorHandler, successHandler } from './client/apiHandler'

const jobList = async (params) => {
  try {
    const result = await fetch.get(`/api/jobs/find-all`, { params: params })
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}

const jobListWithBids = async (params) => {
  try {
    const result = await fetch.get(`/api/jobs/find-all-with-bids`, { params: params })
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}

const createJob = async (body) => {
  try {
    const headers = {
      "Content-Type": "multipart/form-data",
    }
    const result = await fetch.post(`/api/jobs/create`, body, { headers })
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}


const findOne = async (params) => {
  try {
    const result = await fetch.get(`/api/jobs/find`, { params: params })
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}


const updateJob = async (body) => {
  try {
    const result = await fetch.post(`/api/jobs/update`, body)
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}

const deleteJob = async (params) => {
  try {
    const result = await fetch.delete(`/api/jobs/delete`, { params: params })
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}

const updateJobStatus = async (body) => {
  try {
    const result = await fetch.post(`/api/jobs/update-status`, body)
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}


export {
  jobList,
  findOne,
  createJob,
  deleteJob,
  updateJob,
  jobListWithBids,
  updateJobStatus
}