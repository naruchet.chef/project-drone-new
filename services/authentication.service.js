import fetch from './client/api'
import { errorHandler, successHandler } from './client/apiHandler'

const socialLogin = async (body) => {

    try {
        const result = await fetch.post(`/api/authentication/social-login`, body)
        return successHandler(result)
    } catch (error) {
        // return errorHandler(error)
        return false
    }
}

const register = async (body) => {

    try {
        const result = await fetch.post(`/api/authentication/register`, body)
        return successHandler(result)
    } catch (error) {
        return errorHandler(error)
    }
}

export {
    socialLogin,
    register
}