import fetch from './client/api'
import { errorHandler, successHandler } from './client/apiHandler'

const createPilotJobType = async (body) => {
  try {
    const result = await fetch.post(`/api/pilot-job-type/create`, body)
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}

const updatePilotJobType = async (body) => {
  try {
    const result = await fetch.post(`/api/pilot-job-type/update`, body)
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}

const deletePilotJobType = async (params) => {
  try {
    const result = await fetch.delete(`/api/pilot-job-type/delete`, { params: params })
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}


export {
  createPilotJobType,
  updatePilotJobType,
  deletePilotJobType
}