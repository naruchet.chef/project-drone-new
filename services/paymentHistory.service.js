import fetch from './client/api'
import { errorHandler, successHandler } from './client/apiHandler'

const paymentHistoryList = async (params) => {
  try {
    const result = await fetch.get(`/api/payment-history/find-all`, { params: params })
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}

const create = async (body) => {
  try {
    const headers = {
      "Content-Type": "multipart/form-data",
    }
    const result = await fetch.post(`/api/payment-history/create`, body, { headers })
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}


export {
  paymentHistoryList,
  create,
}