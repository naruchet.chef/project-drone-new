import fetch from './client/api'
import { errorHandler, successHandler } from './client/apiHandler'

const createPilotLicense = async (body) => {
  try {
    const headers = {
      "Content-Type": "multipart/form-data",
    }
    const result = await fetch.post(`/api/pilot-license/create`, body, { headers })
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}

const updatePilotLicense = async (body) => {
  try {
    const headers = {
      "Content-Type": "multipart/form-data",
    }
    const result = await fetch.post(`/api/pilot-license/update`, body, { headers })
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}

const deletePilotLicense = async (params) => {
  try {
    const result = await fetch.delete(`/api/pilot-license/delete`, { params: params })
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}


export {
  createPilotLicense,
  deletePilotLicense,
  updatePilotLicense
}