const success = (res, message, data = {}, meta = {}) => {
  const response = {
    status: 200,
    message: message,
    data: data,
    meta: meta
  }
  return res.send(response)
}


const methodNotAllow = (res) => {
  const response = {
    status: 405,
    message: `Method  Not Allowed`,
  }
  return res.send(response)
}


const notFound = (res, message) => {
  const response = {
    status: 404,
    message: message,
  }
  return res.send(response)
}

const badRequest = (res, message) => {
  const response = {
    status: 400,
    message: message,
  }
  return res.send(response)
}

const error = (res, error = {}) => {
  const response = {
    status: 500,
    message: `internal server error`,
    error: error ? error.message : null
  }
  return res.send(response)
}


const unAuth = (res) => {
  const response = {
    status: 401,
    message: `unauthorization`,
  }
  return res.send(response)
}





const handleResponse = {
  success: success,
  methodNotAllow: methodNotAllow,
  notFound: notFound,
  badRequest: badRequest,
  error: error,
  unAuth: unAuth
}
export default handleResponse