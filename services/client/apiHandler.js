import codeMessage from "../../constants/codeMessage";
import { notification } from 'antd'

const successHandler = (result, typeNotification = {}) => {
  let response = {
    status: 404,
    url: null,
    result: null,
    success: false,
  };
  const { data, status } = result;
  const resultStatus = data.status
  if (`${status}` === "200" && `${resultStatus}` === "200" && data) {
    response.status = status;
    response.success = true;
    response.result = data;
    return response;
  } else {
    const message = data.message;
    const errorText = message || codeMessage[resultStatus];
    response.status = resultStatus;
    notification.config({
      duration: 20,
    });
    notification.error({
      message: `Request error ${resultStatus}`,
      description: errorText,
    });
    return response;
  }
};


const errorHandler = (error, emptyResult = null) => {
  const { response } = error
  if (!response) {
    return {
      success: false,
      result: emptyResult,
      message: 'Cannot connect to the server, Check your internet network',
    }
  } else if (response && response.status) {
    const message = response.data && response.data.detail
    const errorText = message || codeMessage[response.status]
    const { status } = response
    if (error.response.data.jwtExpired || `${status}` === '401') {
      logout()
      setTimeout(() => {
        Router.replace('/login').then(() => {
          logout()
        })
      }, 1000)
      return response.data
    }

    if (`${status}` === '404') {
      return response.data
    }
    notification.config({
      duration: 20,
    })
    notification.error({
      message: `Request error ${status}`,
      description: errorText,
    })
    return response.data
  } else {
    notification.config({
      duration: 20,
    })
    notification.error({
      message: 'Unknown Error',
      description: 'An unknown error occurred in the app, please try again. ',
    })
    return {
      success: false,
      result: emptyResult,
      message: 'An unknown error occurred in the app, please try again. ',
    }
  }
}



export { errorHandler, successHandler };
