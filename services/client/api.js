import axios from 'axios'
import _ from 'lodash'
const host = process.env.POS_GRPC_API

const axiosInstance = axios.create({
  baseURL: host,
  timeout: 30000,
})

const auth = (options) => {
  let optionsWithToken = _.merge({}, options)
  // const token = retrieveToken()
  const token = null
  if (token !== null) {
    optionsWithToken = _.merge(optionsWithToken, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
  }
  return optionsWithToken
}

const getWithToken = async (url, options = {}) => {

  let optionsWithToken = auth(options)

  return await axiosInstance.get(url, optionsWithToken)
}

const postWithToken = async (url, body = {}, options = {}) => {
  let optionsWithToken = auth(options)
  return await axiosInstance.post(url, body, optionsWithToken)
}

const putWithToken = async (url, body = {}, options = {}) => {
  let optionsWithToken = auth(options)
  return await axiosInstance.put(url, body, optionsWithToken)
}

const deleteWithToken = async (url, options = {}) => {
  let optionsWithToken = auth(options)
  return await axiosInstance.delete(url, optionsWithToken)
}

export default {
  get: getWithToken,
  post: postWithToken,
  put: putWithToken,
  delete: deleteWithToken,
}
