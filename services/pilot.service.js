import fetch from './client/api'
import { errorHandler, successHandler } from './client/apiHandler'

const pilotList = async (params) => {
  try {
    const result = await fetch.get(`/api/pilot/find-all`, { params: params })
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}

const createPilot = async (body) => {
  try {
    const result = await fetch.post(`/api/pilot/create`, body)
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}


const findOnePilot = async (params) => {
  try {
    const result = await fetch.get(`/api/pilot/find`, { params: params })
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}


const findPilotByAccountId = async (params) => {
  try {
    const result = await fetch.get(`/api/pilot/find-pilot-by-account-id`, { params: params })
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}

const updatePilot = async (body) => {
  try {
    const headers = {
      // "Content-Type": "multipart/form-data",
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
    const result = await fetch.post(`/api/pilot/update`, JSON.stringify(body), { headers })
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}

const deletePilot = async (params) => {
  try {
    const result = await fetch.delete(`/api/pilot/delete`, { params: params })
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}


export {
  pilotList,
  findOnePilot,
  createPilot,
  deletePilot,
  updatePilot,
  findPilotByAccountId
}