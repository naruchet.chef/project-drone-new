import fetch from './client/api'
import { errorHandler, successHandler } from './client/apiHandler'

const pilotCustomer = async (params) => {
  try {
    const result = await fetch.get(`/api/customer/find-all`, { params: params })
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}

const createCustomer = async (body) => {
  try {
    const result = await fetch.post(`/api/customer/create`, body)
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}


const findOneCustomer = async (params) => {
  try {
    const result = await fetch.get(`/api/customer/find`, { params: params })
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}

const updateCustomer = async (body) => {
  try {
    const headers = {
      "Content-Type": "multipart/form-data",
    }
    const result = await fetch.post(`/api/customer/update`, body, { headers })
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}

const deleteCustomer = async (params) => {
  try {
    const result = await fetch.delete(`/api/customer/delete`, { params: params })
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}

const findCustomerByAccountId = async (params) => {
  try {
    const result = await fetch.get(`/api/customer/find-customer-by-account-id`, { params: params })
    return successHandler(result)
  } catch (error) {
    return errorHandler(error)
  }
}


export {
  pilotCustomer,
  createCustomer,
  findOneCustomer,
  updateCustomer,
  deleteCustomer,
  findCustomerByAccountId
}