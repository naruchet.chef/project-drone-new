
module.exports = (sequelize, Sequelize) => {
  const Pilot = sequelize.define("pilot_profile", {
    id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
    },
    created_at: {
      field: 'created_at',
      type: Sequelize.DATEONLY,
      defaultValue: sequelize.fn('NOW')
    },
    updated_at: {
      field: 'updated_at',
      type: Sequelize.DATEONLY,
      defaultValue: sequelize.fn('NOW')
    },
    deleted_at: {
      type: Sequelize.DATEONLY
    },
    account_id: {
      type: Sequelize.STRING
    },
    first_name: {
      type: Sequelize.STRING
    },
    last_name: {
      type: Sequelize.STRING
    },
    email: {
      type: Sequelize.STRING
    },
    account_name: {
      type: Sequelize.STRING
    },
    photo: {
      type: Sequelize.STRING
    },
    password: {
      type: Sequelize.STRING
    },
    company_name: {
      type: Sequelize.STRING
    },
    tel: {
      type: Sequelize.STRING
    },
    insurance: {
      type: Sequelize.STRING
    },
    address: {
      type: Sequelize.STRING
    },
    introduce_yourself: {
      type: Sequelize.STRING
    },
    experience: {
      type: Sequelize.STRING
    },
    license: {
      type: Sequelize.BOOLEAN
    },
    working_day: {
      type: Sequelize.STRING
    },
    drone_type: {
      type: Sequelize.STRING
    },
    drone_model: {
      type: Sequelize.STRING
    },
    bank_account: {
      type: Sequelize.STRING
    },
    bank_account_name: {
      type: Sequelize.STRING
    },
    bank_account_no: {
      type: Sequelize.STRING
    },
    tax_payer: {
      type: Sequelize.STRING
    },
    status: {
      type: Sequelize.STRING
    },
    rating: {
      type: Sequelize.FLOAT
    },
    pay_per_hr: {
      type: Sequelize.INTEGER
    },
    sex: {
      type: Sequelize.STRING
    },
    bank_name_id: {
      type: Sequelize.INTEGER
    },
    job_review: {
      type: Sequelize.INTEGER
    },
    job_all: {
      type: Sequelize.INTEGER
    },
    birth_day: {
      type: Sequelize.DATEONLY,
    },
  });
  return Pilot;
};
