module.exports = (sequelize, Sequelize) => {
  const PilotLicense = sequelize.define("pilot_license", {
    id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
    },
    created_at: {
      field: 'created_at',
      type: Sequelize.DATEONLY,
      defaultValue: sequelize.fn('NOW')
    },
    updated_at: {
      field: 'updated_at',
      type: Sequelize.DATEONLY,
      defaultValue: sequelize.fn('NOW')
    },
    deleted_at: {
      type: Sequelize.DATEONLY
    },
    pilot_profile_id: {
      type: Sequelize.INTEGER,
    },
    license_name: {
      type: Sequelize.STRING
    },
    license_no: {
      type: Sequelize.STRING
    },
    license_photo: {
      type: Sequelize.STRING
    },
    license_date: {
      type: Sequelize.DATEONLY
    },
  });
  return PilotLicense;
};
