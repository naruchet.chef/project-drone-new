module.exports = (sequelize, Sequelize) => {
  const JobLocation = sequelize.define("job_locations", {
    id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
    },
    created_at: {
      field: 'created_at',
      type: Sequelize.DATEONLY,
      defaultValue: sequelize.fn('NOW')
    },
    updated_at: {
      field: 'updated_at',
      type: Sequelize.DATEONLY,
      defaultValue: sequelize.fn('NOW')
    },
    deleted_at: {
      type: Sequelize.DATEONLY
    },
    job_id: {
      type: Sequelize.INTEGER,
    },
    name: {
      type: Sequelize.STRING
    },
    lat: {
      type: Sequelize.STRING
    },
    lng: {
      type: Sequelize.STRING
    },

  });
  return JobLocation;
};
