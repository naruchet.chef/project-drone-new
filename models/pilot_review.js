
module.exports = (sequelize, Sequelize) => {
  const Bids = sequelize.define("pilot_review", {
    id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
    },
    created_at: {
      field: 'created_at',
      type: Sequelize.DATEONLY,
      defaultValue: sequelize.fn('NOW')
    },
    updated_at: {
      field: 'updated_at',
      type: Sequelize.DATEONLY,
      defaultValue: sequelize.fn('NOW')
    },
    deleted_at: {
      type: Sequelize.DATEONLY
    },
    job_id: {
      type: Sequelize.INTEGER
    },
    pilot_profile_id: {
      type: Sequelize.INTEGER
    },
    rating: {
      type: Sequelize.FLOAT
    },
    description: {
      type: Sequelize.STRING
    }
  });
  return Bids;
};
