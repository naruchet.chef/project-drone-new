module.exports = (sequelize, Sequelize) => {
    const Tutorial = sequelize.define("accounts", {
        id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        created_at: {
            field: 'created_at',
            type: Sequelize.DATEONLY,
            defaultValue: sequelize.fn('NOW')
        },
        updated_at: {
            field: 'updated_at',
            type: Sequelize.DATEONLY,
            defaultValue: sequelize.fn('NOW')
        },
        deleted_at: {
            type: Sequelize.DATEONLY
        },
        email: {
            type: Sequelize.STRING
        },
        social_id: {
            type: Sequelize.STRING
        },
        social_type: {
            type: Sequelize.STRING
        },
        account_type: {
            type: Sequelize.STRING
        },
        account_name: {
            type: Sequelize.STRING
        },
        status: {
            type: Sequelize.STRING
        },
        accept_term: {
            type: Sequelize.BOOLEAN
          },
    });
    return Tutorial;
};
