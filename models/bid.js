
module.exports = (sequelize, Sequelize) => {
  const Bids = sequelize.define("bids", {
    id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
    },
    created_at: {
      field: 'created_at',
      type: Sequelize.DATEONLY,
      defaultValue: sequelize.fn('NOW')
    },
    updated_at: {
      field: 'updated_at',
      type: Sequelize.DATEONLY,
      defaultValue: sequelize.fn('NOW')
    },
    deleted_at: {
      type: Sequelize.DATEONLY
    },
    job_id: {
      type: Sequelize.INTEGER
    },
    pilot_profile_id: {
      type: Sequelize.INTEGER
    },
    budget: {
      type: Sequelize.INTEGER
    },
    status: {
      type: Sequelize.STRING
    },
    bid_by: {
      type: Sequelize.STRING
    },
  });
  return Bids;
};
