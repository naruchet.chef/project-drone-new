module.exports = (sequelize, Sequelize) => {
  const PilotJobType = sequelize.define("pilot_job_type", {
    id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
    },
    created_at: {
      field: 'created_at',
      type: Sequelize.DATEONLY,
      defaultValue: sequelize.fn('NOW')
    },
    updated_at: {
      field: 'updated_at',
      type: Sequelize.DATEONLY,
      defaultValue: sequelize.fn('NOW')
    },
    deleted_at: {
      type: Sequelize.DATEONLY
    },
    pilot_profile_id: {
      type: Sequelize.INTEGER,
    },
    job_type_id: {
      type: Sequelize.INTEGER
    },
    salary: {
      type: Sequelize.INTEGER
    }
  });
  return PilotJobType;
};
