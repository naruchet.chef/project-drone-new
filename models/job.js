
module.exports = (sequelize, Sequelize) => {
  const Jobs = sequelize.define("jobs", {
    id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
    },
    created_at: {
      field: 'created_at',
      type: Sequelize.DATEONLY,
      defaultValue: sequelize.fn('NOW')
    },
    updated_at: {
      field: 'updated_at',
      type: Sequelize.DATEONLY,
      defaultValue: sequelize.fn('NOW')
    },
    deleted_at: {
      type: Sequelize.DATEONLY
    },
    job_name: {
      type: Sequelize.STRING
    },
    job_detail: {
      type: Sequelize.STRING
    },
    budget_from: {
      type: Sequelize.STRING
    },
    budget_to: {
      type: Sequelize.STRING
    },
    job_type_id: {
      type: Sequelize.STRING
    },
    drone_type_id: {
      type: Sequelize.STRING
    },
    location: {
      type: Sequelize.STRING
    },
    is_auction: {
      type: Sequelize.STRING
    },
    is_job_start: {
      type: Sequelize.BOOLEAN
    },
    start_date: {
      type: Sequelize.DATEONLY,
    },
    start_date_type: {
      type: Sequelize.STRING
    },
    date_range_start: {
      type: Sequelize.DATEONLY,
    },
    date_range_end: {
      type: Sequelize.DATEONLY,
    },
    accepted_term: {
      type: Sequelize.BOOLEAN
    },
    latitute: {
      type: Sequelize.STRING
    },
    longtitute: {
      type: Sequelize.STRING
    },
    drone_model: {
      type: Sequelize.STRING
    },
    camera_resolution: {
      type: Sequelize.STRING
    },
    period_date: {
      type: Sequelize.STRING
    },
    status: {
      type: Sequelize.STRING
    },
    profile_id: {
      type: Sequelize.INTEGER
    },
    job_link: {
      type: Sequelize.STRING
    },
    price: {
      type: Sequelize.INTEGER
    },
    pilot_profile_id: {
      type: Sequelize.INTEGER
    },
  });
  return Jobs;
};
