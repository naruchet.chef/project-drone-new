module.exports = (sequelize, Sequelize) => {
  const JobImages = sequelize.define("job_images", {
    id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
    },
    created_at: {
      field: 'created_at',
      type: Sequelize.DATEONLY,
      defaultValue: sequelize.fn('NOW')
    },
    updated_at: {
      field: 'updated_at',
      type: Sequelize.DATEONLY,
      defaultValue: sequelize.fn('NOW')
    },
    deleted_at: {
      type: Sequelize.DATEONLY
    },
    job_id: {
      type: Sequelize.INTEGER,
    },
    image:{
      type: Sequelize.STRING,
    }
  });
  return JobImages;
};
