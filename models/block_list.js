
module.exports = (sequelize, Sequelize) => {
  const BlockList = sequelize.define("block", {
    id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
    },
    created_at: {
      field: 'created_at',
      type: Sequelize.DATEONLY,
      defaultValue: sequelize.fn('NOW')
    },
    updated_at: {
      field: 'updated_at',
      type: Sequelize.DATEONLY,
      defaultValue: sequelize.fn('NOW')
    },
    deleted_at: {
      type: Sequelize.DATEONLY
    },
    account_id: {
      type: Sequelize.INTEGER
    },
    ref_id: {
      type: Sequelize.INTEGER
    },
    category: {
      type: Sequelize.STRING
    },
    status: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    },
  });
  return BlockList;
};
