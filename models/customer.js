
module.exports = (sequelize, Sequelize) => {
  const Customer = sequelize.define("customer_profile", {
    id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
    },
    created_at: {
      field: 'created_at',
      type: Sequelize.DATEONLY,
      defaultValue: sequelize.fn('NOW')
    },
    updated_at: {
      field: 'updated_at',
      type: Sequelize.DATEONLY,
      defaultValue: sequelize.fn('NOW')
    },
    deleted_at: {
      type: Sequelize.DATEONLY
    },
    account_id: {
      type: Sequelize.STRING
    },
    first_name: {
      type: Sequelize.STRING
    },
    last_name: {
      type: Sequelize.STRING
    },
    email: {
      type: Sequelize.STRING
    },
    account_name: {
      type: Sequelize.STRING
    },
    photo: {
      type: Sequelize.STRING
    },
  });
  return Customer;
};
