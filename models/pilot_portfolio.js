module.exports = (sequelize, Sequelize) => {
  const PilotPortfolio = sequelize.define("pilot_portfolio", {
    id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
    },
    created_at: {
      field: 'created_at',
      type: Sequelize.DATEONLY,
      defaultValue: sequelize.fn('NOW')
    },
    updated_at: {
      field: 'updated_at',
      type: Sequelize.DATEONLY,
      defaultValue: sequelize.fn('NOW')
    },
    deleted_at: {
      type: Sequelize.DATEONLY
    },
    pilot_profile_id: {
      type: Sequelize.INTEGER,
    },
    portfolio_name: {
      type: Sequelize.STRING
    },
    portfolio_detail: {
      type: Sequelize.STRING
    },
    portfolio_photo: {
      type: Sequelize.STRING
    },
    portfolio_type: {
      type: Sequelize.STRING
    },
  });
  return PilotPortfolio;
};
