
module.exports = (sequelize, Sequelize) => {
  const Bids = sequelize.define("payment_history", {
    id: {
      type: Sequelize.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
    },
    created_at: {
      field: 'created_at',
      type: Sequelize.DATEONLY,
      defaultValue: sequelize.fn('NOW')
    },
    updated_at: {
      field: 'updated_at',
      type: Sequelize.DATEONLY,
      defaultValue: sequelize.fn('NOW')
    },
    deleted_at: {
      type: Sequelize.DATEONLY
    },
    job_id: {
      type: Sequelize.INTEGER
    },
    pilot_profile_id: {
      type: Sequelize.INTEGER
    },
    amount: {
      type: Sequelize.INTEGER
    },
    profile_id: {
      type: Sequelize.INTEGER
    },
    slip: {
      type: Sequelize.STRING
    },
    bank_name: {
      type: Sequelize.STRING
    },
    account_bank_name: {
      type: Sequelize.STRING
    },
    bank_no: {
      type: Sequelize.STRING
    },
  });
  return Bids;
};
