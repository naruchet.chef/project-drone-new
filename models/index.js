const dbConfig = require("../config/db.js");
const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: false,
  port: dbConfig.port,
  ssl_ca: dbConfig.ssl_ca,
  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  },
  define: {
    timestamps: true,
    underscored: true,
  }
});
const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;
db.job = require("./job.js")(sequelize, Sequelize);
db.jobLocation = require("./job_locations")(sequelize, Sequelize);
db.pilot = require("./pilot")(sequelize, Sequelize);
db.pilotLicense = require("./pilot_license")(sequelize, Sequelize);
db.pilotPortfolio = require("./pilot_portfolio")(sequelize, Sequelize);
db.pilotJobType = require("./pilot_job_type")(sequelize, Sequelize);
db.customer = require("./customer")(sequelize, Sequelize);
db.account = require("./account")(sequelize, Sequelize);
db.bid = require("./bid")(sequelize, Sequelize);
db.pilotReview = require("./pilot_review")(sequelize, Sequelize);
db.pilot.hasMany(db.bid);
db.bid.belongsTo(db.pilot, {
  foreignKey: "pilot_profile_id"
});
db.jobImage = require("./job_images")(sequelize, Sequelize);
db.paymentHistory = require("./payment_history")(sequelize, Sequelize);
db.pilot.hasMany(db.job);
db.job.belongsTo(db.pilot, {
  foreignKey: "pilot_profile_id"
});
db.favariteList = require("./favorite_list")(sequelize, Sequelize);
db.blockList = require("./block_list")(sequelize, Sequelize);

module.exports = db;